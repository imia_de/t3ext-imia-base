<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013-2014 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

$extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY]);

\IMIA\ImiaComposer\Utility\Composer::fetch();

/* Hooks */
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['GLOBAL']['extTablesInclusion-PostProcessing'][$_EXTKEY] =
    \IMIA\ImiaBase\Hook\Core\TableConfigurationPostProcessing::class;
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_content.php']['getData'][$_EXTKEY] =
    \IMIA\ImiaBase\Hook\Frontend\ContentObjectRenderer::class;
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem'][$_EXTKEY] =
    \IMIA\ImiaBase\Hook\Backend\PageLayoutViewDrawItemHook::class;
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawFooter'][$_EXTKEY] =
    \IMIA\ImiaBase\Hook\Backend\PageLayoutViewDrawFooterHook::class;
$GLOBALS ['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass'][$_EXTKEY] =
    \IMIA\ImiaBase\Hook\Core\ProcessDatamap::class;
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['configArrayPostProc'][$_EXTKEY] =
    \IMIA\ImiaBase\Hook\Frontend\TypoScriptFrontendController::class . '->configArrayPostProc';
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['tslib_fe-PostProc'][$_EXTKEY] =
    \IMIA\ImiaBase\Hook\Frontend\TypoScriptFrontendController::class . '->constructPostProc';
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['contentPostProc-all'][$_EXTKEY] =
    \IMIA\ImiaBase\Hook\Frontend\TypoScriptFrontendController::class . '->contentPostProc';
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['checkAlternativeIdMethods-PostProc'][$_EXTKEY] =
    \IMIA\ImiaBase\Hook\Frontend\TypoScriptFrontendController::class . '->checkAlternativeIdMethods';
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['determineId-PostProc'][$_EXTKEY] =
    \IMIA\ImiaBase\Hook\Frontend\TypoScriptFrontendController::class . '->determineIdPostProc';
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['isOutputting'][$_EXTKEY] =
    \IMIA\ImiaBase\Hook\Frontend\TypoScriptFrontendController::class . '->isOutputting';
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_content.php']['typoLink_PostProc'][$_EXTKEY] =
    \IMIA\ImiaBase\Hook\Frontend\ContentObjectRenderer::class . '->typoLinkPostProc';
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['clearCachePostProc'][$_EXTKEY] =
    \IMIA\ImiaBase\Hook\Core\DataHandler::class . '->clearCachePostProc';
$GLOBALS ['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processCmdmapClass'][$_EXTKEY] =
    \IMIA\ImiaBase\Hook\Core\ProcessCmdmap::class;
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS'][\TYPO3\CMS\Core\Configuration\FlexForm\FlexFormTools::class]['flexParsing'][$_EXTKEY] =
    \IMIA\ImiaBase\Hook\Core\FlexParsing::class;
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms']['db_new_content_el']['wizardItemsHook'][$_EXTKEY] =
    \IMIA\ImiaBase\Hook\Backend\NewContentElementWizard::class;
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_content.php']['typolinkLinkHandler']['javascript'] =
    \IMIA\ImiaBase\Hook\Core\TypolinkHandler::class;
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['typo3/class.db_list_extra.inc']['getTable'][$_EXTKEY] =
    \IMIA\ImiaBase\Hook\Core\DatabaseRecordList::class;
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS'][\TYPO3\CMS\Frontend\Page\PageRepository::class]['init'][$_EXTKEY] =
    \IMIA\ImiaBase\Hook\Core\PageRepositoryInit::class;

/* Xclasses */
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Backend\View\PageLayoutView::class] =
    ['className' => \IMIA\ImiaBase\Xclass\Backend\View\PageLayoutView::class];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Backend\View\BackendLayoutView::class] =
    ['className' => \IMIA\ImiaBase\Xclass\Backend\View\BackendLayoutView::class];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Frontend\ContentObject\Menu\TextMenuContentObject::class] =
    ['className' => \IMIA\ImiaBase\Xclass\Frontend\ContentObject\Menu\TextMenuContentObject::class];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer::class] =
    ['className' => \IMIA\ImiaBase\Xclass\Frontend\ContentObject\ContentObjectRenderer::class];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Backend\Controller\PageLayoutController::class] =
    ['className' => \IMIA\ImiaBase\Xclass\Backend\Controller\PageLayoutController::class];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Recordlist\RecordList\DatabaseRecordList::class] =
    ['className' => \IMIA\ImiaBase\Xclass\Recordlist\RecordList\DatabaseRecordList::class];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Recordlist\RecordList::class] =
    ['className' => \IMIA\ImiaBase\Xclass\Recordlist\RecordList::class];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Core\Resource\Rendering\YouTubeRenderer::class] =
    ['className' => \IMIA\ImiaBase\Xclass\Core\Resource\Rendering\YouTubeRenderer::class];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Core\Resource\Service\FileProcessingService::class] =
    ['className' => \IMIA\ImiaBase\Xclass\Core\Resource\Service\FileProcessingService::class];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Core\Imaging\IconFactory::class] =
    ['className' => \IMIA\ImiaBase\Xclass\Core\Imaging\IconFactory::class];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Core\Database\Query\Restriction\FrontendRestrictionContainer::class] =
    ['className' => \IMIA\ImiaBase\Xclass\Core\Database\Query\Restriction\FrontendRestrictionContainer::class];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Reports\Report\Status\FalStatus::class] =
    ['className' => \IMIA\ImiaBase\Xclass\Reports\Report\Status\FalStatus::class];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Reports\Report\Status\Typo3Status::class] =
    ['className' => \IMIA\ImiaBase\Xclass\Reports\Report\Status\Typo3Status::class];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Install\SystemEnvironment\Check::class] =
    ['className' => \IMIA\ImiaBase\Xclass\Install\SystemEnvironment\Check::class];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Core\TypoScript\TemplateService::class] =
    ['className' => \IMIA\ImiaBase\Xclass\Core\TypoScript\TemplateService::class];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Core\TypoScript\ExtendedTemplateService::class] =
    ['className' => \IMIA\ImiaBase\Xclass\Core\TypoScript\ExtendedTemplateService::class];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Core\TypoScript\ConfigurationForm::class] =
    ['className' => \IMIA\ImiaBase\Xclass\Core\TypoScript\ConfigurationForm::class];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Backend\Tree\View\ContentCreationPagePositionMap::class] =
    ['className' => \IMIA\ImiaBase\Xclass\Backend\Tree\View\ContentCreationPagePositionMap::class];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Backend\Domain\Repository\Localization\LocalizationRepository::class] =
    ['className' => \IMIA\ImiaBase\Xclass\Backend\Domain\Repository\Localization\LocalizationRepository::class];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Backend\Form\FormDataProvider\DatabaseRowInitializeNew::class] =
    ['className' => \IMIA\ImiaBase\Xclass\Backend\Form\FormDataProvider\DatabaseRowInitializeNew::class];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Backend\Form\FormDataProvider\TcaInline::class] =
    ['className' => \IMIA\ImiaBase\Xclass\Backend\Form\FormDataProvider\TcaInline::class];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Backend\Controller\ContentElement\NewContentElementController::class] =
    ['className' => \IMIA\ImiaBase\Xclass\Backend\Controller\ContentElement\NewContentElementController::class];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Form\Hooks\DataStructureIdentifierHook::class] =
    ['className' => \IMIA\ImiaBase\Xclass\Form\Hooks\DataStructureIdentifierHook::class];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Form\Domain\Finishers\EmailFinisher::class] =
    ['className' => \IMIA\ImiaBase\Xclass\Form\Domain\Finishers\EmailFinisher::class];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\IndexedSearch\Indexer::class] =
    ['className' => \IMIA\ImiaBase\Xclass\IndexedSearch\Indexer::class];

if (TYPO3_MODE === 'BE' && !(TYPO3_REQUESTTYPE & TYPO3_REQUESTTYPE_CLI)) {
    \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
        \TYPO3\CMS\Core\Page\PageRenderer::class
    )->addRequireJsConfiguration([
        'paths' => [
            'TYPO3/CMS/Backend/LayoutModule/DragDrop' => \TYPO3\CMS\Core\Utility\PathUtility::getAbsoluteWebPath(
                \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY, 'Resources/Public/Overrides/Backend/JavaScript/LayoutModule/DragDrop')
            ),
        ],
    ]);
}

if (!\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('imia_pageteaser')) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController::class] =
        ['className' => \IMIA\ImiaBase\Xclass\Frontend\Controller\TypoScriptFrontendController::class];
}

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('linkhandler')) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\Cobweb\Linkhandler\Browser\RecordBrowser::class] =
        ['className' => \IMIA\ImiaBase\Xclass\Linkhandler\Browser\RecordBrowser::class];
}

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('mediace')) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\FoT3\Mediace\ContentObject\ShockwaveFlashObjectContentObject::class] =
        ['className' => \IMIA\ImiaBase\Xclass\Mediace\ContentObject\ShockwaveFlashObjectContentObject::class];
}

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('fluidcontent')) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\FluidTYPO3\Fluidcontent\Service\ConfigurationService::class] =
        ['className' => \IMIA\ImiaBase\Xclass\Fluidcontent\Service\ConfigurationService::class];
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\FluidTYPO3\Fluidcontent\Hooks\WizardItemsHookSubscriber::class] =
        ['className' => \IMIA\ImiaBase\Xclass\Fluidcontent\Hooks\WizardItemsHookSubscriber::class];
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\FluidTYPO3\Fluidcontent\Provider\ContentProvider::class] =
        ['className' => \IMIA\ImiaBase\Xclass\Fluidcontent\Provider\ContentProvider::class];
}

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('fluidpages')) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\FluidTYPO3\Fluidpages\Backend\PageLayoutDataProvider::class] =
        ['className' => \IMIA\ImiaBase\Xclass\Fluidpages\Backend\PageLayoutDataProvider::class];
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\FluidTYPO3\Fluidpages\Service\PageService::class] =
        ['className' => \IMIA\ImiaBase\Xclass\Fluidpages\Service\PageService::class];
}

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('flux')) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\FluidTYPO3\Flux\Form::class] =
        ['className' => \IMIA\ImiaBase\Xclass\Flux\Form::class];
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\FluidTYPO3\Flux\Form\Field\Inline\Fal::class] =
        ['className' => \IMIA\ImiaBase\Xclass\Flux\Form\Field\Inline\Fal::class];
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\FluidTYPO3\Flux\Form\Field\MultiRelation::class] =
        ['className' => \IMIA\ImiaBase\Xclass\Flux\Form\Field\MultiRelation::class];
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\FluidTYPO3\Flux\Form\Field\Relation::class] =
        ['className' => \IMIA\ImiaBase\Xclass\Flux\Form\Field\Relation::class];
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\FluidTYPO3\Flux\Form\Wizard\Suggest::class] =
        ['className' => \IMIA\ImiaBase\Xclass\Flux\Form\Wizard\Suggest::class];
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\FluidTYPO3\Flux\Form\Field\Select::class] =
        ['className' => \IMIA\ImiaBase\Xclass\Flux\Form\Field\Select::class];
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\FluidTYPO3\Flux\Form\Field\Text::class] =
        ['className' => \IMIA\ImiaBase\Xclass\Flux\Form\Field\Text::class];
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\FluidTYPO3\Flux\ViewHelpers\Field\SelectViewHelper::class] =
        ['className' => \IMIA\ImiaBase\Xclass\Flux\ViewHelpers\Field\SelectViewHelper::class];
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\FluidTYPO3\Flux\ViewHelpers\Wizard\SuggestViewHelper::class] =
        ['className' => \IMIA\ImiaBase\Xclass\Flux\ViewHelpers\Wizard\SuggestViewHelper::class];
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\FluidTYPO3\Flux\View\PageLayoutView::class] =
        ['className' => \IMIA\ImiaBase\Xclass\Flux\View\PageLayoutView::class];
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\FluidTYPO3\Flux\View\PreviewView::class] =
        ['className' => \IMIA\ImiaBase\Xclass\Flux\View\PreviewView::class];
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\FluidTYPO3\Flux\Provider\ContentProvider::class] =
        ['className' => \IMIA\ImiaBase\Xclass\Flux\Provider\ContentProvider::class];
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\FluidTYPO3\Flux\ViewHelpers\Field\Inline\FalViewHelper::class] =
        ['className' => \IMIA\ImiaBase\Xclass\Flux\ViewHelpers\Field\Inline\FalViewHelper::class];
}

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('vhs')) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\FluidTYPO3\Vhs\ViewHelpers\Content\Resources\FalViewHelper::class] =
        ['className' => \IMIA\ImiaBase\Xclass\Vhs\ViewHelpers\Content\Resources\FalViewHelper::class];
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\FluidTYPO3\Vhs\ViewHelpers\Content\RenderViewHelper::class] =
        ['className' => \IMIA\ImiaBase\Xclass\Vhs\ViewHelpers\Content\RenderViewHelper::class];
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\FluidTYPO3\Vhs\ViewHelpers\Resource\Record\FalViewHelper::class] =
        ['className' => \IMIA\ImiaBase\Xclass\Vhs\ViewHelpers\Resource\Record\FalViewHelper::class];
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\FluidTYPO3\Vhs\ViewHelpers\Math\ProductViewHelper::class] =
        ['className' => \IMIA\ImiaBase\Xclass\Vhs\ViewHelpers\Math\ProductViewHelper::class];
}

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('pagenotfoundhandling')) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\AawTeam\Pagenotfoundhandling\Controller\PagenotfoundController::class] =
        ['className' => \IMIA\ImiaBase\Xclass\Pagenotfoundhandling\Controller\PagenotfoundController::class];
}

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('realurl')) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\DmitryDulepov\Realurl\Configuration\ConfigurationReader::class] = [
        'className' => \IMIA\ImiaBase\Xclass\Realurl\Configuration\ConfigurationReader::class,
    ];
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\DmitryDulepov\Realurl\Encoder\UrlEncoder::class] = [
        'className' => \IMIA\ImiaBase\Xclass\Realurl\Encoder\UrlEncoder::class,
    ];
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\DmitryDulepov\Realurl\Decoder\UrlDecoder::class] = [
        'className' => \IMIA\ImiaBase\Xclass\Realurl\Decoder\UrlDecoder::class,
    ];
}

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('solr')) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\ApacheSolrForTypo3\Solr\Domain\Site\SiteRepository::class] =
        ['className' => \IMIA\ImiaBase\Xclass\Solr\Domain\Site\SiteRepository::class];
}

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('tw_lucenesearch')) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\Tollwerk\TwLucenesearch\Utility\Indexer::class] =
        ['className' => \IMIA\ImiaBase\Xclass\TwLucenesearch\Utility\Indexer::class];
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\Tollwerk\TwLucenesearch\Controller\LuceneController::class] =
        ['className' => \IMIA\ImiaBase\Xclass\TwLucenesearch\Controller\LuceneController::class];
}

/* Configurations */
$GLOBALS['TYPO3_CONF_VARS']['FE']['addRootLineFields'] .= ', module';

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript($_EXTKEY, 'constants',
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/TypoScript/constants.typoscript">');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript($_EXTKEY, 'setup',
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/TypoScript/setup.typoscript">');

if (!function_exists('user_isInMenuFolder')) {
    function user_isInMenuFolder()
    {
        $inMenuFolder = false;
        if ($GLOBALS['TSFE'] && is_array($GLOBALS['TSFE']->rootLine)) {
            foreach ($GLOBALS['TSFE']->rootLine as $page) {
                if ($page['module'] == 'menu') {
                    $inMenuFolder = true;
                    break;
                }
            }
        }

        return $inMenuFolder;
    }
}

if (!function_exists('user_isBackendLayout')) {
    function user_isBackendLayout($layout)
    {
        return \IMIA\ImiaBase\User\BackendLayout::is($layout);
    }
}

if (!class_exists('Psr\Log\LoggerInterface') && !\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('install')) {
    try {
        new \TYPO3\CMS\Core\Log\Logger(null);
    } catch (\Exception $e) {
        if (!function_exists('rrmdir')) {
            function rrmdir($dir)
            {
                if (!file_exists($dir)) {
                    return true;
                }
                if (!is_dir($dir)) {
                    return unlink($dir);
                }

                foreach (scandir($dir) as $item) {
                    if ($item != '.' && $item != '..') {
                        if (!rrmdir($dir . DIRECTORY_SEPARATOR . $item)) {
                            return false;
                        }
                    }
                }

                return rmdir($dir);
            }
        }

        @rrmdir(PATH_site . 'typo3temp/var/Cache/Data/cache_classes');
    }
}

$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['formDataGroup']['inlineParentRecord'][\TYPO3\CMS\Backend\Form\FormDataProvider\DatabaseEditRow::class] = [];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['formDataGroup']['inlineParentRecord'][\TYPO3\CMS\Backend\Form\FormDataProvider\InitializeProcessedTca::class] = [
    'depends' => [
        \TYPO3\CMS\Backend\Form\FormDataProvider\DatabaseEditRow::class,
    ],
];

if (\TYPO3\CMS\Core\Utility\GeneralUtility::getApplicationContext()->isDevelopment() && !$extConf['disableAssetic']) {
    $GLOBALS['TYPO3_CONF_VARS']['BE']['toolbarItems'][$_EXTKEY] =
        \IMIA\ImiaBase\Toolbar\ToolbarItem::class;
}

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['GLOBAL']['cliKeys']['base_assetic'] = [
    function () {
        \IMIA\ImiaBase\Utility\AssetUtility::parseAssets(null, true);
    },
];

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['GLOBAL']['cliKeys']['base_clearcache'] = [
    function () {
        /** @var \TYPO3\CMS\Core\Cache\CacheManager $cacheManager */
        $cacheManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Core\Cache\CacheManager');
        $cacheManager->setCacheConfigurations($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']);
        new \TYPO3\CMS\Core\Cache\CacheFactory('production', $cacheManager);

        $cacheManager->flushCaches();
        /** @var \TYPO3\CMS\Core\Database\DatabaseConnection $typo3DB */
        $typo3DB = $GLOBALS['TYPO3_DB'];
        $typo3DB->exec_TRUNCATEquery('cache_treelist');

        $dataHandler = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Core\DataHandling\DataHandler');
        foreach (['system', 'all', 'pages'] as $cacheCmd) {
            $_params = ['cacheCmd' => strtolower($cacheCmd)];
            foreach ($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['clearCachePostProc'] as $_funcRef) {
                \TYPO3\CMS\Core\Utility\GeneralUtility::callUserFunction($_funcRef, $_params, $dataHandler);
            }
        }
    },
    '_CLI_scheduler',
];

\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class)->connect(
    TYPO3\CMS\Backend\Controller\EditDocumentController::class,
    'initAfter',
    IMIA\ImiaBase\SignalSlot\EditDocumentControllerSignalSlot::class,
    'initAfter'
);

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] =
    \IMIA\ImiaBase\Command\CrawlCommandController::class;

if ($extConf['bootstrapVisibility']) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript($_EXTKEY, 'setup',
        '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/TypoScript/lib/bootstrapVisibility.typoscript">');
}

if ($extConf['spaceBeforeAfter']) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript($_EXTKEY, 'setup',
        '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/TypoScript/lib/spaceBeforeAfter.typoscript">');
}

$singleSignOnEnabled = (bool)$extConf['singleSignOn'];
if ($singleSignOnEnabled) {
    $GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['beSingleSignOn'] =
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Classes/Dispatcher/BeSingleSignOnDispatcher.php';
}

if (!is_array($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['tx_imiabase_content'])) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['tx_imiabase_content'] = [];
}
if (!isset($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['tx_imiabase_content']['backend'])) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['tx_imiabase_content']['backend'] =
        \TYPO3\CMS\Core\Cache\Backend\Typo3DatabaseBackend::class;
}

if (!is_array($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['tx_imiabase'])) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['tx_imiabase'] = [];
}
if (!isset($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['tx_imiabase']['backend'])) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['tx_imiabase']['backend'] =
        \TYPO3\CMS\Core\Cache\Backend\Typo3DatabaseBackend::class;
}

if (TYPO3_MODE != 'FE') {
    unset($GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling']);
}

$GLOBALS['TYPO3_CONF_VARS']['FE']['jsCompressHandler'] =
    \IMIA\ImiaBase\Handler\JsCompressHandler::class . '->handle';

/** ContentDefender */
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['formDataGroup']['tcaDatabaseRecord'][\IMIA\ImiaBase\ContentDefender\Form\FormDataProvider\TcaCTypeItems::class] = [
    'depends' => [
        \TYPO3\CMS\Backend\Form\FormDataProvider\DatabaseRowDefaultValues::class,
    ],
    'before'  => [
        \TYPO3\CMS\Backend\Form\FormDataProvider\TcaSelectItems::class,
    ],
];

$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['formDataGroup']['tcaDatabaseRecord'][\IMIA\ImiaBase\ContentDefender\Form\FormDataProvider\TcaColPosItems::class] = [
    'depends' => [
        \TYPO3\CMS\Backend\Form\FormDataProvider\TcaSelectItems::class,
    ],
];

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass']['content_defender'] =
    \IMIA\ImiaBase\ContentDefender\Hooks\DatamapDataHandlerHook::class;

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processCmdmapClass']['content_defender'] =
    \IMIA\ImiaBase\ContentDefender\Hooks\CmdmapDataHandlerHook::class;

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms']['db_new_content_el']['wizardItemsHook']['content_defender'] =
    \IMIA\ImiaBase\ContentDefender\Hooks\WizardItemsHook::class;

$GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['EXT:form/Resources/Private/Language/Database.xlf'][] =
    'EXT:' . $_EXTKEY . '/Resources/Private/Language/Override/form/Database.xlf';
$GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['EXT:form/Resources/Private/Language/Database.xlf']['de'] =
    'EXT:' . $_EXTKEY . '/Resources/Private/Language/Override/form/de.Database.xlf';