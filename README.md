IMIA Base for TYPO3
======================

Features
----------------------
-   Static TypoScript with .ts Ending (additionally to .txt)
-   Relative Includes for Static TypoScript
-   Assetic for including Javascripts & Stylesheets with Filters:
    -   Compass & Sass/Scss
    -   CssEmbed
    -   Yui Compressor

Installation
---------------------

### Dependencies for Assetic Filters: ###

#### install java for yui and cssembed ####

    # add non-free to debian/ubuntu debs in apt/sources.list

    $ aptitude install sun-java6-jre

#### install compass and compass plugins ####

    $ aptitude install ruby rubygems

    $ gem update
    $ gem install sass --pre
    $ gem install compass --pre

    $ gem install compass-h5bp

Documentation
---------------------

### Relative TypoScript Includes ###

    <INCLUDE_TYPOSCRIPT: source="FILE_RELATIVE: lib/setup.ts">

### Assetic (TypoScript Setup) ###

#### General Configuration (default values) ####

    config.assetic.settings {
        # path to java
        javaPath = /usr/bin/java

        # compass filter settings
        compass {
            # path to compass
            binPath = /var/lib/gems/1.8/bin/compass

            # compass plugins, seperated with komma (,)
            # possible plugins:
            #   compass-h5bp: HTML5 Boilerplate - https://github.com/sporkd/compass-h5bp/blob/master/README.md
            plugins = compass-h5bp
        }

        # cssembed filter settings
        cssembed {
            # skip not found images
            skip-missing = 1

            # charset of the css file
            charset = utf-8

            # max length of the base64 uris to generate
            max-uri-length = 32768

            # max image size of image to convert
            max-image-size = 1048576
        }

        # yui_js und yui_css filter settings
        yui {
            # path to yuicompressor.jar
            jarPath = typo3conf/ext/imia_base/vendor/heartsentwined/yuicompressor/yuicompressor.jar
        }
    }

#### Page configuration ####
    page = PAGE
    page {
        assetic < config.assetic
        assetic {
            # enable assetic, 1 = compile on each run, 2 = load compiled js/css from cache
            enabled = 1

            javascripts {
                # enable javascripts, 1 = compile on each run, 2 = load compiled js/css from cache
                enabled = 1

                # files to load, file or *[.ext] (all files in a directory)
                files {
                    10 = typo3conf/ext/imia_dummy/Resources/Private/JS/main.js
                    20 = typo3conf/ext/imia_dummy/Resources/Private/JS/vendor/*.js
                }

                # filters, seperated with komma (,)
                # possible filters:
                #   yui_js: Yui Compressor - http://yui.github.com/yuicompressor/
                filters =

                # output, cached file
                output = uploads/tx_imiabase/scripts.js
            }

            stylesheets {
                # enable stylesheets, 1 = compile on each run, 2 = load compiled js/css from cache
                enabled = 1

                # files to load, file or *[.ext] (all files in a directory)
                files {
                    10 = typo3conf/ext/imia_dummy/Resources/Private/SCSS/layout.scss
                    20 = typo3conf/ext/imia_dummy/Resources/Private/SCSS/vendor/*.scss
                }

                # filters, seperated with komma (,)
                # possible filters:
                #   compass: Compass (& Scss) - http://compass-style.org/reference/compass/
                #   cssembed: CSSEmbed - https://github.com/nzakas/cssembed/blob/master/README
                #   cssrewrite: CSSRewrite - Rewriting of url-paths for the different output path
                #   yui_css: Yui Compressor - http://yui.github.com/yuicompressor/
                filters = compass, cssembed, cssrewrite

                # output, cached file
                output = uploads/tx_imiabase/styles.css
            }
        }
    }