#lightbox 2 #

### Manuelle Installation ohne npm

Ordner /dist/lightbox2 nach Resources/Private/TS/vendors/ kopieren.

### JS und CSS einbinden

```js
//app.ts
import '../vendors/lightbox2/js/lightbox.js'
require("../vendors/lightbox2/css/lightbox.css");
```
