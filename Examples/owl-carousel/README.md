#Owl Carousel 2 #

### Install


Package instalation mit npm:

`npm install --save-dev owl.carousel`

oder spezifischer Version

`npm install --save-dev owl.carousel@2.2.0`

oder per Git

- version 2.1.x ([semver](https://docs.npmjs.com/misc/semver))
`npm install --save-dev https://github.com/OwlCarousel2/OwlCarousel2.git#semver:^2.1`
- commit
`npm install --save-dev https://github.com/OwlCarousel2/OwlCarousel2.git#6bd507135fd94c8cc1e334b0342851a7d4da601a`
- master `npm install --save-dev https://github.com/OwlCarousel2/OwlCarousel2.git`


#### Typescript support:

`npm install --save-dev @types/owlcarousel`

#### Load per Webpack

Benötigtes CSS und JS laden:

```js
//app.ts
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel';
```

**alternativ SCSS und JS** 

```js
//app.ts
import 'owl.carousel';
```

```
/* _core.scss */
@import "../TS/node_modules/owl.carousel/src/scss/owl.carousel";
@import "../TS/node_modules/owl.carousel/src/scss/owl.theme.default";
```

### Typescript Komponente

`Slider.ts.example` in `Slider.ts` umbennen und in `src/content` Ordner kopieren.

#### Slider.ts registrieren
Die Slider.ts kann in der Content Klasse geladen werden, damit der Slider in der Anwendung initialisiert wird. 

```
//General.ts
...
import Slider from "./Slider";
...

init() {
  ...
  new Slider();
  ...
}
```




