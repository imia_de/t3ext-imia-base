#Multi-Brand Konfiguration #

### Install

Die beiden Brand-Ordner (BrandA und BrandB) können in den Resources/Private Order
kopiert werden.

Die Main-Style SCSS-Datei muss über den Brand importiert werden.

```
/* styles.scss */
@import "vars";
@import "../../../SCSS/styles.scss";
@import "Styles/content";

```

####Webpack

Die Konfiguration von webpack muss angepasst werden. 
Für jeden Brand muss ein neuer Enty-Point hinzugefügt werden.

```js
//webpack.config.js
...
let entries = {
    //app:  ['./src/app.ts'],
    brandA: './src/branda.ts',
    brandB: './src/brandb.ts',
    rte: './src/rte.ts',
    common: './src/common.ts'
};
```

#####Brands-Config

Der neuen Entry-Point muss die Klasse BaseApp extended um alle JS zu laden.
Die Brand-SCSS wird entspreched dem Pfad eingebunden.

/src/branda.ts
```js
import BaseApp from "./BaseApp";
require('../../Brands/BrandA/SCSS/styles.scss');
class BrandA extends BaseApp {

}
```

/src/brandb.ts
```js
import BaseApp from "./BaseApp";
require('../../Brands/BrandB/SCSS/styles.scss');
class BrandB extends BaseApp {
}
```

### !!!Wichtig
Nach dem Anpassen der webpack Konfiiguration 
muss das watcher-Script neu gestartet werden, 
damit die Änderungen wirksam werden.


####TYPO3

```
includeJSFooter.branda = EXT:{$config.extKey}/Resources/Public/Javascripts/branda.js
includeCSSLibs.branda = EXT:{$config.extKey}/Resources/Public/Stylesheets/branda.css
```

