if (typeof TYPO3.jQuery !== 'undefined') {
    (function($) {
        $(document).ready(function() {
            $('a.toggle-grid').each(function() {
                var ce = $(this).closest('div.t3-page-ce');
                var grid = ce.find('div.t3-grid-container:first');

                $(this).click(function(e) {
                    e.preventDefault();

                    if (!grid.hasClass('grid-hidden')) {
                        grid.addClass('grid-hidden').slideUp();
                        $(this).addClass('collapsed');

                        Cookies.set('content-grid-hidden-' + ce.data('uid'), '1', {path: '', expires: 365});
                    } else {
                        grid.removeClass('grid-hidden').slideDown();
                        $(this).removeClass('collapsed');

                        Cookies.remove('content-grid-hidden-' + ce.data('uid'), {path: ''});
                    }

                    return false;
                });
            });
        });
    })(TYPO3.jQuery);
}