if (typeof TYPO3.jQuery !== 'undefined') {
    (function($) {
        $(document).ready(function(){
            $('.form-section .nav-tabs').each(function(){
                var firstLi = $(this).children('li:first');
                if ($(this).children('li').length <= 1) {
                    $(this).hide();
                    $(firstLi.children('a:first').attr('href')).attr('class', '');
                } else if ($(this).children('li.active').length == 0) {
                    firstLi.addClass('active');
                    $(firstLi.children('a:first').attr('href')).addClass('active');
                }

                var section = $(this).closest('.form-section');
                if (!section.hasClass('fieldset-optimized')) {
                    section.addClass('fieldset-optimized');
                    section.css('padding', '0').css('border', 'none');
                    var label = section.find('label:first');
                    if (label.text().trim()) {
                        label.replaceWith('<h4 class="form-section-headline">' + label.html() + '</h4>');
                    } else {
                        label.remove();
                    }

                    section.children('.form-group').addClass('form-section').css('margin', '0').css('padding-bottom', '15px');
                }

                $(this).css('border-top', '1px solid #ccc');
                $(this).css('border-left', '1px solid #ccc');
                $(this).css('border-right', '1px solid #ccc');
                $(this).children('li').css('margin-top', '-1px');
                $(this).children('li').css('margin-left', '-1px');
            });

            $('.form-section > .form-group > .t3js-formengine-field-item > .form-section').each(function(){
                var section = $(this).parent().closest('fieldset.form-section');
                if (!section.hasClass('fieldset-optimized')) {
                    section.addClass('fieldset-optimized');
                    section.css('padding', '0').css('border', 'none');
                    var label = section.find('label:first');
                    label.replaceWith('<h4 class="form-section-headline">' + label.html() + '</h4>');

                    var group = section.children('.form-group').addClass('form-section').css('margin', '0');
                    group.find('.form-section').each(function(){
                        var subsection = $(this);
                        var removeSection = true;
                        var heading = subsection.closest('.panel-collapse').prev('.panel-heading');
                        if (heading.length > 0 && heading.children('.form-irre-header').length > 0) {
                            removeSection = false;
                        }
                        if (removeSection) {
                            subsection.removeClass('form-section');
                        }
                    });
                }
            });

            $('.form-section > .form-group > .t3js-formengine-field-item .table .col-checkbox').each(function(){
                var tr = $(this).closest('tr');
                var helpLink = tr.find('.t3-help-link');
                if (helpLink.length > 0) {
                    tr.mouseenter(function () {
                        helpLink.trigger('click');
                    }).mouseleave(function () {
                        helpLink.trigger('click');
                    });
                }
            });

            $('.form-section > .form-group > .t3js-formengine-label').each(function(){
                if (!$(this).text().trim() && $(this).parent().children('.form-section').length > 0) {
                    $(this).parent().children('.form-section').css('border', 'none').css('padding', '0');
                    $(this).remove();
                }
            })
        });

        function checkColorPicker() {
            if ($('.t3js-color-picker:not(.minicolors-input)').length > 0) {
                require(['TYPO3/CMS/Backend/ColorPicker'], function(ColorPicker){ColorPicker.initialize()});
            }
        }
        checkColorPicker();

        setTimeout(function(){
            checkColorPicker();
        }, 500);
    })(TYPO3.jQuery);
}