if (typeof TYPO3.jQuery !== 'undefined') {
    (function($) {
        var gridClasses = 'col-1 col-2 col-3 col-4 col-5 col-6 col-7 col-8 col-9 col-10 col-11 col-12 ' +
            ' col-xs-1 col-xs-2 col-xs-3 col-xs-4 col-xs-5 col-xs-6 col-xs-7 col-xs-8 col-xs-9 col-xs-10 col-xs-11 col-xs-12';

        var gridSystem = [{
            grid: 8.33333333,
            col: 1
        }, {
            grid: 16.66666667,
            col: 2
        }, {
            grid: 25,
            col: 3
        }, {
            grid: 33.33333333,
            col: 4
        }, {
            grid: 41.66666667,
            col: 5
        }, {
            grid: 50,
            col: 6
        }, {
            grid: 58.33333333,
            col: 7
        }, {
            grid: 66.66666667,
            col: 8
        }, {
            grid: 75,
            col: 9
        }, {
            grid: 83.33333333,
            col: 10
        }, {
            grid: 91.66666667,
            col: 11
        }, {
            grid: 100,
            col: 12
        }, {
            grid: 10000,
            col: 10000
        }];

        $(document).ready(function() {
            if (!$.resizable && !$.fn.resizable) {
                var waitTimeout = setInterval(function() {
                    if ($.resizable || $.fn.resizable) {
                        clearInterval(waitTimeout);
                        initGrid();
                    }
                }, 200);
            } else {
                initGrid();
            }
        });

        function initGrid() {
            $('div.grid-layout').each(function() {
                var gridLayout = $(this);
                var dataElement = gridLayout.find('input:first');
                var data = JSON.parse(dataElement.val().replace(/'/g, '"'));
                var columns = null;
                var columnsElement = $('select[name="' + dataElement.data('columns') + '"]');
                var responsiveTypes = {
                    'default': gridLayout.find('div.grid-layout-default'),
                    'lg': gridLayout.find('div.grid-layout-lg'),
                    'md': gridLayout.find('div.grid-layout-md'),
                    'sm': gridLayout.find('div.grid-layout-sm'),
                    'xs': gridLayout.find('div.grid-layout-xs')
                };
                gridLayout.closest('.form-section').css('overflow', 'hidden');

                gridLayout.children('h5.enablable').click(function(e) {
                    e.preventDefault();
                    $(this).next('.row').slideToggle();
                    $(this).toggleClass('enabled');

                    updateGridData();
                });

                columnsElement.change(function() {
                    columnsElementChange();
                });
                columnsElementChange();

                function columnsElementChange() {
                    columns = parseInt(columnsElement.val());

                    if (!data[columns]) {
                        data[columns] = {
                            'default': [],
                            'xs': []
                        };

                        for (var i = 0; i < columns; i++) {
                            data[columns]['default'][i] = {
                                'w': 3
                            };
                            data[columns]['xs'][i] = {
                                'w': 12
                            };
                        }
                    }

                    $.each(responsiveTypes, function(type, typeElement) {
                        typeElement.children().remove();
                        if (data[columns][type]) {
                            $.each(data[columns][type], function(key, config) {
                                typeElement.append('<div class="grid-column col-' + config['w'] + ' col-xs-' + config['w'] + '" data-width="' + config['w'] + '"><span class="info">' + config['w'] + ' / 12</span></div>')
                            });

                            if (!typeElement.prev().hasClass('enabled')) {
                                typeElement.slideDown();
                                typeElement.prev().addClass('enabled');
                            }
                        } else {
                            if (typeElement.prev().hasClass('enabled')) {
                                typeElement.hide();
                                typeElement.prev().removeClass('enabled');
                            }

                            $.each(data[columns]['default'], function(key, config) {
                                typeElement.append('<div class="grid-column col-' + config['w'] + ' col-xs-' + config['w'] + '" data-width="' + config['w'] + '"><span class="info">' + config['w'] + ' / 12</span></div>')
                            });
                        }
                    });

                    gridLayout.find('.grid-column').resizable({
                            handles: 'e',
                            create: function(e, ui) {
                            },
                            start: function(e, ui) {
                            },
                            stop: function(e, ui) {
                            },
                            resize: function(e, ui) {
                                var column = $(this);
                                var container = column.parent();
                                var cellPercentWidth = 100 * ui.originalElement.outerWidth() / container.innerWidth();

                                ui.originalElement.css('width', cellPercentWidth + '%');

                                var columnSize = getColumnSize(cellPercentWidth);

                                column
                                    .attr('data-width', columnSize)
                                    .removeClass(gridClasses)
                                    .addClass('col-' + columnSize + ' col-xs-' + columnSize)
                                    .css('width', '')
                                ;

                                column.find('.info').text(columnSize + ' / 12');

                                updateGridData()
                            }
                        }
                    );
                }

                function updateGridData() {
                    data[columns] = {};
                    $.each(responsiveTypes, function(type, typeElement) {
                        if (typeElement.prev().hasClass('enabled')) {
                            data[columns][type] = [];
                            var count = 0;
                            typeElement.find('.grid-column').each(function() {
                                data[columns][type][count] = {
                                    'w': $(this).attr('data-width')
                                };
                                count++;
                            });
                        }
                    });

                    dataElement.val(JSON.stringify(data).replace(/"/g, "'"));
                }

                function getColumnSize(value) {
                    var closest, mindiff = null;

                    for (var i = 0; i < gridSystem.length; ++i) {
                        var diff = Math.abs(gridSystem[i].grid - value);

                        if (mindiff === null || diff < mindiff) {
                            closest = i;
                            mindiff = diff;
                        } else {
                            return gridSystem[closest]['col']; // col number
                        }
                    }

                    return null;
                }
            });
        }
    })(TYPO3.jQuery);
}