<?php

namespace IMIA\Assetic;

use Assetic\Asset\FileAsset;
use Assetic\Asset\HttpAsset;
use Assetic\AssetManager;
use Assetic\AssetWriter;
use Assetic\Factory\AssetFactory;
use Assetic\FilterManager;
use Assetic\Filter;
use Assetic\Util\PathUtils;

class AssetBuilder
{
    /**
     * @var array
     */
    protected $settings;

    /**
     * @param array $settings
     */
    public function __construct($settings)
    {
        $this->setSettings($settings);
    }

    /**
     * @param array $settings
     */
    public function setSettings($settings)
    {
        $this->settings = $settings;
    }

    /**
     * @param array $files
     * @return mixed
     */
    public function build($files)
    {
        $am = new AssetManager();

        $assets = array();
        foreach ($files as $key => $file) {
            $exclude = array();
            if (is_array($file)) {
                if ($file['_']) {
                    $exclude = array_map('trim', explode(',', $file['exclude']));
                    $file = $file['_'];
                } else {
                    continue;
                }
            }

            if (strpos($file, 'http') === 0 || strpos($file, '//') === 0) {
                $assets[] = new HttpAsset($file);
            } else {
                if (preg_match('/^(.+)\*(\.[^.]+)?$/ism', $file, $match)) {
                    $dir = $match[1];
                    $ext = $match[2];

                    if (is_dir($this->settings['rootPath'] . $dir)) {
                        $fileEntries = array();
                        $handle = dir($this->settings['rootPath'] . $dir);
                        while (false !== ($entry = $handle->read())) {
                            if (!in_array(substr($entry, 0, strlen($entry) - strlen($ext)), $exclude)) {
                                if (!is_dir($this->settings['rootPath'] . $dir . '/' . $entry)
                                    && strrpos($entry, $ext) == strlen($entry) - strlen($ext)
                                ) {
                                    $fileEntries[] = $entry;
                                }
                            }
                        }
                        $handle->close();

                        sort($fileEntries);
                        foreach ($fileEntries as $fileEntry) {
                            $assets[] = new FileAsset($this->settings['rootPath'] . $dir . $fileEntry, array(), $this->settings['rootPath'], $dir . $fileEntry);
                        }
                    }
                } else {
                    $assets[] = new FileAsset($this->settings['rootPath'] . $file, array(), $this->settings['rootPath'], $file);
                }
            }
        }

        $fileAliases = array();
        foreach ($assets as $key => $asset) {
            $am->set($key, $asset);
            $fileAliases[] = '@' . $key;
        }

        $fm = $this->getFilterManager($this->settings['compile']['filters']);

        $factory = new AssetFactory($this->settings['rootPath']);
        $factory->setAssetManager($am);
        $factory->setFilterManager($fm);

        $resource = $factory->createAsset(
            $fileAliases,
            $this->settings['compile']['filters'],
            array(
                'output' => $this->settings['compile']['output'],
                'root' => $this->settings['rootPath'],
            )
        );

        return $resource;
    }

    /**
     * @param string $asset
     * @return bool
     */
    public function write($asset)
    {
        $outputFile = $this->settings['rootPath'] . '/' . PathUtils::resolvePath(
                $asset->getTargetPath(), $asset->getVars(), $asset->getValues());

        $this->ensureDirExists(dirname($outputFile));

        if (is_dir(dirname($outputFile))) {
            $aw = new AssetWriter($this->settings['rootPath']);
            $aw->writeAsset($asset);

            if (file_exists($outputFile)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param array $filters
     * @return FilterManager
     */
    protected function getFilterManager(&$filters)
    {
        $fm = new FilterManager();

        foreach ($filters as $key => $filterName) {
            switch ($filterName) {
                case 'yui_css':
                    if (array_key_exists('yui', $this->settings['filters'])) {
                        $jarPath = $this->settings['filters']['yui']['jarPath'];
                        if (!is_file($jarPath)) {
                            $jarPath = $this->settings['rootPath'] . $jarPath;
                        }

                        $filter = new Filter\Yui\CssCompressorFilter(
                            $jarPath,
                            $this->settings['javaPath'] ? $this->settings['javaPath'] : '/usr/bin/java'
                        );
                    }
                    break;
                case 'yui_js':
                    if (array_key_exists('yui', $this->settings['filters'])) {
                        $jarPath = $this->settings['filters']['yui']['jarPath'];
                        if (!is_file($jarPath)) {
                            $jarPath = $this->settings['rootPath'] . $jarPath;
                        }

                        $filter = new Filter\Yui\JsCompressorFilter(
                            $jarPath,
                            $this->settings['javaPath'] ? $this->settings['javaPath'] : '/usr/bin/java'
                        );
                    }
                    break;
                case 'compass':
                    if (array_key_exists('compass', $this->settings['filters'])) {
                        $filter = new Filter\CompassFilter($this->settings['filters']['compass']['binPath']
                            ? $this->settings['filters']['compass']['binPath']
                            : '/usr/bin/compass');

                        $filter->setCacheLocation($this->settings['cachePath']);
                        $filter->setHomeEnv(false);

                        if (array_key_exists('plugins', $this->settings['filters']['compass'])) {
                            $filter->setPlugins(array_map('trim', $this->settings['filters']['compass']['plugins']));
                        }
                        if (array_key_exists('images-dir', $this->settings['filters']['compass'])) {
                            $filter->setImagesDir($this->settings['rootPath'] . $this->settings['filters']['compass']['images-dir']);
                            $filter->setHttpImagesPath('/' . $this->settings['filters']['compass']['images-dir']);
                        }
                        if (array_key_exists('fonts-dir', $this->settings['filters']['compass'])) {
                            $filter->setFontsDir($this->settings['rootPath'] . $this->settings['filters']['compass']['fonts-dir']);
                            $filter->setHttpFontsPath('/' . $this->settings['filters']['compass']['fonts-dir']);
                        }
                        if (array_key_exists('generated-images-dir', $this->settings['filters']['compass'])) {
                            $filter->setGeneratedImagesPath($this->settings['rootPath'] . $this->settings['filters']['compass']['generated-images-dir']);
                            $filter->setHttpGeneratedImagesPath('/' . $this->settings['filters']['compass']['generated-images-dir']);
                        }
                    }
                    break;
                case 'sassc':
                    if (array_key_exists('sassc', $this->settings['filters'])) {
                        $filter = new \IMIA\Assetic\Filter\SasscFilter($this->settings['filters']['sassc']['binPath'] ?: '/usr/local/bin/sassc');

                        if ($this->settings['filters']['sassc']['loadPaths'] && is_array($this->settings['filters']['sassc']['loadPaths'])) {
                            $loadPaths = array();
                            foreach ($this->settings['filters']['sassc']['loadPaths'] as $loadPath) {
                                $loadPaths[] = $this->settings['rootPath'] . $loadPath;
                            }

                            $filter->setLoadPaths($loadPaths);
                        }
                    }
                    break;
                case 'less':
                    if (array_key_exists('less', $this->settings['filters'])) {
                        $filter = new Filter\LessFilter($this->settings['filters']['less']['nodePath'] ?: '/usr/local/bin/node', $this->settings['filters']['less']['nodeLibs']);
                    }
                    break;
                case 'autoprefixer':
                    if (array_key_exists('autoprefixer', $this->settings['filters'])) {
                        $filter = new \IMIA\Assetic\Filter\AutoprefixerFilter($this->settings['filters']['autoprefixer']['binPath'] ?: '/usr/local/bin/sassc');

                        if ($this->settings['filters']['autoprefixer']['browsers'] && is_array($this->settings['filters']['autoprefixer']['browsers'])) {
                            $filter->setBrowsers($this->settings['filters']['autoprefixer']['browsers']);
                        }
                    }
                    break;
                case 'cssembed':
                    if (array_key_exists('cssembed', $this->settings['filters'])) {
                        $jarPath = $this->settings['filters']['yui']['jarPath'];
                        if (!is_file($jarPath)) {
                            $jarPath = $this->settings['rootPath'] . $jarPath;
                        }

                        $filter = new Filter\CssEmbedFilter(
                            $jarPath ?: dirname(dirname(dirname(__DIR__))) . '/vendor/nzakas/cssembed/cssembed-0.4.5.jar',
                            $this->settings['javaPath'] ? $this->settings['javaPath'] : '/usr/bin/java'
                        );
                        $filter->setRoot($this->settings['rootPath']);

                        if (array_key_exists('skip-missing', $this->settings['filters']['cssembed'])) {
                            $filter->setSkipMissing((bool)$this->settings['filters']['cssembed']['skip-missing']);
                        }
                        if (array_key_exists('charset', $this->settings['filters']['cssembed'])) {
                            $filter->setCharset($this->settings['filters']['cssembed']['charset']);
                        }
                        if (array_key_exists('max-uri-length', $this->settings['filters']['cssembed'])) {
                            $filter->setMaxUriLength((int)$this->settings['filters']['cssembed']['max-uri-length']);
                        }
                        if (array_key_exists('max-image-size', $this->settings['filters']['cssembed'])) {
                            $filter->setMaxImageSize((int)$this->settings['filters']['cssembed']['max-image-size']);
                        }
                    }
                    break;
                case 'cssrewrite':
                    $filter = new Filter\CssRewriteFilter();
                    break;
            }

            if ($filter) {
                if ($filter instanceOf Filter\BaseProcessFilter) {
                    $filter->setTimeout((int)$this->settings['timeout'] ?: 180);
                }

                $fm->set($filterName, $filter);
            } else {
                unset($filters[$key]);
            }
        }

        return $fm;
    }

    /**
     * @param string $dir
     */
    protected function ensureDirExists($dir)
    {
        if (!is_dir($dir)) {
            $this->ensureDirExists(dirname($dir));
            @mkdir($dir);
        }
    }
}