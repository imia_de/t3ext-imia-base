<?php

namespace IMIA\Core;

use Symfony\Component\Yaml\Yaml;

class Config
{
    /**
     * @var array
     */
    protected $parameters = array();

    /**
     * @param string $rootPath
     * @param string $configFile
     */
    public function __construct($rootPath, $configFile)
    {
        $this->parameters = Yaml::parse($rootPath . '/' . $configFile);
        $this->parameters['rootPath'] = $rootPath;

        $this->insertParams($this->parameters);
    }

    /**
     * @param array $params
     */
    protected function insertParams(&$params)
    {
        foreach ($params as $paramKey => $param) {
            if (is_array($param)) {
                $this->insertParams($params[$paramKey]);
            } elseif (is_string($param)) {
                if (preg_match_all('/%([^%]+)%/', $param, $matches)) {
                    foreach ($matches[0] as $key => $match) {
                        if (array_key_exists($matches[1][$key], $this->parameters)) {
                            $params[$paramKey] = str_replace($match, $this->parameters[$matches[1][$key]], $param);
                        }
                    }
                }
            }
        }
    }

    /**
     * Get a config parameter
     *
     * @param string $key
     * @return mixed
     */
    public function get($key)
    {
        if (array_key_exists($key, $this->parameters)) {
            return $this->parameters[$key];
        } else {
            return false;
        }
    }
}