<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Registry;

use IMIA\ImiaBase\Utility\Cache;
use TYPO3\CMS\Core\Imaging\IconRegistry;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Lang\LanguageService;

/**
 * @package     imia_base
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class FluidStyledContentRegistry
{
    /**
     * @var array
     */
    static protected $extensions = [];

    /**
     * @var array
     */
    static protected $pageExtensionProviders = [];

    /**
     * @var array
     */
    static protected $contentExtensionProviders = [];

    /**
     * @var array
     */
    static protected $pageLayouts = [];

    /**
     * @var array
     */
    static protected $contentElements = [];

    /**
     * @var array
     */
    static protected $contentElementConfiguration = [];

    /**
     * @var array
     */
    static protected $contentElementsEnabled = [];

    /**
     * @var array
     */
    static protected $contentElementGroups = [];

    /**
     * @var array
     */
    static protected $iconProviders = [];

    /**
     * @var string
     */
    static protected $currentExtKey = null;

    /**
     * @var array
     */
    static protected $data;

    /**
     * @internal
     */
    static public function loadPageTS()
    {
        if (self::getData('pageTs')) {
            ExtensionManagementUtility::addPageTSConfig(self::getData('pageTs'));
        }
    }

    /**
     * @internal
     */
    static public function loadTCA()
    {
        if (self::getData('tca')) {
            eval(self::getData('tca'));

            $newItems = [];
            foreach ($GLOBALS['TCA']['sys_template']['columns']['include_static_file']['config']['items'] as $item) {
                if (strpos($item[1], 'EXT:fluid_styled_content') === false) {
                    $newItems[] = $item;
                }
            }
            $GLOBALS['TCA']['sys_template']['columns']['include_static_file']['config']['items'] = $newItems;
        }
    }

    /**
     * @internal
     */
    static public function loadTypoScript()
    {
        if (self::getData('typoscript')) {
            ExtensionManagementUtility::addTypoScript('imia_base', 'setup', self::getData('typoscript'));
        }
        if (self::getData('typoscriptConstants')) {
            ExtensionManagementUtility::addTypoScript('imia_base', 'constants', self::getData('typoscriptConstants'));
        }
    }

    /**
     * @internal
     */
    static public function loadIcons()
    {
        if (self::getData('icons') && count(self::getData('icons')) > 0) {
            /** @var IconRegistry $iconRegistry */
            $iconRegistry = GeneralUtility::makeInstance(IconRegistry::class);

            foreach (self::getData('icons') as $identifier => $conf) {
                $iconRegistry->registerIcon(
                    $identifier,
                    $conf['type'] == 'svg'
                        ? \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class
                        : \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
                    ['source' => $conf['path']]
                );
            }
        }
    }

    /**
     * @internal
     * @return array
     */
    static public function getIconProviders()
    {
        static $iconProviders;

        if (!$iconProviders) {
            $iconProviders = [];
            if (is_array(self::getData('iconProvider'))) {
                foreach (self::getData('iconProvider') as $config) {
                    $iconProviders[] = GeneralUtility::makeInstance($config['class'], $config['options']);
                }
            }
        }

        return $iconProviders;
    }

    /**
     * @param string $key
     * @return array|string
     */
    static protected function getData($key = null)
    {
        if (!self::$data) {
            self::$data = [];
            if (ExtensionManagementUtility::isLoaded('fluid_styled_content')) {
                $cache = new Cache('fluidStyledContentRegistry');
                if ($cache && $cache->exists()) {
                    self::$data = $cache->load();
                } else {
                    self::$data = [];

                    foreach ($GLOBALS['TYPO3_LOADED_EXT'] as $extKey => $extInfo) {
                        $extPath = ExtensionManagementUtility::extPath($extKey);
                        $settingsPath = $extPath . 'ext_base.php';

                        if (file_exists($settingsPath)) {
                            self::$extensions[$extKey] = $settingsPath;
                        }
                    }

                    foreach (self::$extensions as $extKey => $settingsPath) {
                        self::$currentExtKey = $extKey;
                        require_once($settingsPath);
                    }

                    self::$data['pageTs'] = '';
                    self::$data['typoscript'] = '';
                    self::$data['typoscriptConstants'] = '';
                    self::$data['tca'] = '';
                    self::$data['icons'] = [];
                    self::$data['iconProvider'] = self::$iconProviders;

                    if (count(self::$pageExtensionProviders) > 1 || count(self::$contentExtensionProviders) > 1) {
                        self::$data['typoscript'] .= '
                            <INCLUDE_TYPOSCRIPT: source="FILE:EXT:fluid_styled_content/Configuration/TypoScript/setup.txt">
                            <INCLUDE_TYPOSCRIPT: source="FILE:EXT:fluid_styled_content/Configuration/TypoScript/Styling/setup.txt">
                            <INCLUDE_TYPOSCRIPT: source="FILE:EXT:imia_base/Configuration/TypoScript/lib/fluidstyledcontent.typoscript">
                        ';
                        self::$data['typoscriptConstants'] .= '
                            <INCLUDE_TYPOSCRIPT: source="FILE:EXT:fluid_styled_content/Configuration/TypoScript/constants.txt">
                        ';
                    }

                    foreach (self::$pageLayouts as $name => $config) {
                        self::$data['pageTs'] .= '
                            mod.web_layout.BackendLayouts.' . $name . ' {
                                title = ' . $config['title'] . '
                                icon = ' . $config['icon'] . '
                                config {
                                    backend_layout {
                            ' . self::arrayToTypoScript($config['config'], 3) . '
                                    }
                                }
                            }';
                    }
                    foreach (self::$contentElementGroups as $groupName => $groupConfig) {
                        if (!in_array($groupName, ['common', 'menu', 'special', 'forms', 'plugins'])) {
                            self::$data['pageTs'] .= '
                                mod.wizards.newContentElement.wizardItems {
                                    ' . $groupName . ' {
                                        header = ' . $groupConfig['title'] . '
                                        show = *
                                    }
                                }';

                            self::$data['tca'] .= '
                                $GLOBALS["TCA"]["tt_content"]["columns"]["CType"]["config"]["items"][] = ["' . $groupConfig['title'] . '", "--div--"];
                            ';
                        }

                        foreach ($groupConfig['contentElements'] as $contentName) {
                            if (in_array($contentName, self::$contentElementsEnabled)) {
                                $contentElement = self::$contentElements[$contentName];

                                $ctype = 'ibcontent_' . str_replace('/', '_', strtolower($contentName));
                                $iconPath = GeneralUtility::getFileAbsFileName($contentElement['icon']);
                                if ($iconPath && is_file($iconPath)) {
                                    $type = 'image';
                                    switch (mime_content_type($iconPath)) {
                                        case 'image/svg+xml':
                                        case 'image/svg':
                                            $type = 'svg';
                                            break;
                                    }

                                    $iconIdentifier = 'mimetypes-x-content-' . $ctype;
                                    self::$data['icons'][$iconIdentifier] = [
                                        'path' => $iconPath,
                                        'type' => $type,
                                    ];
                                } else {
                                    $iconIdentifier = $contentElement['icon'];
                                }

                                self::$data['pageTs'] .= '
                                    mod.wizards.newContentElement.wizardItems {
                                        ' . $contentElement['group'] . ' {
                                            elements {
                                                ' . $ctype . ' {
                                                    iconIdentifier = ' . $iconIdentifier . '
                                                    title = ' . $contentElement['title'] . '
                                                    description = ' . $contentElement['description'] . '
                                                    tt_content_defValues {
                                                        CType = ' . $ctype . '
                                                    }
                                                }
                                            }
                                        }
                                    }';

                                if (isset(self::$contentElementConfiguration[$contentName])) {
                                    self::$data['pageTs'] .= '
                                        mod.ctypes.' . $ctype . ' = ' . json_encode(self::$contentElementConfiguration[$contentName]);
                                }

                                self::$data['typoscript'] .= '
                                    tt_content.' . $ctype . ' =< lib.contentElement
                                    tt_content.' . $ctype . ' {
                                        templateName = ' . $contentName . '
                                        dataProcessing {
                                            10 = IMIA\ImiaBase\DataProcessing\ContentElementProcessor
                                        }
                                        stdWrap.editIcons = tt_content: header [header_layout], pages
                                    }';

                                self::$data['tca'] .= '
                                    $GLOBALS["TCA"]["tt_content"]["ctrl"]["typeicon_classes"]["' . $ctype . '"] = "' . $iconIdentifier . '";
                                ';

                                if (!in_array($groupName, ['common', 'menu', 'special', 'forms', 'plugins'])) {
                                    self::$data['tca'] .= '
                                        $GLOBALS["TCA"]["tt_content"]["columns"]["CType"]["config"]["items"][] = ["' . $contentElement['title'] . '", "' . $ctype . '", "' . $iconIdentifier . '"];
                                    ';
                                } else {
                                    if ($groupName != 'plugins') {
                                        self::$data['pageTs'] .= '
                                            mod.wizards.newContentElement.wizardItems.' . $contentElement['group'] . '.show := addToList(' . $ctype . ')';
                                    }

                                    $groupLabel = 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:CType.div.standard';
                                    switch ($groupName) {
                                        case 'plugins':
                                            $groupLabel = 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:CType.div.lists';
                                            break;
                                        case 'menu':
                                            $groupLabel = 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:CType.div.menu';
                                            break;
                                        case 'special':
                                            $groupLabel = 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:CType.div.special';
                                            break;
                                        case 'forms':
                                            $groupLabel = 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:CType.div.forms';
                                            break;
                                    }

                                    self::$data['tca'] .= '
                                        $newItems = [];
                                        $found = 0;
                                        foreach ($GLOBALS["TCA"]["tt_content"]["columns"]["CType"]["config"]["items"] as $item) {
                                            if ($item[1] == "--div--") {
                                                if ($found == 1) {
                                                    $found = 2;                                    
                                                    $newItems[] = ["' . $contentElement['title'] . '", "' . $ctype . '", "' . $iconIdentifier . '"];
                                                }
                                                if (!$found && $item[0] == "' . $groupLabel . '") {
                                                    $found = 1;
                                                }
                                            }
                                            $newItems[] = $item;
                                        }
                                        if ($found != 2) {
                                            $newItems[] = ["' . $contentElement['title'] . '", "' . $ctype . '", "' . $iconIdentifier . '"];
                                        }
                                        $GLOBALS["TCA"]["tt_content"]["columns"]["CType"]["config"]["items"] = $newItems;
                                    ';
                                }

                                if ($GLOBALS['TCA']['tt_content']['types']['ibcontent_' . $contentElement['type'] . '']['showitem']) {
                                    self::$data['tca'] .= '
                                        $GLOBALS["TCA"]["tt_content"]["types"]["' . $ctype . '"]["showitem"] = $GLOBALS["TCA"]["tt_content"]["types"]["ibcontent_' . $contentElement['type'] . '"]["showitem"];
                                    ';
                                } else {
                                    self::$data['tca'] .= '
                                        $GLOBALS["TCA"]["tt_content"]["types"]["' . $ctype . '"]["showitem"] = "' . $contentElement['type'] . '";
                                    ';
                                }

                                if (in_array($contentElement['type'], ['grid', 'accordion', 'tabs', 'gridfixed1', 'gridfixed2', 'gridfixed3'])) {
                                    self::$data['tca'] .= '
                                        $GLOBALS["TCA"]["tt_content"]["special"]["' . $contentElement['type'] . '"][] = "' . $ctype . '";
                                    ';
                                }

                                $flexFormLookup = false;
                                foreach (self::$contentExtensionProviders as $extKey) {
                                    if ($flexFormLookup) {
                                        $flexformTestPath = 'EXT:' . $extKey . '/Configuration/FlexForms/Content/' . $contentName . '.xml';
                                        if (is_file(GeneralUtility::getFileAbsFileName($flexformTestPath))) {
                                            $contentElement['flexform'] = $flexformTestPath;
                                        }
                                    }

                                    if ($extKey == $contentElement['registeredExt']) {
                                        $flexFormLookup = true;
                                    }
                                }

                                if ($contentElement['flexform']) {
                                    self::$data['tca'] .= '
                                        $GLOBALS["TCA"]["tt_content"]["columns"]["ib_flexform"]["config"]["ds"]["' . $ctype . '"] = "FILE:' . $contentElement['flexform'] . '";
                                        $GLOBALS["TCA"]["tt_content"]["columns"]["ib_flexform"]["displayCond"] .= ",' . $ctype . '";
                                    ';
                                }
                            }
                        }
                    }

                    $count = 10;
                    foreach (self::$pageExtensionProviders as $extKey) {
                        self::$data['typoscript'] .= '
                            lib.stdContent {
                                templateRootPaths.' . $count . ' = EXT:' . $extKey . '/Resources/Private/Templates/Page/
                                partialRootPaths.' . $count . ' = EXT:' . $extKey . '/Resources/Private/Partials/Page/
                                layoutRootPaths.' . $count . ' = EXT:' . $extKey . '/Resources/Private/Layouts/Page/
                            }';
                        $count++;
                    }

                    $count = 10;
                    foreach (self::$contentExtensionProviders as $extKey) {
                        self::$data['typoscript'] .= '
                            lib.contentElement {
                                templateRootPaths.' . $count . ' = EXT:' . $extKey . '/Resources/Private/Templates/Content/
                                partialRootPaths.' . $count . ' = EXT:' . $extKey . '/Resources/Private/Partials/Content/
                                layoutRootPaths.' . $count . ' = EXT:' . $extKey . '/Resources/Private/Layouts/Content/
                            }';
                        $count++;
                    }
                    self::$data['enabled'] = self::$contentElementsEnabled;

                    if ($cache) {
                        $cache->write(self::$data);
                    }
                }
            }
        }

        if ($key) {
            if (array_key_exists($key, self::$data)) {
                return self::$data[$key];
            } else {
                return null;
            }
        } else {
            return self::$data;
        }
    }

    /**
     * @param array $names
     */
    static public function enableContentElements(array $names)
    {
        if (!in_array(self::$currentExtKey, self::$contentExtensionProviders)) {
            self::$contentExtensionProviders[] = self::$currentExtKey;
        }

        foreach ($names as $name) {
            if (!in_array($name, self::$contentElementsEnabled) && array_key_exists($name, self::$contentElements)) {
                self::$contentElementsEnabled[] = $name;
            }
        }
    }

    /**
     * @param string $provider
     * @param array $options
     */
    static public function registerIconProvider($provider, $options)
    {
        self::$iconProviders[] = [
            'class'   => $provider,
            'options' => $options,
        ];
    }

    /**
     * @param string $name Resources/Private/Templates/Content/$name.html
     * @param string $group Group in Content Wizard
     * @param string $icon Iconpath for Backend
     * @param string $type Type for tt_content
     * @param string $title Alternative title label
     * @param string $description Alternative description label
     * @param string $flexform Alternative path
     */
    static public function modifyContentElement($name, $group = null, $icon = null, $type = null, $title = null, $description = null, $flexform = null)
    {
        $name = ucfirst($name);
        if (!in_array(self::$currentExtKey, self::$contentExtensionProviders)) {
            self::$contentExtensionProviders[] = self::$currentExtKey;
        }

        if (isset(self::$contentElements[$name])) {
            if ($group) {
                self::$contentElements[$name]['group'] = $group;
            }
            if ($icon) {
                self::$contentElements[$name]['icon'] = $icon;
            }
            if ($title) {
                self::$contentElements[$name]['title'] = $title;
            }
            if ($description) {
                self::$contentElements[$name]['description'] = $description;
            }
            if ($type) {
                self::$contentElements[$name]['type'] = $type;
            }
            if ($group) {
                self::$contentElements[$name]['group'] = $group;
            }
            if ($flexform) {
                if (strpos($flexform, 'EXT:') !== 0) {
                    $flexform = 'EXT:' . self::$currentExtKey . '/Configuration/FlexForms/Content/' . $name . '.xml';
                }
                $absFlexformPath = GeneralUtility::getFileAbsFileName($flexform);
                if (!file_exists($absFlexformPath)) {
                    $flexform = null;
                }

                self::$contentElements[$name]['flexform'] = $flexform;
            }
        }
    }

    /**
     * @param string $name Resources/Private/Templates/Content/$name.html
     * @param string $group Group in Content Wizard
     * @param string $icon Iconpath for Backend
     * @param string $type Type for tt_content
     * @param string $title Alternative title label, Default: Resources/Private/Language/Backend.xlf:contentelement.$name
     * @param string $description Alternative description label, Default: Resources/Private/Language/Backend.xlf:contentelement.$name.description
     * @param string $groupTitle Alternative group label, Default: Resources/Private/Language/Backend.xlf:contentgroup.$group
     * @param string $flexform Alternative path, Default: Configuration/Flexform/Content/$name.xml
     * @param array $configuration
     * @param bool $disabled
     */
    static public function registerContentElement($name, $group, $icon, $type = 'default', $title = null, $groupTitle = null,
                                                  $description = null, $flexform = null, $disabled = false, $configuration = [])
    {
        $name = ucfirst($name);
        $langKey = str_replace('/', '.', strtolower($name));
        if (!in_array(self::$currentExtKey, self::$contentExtensionProviders)) {
            self::$contentExtensionProviders[] = self::$currentExtKey;
        }

        $titleLangKey = 'LLL:EXT:' . self::$currentExtKey . '/Resources/Private/Language/Backend.xlf:contentelement.' . $langKey;
        $descriptionLangKey = 'LLL:EXT:' . self::$currentExtKey . '/Resources/Private/Language/Backend.xlf:contentelement.' . $langKey . '.description';
        $groupTitleLangKey = 'LLL:EXT:' . self::$currentExtKey . '/Resources/Private/Language/Backend.xlf:contentgroup.' . strtolower($group);

        if (!$flexform || strpos($flexform, 'EXT:') !== 0) {
            $flexform = 'EXT:' . self::$currentExtKey . '/Configuration/FlexForms/Content/' . $name . '.xml';
        }
        $absFlexformPath = GeneralUtility::getFileAbsFileName($flexform);
        if (!file_exists($absFlexformPath)) {
            $flexform = null;
        }

        self::$contentElements[$name] = [
            'title'         => ($title ? $title : (self::getLanguageService()->sL($titleLangKey) ? $titleLangKey : $name)),
            'icon'          => $icon,
            'group'         => $group,
            'description'   => ($description ? $description : (self::getLanguageService()->sL($descriptionLangKey) ? $descriptionLangKey : '')),
            'flexform'      => $flexform,
            'type'          => $type,
            'registeredExt' => self::$currentExtKey,
        ];

        if (!array_key_exists($group, self::$contentElementGroups)) {
            self::$contentElementGroups[$group] = [
                'title'           => ($groupTitle ? $groupTitle : (self::getLanguageService()->sL($groupTitleLangKey) ? $groupTitleLangKey : $group)),
                'contentElements' => [$name],
            ];
        } else {
            self::$contentElementGroups[$group]['contentElements'][] = $name;
        }

        if (!$disabled) {
            if (!in_array($name, self::$contentElementsEnabled)) {
                self::$contentElementsEnabled[] = $name;
            }
        }

        if ($configuration && count($configuration)) {
            self::$contentElementConfiguration[$name] = $configuration;
        }
    }

    /**
     * @param string $name Resources/Private/Templates/Page/$name.html
     * @param string $icon Iconpath for Backend
     * @param array $rows BackendLayout
     * @param string $title Alternative title label, Default: Resources/Private/Language/Backend.xlf:pagelayout.$name
     */
    static public function registerPageLayout($name, $icon, array $rows, $title = null)
    {
        $name = ucfirst($name);
        if (!in_array(self::$currentExtKey, self::$pageExtensionProviders)) {
            self::$pageExtensionProviders[] = self::$currentExtKey;
        }

        $colCount = 1;
        $rowConfig = [];
        foreach ($rows as $key => $row) {
            $colCountTest = 0;
            $rowConfig[$key] = [
                'columns' => [],
            ];
            foreach ($row as $columnKey => $column) {
                $rowConfig[$key]['columns'][$columnKey] = $column;
                if (array_key_exists('colspan', $column)) {
                    $colCountTest += (int)$column['colspan'];
                } else {
                    $colCountTest++;
                }
                if (array_key_exists('name', $column)) {
                    $langKeyColumn = 'LLL:EXT:' . self::$currentExtKey . '/Resources/Private/Language/Backend.xlf:pagelayout.' .
                        strtolower($name) . '.' . strtolower($column['name']);

                    if (self::getLanguageService()->sL($langKeyColumn)) {
                        $rowConfig[$key]['columns'][$columnKey]['name'] = $langKeyColumn;
                    }
                }
            }
            if ($colCountTest > $colCount) {
                $colCount = $colCountTest;
            }
        }

        $titleLangKey = 'LLL:EXT:' . self::$currentExtKey . '/Resources/Private/Language/Backend.xlf:pagelayout.' . strtolower($name);
        self::$pageLayouts[$name] = [
            'title'  => ($title ? $title : (self::getLanguageService()->sL($titleLangKey) ? $titleLangKey : $name)),
            'icon'   => $icon,
            'config' => [
                'rowCount' => count($rows),
                'colCount' => $colCount,
                'rows'     => $rowConfig,
            ],
        ];
    }

    /**
     * @return array
     */
    static public function getContentElementsEnabled()
    {
        return self::getData('enabled') ?: [];
    }

    /**
     * @return array
     */
    static public function getPageExtensionProviders()
    {
        if (!self::$pageExtensionProviders) {
            foreach ($GLOBALS['TYPO3_LOADED_EXT'] as $extKey => $extInfo) {
                $extPath = ExtensionManagementUtility::extPath($extKey);
                $settingsPath = $extPath . 'ext_base.php';

                if (file_exists($settingsPath)) {
                    self::$extensions[$extKey] = $settingsPath;
                }
            }

            foreach (self::$extensions as $extKey => $settingsPath) {
                self::$currentExtKey = $extKey;
                require_once($settingsPath);
            }
        }

        return self::$pageExtensionProviders;
    }

    /**
     * @param array $config
     * @param integer $depth
     * @return string
     */
    static protected function arrayToTypoScript($config, $depth = 0)
    {
        $typoscript = '';
        $assocKeys = array_keys($config) !== range(0, count($config) - 1);
        foreach ($config as $key => $value) {
            if (!$assocKeys) {
                $key++;
            }
            $typoscript .= str_pad('', $depth * 4, ' ') . $key;

            if (is_array($value)) {
                $typoscript .= ' {' . "\n" . self::arrayToTypoScript($value, $depth + 1) .
                    str_pad('', $depth * 4, ' ') . '}' . "\n";
            } else {
                if (is_bool($value)) {
                    $value = $value ? '1' : '0';
                }
                $typoscript .= ' = ' . $value . "\n";
            }
        }

        return $typoscript;
    }

    /**
     * @return LanguageService
     */
    static protected function getLanguageService()
    {
        if ($GLOBALS['LANG']) {
            return $GLOBALS['LANG'];
        } else {
            /** @var LanguageService $languageService */
            $languageService = GeneralUtility::makeInstance(LanguageService::class);

            return $languageService;
        }
    }
}