<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\TypoScript\Parser;

use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  TypoScript\Parser
 * @author      David Frerich <d.frerich@imia.de>
 */
class TypoScriptParser extends \TYPO3\CMS\Core\TypoScript\Parser\TypoScriptParser
{
    /**
     * Checks the input string (un-parsed TypoScript) for include-commands ("<INCLUDE_TYPOSCRIPT: ....")
     *
     * @param string $string Unparsed TypoScript
     * @param integer $cycle_counter Counter for detecting endless loops
     * @param boolean $returnFiles When set an array containing the resulting typoscript and all included files will get returned
     * @param string $path Path to typoscript (for relative includes)
     * @return string Complete TypoScript with includes added.
     * @static
     */
    static public function checkRelativeIncludeLines($string, $cycle_counter = 1, $returnFiles = false, $path = '')
    {
        $includedFiles = [];
        if ($cycle_counter > 100) {
            GeneralUtility::sysLog('It appears like TypoScript code is looping over itself. Check your templates for "&lt;INCLUDE_TYPOSCRIPT: ..." tags',
                    'Core', GeneralUtility::SYSLOG_SEVERITY_WARNING);
            if ($returnFiles) {
                return [
                        'typoscript' => '',
                        'files'      => $includedFiles,
                ];
            }

            return '
### ERROR: Recursion!
';
        }
        $splitStr = '<INCLUDE_TYPOSCRIPT:';
        if (strstr($string, $splitStr)) {
            $newString = '';
            // Adds line break char before/after
            $allParts = explode($splitStr, LF . $string . LF);
            foreach ($allParts as $c => $v) {
                // First goes through
                if (!$c) {
                    $newString .= $v;
                } elseif (preg_match('/\\r?\\n\\s*$/', $allParts[$c - 1])) {
                    $subparts = explode('>', $v, 2);
                    // There must be a line-break char after
                    if (preg_match('/^\\s*\\r?\\n/', $subparts[1])) {
                        // SO, the include was positively recognized:
                        $params = GeneralUtility::get_tag_attributes($subparts[0]);
                        if ($params['source']) {
                            $sourceParts = explode(':', $params['source'], 2);
                            switch (strtolower(trim($sourceParts[0]))) {
                                case 'file':
                                    $newString .= $splitStr . $v;
                                    break;
                                case 'file_relative':
                                case 'rfile':
                                    $absPath = GeneralUtility::getFileAbsFileName($path);
                                    if (!$absPath && is_dir($path)) {
                                        $absPath = $path;
                                    }

                                    if ($absPath) {
                                        $filename = $absPath . trim($sourceParts[1]);

                                        if (realpath($filename)) {
                                            $filename = realpath($filename);
                                        }
                                    } else {
                                        $filename = '';
                                    }

                                    // Must exist and must not contain '..' and must be relative
                                    if (strcmp($filename, '')) {
                                        // Check for allowed files
                                        if (GeneralUtility::verifyFilenameAgainstDenyPattern($filename)) {
                                            if (@is_file($filename)) {
                                                // Check for includes in included text
                                                $filePath = str_replace(PATH_site, '', dirname($filename) . '/');

                                                $includedFiles[] = $filename;
                                                $included_text = self::checkRelativeIncludeLines(GeneralUtility::getUrl($filename), $cycle_counter + 1, $returnFiles, $filePath);

                                                // If the method also has to return all included files, merge currently included
                                                // files with files included by recursively calling itself
                                                if ($returnFiles && is_array($included_text)) {
                                                    $includedFiles = array_merge($includedFiles, $included_text['files']);
                                                    $included_text = $included_text['typoscript'];
                                                }
                                                $newString .= $included_text . LF;
                                            } else {
                                                $newString .= '
    ### ERROR: File "' . $filename . '" was not was not found.

    ';
                                                GeneralUtility::sysLog('File "' . $filename . '" was not found.', 'Core', GeneralUtility::SYSLOG_SEVERITY_WARNING);
                                            }
                                        } else {
                                            $newString .= '
    ### ERROR: File "' . $filename . '" was not included since it is not allowed due to fileDenyPattern

    ';
                                            GeneralUtility::sysLog('File "' . $filename . '" was not included since it is not allowed due to fileDenyPattern',
                                                    'Core', GeneralUtility::SYSLOG_SEVERITY_WARNING);
                                        }
                                    }
                                    break;
                            }
                        }
                        $newString .= $subparts[1];
                    } else {
                        $newString .= $splitStr . $v;
                    }
                } else {
                    $newString .= $splitStr . $v;
                }
            }
            // Not the first/last linebreak char.
            $string = substr($newString, 1, -1);
        }
        // When all included files should get returned, simply return an compound array containing
        // the TypoScript with all "includes" processed and the files which got included
        if ($returnFiles) {
            return [
                    'typoscript' => $string,
                    'files'      => $includedFiles,
            ];
        }

        return $string;
    }
}