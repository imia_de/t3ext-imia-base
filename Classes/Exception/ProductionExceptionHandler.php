<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2024+ IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

declare(strict_types=1);

namespace IMIA\ImiaBase\Exception;

use TYPO3\CMS\Frontend\ContentObject\AbstractContentObject;
use TYPO3\CMS\Frontend\ContentObject\Exception\ProductionExceptionHandler as TYPO3ProductionExceptionHandler;

class ProductionExceptionHandler extends TYPO3ProductionExceptionHandler
{

    public function handle(\Exception $exception, AbstractContentObject $contentObject = null, $contentObjectConfiguration = [])
    {
        $errorOutput = parent::handle($exception, $contentObject, $contentObjectConfiguration);

        $errorMail = \getenv('ERROR_MAIL') ?: ($_ENV['ERROR_MAIL'] ?: ($_SERVER['ERROR_MAIL'] ?: $_SERVER['REDIRECT_ERROR_MAIL']));

        if ($errorMail) {
            $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 'https://' : 'http://';

            $errorMessage = 'URL: ' . $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . "\n";
            $errorMessage .= 'Output: ' . $errorOutput . "\n\n";
            $errorMessage .= @sprintf('Error in file "%s" at line %d: %s' . "\n", $exception->getFile(), $exception->getLine(), $exception->getMessage());

            \file_put_contents(PATH_site . 'typo3temp/logs/error.log', \date('d.m.Y H:i:s') . ' - ' . $errorMessage . "\n", FILE_APPEND | LOCK_EX);
            $errorMsgFile = PATH_site . 'typo3temp/var/Cache/Code/cache_core/error_' . \md5($errorMessage) . '.lock';

            if (!\file_exists($errorMsgFile) || \filemtime($errorMsgFile) < (\time() - 60 * 60)) {
                \file_put_contents($errorMsgFile, $errorMessage, LOCK_EX);

                /**
                if (
                    \is_int(\strpos($errorMessage, 'Allowed memory size')) ||
                    \is_int(\strpos($errorMessage, 'The given string was not appended with a valid HMAC.')) ||
                    \is_int(\strpos($errorMessage, 'The HMAC of the form could not be validated')) ||
                    \is_int(\strpos($errorMessage, 'The current host header value does not match the configured trusted hosts pattern')) ||
                    \is_int(\strpos($errorMessage, 'No powermail parameters are given')) ||
                    \is_int(\strpos($errorMessage, '(controller "Form") is not allowed by this plugin')) ||
                    \is_int(\strpos($errorMessage, 'A hashed string must contain at least')) ||
                    \is_int(\strpos($errorMessage, 'is not allowed by plugin'))
                ) {
                    return $errorOutput;
                }
                */

                if (\getenv('StagingEnv') === 'active') {
                    // We are on Staging: Don't send any mails - because of pen testing
                    \error_log($errorMessage); // ~> goes to ./logs/
                } else {
                    // We are on Production: Send the mail
                    $domain = $_SERVER['HTTP_HOST'];
                    \mail(
                        $errorMail,
                        'TYPO3 Content Error' . ($domain ? ' | ' . $domain : ''),
                        $errorMessage,
                        'From: dev@imia.de' . "\r\n" .
                        'Reply-To: dev@imia.de' . "\r\n" .
                        'X-Mailer: PHP/' . \phpversion()
                    );
                }
            }
        }

        return $errorOutput;
    }
}
