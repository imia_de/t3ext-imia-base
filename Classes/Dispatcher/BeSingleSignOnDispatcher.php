<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/** @var \TYPO3\CMS\Core\Database\DatabaseConnection $t3Db */
$t3Db = $GLOBALS['TYPO3_DB'];

$sesId = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('ses');
$beSession = $t3Db->exec_SELECTgetSingleRow('*', 'be_sessions',
    'ses_id = ' . $t3Db->fullQuoteStr($sesId, 'be_session')
);

if ($beSession) {
    if ($beSession['ses_iplock'] && $beSession['ses_iplock'] != '*') {
        if ($_SERVER['HTTP_X_FORWARDED_FOR'] != $beSession['ses_iplock'] && $_SERVER['REMOTE_ADDR'] != $beSession['ses_iplock']) {
            $dontBlock = false;
            if ($_SERVER['HTTP_REFERER']) {
                $urlInfo = parse_url($_SERVER['HTTP_REFERER']);

                if ($urlInfo && $urlInfo['host']) {
                    if ((gethostbyname($urlInfo['host']) == $beSession['ses_iplock']) || $beSession['ses_iplock'] == '[DISABLED]') {

                        if($beSession['ses_iplock'] != '[DISABLED]') {
                            $sesId = substr(sha1($beSession['ses_id'] . '_' . $_SERVER['REMOTE_ADDR']), 0, 32);
                            $beSession['ses_id'] = $sesId;
                            $beSession['ses_iplock'] = $_SERVER['REMOTE_ADDR'];
                        }

                        $existingBeSession = $t3Db->exec_SELECTgetSingleRow('*', 'be_sessions',
                            'ses_id = ' . $t3Db->fullQuoteStr($sesId, 'be_session'));

                        if (!$existingBeSession) {
                            $t3Db->exec_INSERTquery('be_sessions', $beSession);
                        }
                        $dontBlock = true;
                    }
                }
            }

            if (!$dontBlock) {
                header('HTTP/1.1 401 Unauthorized');
                exit;
            }
        }
    }

    $settings = $GLOBALS['TYPO3_CONF_VARS']['SYS'];
    $cookieDomain = getCookieDomain();
    $cookiePath = $cookieDomain ? '/' : \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('TYPO3_SITE_PATH');
    $cookieExpire = 0;
    $cookieSecure = (bool)$settings['cookieSecure'] && \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('TYPO3_SSL');
    $cookieHttpOnly = (bool)$settings['cookieHttpOnly'];

    if ((int)$settings['cookieSecure'] !== 1 || \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('TYPO3_SSL')) {
        setcookie('be_typo_user', $sesId, $cookieExpire, $cookiePath.'; SameSite=None; Secure', $cookieDomain, $cookieSecure, $cookieHttpOnly);

        if ($_REQUEST['redirect']) {
            \TYPO3\CMS\Core\Utility\HttpUtility::redirect($_REQUEST['redirect']);
        } else {
            header('Content-type: text/javascript');
            echo 'var BeSingleSignOn = true';
        }
    }
} else {
    header('HTTP/1.1 401 Unauthorized');
}
exit;

/**
 * @return string
 */
function getCookieDomain()
{
    $result = '';
    $cookieDomain = $GLOBALS['TYPO3_CONF_VARS']['SYS']['cookieDomain'];

    if (!empty($GLOBALS['TYPO3_CONF_VARS']['BE']['cookieDomain'])) {
        $cookieDomain = $GLOBALS['TYPO3_CONF_VARS']['BE']['cookieDomain'];
    }
    if ($cookieDomain) {
        if ($cookieDomain[0] == '/') {
            $match = [];
            $matchCnt = @preg_match($cookieDomain, \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('TYPO3_HOST_ONLY'), $match);
            if ($matchCnt === false) {
                \TYPO3\CMS\Core\Utility\GeneralUtility::sysLog('The regular expression for the cookie domain (' . $cookieDomain . ') contains errors. The session is not shared across sub-domains.', 'core', GeneralUtility::SYSLOG_SEVERITY_ERROR);
            } elseif ($matchCnt) {
                $result = $match[0];
            }
        } else {
            $result = $cookieDomain;
        }
    }

    return $result;
}