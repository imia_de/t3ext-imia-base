<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Traits;

use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  Trait
 * @author      David Frerich <d.frerich@imia.de>
 */
trait HookTrait
{
    /**
     * @var array
     */
    static protected $hookObjectsStatic;

    /**
     * @var array
     */
    protected $hookObjects;

    /**
     * @param string $method
     * @param array $args
     */
    static protected function callHookStatic($method, $args)
    {
        foreach (self::getHookObjectsStatic() as $hookObject) {
            if (method_exists($hookObject, $method)) {
                call_user_func_array([$hookObject, $method], $args);
            }
        }
    }

    /**
     * @return array
     */
    static protected function getHookObjectsStatic()
    {
        if (!self::$hookObjectsStatic) {
            self::$hookObjectsStatic = [];

            $className = self::class;
            $classNameWithoutVendor = substr($className, strpos($className, '\\') + 1);
            $extensionName = substr($classNameWithoutVendor, 0, strpos($classNameWithoutVendor, '\\'));
            $extKey = 'tx_' . strtolower($extensionName);

            $className = self::class;
            $className = substr($className, strrpos($className, '\\') + 1);
            $controllerName = str_replace('CommandController', '', str_replace('Controller', '', $className));

            if (is_array($GLOBALS['TYPO3_CONF_VARS']['EXTCONF'][$extKey][$controllerName])) {
                foreach ($GLOBALS['TYPO3_CONF_VARS']['EXTCONF'][$extKey][$controllerName] as $_classRef) {
                    self::$hookObjectsStatic[] = GeneralUtility::getUserObj($_classRef);
                }
            }
        }

        return self::$hookObjectsStatic;
    }

    /**
     * @param string $method
     * @param array $args
     */
    protected function callHook($method, $args)
    {
        foreach ($this->getHookObjects() as $hookObject) {
            if (method_exists($hookObject, $method)) {
                call_user_func_array([$hookObject, $method], $args);
            }
        }
    }

    /**
     * @return array
     */
    protected function getHookObjects()
    {
        if (!$this->hookObjects) {
            $this->hookObjects = [];

            $extKey = null;
            if (isset($this->extensionName) && $this->extensionName) {
                $extKey = 'tx_' . strtolower($this->extensionName);
            } else {
                $className = get_class($this);
                $classNameWithoutVendor = substr($className, strpos($className, '\\') + 1);
                $extensionName = substr($classNameWithoutVendor, 0, strpos($classNameWithoutVendor, '\\'));
                $extKey = 'tx_' . strtolower($extensionName);
            }

            $controllerName = null;
            if (isset($this->request)) {
                $controllerName = $this->request->getControllerName();
            }
            if (!$controllerName && isset($this->controllerName)) {
                $controllerName = $this->controllerName;
            }
            if (!$controllerName) {
                $className = get_class($this);
                $className = substr($className, strrpos($className, '\\') + 1);
                $controllerName = str_replace('CommandController', '', str_replace('Controller', '', $className));
            }

            if (is_array($GLOBALS['TYPO3_CONF_VARS']['EXTCONF'][$extKey][$controllerName])) {
                foreach ($GLOBALS['TYPO3_CONF_VARS']['EXTCONF'][$extKey][$controllerName] as $_classRef) {
                    $this->hookObjects[] = GeneralUtility::getUserObj($_classRef);
                }
            }
        }

        return $this->hookObjects;
    }
}
