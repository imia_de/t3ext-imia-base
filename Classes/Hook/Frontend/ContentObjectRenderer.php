<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Hook\Frontend;

use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectGetDataHookInterface;

/**
 * @package     imia_base
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class ContentObjectRenderer implements ContentObjectGetDataHookInterface
{
    /**
     * @param array $params
     * @param \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer $parentObject
     */
    public function typoLinkPostProc(&$params, \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer &$parentObject)
    {
        if ($params['finalTagParts']['TYPE'] == 'page') {
            $page = $GLOBALS['TSFE']->sys_page->getPage($params['conf']['parameter'], true);

            if (in_array($page['doktype'], [3, 103, 113, 123, 133, 143, 153, 163, 173, 183]) && $page['url']) { // external url
                $url = $page['url'];

                switch ($page['urltype']) {
                    case 1:
                        $url = 'http://' . $url;
                        break;
                    case 2:
                        $url = 'ftp://' . $url;
                        break;
                    case 3:
                        $url = 'mailto://' . $url;
                        break;
                    case 4:
                        $url = 'https://' . $url;
                        break;
                    default:
                        break;
                }

                $parentObject->lastTypoLinkUrl = $url;
                $params['finalTagParts']['url'] = $url;
                $params['finalTagParts']['TYPE'] = 'url';

                $title = $params['conf']['title'];
                if ($params['conf']['title.']) {
                    $title = $parentObject->stdWrap($title, $params['conf']['title.']);
                }

                $params['finalTag'] = '<a href="' . htmlspecialchars($params['finalTagParts']['url']) . '"' .
                    ((string)$title !== '' ? ' title="' . htmlspecialchars($title) . '"' : '')
                    . $params['finalTagParts']['targetParams']
                    //. ($linkClass ? ' class="' . $linkClass . '"' : '')
                    . $params['finalTagParts']['aTagParams']
                    . '>';
            }
        }
    }

    /**
     * Extends the getData()-Method of \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer to process more/other commands
     *
     * @param string $getDataString Full content of getData-request e.g. "TSFE:id // field:title // field:uid
     * @param array $fields Current field-array
     * @param string $sectionValue Currently examined section value of the getData request e.g. "field:title
     * @param string $returnValue Current returnValue that was processed so far by getData
     * @param \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer $parentObject Parent content object
     * @return string Get data result
     */
    public function getDataExtension($getDataString, array $fields, $sectionValue, $returnValue, \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer &$parentObject)
    {
        $parts = explode(':', $sectionValue, 2);
        $type = strtolower(trim($parts[0]));
        $typesWithOutParameters = ['level', 'date', 'current'];
        $key = trim($parts[1]);

        if (($key != '') || in_array($type, $typesWithOutParameters)) {
            switch ($type) {
                case 'db':
                    $db_rec = null;
                    $selectParts = GeneralUtility::trimExplode(':', $key);
                    if ($selectParts[1]) {
                        if (strpos($selectParts[0], '.') === false) {
                            $db_rec = $GLOBALS['TSFE']->sys_page->getRawRecord($selectParts[0], $selectParts[1]);
                            $db_rec = $GLOBALS['TSFE']->sys_page->getRecordOverlay(
                                $selectParts[0],
                                $db_rec,
                                $GLOBALS['TSFE']->sys_language_content,
                                $GLOBALS['TSFE']->sys_language_contentOL);
                        } else {
                            $tableParts = GeneralUtility::trimExplode('.', $selectParts[0]);
                            $db_rec = $this->getDb()->exec_SELECTgetSingleRow($selectParts[2], $tableParts[0],
                                $tableParts[1] . ' = ' . $selectParts[1] . $GLOBALS['TSFE']->sys_page->enableFields($tableParts[0]));
                        }
                    }

                    if ($db_rec && is_array($db_rec) && $selectParts[2]) {
                        $returnValue = $db_rec[$selectParts[2]];
                    }
                    break;
            }
        }

        return $returnValue;
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}
