<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Hook\Frontend;

use IMIA\Assetic\AssetBuilder;
use Assetic\AssetWriter;
use IMIA\ImiaBase\Registry\FluidStyledContentRegistry;
use IMIA\ImiaBase\Utility\AsseticParser;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class TypoScriptFrontendController implements SingletonInterface
{
    /**
     * @var string
     */
    protected $appendToBody = '';

    /**
     * @param array $params
     * @param \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController $tsfe
     */
    public function constructPostProc($params, &$tsfe)
    {
        if (($_SERVER['HTTP_X_FORWARDED_FOR'] ?: $_SERVER['REMOTE_ADDR']) == $_SERVER['SERVER_ADDR']) {
            $tsfe->forceTemplateParsing = true;
        }
    }

    /**
     * @param array $params
     * @param \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController $tsfe
     */
    public function configArrayPostProc($params, &$tsfe)
    {
        $extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['imia_base']);

        if (!$extConf['disableAssetic']) {
            /** @var AsseticParser $asseticParser */
            $asseticParser = GeneralUtility::makeInstance(AsseticParser::class);
            try {
                if (TYPO3_MODE == 'FE' && $GLOBALS['TSFE']) {
                    if (is_array($GLOBALS['TSFE']->tmpl->setup['config.']) && array_key_exists('assetic.', $GLOBALS['TSFE']->tmpl->setup['config.'])) {
                        $asseticParser->parse($GLOBALS['TSFE']->tmpl->setup['config.']); // global
                    }
                    if (is_array($GLOBALS['TSFE']->pSetup) && array_key_exists('assetic.', $GLOBALS['TSFE']->pSetup)) {
                        $asseticParser->parse($GLOBALS['TSFE']->pSetup);
                    }
                }
            } catch (\Exception $e) {
                echo('<pre>' . $e->getMessage());
                exit;
            }
        }

        $this->generateHTMLTag($tsfe);

        if (isset($asseticParser)) {
            $this->appendToBody = $asseticParser->getAppendToBody();
        }
    }

    /**
     * @param array $params
     * @param \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController $tsfe
     */
    public function isOutputting($params, &$tsfe)
    {
        if (is_array($tsfe->config['config']) && array_key_exists('browserCachePeriod', $tsfe->config['config'])) {
            if ($tsfe->config['config']['browserCachePeriod'] != '') {
                $browserCachePeriod = (int)$tsfe->config['config']['browserCachePeriod'];
                $browserExpires = $GLOBALS['EXEC_TIME'] + $browserCachePeriod;

                if ($tsfe->cacheExpires > $browserExpires) {
                    $tsfe->cacheExpires = $browserExpires; //override default typo3 cache expires
                }
            }
        }
    }

    /**
     * @param \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController $tsfe
     */
    protected function generateHTMLTag(&$tsfe)
    {
        $theCharset = $tsfe->metaCharset;
        $htmlTagAttributes = [];
        $htmlLang = $tsfe->config['config']['htmlTag_langKey'] ?: ($tsfe->sys_language_isocode ?: 'en');
        // Set content direction: (More info: http://www.tau.ac.il/~danon/Hebrew/HTML_and_Hebrew.html)
        if ($tsfe->config['config']['htmlTag_dir']) {
            $htmlTagAttributes['dir'] = htmlspecialchars($tsfe->config['config']['htmlTag_dir']);
        }
        // Setting document type:
        $docTypeParts = [];
        $xmlDocument = true;
        // Part 1: XML prologue
        switch ((string)$tsfe->config['config']['xmlprologue']) {
            case 'none':
                $xmlDocument = false;
                break;
            case 'xml_10':
                $docTypeParts[] = '<?xml version="1.0" encoding="' . $theCharset . '"?>';
                break;
            case 'xml_11':
                $docTypeParts[] = '<?xml version="1.1" encoding="' . $theCharset . '"?>';
                break;
            case '':
                if ($tsfe->xhtmlVersion) {
                    $docTypeParts[] = '<?xml version="1.0" encoding="' . $theCharset . '"?>';
                } else {
                    $xmlDocument = false;
                }
                break;
            default:
                $docTypeParts[] = $tsfe->config['config']['xmlprologue'];
        }
        // Part 2: DTD
        $doctype = $tsfe->config['config']['doctype'];
        if ($doctype) {
            switch ($doctype) {
                case 'xhtml_trans':
                    $docTypeParts[] = '<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
                    break;
                case 'xhtml_strict':
                    $docTypeParts[] = '<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
                    break;
                case 'xhtml_frames':
                    $docTypeParts[] = '<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">';
                    break;
                case 'xhtml_basic':
                    $docTypeParts[] = '<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML Basic 1.0//EN"
    "http://www.w3.org/TR/xhtml-basic/xhtml-basic10.dtd">';
                    break;
                case 'xhtml_11':
                    $docTypeParts[] = '<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.1//EN"
    "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">';
                    break;
                case 'xhtml_2':
                    $docTypeParts[] = '<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 2.0//EN"
    "http://www.w3.org/TR/xhtml2/DTD/xhtml2.dtd">';
                    break;
                case 'xhtml+rdfa_10':
                    $docTypeParts[] = '<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
    "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">';
                    break;
                case 'html5':
                    $docTypeParts[] = '<!DOCTYPE html>';
                    break;
                case 'none':
                    break;
                default:
                    $docTypeParts[] = $doctype;
            }
        } else {
            $docTypeParts[] = '<!DOCTYPE html>';
        }
        if ($tsfe->xhtmlVersion) {
            $htmlTagAttributes['xml:lang'] = $htmlLang;
        }
        if ($tsfe->xhtmlVersion < 110 || $doctype === 'html5') {
            $htmlTagAttributes['lang'] = $htmlLang;
        }
        if ($tsfe->xhtmlVersion || $doctype === 'html5' && $xmlDocument) {
            // We add this to HTML5 to achieve a slightly better backwards compatibility
            $htmlTagAttributes['xmlns'] = 'http://www.w3.org/1999/xhtml';
            if (is_array($tsfe->config['config']['namespaces.'])) {
                foreach ($tsfe->config['config']['namespaces.'] as $prefix => $uri) {
                    // $uri gets htmlspecialchared later
                    $htmlTagAttributes['xmlns:' . htmlspecialchars($prefix)] = $uri;
                }
            }
        }

        if ($tsfe->config['config']['htmlTag_setParams'] !== 'none') {
            $_attr = $tsfe->config['config']['htmlTag_setParams'] ? $tsfe->config['config']['htmlTag_setParams'] : GeneralUtility::implodeAttributes($htmlTagAttributes);
        } else {
            $_attr = '';
        }

        $tsfe->config['config']['htmlTag_stdWrap.']['override'] =
            '<!--[if lt IE 7]>      <html prefix="og: http://ogp.me/ns#" class="no-js lt-ie9 lt-ie8 lt-ie7" ' . $_attr . '> <![endif]-->' . "\n" .
            '<!--[if IE 7]>         <html prefix="og: http://ogp.me/ns#" class="no-js lt-ie9 lt-ie8" ' . $_attr . '> <![endif]-->' . "\n" .
            '<!--[if IE 8]>         <html prefix="og: http://ogp.me/ns#" class="no-js lt-ie9" ' . $_attr . '> <![endif]-->' . "\n" .
            '<!--[if gt IE 8]><!--> <html prefix="og: http://ogp.me/ns#" class="no-js" ' . $_attr . '> <!--<![endif]-->';
    }

    /**
     * @param array $params
     * @param \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController $tsfe
     */
    public function determineIdPostProc($params, &$tsfe)
    {
        $extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['imia_base']);
        $disableFrontendBackendUser = (bool)$extConf['disableFrontendBackendUser'];

        if (($disableFrontendBackendUser || $GLOBALS['disableFrontend-beUserLogin']) && $tsfe->beUserLogin) {
            $hidden = $tsfe->page['hidden'];
            if ($tsfe->page['starttime'] && $tsfe->page['starttime'] < time()) {
                $hidden = true;
            }
            if ($tsfe->page['endtime'] && $tsfe->page['endtime'] > time()) {
                $hidden = true;
            }

            if (!$hidden) {
                $tsfe->beUserLogin = false;
            }
        }

        FluidStyledContentRegistry::loadTypoScript();
    }

    /**
     * @param array $params
     * @param \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController $tsfe
     */
    public function checkAlternativeIdMethods($params, &$tsfe)
    {
        if (!$tsfe->isBackendUserLoggedIn() && !ExtensionManagementUtility::isLoaded('imia_base_ext')) {
            ExtensionManagementUtility::loadExtTables(true);
            if (is_array($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['GLOBAL']['extTablesInclusion-PostProcessing'])) {
                foreach ($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['GLOBAL']['extTablesInclusion-PostProcessing'] as $classReference) {
                    /** @var $hookObject \TYPO3\CMS\Core\Database\TableConfigurationPostProcessingHookInterface */
                    $hookObject = GeneralUtility::getUserObj($classReference);
                    if (!$hookObject instanceof \TYPO3\CMS\Core\Database\TableConfigurationPostProcessingHookInterface) {
                        throw new \UnexpectedValueException(
                            '$hookObject "' . $classReference . '" must implement interface TYPO3\\CMS\\Core\\Database\\TableConfigurationPostProcessingHookInterface',
                            1320585902
                        );
                    }
                    $hookObject->processData();
                }
            }
        }
    }

    /**
     * @param array $config
     * @param \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController $tsfe
     */
    public function contentPostProc($config, &$tsfe)
    {
        $tsfe->content = str_replace('<!--NO_CONTENT_CACHE-->', '', $tsfe->content);
        if ($this->appendToBody) {
            $needle = '</body>';
            $pos = strpos($tsfe->content, $needle);

            if ($pos !== false) {
                $tsfe->content = substr_replace($tsfe->content, $this->appendToBody . $needle, $pos, strlen($needle));
            }
        }

        if ($tsfe->config && is_array($tsfe->config) && array_key_exists('config', $tsfe->config) && isset($tsfe->config['config']['cdnURL'])) {
            $cdnURL = $tsfe->config['config']['cdnURL'];

            $hostSchema = 'http' . (GeneralUtility::getIndpEnv('TYPO3_SSL') ? 's' : '') .
                '://' .GeneralUtility::getIndpEnv('HTTP_HOST') . '/';

            $GLOBALS['TSFE']->content = preg_replace(
                '/(<(link rel="stylesheet"|link rel="shortcut|link rel="icon|link rel="apple-touch-icon|link rel="mask-icon|img|script)[^>]*(src|href)[ ]*=[ ]*")' . preg_quote($hostSchema, '/') . '/ism',
                '$1',
                $GLOBALS['TSFE']->content
            );
            $GLOBALS['TSFE']->content = preg_replace(
                '/(<(link rel="stylesheet"|link rel="shortcut|link rel="icon|link rel="apple-touch-icon|link rel="mask-icon|img|script)[^>]*(src|href)[ ]*=[ ]*")(\/)?(typo3temp|fileadmin|uploads|typo3conf|file)/ism',
                '$1' . $cdnURL . '$4$5',
                $GLOBALS['TSFE']->content
            );

            if (preg_match_all('/<img[^>]*srcset[ ]*=[ ]*"([^"]+)/ism', $GLOBALS['TSFE']->content, $matches)) {
                $replacesDone = [];
                foreach ($matches[1] as $key => $val) {
                    $srcset = array_map('trim', explode(',', $val));
                    $newSrcset = [];
                    foreach ($srcset as $srcsetVal) {
                        $srcsetVal = str_ireplace($hostSchema, '', $srcsetVal);

                        if (stripos($srcsetVal, 'typo3temp') === 0
                            || stripos($srcsetVal, 'typo3conf') === 0
                            || stripos($srcsetVal, 'fileadmin') === 0
                            || stripos($srcsetVal, 'uploads') === 0
                            || stripos($srcsetVal, 'file') === 0) {
                            $newSrcset[] = $cdnURL . $srcsetVal;
                        } else {
                            $newSrcset[] = $srcsetVal;
                        }
                    }

                    $newVal = implode(',', $newSrcset);
                    if (!in_array($newVal, $replacesDone)) {
                        $replacesDone[] = $newVal;
                        $GLOBALS['TSFE']->content = str_replace($val, $newVal, $GLOBALS['TSFE']->content);
                    }
                }
            }
        }
    }
}