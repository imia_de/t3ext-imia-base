<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Hook\Backend;

use TYPO3\CMS\Backend\Wizard\NewContentElementWizardHookInterface;

/**
 * @package     imia_base
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class NewContentElementWizard implements NewContentElementWizardHookInterface
{
    /**
     * @inheritdoc
     */
    public function manipulateWizardItems(&$wizardItems, &$parentObject)
    {
        foreach ($wizardItems as &$wizardItem) {
            if (isset($wizardItem['tt_content_defValues']) && is_array($wizardItem['tt_content_defValues'])) {
                foreach ($wizardItem['tt_content_defValues'] as $column => $defValue) {
                    if (is_array($defValue)) {
                        if (isset($GLOBALS['TCA']['tt_content']['columns'][substr($column, 0, -1)]['config']['type'])) {
                            $type = $GLOBALS['TCA']['tt_content']['columns'][substr($column, 0, -1)]['config']['type'];

                            if ($type == 'flex') {
                                foreach ($defValue as $key => $value) {
                                    if (is_array($value)) {
                                        foreach ($value as $subkey => $subvalue) {
                                            $wizardItem['params'] .= '&defVals[tt_content][' . $column . $key . $subkey . ']=' . rawurlencode($subvalue);
                                        }
                                    }
                                }
                            } elseif ($type == 'inline') {
                                foreach ($defValue as $key => $value) {
                                    if (is_array($value)) {
                                        foreach ($value as $subkey => $subvalue) {
                                            $wizardItem['params'] .= '&defVals[tt_content][' . $column . $key . $subkey . ']=' . rawurlencode($subvalue);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}