<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Hook\Backend;

use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class BackendController
{
    /**
     * @param array $conf
     * @param \TYPO3\CMS\Backend\Controller\BackendController $backendController
     */
    public function renderPreProcess($conf, $backendController)
    {
        $webRoot = str_replace(GeneralUtility::getIndpEnv('TYPO3_DOCUMENT_ROOT'), '', PATH_site);
        $resourcePath = $webRoot . ExtensionManagementUtility::siteRelPath('imia_base') . 'Resources/Public/';
        $backendController->addCssFile('imiaBaseTypo3.css', $resourcePath . 'Stylesheets/typo3.css');
    }

    /**
     * @param array $conf
     * @param \TYPO3\CMS\Backend\Controller\BackendController $backendController
     */
    public function renderPostProcess(&$conf, $backendController)
    {
        $extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['imia_base']);
        $singleSignOnEnabled = (bool)$extConf['singleSignOn'];
        if ($singleSignOnEnabled) {
            $addHtml = '';
            if ($_COOKIE['be_typo_user']) {
                $cachePath = PATH_site . 'typo3temp/var/Cache/Code/cache_core/besinglesignon_' . $_COOKIE['be_typo_user'] . '.temp';
                if (isset($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['rsaauth'])) {
                    $rsaAuthConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['rsaauth']);
                    if (isset($rsaAuthConf['temporaryDirectory']) && $rsaAuthConf['temporaryDirectory']) {
                        $cachePath = $rsaAuthConf['temporaryDirectory'] . '/' . 'besinglesignon_' . $_COOKIE['be_typo_user'] . '.temp';
                    }
                }

                if (file_exists($cachePath) && 1==2) {
                    $addHtml = file_get_contents($cachePath);
                } else {
                    $domains = [];
                    $result = $this->getDb()->exec_SELECTquery('domainName', 'sys_domain', 'hidden = 0 AND redirectTo = "" AND singlesignon_disabled != 1');
                    while ($row = $this->getDb()->sql_fetch_assoc($result)) {
                        $domains[] = 'http' . ($_SERVER['HTTPS'] == 'on' ? 's' : '') . '://' . $row['domainName'];
                    }
                    $addHtml .= '<div id="imiaSingleSignOn" data-domains="' . implode(' | ', $domains) . '" data-url="/?eID=beSingleSignOn&ses=' . $_COOKIE['be_typo_user'] . '">
                        <script type="text/javascript">
                            (function($){
                                setTimeout(function(){
                                    var singleSignOn = $("#imiaSingleSignOn")
                                    var url = singleSignOn.data("url");
                                    $.each(singleSignOn.data("domains").split(" | "), function(key, data) {
                                        $.getScript(data + url);
                                    });
                                }, 1500);
                            })(TYPO3.jQuery);                      
                        </script>
                    </div>';

                    file_put_contents($cachePath, $addHtml);
                }
            }

            $conf['content'] = str_replace('</body>', $addHtml . "\n" . '</body>', $conf['content']);
        }
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}