<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Hook\Backend;

use IMIA\ImiaBase\Registry\FluidStyledContentRegistry;
use IMIA\ImiaBase\Traits\HookTrait;
use IMIA\ImiaBase\Utility\BackendTypoScript;
use IMIA\ImiaBase\Utility\ProcessingHelper;
use IMIA\ImiaBase\Xclass\Core\Imaging\IconFactory;
use TYPO3\CMS\Backend\Controller\PageLayoutController;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Backend\View\PageLayoutView;
use TYPO3\CMS\Backend\View\PageLayoutViewDrawItemHookInterface;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Database\Query\Expression\ExpressionBuilder;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
use TYPO3\CMS\Core\Type\Bitmask\Permission;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\StringUtility;
use TYPO3\CMS\Core\Versioning\VersionState;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;
use TYPO3\CMS\Frontend\Page\PageRepository;
use TYPO3\CMS\Lang\LanguageService;

/**
 * @package     imia_base
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class PageLayoutViewDrawItemHook implements PageLayoutViewDrawItemHookInterface
{
    use HookTrait;
    
    /**
     * @param PageLayoutView $parentObject
     * @param boolean $drawItem
     * @param string $headerContent
     * @param string $itemContent
     * @param array $row
     * @throws \TYPO3\CMS\Core\Configuration\FlexForm\Exception\InvalidIdentifierException
     * @throws \TYPO3\CMS\Core\Configuration\FlexForm\Exception\InvalidParentRowException
     * @throws \TYPO3\CMS\Core\Configuration\FlexForm\Exception\InvalidParentRowLoopException
     * @throws \TYPO3\CMS\Core\Configuration\FlexForm\Exception\InvalidParentRowRootException
     * @throws \TYPO3\CMS\Core\Configuration\FlexForm\Exception\InvalidPointerFieldValueException
     * @throws \TYPO3\CMS\Core\Configuration\FlexForm\Exception\InvalidTcaException
     */
    public function preProcess(PageLayoutView &$parentObject, &$drawItem, &$headerContent, &$itemContent, array &$row)
    {
        static $tsConfig;
        if (!$tsConfig) {
            $tsConfig = BackendUtility::getPagesTSconfig($parentObject->id);
        }

        /** @var \IMIA\ImiaBase\Xclass\Backend\View\PageLayoutView $parentObject */
        if ($row['header']) {
            $infoArr = [];
            $parentObject->getProcessedValue('tt_content', 'header_position,header_layout,header_link', $row, $infoArr);

            $headerNote = '[' . $this->renderTCAItemLabel('tt_content', 'header_layout', $row['header_layout'], $tsConfig) . '] ';
            $headerNote2 = '';

            $headerInfos = [];
            if ($row['header_position']) {
                $headerInfos[] = $this->renderTCAItemLabel('tt_content', 'header_position', $row['header_position'], $tsConfig);
            }
            if (isset($tsConfig['mod.']['web_layout.']['content.']['headerinfo'])) {
                foreach (explode(',', $tsConfig['mod.']['web_layout.']['content.']['headerinfo']) as $column) {
                    if ($row[$column]) {
                        $headerInfos[] = $this->renderTCAItemLabel('tt_content', $column, $row[$column], $tsConfig);
                    }
                }
            }
            if (count($headerInfos)) {
                $headerNote2 = '<br><em style="font-weight: normal">' . implode(', ', $headerInfos) . '</em>';
            }

            $headerContent =
                $parentObject->linkEditContent(
                    ($row['date']
                        ? htmlspecialchars($parentObject->itemLabels['date'] . ' ' . BackendUtility::date($row['date'])) . '<br />'
                        : '') .
                    '<strong>' . $headerNote .
                    $parentObject->renderText($row['header']) . $headerNote2 .
                    '</strong>', $row) . '<br />';
        }

        if ($row['subheader']) {
            if (isset($tsConfig['TCEFORM.']['tt_content.']['subheader.']['label'])) {
                $headerNote = $tsConfig['TCEFORM.']['tt_content.']['subheader.']['label'] . ': ';
            } else {
                $headerNote = '[' . $this->renderTCAItemLabel('tt_content', 'header_layout', $row['header_layout'] + 1, $tsConfig) . '] ';
            }
            $headerNote .= $parentObject->renderText($row['subheader']);

            $itemContent .=
                $parentObject->linkEditContent($headerNote, $row) .
                ($row['CType'] == 'header' ? '' : '<br style="margin-bottom: 6px" />');

            $row['subheader'] = '';
        }

        $renderGrid = false;
        $renderGridType = null;
        foreach ($GLOBALS['TCA']['tt_content']['special'] as $specialType => $cTypes) {
            if (in_array($row['CType'], $cTypes)) {
                $renderGrid = true;
                $renderGridType = $specialType;
                break;
            }
        }
        if ($renderGrid) {
            $itemContent .= $this->renderBackendGrid($parentObject, $row, $renderGridType);
            $drawItem = false;
        }

        if (strpos($row['CType'], 'ibcontent_') === 0) {
            $page = $this->getPageLayoutController()->pageinfo;
            $typoScript = BackendTypoScript::get($page['uid']);

            if (isset($typoScript['lib.']['contentElement.']) && is_array($typoScript['lib.']['contentElement.']) && isset($typoScript['lib.']['contentElement.']['templateRootPaths.'])) {

                /** @var StandaloneView $standaloneView */
                $standaloneView = GeneralUtility::makeInstance(StandaloneView::class);
                $standaloneView->setTemplate($typoScript['tt_content.'][$row['CType'] . '.']['templateName']);
                $standaloneView->setTemplateRootPaths($typoScript['lib.']['contentElement.']['templateRootPaths.']);
                $standaloneView->setLayoutRootPaths($typoScript['lib.']['contentElement.']['layoutRootPaths.']);
                $standaloneView->setPartialRootPaths($typoScript['lib.']['contentElement.']['partialRootPaths.']);

                if ($row['CType'] == 'ibcontent_icons') {
                    FluidStyledContentRegistry::getIconProviders();
                }

                $processedData = ProcessingHelper::process(['data' => $row], ['page' => $page, 'record' => $row]);
                $pageOutput = $standaloneView->renderSection('BackendLayout', $processedData, true);

                if (trim($pageOutput)) {
                    $drawItem = false;
                    $itemContent .= '<div class="preview-content"><span class="link-area">' . $parentObject->linkEditContent('', $row) . '</span>' . $pageOutput . '</div>';
                }
            }
        }
    }

    /**
     * @param string $table
     * @param string $column
     * @param string $item
     * @param array $tsConfig
     * @return string
     */
    protected function renderTCAItemLabel($table, $column, $item, array $tsConfig = [])
    {
        if (isset($tsConfig['TCEFORM.'][$table . '.'][$column . '.']['altLabels.'][$item])) {
            return LocalizationUtility::translate($tsConfig['TCEFORM.'][$table . '.'][$column . '.']['altLabels.'][$item], 'ImiaBase');
        } elseif (isset($tsConfig['TCEFORM.'][$table . '.'][$column . '.']['addItems.'][$item])) {
            return LocalizationUtility::translate($tsConfig['TCEFORM.'][$table . '.'][$column . '.']['addItems.'][$item], 'ImiaBase');
        }

        $label = '[' . $item . ']';
        if (isset($GLOBALS['TCA'][$table]['columns'][$column])
            && isset($GLOBALS['TCA'][$table]['columns'][$column]['config']['items'])
            && is_array($GLOBALS['TCA'][$table]['columns'][$column]['config']['items'])
        ) {
            foreach ($GLOBALS['TCA'][$table]['columns'][$column]['config']['items'] as $tcaItem) {
                if ($tcaItem[1] == $item) {
                    $label = LocalizationUtility::translate($tcaItem[0], 'ImiaBase');
                    break;
                }
            }
        }

        return $label;
    }

    /**
     * @param \IMIA\ImiaBase\Xclass\Backend\View\PageLayoutView $parentObject
     * @param array $parentRow
     * @param string $renderGridType
     * @return string
     */
    protected function renderBackendGrid($parentObject, $parentRow, $renderGridType)
    {
        $originalRow = $parentRow;
        if (isset($parentRow['l18n_parent']) && $parentRow['l18n_parent']) {
            $parentRow = BackendUtility::getRecord('tt_content', $parentRow['l18n_parent']);
        }

        $out = '';
        $cList = [];
        $columnTitles = [];

        $lP = $parentObject->getCurrentLanguage();
        if ($renderGridType == 'grid') {
            $gridLayouts = json_decode(str_replace("'", '"', $parentRow['grid_layout']), true);
            $gridLayout = $gridLayouts[(int)$parentRow['grid_columns']]['default'];
        } elseif ($renderGridType == 'gridfixed1') {
            $gridLayout = [
                0 => [
                    'w'    => 12,
                ],
            ];
        } elseif ($renderGridType == 'gridfixed2') {
            $gridLayout = [
                0 => [
                    'w'    => 6,
                ],
                1 => [
                    'w'    => 6,
                ],
            ];
        } elseif ($renderGridType == 'gridfixed3') {
            $gridLayout = [
                0 => [
                    'w'    => 4,
                ],
                1 => [
                    'w'    => 4,
                ],
                2 => [
                    'w'    => 4,
                ],
            ];
        } else {
            $result = $this->getDb()->exec_SELECTquery('*', 'tx_imiabase_gridcontainers',
                'content = ' . $originalRow['uid'] . BackendUtility::deleteClause('tx_imiabase_gridcontainers'), '', 'sorting ASC');

            $gridLayout = [];
            while ($row = $this->getDb()->sql_fetch_assoc($result)) {
                if ($row['l10n_parent']) {
                    $row['uid'] = (int)$row['l10n_parent'];
                }

                $gridLayout[(int)$row['uid']] = [
                    'w'    => 12,
                    'name' => $row['title'],
                ];
            }
        }

        if (count($gridLayout) == 0) {
            return '';
        }

        $rowCount = 1;
        $colCount = 1;
        $backendLayout = [];
        $rowSize = 12;

        $col = 1;
        foreach ($gridLayout as $column => $config) {
            $cList[] = $column;

            if ($rowSize < $config['w']) {
                $rowSize = 12;
                $rowCount++;
                $col = 1;
                if (is_array($backendLayout[$rowCount]) && count($backendLayout[$rowCount]) > $colCount) {
                    $colCount = count($backendLayout[$rowCount]);
                }
            }

            $rowSize -= $config['w'];
            $backendLayout[$rowCount][$col++] = [
                'colPos'  => $column,
                'colspan' => $config['w'],
                'rowspan' => 1,
            ];
            $columnTitles[$column] = isset($config['name'])
                ? $config['name']
                : sprintf($this->getLanguageService()->sL('LLL:EXT:imia_base/Resources/Private/Language/Backend.xlf:gridlayout.grid.column'), ($column + 1));
        }
        if (count($backendLayout[$rowCount]) > $colCount) {
            $colCount = count($backendLayout[$rowCount]);
        }

        /** @var IconFactory $iconFactory */
        $iconFactory = GeneralUtility::makeInstance(IconFactory::class);

        /** @var ExpressionBuilder $expressionBuilder */
        $expressionBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tt_content')
            ->getExpressionBuilder();

        $pageinfo = BackendUtility::readPageAccess($parentRow['pid'], '');
        $pageTitleParamForAltDoc = '&recTitle=' . rawurlencode(BackendUtility::getRecordTitle('pages', BackendUtility::getRecordWSOL('pages', $parentRow['pid']), true));

        $showLanguage = $expressionBuilder->in('sys_language_uid', [$lP, -1]);

        $content = [];
        $head = [];
        $defaultLanguageElementsByColumn = [];

        // Select content records per column
        $contentRecordsPerColumn = $parentObject->getContentRecordsPerColumn('table', $parentRow['pid'], array_values($cList), $showLanguage, '`grid_parent` = ' . (int)$parentRow['uid']);
        // For each column, render the content into a variable:
        foreach ($cList as $columnId) {
            if (!isset($contentElementCache[$columnId])) {
                $contentElementCache[$columnId] = [];
            }

            // Start wrapping div
            $content[$columnId] .= '<div data-colpos="' . $columnId . '" data-language-uid="' . $lP . '" class="t3js-sortable t3js-sortable-lang t3js-sortable-lang-' . $lP . ' t3-page-ce-wrapper';
            if (empty($contentRecordsPerColumn[$columnId])) {
                $content[$columnId] .= ' t3-page-ce-empty';
            }
            $content[$columnId] .= '">';

            $link = '';
            if ($this->getPageLayoutController()->contentIsNotLockedForEditors()) {
                if ($parentObject->option_newWizard) {
                    $urlParameters = [
                        'id'               => $parentRow['pid'],
                        'sys_language_uid' => $lP,
                        'colPos'           => $columnId,
                        'grid_parent'      => $parentRow['l18n_parent'] ?: $parentRow['uid'],
                        'uid_pid'          => $parentRow['pid'],
                        'returnUrl'        => GeneralUtility::getIndpEnv('REQUEST_URI'),
                    ];
                    $tsConfig = BackendUtility::getModTSconfig($parentRow['pid'], 'mod');
                    $moduleName = isset($tsConfig['properties']['newContentElementWizard.']['override'])
                        ? $tsConfig['properties']['newContentElementWizard.']['override']
                        : 'new_content_element';
                    $url = BackendUtility::getModuleUrl($moduleName, $urlParameters);
                } else {
                    $urlParameters = [
                        'edit'      => [
                            'tt_content' => [
                                $parentRow['pid'] => 'new',
                            ],
                        ],
                        'defVals'   => [
                            'tt_content' => [
                                'colPos'           => $columnId,
                                'sys_language_uid' => $lP,
                                'grid_parent'      => $parentRow['l18n_parent'] ?: $parentRow['uid'],
                            ],
                        ],
                        'returnUrl' => GeneralUtility::getIndpEnv('REQUEST_URI'),
                    ];
                    $url = BackendUtility::getModuleUrl('record_edit', $urlParameters);
                }

                $link = '<a href="' . htmlspecialchars($url) . '" title="'
                    . htmlspecialchars($this->getLanguageService()->getLL('newContentElement')) . '" class="btn btn-default btn-sm">'
                    . $iconFactory->getIcon('actions-document-new', Icon::SIZE_SMALL)->render()
                    . ' '
                    . htmlspecialchars($this->getLanguageService()->getLL('content')) . '</a>';
            }
            if ($this->getBackendUser()->checkLanguageAccess($lP)) {
                $content[$columnId] .= '
                    <div class="t3-page-ce t3js-page-ce" data-page="' . (int)$parentRow['pid'] . '" id="' . StringUtility::getUniqueId() . '">
                        <div class="t3js-page-new-ce t3-page-ce-wrapper-new-ce" id="colpos-' . $columnId . '-' . 'page-' . $parentRow['pid'] . '-' . 'grid-' . $parentRow['uid'] . '-' . StringUtility::getUniqueId() . '">'
                    . $link
                    . '</div>
                        <div class="t3-page-ce-dropzone-available t3js-page-ce-dropzone-available"></div>
                    </div>
                    ';
            }
            $editUidList = '';
            if (!isset($contentRecordsPerColumn[$columnId]) || !is_array($contentRecordsPerColumn[$columnId])) {
                /** @var FlashMessage $message */
                $message = GeneralUtility::makeInstance(
                    FlashMessage::class,
                    $this->getLanguageService()->sL('LLL:EXT:backend/Resources/Private/Language/locallang_layout.xlf:error.invalidBackendLayout'),
                    '',
                    FlashMessage::WARNING
                );
                /** @var FlashMessageService $service */
                $service = GeneralUtility::makeInstance(FlashMessageService::class);
                $queue = $service->getMessageQueueByIdentifier();
                $queue->addMessage($message);
            } else {
                $rowArr = $contentRecordsPerColumn[$columnId];
                $parentObject->generateTtContentDataArray($rowArr);

                foreach ((array)$rowArr as $rKey => $row) {
                    $contentElementCache[$columnId][$row['uid']] = $row;
                    if ($parentObject->tt_contentConfig['languageMode']) {
                        $languageColumn[$columnId][$lP] = $head[$columnId] . $content[$columnId];
                        if (!$parentObject->defLangBinding) {
                            $languageColumn[$columnId][$lP] .= $parentObject->newLanguageButton(
                                $parentObject->getNonTranslatedTTcontentUids($defaultLanguageElementsByColumn, $parentRow['pid'], $lP),
                                $lP,
                                $columnId
                            );
                        }
                    }
                    if (is_array($row) && !VersionState::cast($row['t3ver_state'])->equals(VersionState::DELETE_PLACEHOLDER)) {
                        $singleElementHTML = '';
                        if (!$lP && ($parentObject->defLangBinding || $row['sys_language_uid'] != -1)) {
                            $defaultLanguageElementsByColumn[] = (isset($row['_ORIG_uid']) ? $row['_ORIG_uid'] : $row['uid']);
                        }
                        $editUidList .= $row['uid'] . ',';
                        $disableMoveAndNewButtons = $parentObject->defLangBinding && $lP > 0;
                        if (!$parentObject->tt_contentConfig['languageMode']) {
                            $singleElementHTML .= '<div class="t3-page-ce-dragitem" id="' . StringUtility::getUniqueId() . '">';
                        }
                        $singleElementHTML .= $parentObject->tt_content_drawHeader(
                            $row,
                            $parentObject->tt_contentConfig['showInfo'] ? 15 : 5,
                            $disableMoveAndNewButtons,
                            true,
                            $this->getBackendUser()->doesUserHaveAccess($pageinfo, Permission::CONTENT_EDIT)
                        );
                        $innerContent = '<div ' . ($row['_ORIG_uid'] ? ' class="ver-element"' : '') . '>'
                            . $parentObject->tt_content_drawItem($row) . '</div>';
                        $singleElementHTML .= '<div class="t3-page-ce-body-inner">' . $innerContent . '</div>'
                            . $parentObject->tt_content_drawFooter($row);
                        $isDisabled = $parentObject->isDisabled('tt_content', $row);
                        $statusHidden = $isDisabled ? ' t3-page-ce-hidden t3js-hidden-record' : '';
                        $displayNone = !$parentObject->tt_contentConfig['showHidden'] && $isDisabled ? ' style="display: none;"' : '';
                        $highlightHeader = false;
                        if ($parentObject->checkIfTranslationsExistInLanguage([], (int)$row['sys_language_uid']) && (int)$row['l18n_parent'] === 0) {
                            $highlightHeader = true;
                        }
                        $singleElementHTML = '<div class="t3-page-ce ' . ($highlightHeader ? 't3-page-ce-danger' : '') . ' t3js-page-ce t3js-page-ce-sortable ' . $statusHidden . '" id="element-tt_content-'
                            . $row['uid'] . '" data-table="tt_content" data-uid="' . $row['uid'] . '"' . $displayNone . '>' . $singleElementHTML . '</div>';

                        if ($parentObject->tt_contentConfig['languageMode']) {
                            $singleElementHTML .= '<div class="t3-page-ce t3js-page-ce">';
                        }
                        $singleElementHTML .= '<div class="t3js-page-new-ce t3-page-ce-wrapper-new-ce" id="colpos-' . $columnId . '-' . 'page-' . $parentRow['pid'] . '-' . 'grid-' . $parentRow['uid'] . '-' . StringUtility::getUniqueId() . '">';

                        // Add icon "new content element below"
                        if (!$disableMoveAndNewButtons
                            && $this->getPageLayoutController()->contentIsNotLockedForEditors()
                            && $this->getBackendUser()->checkLanguageAccess($lP)
                        ) {
                            // New content element:
                            if ($parentObject->option_newWizard) {
                                $urlParameters = [
                                    'id'               => $row['pid'],
                                    'sys_language_uid' => $row['sys_language_uid'],
                                    'colPos'           => $row['colPos'],
                                    'grid_parent'      => $parentRow['l18n_parent'] ?: $parentRow['uid'],
                                    'uid_pid'          => -$row['uid'],
                                    'returnUrl'        => GeneralUtility::getIndpEnv('REQUEST_URI'),
                                ];
                                $tsConfig = BackendUtility::getModTSconfig($row['pid'], 'mod');
                                $moduleName = isset($tsConfig['properties']['newContentElementWizard.']['override'])
                                    ? $tsConfig['properties']['newContentElementWizard.']['override']
                                    : 'new_content_element';
                                $url = BackendUtility::getModuleUrl($moduleName, $urlParameters);
                            } else {
                                $urlParameters = [
                                    'edit'      => [
                                        'tt_content' => [
                                            -$row['uid'] => 'new',
                                        ],
                                    ],
                                    'returnUrl' => GeneralUtility::getIndpEnv('REQUEST_URI'),
                                ];
                                $url = BackendUtility::getModuleUrl('record_edit', $urlParameters);
                            }
                            $singleElementHTML .= '
								<a href="' . htmlspecialchars($url) . '" title="'
                                . htmlspecialchars($this->getLanguageService()->getLL('newContentElement')) . '" class="btn btn-default btn-sm">'
                                . $iconFactory->getIcon('actions-document-new', Icon::SIZE_SMALL)->render()
                                . ' '
                                . htmlspecialchars($this->getLanguageService()->getLL('content')) . '</a>
							';
                        }
                        $singleElementHTML .= '</div></div><div class="t3-page-ce-dropzone-available t3js-page-ce-dropzone-available"></div></div>';
                        if ($parentObject->defLangBinding && $parentObject->tt_contentConfig['languageMode']) {
                            $defLangBinding[$columnId][$lP][$row[$lP ? 'l18n_parent' : 'uid']] = $singleElementHTML;
                        } else {
                            $content[$columnId] .= $singleElementHTML;
                        }
                    } else {
                        unset($rowArr[$rKey]);
                    }
                }
                $content[$columnId] .= '</div>';
                $colTitle = $columnTitles[$columnId];
                $editParam = $parentObject->doEdit && !empty($rowArr)
                    ? '&edit[tt_content][' . $editUidList . ']=edit' . $pageTitleParamForAltDoc
                    : '';
                $head[$columnId] .= $parentObject->tt_content_drawColHeader($colTitle, $editParam);
            }
        }

        $gridClass = 't3-grid-container';
        if ($_COOKIE['content-grid-hidden-' . $parentRow['uid']]) {
            $gridClass .= ' grid-hidden';
        }

        $this->callHook('gridClass', [&$gridClass, $parentRow, $this]);

        $grid = '<div class="' . $gridClass . '"><table border="0" cellspacing="0" cellpadding="0" width="100%" class="t3-page-columns t3-grid-table t3js-page-columns">';
        $grid .= '<colgroup>';
        for ($i = 0; $i < $colCount; $i++) {
            $grid .= '<col />';
        }
        $grid .= '</colgroup>';

        for ($row = 1; $row <= $rowCount; $row++) {
            $grid .= '<tr>';
            for ($col = 1; $col <= $colCount; $col++) {
                if (isset($backendLayout[$row][$col])) {
                    $columnConfig = $backendLayout[$row][$col];

                    $columnKey = (int)$columnConfig['colPos'];
                    $colSpan = (int)$columnConfig['colspan'];
                    $rowSpan = (int)$columnConfig['rowspan'];
                    $grid .= '<td valign="top"' .
                        ($colSpan > 0 ? ' colspan="' . $colSpan . '"' : '') .
                        ($rowSpan > 0 ? ' rowspan="' . $rowSpan . '"' : '') .
                        ' data-colpos="' . (int)$columnConfig['colPos'] . '" data-grid="' . (int)$parentRow['uid'] . '" data-language-uid="' . $lP . '" class="t3js-page-lang-column-' . $lP . ' t3js-page-column t3-grid-cell t3-page-column t3-page-column-' . $columnKey .
                        ((!isset($columnConfig['colPos']) || $columnConfig['colPos'] === '') ? ' t3-grid-cell-unassigned' : '') .
                        ($colSpan > 0 ? ' t3-gridCell-width' . $colSpan : '') .
                        ($rowSpan > 0 ? ' t3-gridCell-height' . $rowSpan : '') . '">';

                    if (isset($columnConfig['colPos']) && $columnConfig['colPos'] !== '' && $head[$columnKey]) {
                        $grid .= $head[$columnKey] . $content[$columnKey];
                    } elseif (isset($columnConfig['colPos']) && $columnConfig['colPos'] !== '') {
                        $grid .= $parentObject->tt_content_drawColHeader($this->getLanguageService()->getLL('noAccess'));
                    } elseif (isset($columnConfig['name']) && $columnConfig['name'] !== '') {
                        $grid .= $parentObject->tt_content_drawColHeader($this->getLanguageService()->sL($columnConfig['name'])
                            . ' (' . $this->getLanguageService()->getLL('notAssigned') . ')');
                    } else {
                        $grid .= $parentObject->tt_content_drawColHeader($this->getLanguageService()->getLL('notAssigned'));
                    }

                    $grid .= '</td>';
                }
            }
            $grid .= '</tr>';
        }
        $out .= $grid . '</table></div>';
        $out .= BackendUtility::cshItem($parentObject->descrTable, 'columns_multi', null, '<span class="btn btn-default btn-sm">|</span>');

        return $out;
    }

    /**
     * @return LanguageService
     */
    protected function getLanguageService()
    {
        return $GLOBALS['LANG'];
    }

    /**
     * @return PageLayoutController
     */
    protected function getPageLayoutController()
    {
        return $GLOBALS['SOBE'];
    }

    /**
     * @return BackendUserAuthentication
     */
    protected function getBackendUser()
    {
        return $GLOBALS['BE_USER'];
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}