<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Hook\Backend;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Backend\View\PageLayoutView;
use TYPO3\CMS\Backend\View\PageLayoutViewDrawFooterHookInterface;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Lang\LanguageService;

/**
 * @package     imia_base
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class PageLayoutViewDrawFooterHook implements PageLayoutViewDrawFooterHookInterface
{
    /**
     * @var array
     */
    protected static $tsConfig = [];

    /**
     * @var array
     */
    protected static $contentConfig = [];

    /**
     * @param PageLayoutView $parentObject
     * @param array $info
     * @param array $row
     */
    public function preProcess(PageLayoutView &$parentObject, &$info, array &$row)
    {
        static $tsConfig;
        if (!$tsConfig) {
            $tsConfig = BackendUtility::getPagesTSconfig($parentObject->id);
        }

        foreach ($info as $key => $value) {
            if (!$value) {
                unset($info[$key]);
            }
        }

        $extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['imia_base']);
        if ($extConf['bootstrapVisibility']) {
            if ($row['bootstrap_visibility']) {
                $bsv = array_map('trim', explode(',', $row['bootstrap_visibility']));

                $webRoot = str_replace(GeneralUtility::getIndpEnv('TYPO3_DOCUMENT_ROOT'), '', PATH_site);
                $resourcePath = $webRoot . ExtensionManagementUtility::siteRelPath('imia_base') . 'Resources/Public/';

                foreach ($bsv as $visibility) {
                    $label = $this->getLanguageService()
                        ->sL('LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:content_bootstrapvisibility_' . $visibility);

                    array_unshift($info, '<span><img src="' . $resourcePath . 'Images/Resolution/' . $visibility . '.png" alt="" style="vertical-align: middle"> ' .
                        '<span style="vertical-align: middle">' . $label . '</span></span>');
                }
            }
        }

        if ($extConf['spaceBeforeAfter']) {
            if ($row['spaceAfter']) {
                array_unshift($info, self::renderTCAField($row, 'spaceAfter'));
            }
            if ($row['spaceBefore']) {
                array_unshift($info, self::renderTCAField($row, 'spaceBefore'));
            }
        }

        if ($row['frame_class'] && $row['frame_class'] != 'default') {
            array_unshift($info, self::renderTCAField($row, 'frame_class'));
        }
        if ($row['layout'] && $row['layout'] != 'default') {
            array_unshift($info, self::renderTCAField($row, 'layout'));
        }

        if (isset($tsConfig['mod.']['web_layout.']['content.']['footerinfo'])) {
            foreach (explode(',', $tsConfig['mod.']['web_layout.']['content.']['footerinfo']) as $column) {
                if ($row[$column]) {
                    $info[] = self::renderTCAField($row, $column);
                }
            }
        }
    }

    /**
     * @param array $row
     * @param $field
     * @return string
     */
    public static function renderTCAField($row, $field)
    {
        if ($row[$field]) {
            $tsConfig = self::getTsConfig($row, $field);
            $config = self::getContentConfig($row, $field);

            $items = [];
            if (isset($config['config']['items'])) {
                foreach ($config['config']['items'] as $item) {
                    $items[$item[1]] = $item[0];
                }
            }

            $label = $tsConfig['label'] ?: $config['label'];
            $label = $tsConfig['label.'][$GLOBALS['LANG']->lang] ?: $label;
            $label = $GLOBALS['LANG']->sL($label);

            $valueDisplay = '';
            $values = explode(',', (string)$row[$field]);
            foreach ($values as $value) {
                if (array_key_exists($value, $items)) {
                    if ($tsConfig['altLabels.'] && array_key_exists($value, $tsConfig['altLabels.'])) {
                        $value = $tsConfig['altLabels.'][$value];
                    } else {
                        $value = $items[$value];
                    }
                } elseif ($tsConfig['addItems.'] && array_key_exists($value, $tsConfig['addItems.'])) {
                    $value = $tsConfig['addItems.'][$value];
                }
                $value = $GLOBALS['LANG']->sL($value);

                if ($value) {
                    if ($valueDisplay) {
                        $valueDisplay .= ', ' . $value;
                    } else {
                        $valueDisplay .= $value;
                    }
                }
            }

            return '<strong>' . $label . (strpos($label, ':') >= strlen($label) - 2 ? '' : ':') . '</strong> ' . $valueDisplay;
        } else {
            return '';
        }
    }

    /**
     * @param array $row
     * @param string $field
     * @return array
     */
    public static function getContentConfig($row, $field)
    {
        if (!self::$contentConfig[$field]) {
            self::$contentConfig[$field] = $GLOBALS['TCA']['tt_content']['columns'][$field];
        }

        return self::$contentConfig[$field];
    }

    /**
     * @param array $row
     * @param string $field
     * @return array
     */
    public static function getTsConfig($row, $field = null)
    {
        if (!self::$tsConfig[$row['pid']]) {
            self::$tsConfig[$row['pid']] = BackendUtility::getTCEFORM_TSconfig('tt_content', $row);
        }

        if ($field) {
            return self::$tsConfig[$row['pid']][$field];
        } else {
            return self::$tsConfig[$row['pid']];
        }
    }

    /**
     * @return LanguageService
     */
    protected function getLanguageService()
    {
        return $GLOBALS['LANG'];
    }
}