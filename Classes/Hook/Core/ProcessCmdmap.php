<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Hook\Core;

use IMIA\ImiaBase\ContentDefender\Hooks\DatamapDataHandlerHook;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Database\RelationHandler;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class ProcessCmdmap implements SingletonInterface
{
    /**
     * @var array
     */
    protected $contentMappingArray = [];

    /**
     * @param DataHandler $dataHandler
     */
    public function processCmdmap_beforeStart(\TYPO3\CMS\Core\DataHandling\DataHandler &$dataHandler)
    {
        $addCommands = $addCommandsGridContainers = [];
        if (isset($dataHandler->cmdmap['tt_content'])) {
            foreach ($dataHandler->cmdmap['tt_content'] as $uids => $config) {
                $uids = array_map('intval', explode(',', (string)$uids));

                foreach ($config as $cmd => $value) {
                    if ($cmd == 'delete' && $value) {
                        $childRows = [];
                        $result = $this->getDb()->exec_SELECTquery('uid,sys_language_uid,l18n_parent', 'tt_content', 'deleted = 0 AND uid IN(' . implode(',', $uids) . ')');
                        while ($row = $this->getDb()->sql_fetch_assoc($result)) {
                            if ($row['l18n_parent']) {
                                $childRows = array_merge($childRows, $this->getGridChildren($row));
                            }
                        }
                        foreach ($childRows as $childRow) {
                            $addCommands[$childRow['uid']] = [
                                $cmd => $value,
                            ];
                        }
                    } elseif (($cmd == 'localize' || $cmd == 'copyToLanguage') && $value) {
                        DatamapDataHandlerHook::$bypassAccessCheckForRecords = true; // to fix the ContentDefender problem with grid_children
                        foreach ($uids as $uid) {
                            $gridContainerConfig = $GLOBALS['TCA']['tt_content']['columns']['grid_containers']['config'];

                            /** @var RelationHandler $relationHandler */
                            $relationHandler = GeneralUtility::makeInstance(RelationHandler::class);
                            $relationHandler->registerNonTableValues = (bool)$gridContainerConfig['allowedIdValues'];
                            $relationHandler->start($value, $gridContainerConfig['foreign_table'], $gridContainerConfig['MM'], $uid, 'tt_content', $gridContainerConfig);

                            foreach ($relationHandler->getValueArray() as $foreignRecordUid) {
                                $addCommandsGridContainers[$foreignRecordUid] = [
                                    $cmd => $value,
                                ];
                            }

                            $children = $this->getGridChildren(['uid' => $uid, 'sys_language_uid' => 0]);
                            foreach ($children as $child) {
                                $addCommands[$child['uid']] = [
                                    $cmd => $value,
                                ];

                                /** @var RelationHandler $relationHandler */
                                $relationHandler = GeneralUtility::makeInstance(RelationHandler::class);
                                $relationHandler->registerNonTableValues = (bool)$gridContainerConfig['allowedIdValues'];
                                $relationHandler->start($value, $gridContainerConfig['foreign_table'], $gridContainerConfig['MM'], $child['uid'], 'tt_content', $gridContainerConfig);

                                foreach ($relationHandler->getValueArray() as $foreignRecordUid) {
                                    $addCommandsGridContainers[$foreignRecordUid] = [
                                        $cmd => $value,
                                    ];
                                }
                            }
                        }
                    }
                }
            }

            if (count($addCommands)) {
                $dataHandler->cmdmap['tt_content'] += $addCommands;
            }
            if (count($addCommandsGridContainers)) {
                if (!isset($dataHandler->cmdmap['tx_imiabase_gridcontainers'])) {
                    $dataHandler->cmdmap['tx_imiabase_gridcontainers'] = [];
                }
                $dataHandler->cmdmap['tx_imiabase_gridcontainers'] += $addCommandsGridContainers;
            }
        }
    }

    /**
     * @param string $command
     * @param string $table
     * @param int $id
     * @param string $value
     * @param \TYPO3\CMS\Core\DataHandling\DataHandler $dataHandler
     * @param bool|array $pasteUpdate
     * @param array $pasteDatamap
     */
    public function processCmdmap_postProcess($command, $table, $id, $value, $dataHandler, $pasteUpdate, $pasteDatamap)
    {
        if ($table == 'tt_content') {
            if (isset($dataHandler->copyMappingArray[$table]) && is_array($dataHandler->copyMappingArray[$table])) {
                foreach ($dataHandler->copyMappingArray[$table] as $key => $val) {
                    $this->contentMappingArray[$key] = $val;
                }
            }
            $newRecordId = $dataHandler->copyMappingArray[$table][$id];

            if ($newRecordId) {
                switch ($command) {
                    case 'copyToLanguage':
                        $oldRecord = BackendUtility::getRecord($table, $id, 'uid,grid_parent');
                        if ($oldRecord['grid_parent']) {
                            if (isset($this->contentMappingArray[$oldRecord['grid_parent']])) {
                                $this->getDb()->exec_UPDATEquery('tt_content', 'uid = ' . $newRecordId, [
                                    'grid_parent' => $this->contentMappingArray[$oldRecord['grid_parent']],
                                ]);
                            }
                        }
                        break;
                    case 'copy':
                        $oldRecord = BackendUtility::getRecord($table, $id, 'uid,sys_language_uid');
                        $newRecord = BackendUtility::getRecord($table, $newRecordId, 'uid,pid,sys_language_uid,l18n_parent,CType');
                        if (in_array($newRecord['CType'], ['ibcontent_tabs', 'ibcontent_accordion'])) {
                            $children = $this->getGridChildren(['uid' => $newRecordId, 'sys_language_uid' => $oldRecord['sys_language_uid']]);

                            $result = $this->getDb()->exec_SELECTquery('*', 'tx_imiabase_gridcontainers',
                                'content = ' . (int)$id . BackendUtility::deleteClause('tx_imiabase_gridcontainers'));
                            while ($row = $this->getDb()->sql_fetch_assoc($result)) {
                                foreach ($children as $child) {
                                    if ((int)$child['colPos'] == (int)$row['uid']) {
                                        $newGridContainerId = $dataHandler->copyMappingArray['tx_imiabase_gridcontainers'][$row['uid']];
                                        if ($newGridContainerId) {
                                            $this->getDb()->exec_UPDATEquery('tt_content', 'uid = ' . $child['uid'], [
                                                'colPos' => $newGridContainerId
                                            ]);
                                        }
                                    }
                                }
                            }
                        }

                        if ($oldRecord['sys_language_uid'] === 0 && $newRecord['sys_language_uid'] > 0) {
                            $this->getDb()->exec_UPDATEquery('tt_content', 'uid = ' . $newRecordId, [
                                'l10n_source' => $id
                            ]);
                        }

                        $oldChildren = $this->getGridChildren(['uid' => $oldRecord['uid'], 'sys_language_uid' => $oldRecord['sys_language_uid']]);
                        foreach ($oldChildren as $oldChild) {
                            if (in_array($oldChild['uid'], $dataHandler->copyMappingArray[$table])) {
                                $this->getDb()->exec_UPDATEquery('tt_content', 'uid = ' . $oldChild['uid'], ['grid_parent' => $newRecordId]);
                            }
                        }
                        break;
                }
            }
        }
    }

    /**
     * @param array $row
     * @return array
     */
    protected function getGridChildren($row)
    {
        $result = $this->getDb()->exec_SELECTquery('uid,sys_language_uid,l18n_parent', 'tt_content',
            'deleted = 0 AND sys_language_uid = ' . (int)$row['sys_language_uid'] .
            ' AND grid_parent IN (' . (isset($row['l18n_parent']) && $row['l18n_parent']
                ? (int)$row['l18n_parent'] . ',' . (int)$row['uid'] : (int)$row['uid']) . ')');

        $children = [];
        while ($childRow = $this->getDb()->sql_fetch_assoc($result)) {
            if ($childRow['uid'] != $row['uid']) {
                $children[] = $childRow;
                $children = array_merge($children, $this->getGridChildren($childRow));
            }
        }

        return $children;
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}