<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Hook\Core;

use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

/**
 * @package     imia_base
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class FlexParsing
{
    /**
     * @param array $dataStructure
     * @param string $identifier
     * @return array
     */
    public function parseDataStructureByIdentifierPostProcess($dataStructure, $identifier)
    {
        if (is_array($dataStructure['sheets'])) {
            foreach ($dataStructure['sheets'] as &$sheet) {
                if (is_array($sheet['ROOT']['el'])) {
                    foreach ($sheet['ROOT']['el'] as $name => &$element) {
                        if (isset($element['TCEforms']['config'])) {
                            $config = &$element['TCEforms']['config'];

                            if (is_array($config) && $config['type']) {
                                switch ($config['type']) {
                                    case 'files':
                                        unset($config['type']);

                                        $defaultConfig = [
                                            'appearance'       => [
                                                'headerThumbnail'            => [
                                                    'width'  => '64m',
                                                    'height' => '64m',
                                                ],
                                            ],
                                        ];

                                        $allowedExtensions = '';
                                        if (array_key_exists('allowedExtensions', $config)) {
                                            $allowedExtensions = $config['allowedExtensions'];
                                            unset($config['allowedExtensions']);
                                        }
                                        $disallowedExtensions = '';
                                        if (array_key_exists('disallowedExtensions', $config)) {
                                            $disallowedExtensions = $config['disallowedExtensions'];
                                            unset($config['disallowedExtensions']);
                                        }
                                        ArrayUtility::mergeRecursiveWithOverrule($defaultConfig, $config);

                                        $config = ExtensionManagementUtility::getFileFieldTCAConfig($name, $defaultConfig, $allowedExtensions, $disallowedExtensions);
                                        break;
                                    case 'filesImage':
                                        unset($config['type']);

                                        $defaultConfig = [
                                            'appearance'       => [
                                                'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference',
                                                'headerThumbnail'            => [
                                                    'width'  => '64m',
                                                    'height' => '64m',
                                                ],
                                            ],
                                            'overrideChildTca' => [
                                                'types' => [
                                                    File::FILETYPE_UNKNOWN     => [
                                                        'showitem' => '--palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                                                    ],
                                                    File::FILETYPE_TEXT        => [
                                                        'showitem' => '--palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                                                    ],
                                                    File::FILETYPE_IMAGE       => [
                                                        'showitem' => '--palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                                                    ],
                                                    File::FILETYPE_AUDIO       => [
                                                        'showitem' => '--palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                                                    ],
                                                    File::FILETYPE_VIDEO       => [
                                                        'showitem' => '--palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                                                    ],
                                                    File::FILETYPE_APPLICATION => [
                                                        'showitem' => '--palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                                                    ],
                                                ],
                                            ],
                                        ];

                                        $allowedExtensions = '';
                                        if (array_key_exists('allowedExtensions', $config)) {
                                            $allowedExtensions = $config['allowedExtensions'];
                                            unset($config['allowedExtensions']);
                                        }
                                        $disallowedExtensions = '';
                                        if (array_key_exists('disallowedExtensions', $config)) {
                                            $disallowedExtensions = $config['disallowedExtensions'];
                                            unset($config['disallowedExtensions']);
                                        }
                                        ArrayUtility::mergeRecursiveWithOverrule($defaultConfig, $config);

                                        $config = ExtensionManagementUtility::getFileFieldTCAConfig($name, $defaultConfig, $allowedExtensions, $disallowedExtensions);
                                        break;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $dataStructure;
    }
}