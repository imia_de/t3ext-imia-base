<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Hook\Core;

use IMIA\ImiaBase\Registry\FluidStyledContentRegistry;
use IMIA\ImiaBase\User\Grid;
use IMIA\ImiaBase\Utility\ExtensionUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

/**
 * @package     imia_base
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class TableConfigurationPostProcessing implements \TYPO3\CMS\Core\Database\TableConfigurationPostProcessingHookInterface
{
    public function processData()
    {
        if (ExtensionManagementUtility::isLoaded('flux')) {
            ExtensionUtility::addDisplayCond('tt_content', 'tx_fed_fcefile', 'FIELD:CType:=:fluidcontent_content');
        }

        $extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['imia_base']);
        if ($extConf['bootstrapVisibility'] || $extConf['bootstrapVisibilityPages']) {
            $bootstrapVisibilityColumn = [
                'bootstrap_visibility' => [
                    'label'     => 'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:content_bootstrapvisibility',
                    'l10n_mode' => 'exclude',
                    'config'    => [
                        'items'      => [
                            [
                                'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:content_bootstrapvisibility_xs',
                                'xs',
                                'EXT:imia_base/Resources/Public/Images/Resolution/xs.png',
                            ],
                            [
                                'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:content_bootstrapvisibility_sm',
                                'sm',
                                'EXT:imia_base/Resources/Public/Images/Resolution/sm.png',
                            ],
                            [
                                'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:content_bootstrapvisibility_md',
                                'md',
                                'EXT:imia_base/Resources/Public/Images/Resolution/md.png',
                            ],
                            [
                                'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:content_bootstrapvisibility_lg',
                                'lg',
                                'EXT:imia_base/Resources/Public/Images/Resolution/lg.png',
                            ],
                            [
                                'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:content_bootstrapvisibility_xl',
                                'xl',
                                'EXT:imia_base/Resources/Public/Images/Resolution/xl.png',
                            ],
                        ],
                        'renderMode' => 'checkbox',
                        'maxitems'   => '9999',
                        'minitems'   => '0',
                        'type'       => 'select',
                        'renderType' => 'selectCheckBox',
                    ],
                ],
            ];

            if ($extConf['bootstrapVisibility']) {
                ExtensionManagementUtility::addTCAcolumns('tt_content', $bootstrapVisibilityColumn);
                $GLOBALS['TCA']['tt_content']['palettes']['hidden']['showitem'] .= ', --linebreak--, bootstrap_visibility';
            }

            if ($extConf['bootstrapVisibilityPages']) {
                ExtensionManagementUtility::addTCAcolumns('pages', $bootstrapVisibilityColumn);
                $GLOBALS['TCA']['pages']['palettes']['visibility']['showitem'] .= ', --linebreak--, bootstrap_visibility';
            }

            if ($extConf['singleSignOn']) {
                $singleSignOnDisabledColumn = [
                    'singlesignon_disabled' => [
                        'label'  => 'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:domain_singlesignondisabled',
                        'config' => [
                            'type'  => 'check',
                            'items' => [
                                ['LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:domain_singlesignondisabled_check'],
                            ],
                        ],
                    ],
                ];

                ExtensionManagementUtility::addTCAcolumns('sys_domain', $singleSignOnDisabledColumn);
                ExtensionManagementUtility::addToAllTCAtypes('sys_domain', 'singlesignon_disabled', '', 'after:domainName');
            }
        }

        $extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['imia_base']);
        if ($extConf['spaceBeforeAfter']) {
            $GLOBALS['TCA']['tt_content']['palettes']['frames']['showitem'] = preg_replace('/space_before_class.*?space_after_class_formlabel/ism',
                'spaceBefore;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:spaceBefore_formlabel,spaceAfter;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:spaceAfter_formlabel',
                $GLOBALS['TCA']['tt_content']['palettes']['frames']['showitem']);
        }

        $GLOBALS['TCA']['tt_content']['types']['fluidcontent_content']['showitem'] =
            str_replace('layout;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:layout_formlabel', '',
                $GLOBALS['TCA']['tt_content']['types']['fluidcontent_content']['showitem']);

        FluidStyledContentRegistry::loadPageTS();
        FluidStyledContentRegistry::loadTCA();
        FluidStyledContentRegistry::loadIcons();

        if (in_array('Grid', FluidStyledContentRegistry::getContentElementsEnabled())
            || in_array('Accordion', FluidStyledContentRegistry::getContentElementsEnabled())
            || in_array('Tabs', FluidStyledContentRegistry::getContentElementsEnabled())
        ) {
            ExtensionManagementUtility::addFieldsToPalette('tt_content', 'general', 'grid_parent', 'after:colPos');
            $GLOBALS['TCA']['tt_content']['columns']['colPos']['config']['itemsProcFunc'] = Grid::class . '->colPosListItemProcFunc';
            $GLOBALS['TCA']['tt_content']['ctrl']['copyAfterDuplFields'] .= ',grid_parent';
        }

        $GLOBALS['TCA']['be_groups']['columns']['pagetypes_select']['config']['maxitems'] = 9999;

        $specialTypes = [];
        foreach ($GLOBALS['TCA']['tt_content']['special'] as $specialType => $cTypes) {
            $specialTypes = array_merge($specialTypes, $cTypes);
        }
        $GLOBALS['TCA']['tt_content']['columns']['grid_parent']['config']['foreign_table_where'] .=
            ' AND tt_content.CType IN ("' . implode('","', $specialTypes) . '")';

        // Media content element
        $GLOBALS['TCA']['tt_content']['types']['media'] = $GLOBALS['TCA']['tt_content']['types']['textmedia'];
        unset($GLOBALS['TCA']['tt_content']['types']['media']['columnsOverrides']);
        $GLOBALS['TCA']['tt_content']['types']['media']['showitem'] = preg_replace('/bodytext[^,]*,/ism', '',
            $GLOBALS['TCA']['tt_content']['types']['media']['showitem']);
        array_splice($GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'], 6, 0, [
            ['LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:content_ctype_media', 'media', 'content-media']]);
    }
}