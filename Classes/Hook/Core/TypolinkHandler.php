<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Hook\Core;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\Service\TypoLinkCodecService;

/**
 * Handler for gaoptout javascript links.
 */
class TypolinkHandler implements \TYPO3\CMS\Core\SingletonInterface
{
    /**
     * Processes the link generation.
     *
     * @param string $linkText Text to be used inside the <a> tag
     * @param array $configuration Parent TS configuration
     * @param string $linkHandlerKeyword Keyword that triggered the typolink handler
     * @param string $linkHandlerValue
     * @param string $mixedLinkParameter
     * @param ContentObjectRenderer $contentObjectRenderer
     * @return string|array
     */
    public function main($linkText, $configuration, $linkHandlerKeyword, $linkHandlerValue, $mixedLinkParameter, $contentObjectRenderer)
    {
        if (stripos($mixedLinkParameter, 'javascript:gaoptout') !== false) {
            $linkParameterParts = GeneralUtility::makeInstance(TypoLinkCodecService::class)->decode($mixedLinkParameter);

            return [
                'href'   => $linkParameterParts['url'],
                'target' => $linkParameterParts['target'],
                'class'  => $linkParameterParts['class'],
                'title'  => $linkParameterParts['title'],
            ];
        }

        return $linkText;
    }
}