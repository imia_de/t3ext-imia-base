<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Hook\Core;

/**
 * @package     imia_base
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class DatabaseRecordList implements \TYPO3\CMS\Backend\RecordList\RecordListGetTableHookInterface
{
    /**
     * @param string $table
     * @param integer $pageId
     * @param string $additionalWhereClause
     * @param array $selectedFieldsList
     * @param \TYPO3\CMS\Recordlist\RecordList\DatabaseRecordList $parentObject
     */
    public function getDBlistQuery($table, $pageId, &$additionalWhereClause, &$selectedFieldsList, &$parentObject)
    {
        if ($table == 'tt_content') {
            if ($additionalWhereClause) {
                $additionalWhereClause = '(' . $additionalWhereClause . ') AND ';
            }
            $additionalWhereClause .= '(tt_content.grid_parent = 0 OR tt_content.grid_parent IS NULL)';
        }
    }
}