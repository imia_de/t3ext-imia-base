<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (d.frerich@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Hook\Core;

use IMIA\ImiaBase\Utility\ContentCache;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;

/**
 * @package     imia_base
 * @subpackage  Hook\Core
 * @author      David Frerich <d.frerich@imia.de>
 */
class ProcessDatamap implements SingletonInterface
{
    /**
     * @var \TYPO3\CMS\Core\Cache\Frontend\FrontendInterface|boolean
     */
    static protected $cache = null;

    /**
     * @var array
     */
    static protected $clearedContents = [];

    /**
     * @var array
     */
    protected $updateOriginalRecords = [];

    /**
     * @param array $incomingFieldArray
     * @param string $table
     * @param integer $id
     * @param DataHandler $dataHandler
     */
    public function processDatamap_preProcessFieldArray($incomingFieldArray, $table, $id, $dataHandler)
    {
        if ($table == 'tt_content' && is_numeric($id) && isset($incomingFieldArray['sys_language_uid'])) {
            $record = BackendUtility::getRecord($table, $id);

            if ($record && $record['sys_language_uid'] != $incomingFieldArray['sys_language_uid']) {
                $this->updateOriginalRecords[] = [
                    'uid'            => $record['uid'],
                    'newLanguageUid' => $incomingFieldArray['sys_language_uid'],
                ];
            }
        }
    }

    /**
     * @param DataHandler $dataHandler
     */
    public function processDatamap_afterAllOperations($dataHandler)
    {
        foreach ($dataHandler->datamap as $table => $data) {
            foreach ($data as $rawId => $fields) {
                if (MathUtility::canBeInterpretedAsInteger($rawId)) {
                    $fields['uid'] = $rawId;
                } else {
                    $fields['uid'] = $dataHandler->substNEWwithIDs[$rawId];
                }

                switch ($table) {
                    case 'tt_content':
                        $record = BackendUtility::getRecord($table, $fields['uid']);
                        if ((!isset($fields['pid']) || $fields['pid'] > 0 || $fields['pid'] == null) && $fields['grid_children']) {
                            if ($record['l18n_parent']) {
                                $this->getDb()->exec_UPDATEquery($table, 'uid IN (' . $fields['grid_children'] . ')', [
                                    'grid_parent' => $record['l18n_parent'],
                                ]);
                            }
                        }

                        $this->clearContentCache($fields['uid'], $record);
                        break;
                    case 'pages':
                        if (self::getCache()) {
                            self::getCache()->flushByTag('page-' . $fields['uid']);
                        }
                        break;
                    default:
                        if (ContentCache::enabled()) {
                            ContentCache::flushByTag('contentrecord-' . $table . '-' . $fields['uid']);
                        }
                        break;
                }
            }
        }

        foreach ($this->updateOriginalRecords as $recordData) {
            $children = $this->getGridChildren(['uid' => $recordData['uid']]);
            if ($children && count($children) > 0) {
                foreach ($children as $child) {
                    $this->getDb()->exec_UPDATEquery('tt_content', 'uid = ' . $child['uid'], ['sys_language_uid' => $recordData['newLanguageUid']]);
                }
            }
        }
    }

    /**
     * @param integer $uid
     * @param array $record
     */
    protected function clearContentCache($uid, $record = null)
    {
        if (!in_array((int)$uid, self::$clearedContents)) {
            self::$clearedContents[] = (int)$uid;

            if (!is_array($record)) {
                $record = BackendUtility::getRecord('tt_content', $uid, 'uid, pid, grid_parent, l18n_parent');
            }

            if (self::getCache()) {
                self::getCache()->flushByTag('content-' . $uid);
            }
            if (ContentCache::enabled()) {
                ContentCache::flushByTag('content-' . $uid);
                ContentCache::flushByTag('contentspecial-' . $uid);
            }

            $identifier = 'tt_content_' . $uid;
            $where = '(records = "' . $identifier . '" OR records = "%,' . $identifier .
                '" OR records = "' . $identifier . ',%" OR records = "%,' . $identifier . ',%")';
            if (isset($record['l18n_parent']) && $record['l18n_parent']) {
                $identifier = 'tt_content_' . $record['l18n_parent'];
                $where = '(' . $where . ' OR (records = "' . $identifier . '" OR records = "%,' . $identifier .
                    '" OR records = "' . $identifier . ',%" OR records = "%,' . $identifier . ',%"))';
            }

            $result = $this->getDb()->exec_SELECTquery('uid, pid, grid_parent, l18n_parent', 'tt_content',
                $where . ' AND deleted = 0 AND uid != ' . (int)$uid);

            while ($row = $this->getDb()->sql_fetch_assoc($result)) {
                $this->clearContentCache($row['uid'], $row);
            }

            if (isset($record['tx_flux_parent']) && $record['tx_flux_parent']) {
                $this->clearContentCache($record['tx_flux_parent']);
            }
            if (isset($record['grid_parent']) && $record['grid_parent']) {
                $this->clearContentCache($record['grid_parent']);
            }

            $where = 'grid_parent = ' . (int)$uid;
            if (isset($record['l18n_parent']) && $record['l18n_parent']) {
                $where = '(' . $where . ' OR grid_parent = ' . (int)$record['l18n_parent'].')';
            }
            $result = $this->getDb()->exec_SELECTquery('uid, pid, grid_parent, l18n_parent', 'tt_content',
                $where . ' AND deleted = 0 AND uid != ' . (int)$uid);

            while ($row = $this->getDb()->sql_fetch_assoc($result)) {
                $this->clearContentCache($row['uid'], $row);
            }
        }
    }

    /**
     * @param array $row
     * @return array
     */
    protected function getGridChildren($row)
    {
        $result = $this->getDb()->exec_SELECTquery('*', 'tt_content',
            'deleted = 0 AND grid_parent = ' . (int)$row['uid']);

        $children = [];
        while ($childRow = $this->getDb()->sql_fetch_assoc($result)) {
            if ($childRow['uid'] != $row['uid']) {
                $children[] = $childRow;
                $children = array_merge($children, $this->getGridChildren($childRow));
            }
        }

        return $children;
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }

    /**
     * @return bool|\TYPO3\CMS\Core\Cache\Frontend\FrontendInterface
     */
    protected static function getCache()
    {
        if (self::$cache === null) {
            if ($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['tx_imiabase']) {
                /** @var CacheManager $cacheManager */
                $cacheManager = $contentCache = GeneralUtility::makeInstance(CacheManager::class);

                try {
                    self::$cache = $cacheManager->getCache('tx_imiabase');
                } catch (\Exception $e) {
                }
            }
        }

        return self::$cache;
    }
}