<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\ContentDefender\Form\FormDataProvider;

use IMIA\ImiaBase\ContentDefender\BackendLayout\BackendLayoutConfiguration;
use TYPO3\CMS\Backend\Form\FormDataProviderInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  ContentDefender
 * @author      David Frerich <d.frerich@imia.de>
 */
class TcaCTypeItems implements FormDataProviderInterface
{
    /**
     * @param array $result
     * @return array
     */
    public function addData(array $result)
    {
        if ('tt_content' !== $result['tableName']) {
            return $result;
        }

        $pageId = !empty($result['effectivePid']) ? (int)$result['effectivePid'] : (int)$result['databaseRow']['pid'];
        $backendLayoutConfiguration = BackendLayoutConfiguration::createFromPageId($pageId);

        $colPos = (int)$result['databaseRow']['colPos'];
        $columnConfiguration = $backendLayoutConfiguration->getConfigurationByColPos($colPos, $result['databaseRow']);
        if (empty($columnConfiguration) || (empty($columnConfiguration['allowed.']) && empty($columnConfiguration['disallowed.']))) {
            return $result;
        }

        $allowedConfiguration = $columnConfiguration['allowed.'] ?? [];
        foreach ($allowedConfiguration as $field => $value) {
            if (empty($result['processedTca']['columns'][$field]['config']['items'])) {
                continue;
            }

            $allowedValues = GeneralUtility::trimExplode(',', $value);
            $result['processedTca']['columns'][$field]['config']['items'] = array_filter(
                $result['processedTca']['columns'][$field]['config']['items'],
                function ($item) use ($allowedValues) {
                    return in_array($item[1], $allowedValues);
                }
            );
        }

        $disallowedConfiguration = $columnConfiguration['disallowed.'] ?? [];
        foreach ($disallowedConfiguration as $field => $value) {
            if (empty($result['processedTca']['columns'][$field]['config']['items'])) {
                continue;
            }

            $disAllowedValues = GeneralUtility::trimExplode(',', $value);
            $result['processedTca']['columns'][$field]['config']['items'] = array_filter(
                $result['processedTca']['columns'][$field]['config']['items'],
                function ($item) use ($disAllowedValues) {
                    return !in_array($item[1], $disAllowedValues);
                }
            );
        }

        return $result;
    }
}
