<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\ContentDefender\BackendLayout;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Backend\View\BackendLayoutView;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  ContentDefender
 * @author      David Frerich <d.frerich@imia.de>
 */
class BackendLayoutConfiguration
{
    /**
     * @var array
     */
    protected $backendLayout;

    /**
     * @var array
     */
    protected static $columnConfiguration = [];

    /**
     * @param array $backendLayout
     */
    public function __construct(array $backendLayout)
    {
        $this->backendLayout = $backendLayout;
    }

    /**
     * @param int $pageId
     * @return BackendLayoutConfiguration
     */
    public static function createFromPageId($pageId)
    {
        $backendLayoutView = GeneralUtility::makeInstance(BackendLayoutView::class);
        $backendLayout = $backendLayoutView->getSelectedBackendLayout($pageId);
        if (null === $backendLayout) {
            $backendLayout = [
                'config' => '',
            ];
        }

        return new self($backendLayout);
    }

    /**
     * @param int $colPos
     * @param array $record
     * @return array
     */
    public function getConfigurationByColPos($colPos, $record = null)
    {
        if (isset($record['grid_parent']) && $record['grid_parent']) {
            $parentRecord = BackendUtility::getRecord('tt_content', $record['grid_parent']);

            $configurationIdentifier = 'grid_' . str_replace('ibcontent_', '', $parentRecord['CType']);
            if (!isset(self::$columnConfiguration[$configurationIdentifier][$colPos])) {
                $tsConfig = BackendUtility::getPagesTSconfig($parentRecord['pid']);
                $config = json_decode($tsConfig['mod.']['ctypes.'][$parentRecord['CType']], true);

                if (is_array($config)) {
                    self::$columnConfiguration[$configurationIdentifier][$colPos] = array_merge([
                        'colPos' => $colPos,
                    ], $config);
                } else {
                    self::$columnConfiguration[$configurationIdentifier][$colPos] = [
                        'colPos' => $colPos,
                    ];
                }
            }
        } else {
            $configurationIdentifier = md5($this->backendLayout['config']);
            if (!isset(self::$columnConfiguration[$configurationIdentifier][$colPos])) {
                if (empty($this->backendLayout['__config']['backend_layout.']['rowCount'])
                    || empty($this->backendLayout['__config']['backend_layout.']['colCount'])
                    || !in_array($colPos, array_map('intval', $this->backendLayout['__colPosList']), true)
                ) {
                    return self::$columnConfiguration[$configurationIdentifier][$colPos] = [];
                }

                $configuration = [];
                foreach ($this->backendLayout['__config']['backend_layout.']['rows.'] as $row) {
                    if (empty($row['columns.'])) {
                        continue;
                    }

                    foreach ($row['columns.'] as $column) {
                        if ($column['colPos'] !== '' && $colPos === (int)$column['colPos']) {
                            $configuration = $column;
                            break 2;
                        }
                    }
                }
                self::$columnConfiguration[$configurationIdentifier][$colPos] = $configuration;
            }
        }

        return self::$columnConfiguration[$configurationIdentifier][$colPos];
    }
}
