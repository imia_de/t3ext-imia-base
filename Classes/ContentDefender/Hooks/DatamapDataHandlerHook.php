<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\ContentDefender\Hooks;

use IMIA\ImiaBase\ContentDefender\BackendLayout\BackendLayoutConfiguration;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;

/**
 * @package     imia_base
 * @subpackage  ContentDefender
 * @author      David Frerich <d.frerich@imia.de>
 */
class DatamapDataHandlerHook extends AbstractDataHandlerHook
{
    /**
     * @var bool
     */
    static public $bypassAccessCheckForRecords = false;

    /**
     * @param DataHandler $dataHandler
     * @return void
     */
    public function processDatamap_beforeStart(DataHandler $dataHandler)
    {
        $datamap = $dataHandler->datamap;
        if (empty($datamap['tt_content']) || $dataHandler->bypassAccessCheckForRecords || self::$bypassAccessCheckForRecords) {
            return;
        }

        foreach ($datamap['tt_content'] as $id => $incomingFieldArray) {
            if (!isset($incomingFieldArray['grid_parent']) || !$incomingFieldArray['grid_parent']) {
                $incomingFieldArray['uid'] = $id;
                if (MathUtility::canBeInterpretedAsInteger($id)) {
                    $incomingFieldArray = array_merge(BackendUtility::getRecord('tt_content', $id), $incomingFieldArray);
                }

                $pageId = (int)$incomingFieldArray['pid'];
                if ($pageId < 0) {
                    $previousRecord = BackendUtility::getRecord('tt_content', abs($pageId), 'pid');
                    $pageId = (int)$previousRecord['pid'];
                    $incomingFieldArray['pid'] = $pageId;
                }
                $colPos = (int)$incomingFieldArray['colPos'];

                $backendLayoutConfiguration = BackendLayoutConfiguration::createFromPageId($pageId);
                $columnConfiguration = $backendLayoutConfiguration->getConfigurationByColPos($colPos, $incomingFieldArray);

                if (!$this->isRecordAllowedByRestriction($columnConfiguration, $incomingFieldArray)) {
                    unset($dataHandler->datamap['tt_content'][$id]);
                    $dataHandler->log(
                        'tt_content',
                        $id,
                        1,
                        $pageId,
                        1,
                        'The record "%s" couldn\'t be saved due to disallowed value(s).',
                        23,
                        [
                            $incomingFieldArray[$GLOBALS['TCA']['tt_content']['ctrl']['label']],
                        ]
                    );
                }

                if (!$this->isRecordAllowedByItemsCount($columnConfiguration, $incomingFieldArray)) {
                    // DataHandler copies a record by first add a new content element (in the old colPos) and then adjust
                    // the colPos information to the target colPos. This means we have to allow this element to be added
                    // even if the maxitems is reached already. The copy command was checked in CmdmapDataHandlerHook.
                    if (empty($dataHandler->cmdmap) && !empty(GeneralUtility::_GP('CB')['paste'])) {
                        continue;
                    }

                    unset($dataHandler->datamap['tt_content'][$id]);
                    $dataHandler->log(
                        'tt_content',
                        $id,
                        1,
                        $pageId,
                        1,
                        'The record "%s" couldn\'t be saved due to reached maxitems configuration of %d.',
                        27,
                        [
                            $incomingFieldArray[$GLOBALS['TCA']['tt_content']['ctrl']['label']],
                            $columnConfiguration['maxitems'],
                        ]
                    );
                }
            }
        }
    }

    public function processDatamap_afterAllOperations(DataHandler $dataHandler)
    {
        $this->contentRepository->substituteNewIdsWithUids($dataHandler->substNEWwithIDs);
    }
}
