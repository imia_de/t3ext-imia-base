<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Utility;

use TYPO3\CMS\Core\Imaging\IconRegistry;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  Utility
 * @author      David Frerich <d.frerich@imia.de>
 */
class ExtensionUtility
{
    /**
     * @param string $extKey
     * @param string $name
     * @param string $path
     * @param string $label
     */
    static public function registerPageIcon($extKey, $name, $path, $label = null)
    {
        $iconPath = ExtensionManagementUtility::siteRelPath($extKey) . $path;

        $GLOBALS['TCA']['pages']['columns']['module']['config']['items'][] = [$label ?: $name, $name, $iconPath];

        /** @var IconRegistry $iconRegistry */
        $iconRegistry = GeneralUtility::makeInstance(IconRegistry::class);
        $iconRegistry->registerIcon(
            'tcarecords-pages-contains-' . $name,
            \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
            ['source' => $iconPath]
        );
        $GLOBALS['TCA']['pages']['ctrl']['typeicon_classes']['contains-' . $name] = 'tcarecords-pages-contains-' . $name;
    }

    /**
     * @param string $table
     * @param string|array $fields
     * @param mixed $condition
     * @param string $requestUpdateField
     */
    static public function addDisplayCond($table, $fields, $condition, $requestUpdateField = null)
    {
        if (!is_array($fields)) {
            $fields = [$fields];
        }

        foreach ($fields as $field) {
            if ($GLOBALS['TCA'][$table]['columns'][$field]['displayCond']) {
                if (is_array($GLOBALS['TCA'][$table]['columns'][$field]['displayCond'])) {
                    if (isset($GLOBALS['TCA'][$table]['columns'][$field]['displayCond']['AND'])) {
                        $GLOBALS['TCA'][$table]['columns'][$field]['displayCond']['AND'][] = $condition;
                    } else {
                        $GLOBALS['TCA'][$table]['columns'][$field]['displayCond'] = ['AND' => [
                            'AND' => $GLOBALS['TCA'][$table]['columns'][$field]['displayCond'],
                            $condition,
                        ]];
                    }
                } else {
                    $GLOBALS['TCA'][$table]['columns'][$field]['displayCond'] = ['AND' => [
                        $GLOBALS['TCA'][$table]['columns'][$field]['displayCond'],
                        $condition,
                    ]];
                }
            } else {
                $GLOBALS['TCA'][$table]['columns'][$field]['displayCond'] = $condition;
            }

            if ($requestUpdateField) {
                $GLOBALS['TCA'][$table]['columns'][$field]['onChange'] = 'reload';
            }
        }
    }
}