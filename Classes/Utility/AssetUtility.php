<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Utility;

use IMIA\ImiaBase\Utility\AsseticParser;
use TYPO3\CMS\Core\TypoScript\ExtendedTemplateService;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  Utility
 * @author      David Frerich <d.frerich@imia.de>
 */
class AssetUtility
{
    /**
     * @param array $extension
     * @param boolean $force
     * @return array
     */
    static public function parseAssets($extension = null, $force = false)
    {
        $extensions = [];
        if ($extension) {
            $extensions[] = $extension;
        } else {
            $extensions = self::getExtensionAssets();
        }

        /** @var AsseticParser $asseticParser */
        $asseticParser = GeneralUtility::makeInstance(AsseticParser::class);

        $parsedAssets = [];
        foreach ($extensions as $extension) {
            foreach ($extension['config'] as $extensionConfig) {
                $parsedAssets = array_merge($parsedAssets, $asseticParser->parse($extensionConfig['setup'], $force));
                foreach ($extensionConfig['pages'] as $pageConfig) {
                    $parsedAssets = array_merge($parsedAssets, $asseticParser->parse($pageConfig, $force));
                }
            }
        }

        $compiledAssets = [];
        foreach ($parsedAssets as $parsedAsset) {
            $compiledAssets[] = str_replace('typo3conf/ext/' . $extension['ext_key'] . '/', '', $parsedAsset);
        }
        sort($compiledAssets);

        return $compiledAssets;
    }

    /**
     * @return array
     */
    static public function getExtensionAssets()
    {
        $extensions = [];
        $typoScriptExtensions = self::getExtensionTypoScripts();
        foreach ($typoScriptExtensions as $extension) {
            $hasAsset = false;
            $extension['config'] = [];
            foreach ($extension['typoScripts'] as $typoScript) {
                $hasConfigAsset = false;
                $setup = self::loadTypoScript([$typoScript]);
                $pageConfigs = self::findTypoScriptPages($setup);
                $config = [
                    'setup' => $setup,
                    'pages' => [],
                ];

                if (count($pageConfigs) > 0) {
                    foreach ($pageConfigs as $pageConfig) {
                        $pageSetup = $setup;
                        foreach (explode('.', $pageConfig) as $pageKey) {
                            $pageSetup = $pageSetup[$pageKey . '.'];
                        }

                        if (isset($pageSetup['assetic.'])) {
                            $hasAsset = true;
                            $hasConfigAsset = true;
                            $config['pages'][] = $pageSetup;
                        }
                    }
                }
                if (isset($setup['config.']['assetic.']) && $setup['config.']['assetic.']['enabled']) {
                    $hasAsset = true;
                    $hasConfigAsset = true;
                }

                if ($hasConfigAsset) {
                    $extension['config'][] = $config;
                }
            }

            if ($hasAsset) {
                self::getAssetsForExtension($extension);

                $extensions[$extension['ext_key']] = $extension;
            }
        }

        return $extensions;
    }

    /**
     * @param array $extension
     */
    static private function getAssetsForExtension(&$extension)
    {
        $extension['assets'] = [];
        foreach ($extension['config'] as $config) {
            if (isset($config['setup']['config.']['assetic.'])) {
                foreach ($config['setup']['config.']['assetic.'] as $name => $assetCollection) {
                    if (is_array($assetCollection) && array_key_exists('output', $assetCollection)
                        && array_key_exists('enabled', $assetCollection) && $assetCollection['enabled']
                        && array_key_exists('files.', $assetCollection) && is_array($assetCollection['files.'])
                    ) {
                        $extension['assets'][] = preg_replace('/EXT:[^\/]+\//ism', '', $assetCollection['output']) .
                            ((int)$assetCollection['enabled'] > 1 ? ' [SHY]' : '');;
                    }
                }
            }
            foreach ($config['pages'] as $pageConfig) {
                if (isset($pageConfig['assetic.'])) {
                    foreach ($pageConfig['assetic.'] as $name => $assetCollection) {
                        if (is_array($assetCollection) && array_key_exists('output', $assetCollection)
                            && array_key_exists('enabled', $assetCollection) && $assetCollection['enabled']
                            && array_key_exists('files.', $assetCollection) && is_array($assetCollection['files.'])
                        ) {
                            $extension['assets'][] = preg_replace('/EXT:[^\/]+\//ism', '', $assetCollection['output']) .
                                ((int)$assetCollection['enabled'] > 1 ? ' [SHY]' : '');
                        }
                    }
                }
            }
        }

        $extension['assets'] = array_unique($extension['assets']);
        sort($extension['assets']);
    }

    /**
     * @param array $setup
     * @return array
     */
    static private function findTypoScriptPages($setup)
    {
        $pages = [];
        foreach ($setup as $key => $value) {
            if ($value == 'PAGE') {
                $pages[] = $key;
            } elseif (is_array($value)) {
                $subpages = self::findTypoScriptPages($value);
                foreach ($subpages as $subpage) {
                    $pages[] = $key . $subpage;
                }
            }
        }

        return $pages;
    }

    /**
     * @param array $staticTypoScriptPaths
     * @return array
     */
    static private function loadTypoScript($staticTypoScriptPaths)
    {
        // workaround fix
        array_unshift($staticTypoScriptPaths, 'EXT:imia_base/Configuration/TypoScript');

        $typoScript = self::getTypoScriptService();
        $typoScript->tt_track = 0;
        $typoScript->includeStaticTypoScriptSources('0', 0, 0, [
            'includeStaticAfterBasedOn' => 1,
            'static_file_mode'          => 3,
            'include_static_file'       => implode(',', $staticTypoScriptPaths),
        ]);
        $typoScript->generateConfig();

        return $typoScript->setup;
    }

    /**
     * @return ExtendedTemplateService
     */
    static private function getTypoScriptService()
    {
        /** @var $extendedTemplateService ExtendedTemplateService */
        $extendedTemplateService = GeneralUtility::makeInstance(ExtendedTemplateService::class);

        return $extendedTemplateService;
    }

    /**
     * @return array
     */
    static private function getExtensionTypoScripts()
    {
        $extensions = [];
        foreach (self::getStaticTypoScripts() as $staticTypoScript) {
            preg_match('/EXT:([^\/]+)/ism', $staticTypoScript, $match);
            $extKey = $match[1];
            if ($extKey && ExtensionManagementUtility::isLoaded($extKey)) {
                if (!array_key_exists($extKey, $extensions)) {
                    if (strpos(ExtensionManagementUtility::extPath($extKey), PATH_site . 'typo3conf/ext') !== false) {
                        $extensions[$extKey] = [
                            'ext_key'     => $extKey,
                            'typoScripts' => [],
                        ];

                        $EM_CONF = [];
                        @include(ExtensionManagementUtility::extPath($extKey, 'ext_emconf.php'));
                        $emConf = is_array($EM_CONF) ? $EM_CONF : [null];
                        $extensions[$extKey]['em_conf'] = array_shift($emConf);
                    } else {
                        $extensions[$extKey] = false;
                    }
                }

                if ($extensions[$extKey]) {
                    $extensions[$extKey]['typoScripts'][] = $staticTypoScript;
                }
            }
        }

        $localExtensions = [];
        foreach ($extensions as $extension) {
            if ($extension) {
                $localExtensions[] = $extension;
            }
        }

        return $localExtensions;
    }

    /**
     * @return array
     */
    static private function getStaticTypoScripts()
    {
        $staticTypoScripts = [];
        foreach ($GLOBALS['TCA']['sys_template']['columns']['include_static_file']['config']['items'] as $typoScript) {
            $staticTypoScripts[] = $typoScript[1];
        }

        return $staticTypoScripts;
    }
}