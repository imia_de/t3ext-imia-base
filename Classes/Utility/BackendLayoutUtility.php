<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutinos (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Utility;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  ViewHelpers
 * @author      Lars Siemon <l.siemon@imia.de>
 */
class BackendLayoutUtility
{
    /**
     * Returns the edit link for the record or only for the specified columns.
     * @param $returnPageUid
     * @param $pageUid
     * @param $table
     * @param $columns
     * @return string
     */
    public static function getRecordEditLink($returnPageUid, $pageUid, $table, $columns = null)
    {
        $url = null;
        $returnUrl = BackendUtility::getModuleUrl('web_layout', [
            'id' => $returnPageUid ?: $pageUid,
        ]);
        $uriBuilder = GeneralUtility::makeInstance('TYPO3\CMS\Backend\Routing\UriBuilder');

        $linkConfig = [
            'edit'      => [
                $table => [
                    $pageUid => 'edit',
                ],
            ],
            'returnUrl' => $returnUrl,
        ];

        if ($columns) {
            $linkConfig['columnsOnly'] = $columns;
        }

        return $uriBuilder->buildUriFromRoute('record_edit', [$linkConfig]);
    }

    /**
     * Returns the the edit icon html.
     * @return string
     */
    public static function getEditIconHtml()
    {
        $webRoot = str_replace(GeneralUtility::getIndpEnv('TYPO3_DOCUMENT_ROOT'), '', PATH_site);

        return '
            <span class="t3js-icon icon icon-size-small icon-state-default icon-actions-document-open" data-identifier="actions-document-open">
                <span class="icon-markup">
                    <img src="' . $webRoot . 'typo3/sysext/core/Resources/Public/Icons/T3Icons/actions/actions-document-open.svg" width="16" height="16">
                </span>
            </span>';
    }
}