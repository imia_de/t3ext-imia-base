<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Utility;

use IMIA\Assetic\AssetBuilder;
use Assetic\AssetWriter;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  Utility
 * @author      David Frerich <d.frerich@imia.de>
 */
class AsseticParser
{
    /**
     * @var array
     */
    protected static $compiledAssets = [];

    /**
     * @var string
     */
    protected $appendToBody;

    /**
     * @var array
     */
    protected $settings = [];

    /**
     * @param array $config
     * @param boolean $forceParsing
     * @return array
     */
    public function parse($config, $forceParsing = false)
    {
        if (isset($config['assetic.']['settings.'])) {
            $this->settings = $this->parseTSSetup((array)$config['assetic.']['settings.']);
        }
        $assets = $this->addAssets((array)$this->parseTSSetup($config['assetic.']), $forceParsing); // global

        return $assets;
    }

    /**
     * @param array $setup
     * @return array
     */
    protected function parseTSSetup($setup)
    {
        $config = [];
        if (is_array($setup)) {
            foreach ($setup as $key => $value) {
                if (is_array($value)) {
                    $value = $this->parseTSSetup($value);
                } elseif (is_string($value) && strpos($value, 'EXT:') === 0) {
                    $value = preg_replace('/^EXT:[ ]*/', 'typo3conf/ext/', $value);
                }

                if (strrpos($key, '.') === strlen($key) - 1) {
                    $key = substr($key, 0, -1);
                }

                $oldValue = null;
                if ($config[$key]) {
                    $oldValue = $config[$key];
                }
                $config[$key] = $value;

                if ($oldValue !== null) {
                    $config[$key]['_'] = $oldValue;
                }
            }
        }

        return $config;
    }

    /**
     * @param array $config
     * @param boolean $forceParsing
     * @return array
     */
    public function addAssets($config, $forceParsing = false)
    {
        $compiledAssets = [];
        if (array_key_exists('enabled', $config) && $config['enabled'] == 1) {
            unset($config['enabled']);

            foreach ($config as $name => $assetCollection) {
                if (is_array($assetCollection) && array_key_exists('output', $assetCollection)
                    && array_key_exists('files', $assetCollection) && is_array($assetCollection['files'])
                    && array_key_exists('enabled', $assetCollection) && $assetCollection['enabled']
                ) {
                    $asseticCompile = $forceParsing;

                    $requireJS = false;
                    if ($assetCollection['requireJS']) {
                        $requireJS = true;
                    }

                    if (file_exists(PATH_site . $assetCollection['output'])) {
                        $time = filemtime(PATH_site . $assetCollection['output']);
                    } else {
                        $time = 0;
                    }

                    $cacheTime = $time;
                    if (strtolower($_REQUEST['assetic']) == ($name)) {
                        $asseticCompile = true;
                    } elseif ($assetCollection['enabled'] == 2 && in_array(strtolower($_REQUEST['assetic']), ['all', 'compile'])) {
                        $asseticCompile = true;
                    } elseif ($assetCollection['enabled'] == 1) {
                        $files = array_merge(is_array($assetCollection['files']) ? $assetCollection['files'] : [],
                            is_array($assetCollection['monitorFiles']) ? $assetCollection['monitorFiles'] : []);

                        if ($requireJS) {
                            $files = array_merge($files,
                                is_array($assetCollection['staticFiles']) ? $assetCollection['staticFiles'] : []);

                            if ($assetCollection['appFile']) {
                                $files[] = $assetCollection['appFile'];
                            }
                        }

                        foreach ($files as $file) {
                            $exclude = [];
                            if (is_array($file)) {
                                if ($file['_']) {
                                    $exclude = array_map('trim', explode(',', $file['exclude']));
                                    $file = $file['_'];
                                } else {
                                    continue;
                                }
                            }

                            if (strpos($file, '*')) {
                                $dir = substr($file, 0, strpos($file, '*'));
                                $ext = substr($file, strpos($file, '*') + 1);

                                $handle = dir(PATH_site . $dir);
                                if ($handle) {
                                    while (false !== ($entry = $handle->read())) {
                                        if ($entry != '.' && $entry != '..') {
                                            if (preg_match('/' . preg_quote($ext) . '$/ism', $entry)) {
                                                if (!in_array(substr($entry, 0, strlen($entry) - strlen($ext)), $exclude)) {
                                                    if (file_exists(PATH_site . $dir . $entry)) {
                                                        $filetime = filemtime(PATH_site . $dir . $entry);
                                                    } else {
                                                        $filetime = time();
                                                    }

                                                    if ($filetime > $time) {
                                                        if ($filetime > $cacheTime) {
                                                            $cacheTime = $filetime;
                                                        }
                                                        $asseticCompile = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    $handle->close();
                                }
                            } else {
                                if (file_exists(PATH_site . $file)) {
                                    $filetime = filemtime(PATH_site . $file);
                                } else {
                                    $filetime = time();
                                }

                                if ($filetime > $time) {
                                    if ($filetime > $cacheTime) {
                                        $cacheTime = $filetime;
                                    }
                                    $asseticCompile = true;
                                }
                            }
                        }
                    }

                    if ($requireJS) {
                        $compiledAssets = array_merge($compiledAssets,
                            $this->addRequireJSAsset($assetCollection, $asseticCompile, $cacheTime)
                        );
                    } else {
                        if ($asseticCompile) {
                            if (!in_array($assetCollection['output'], self::$compiledAssets)) {
                                $this->createAssetCollection($assetCollection);
                                self::$compiledAssets[] = $assetCollection['output'];
                                $compiledAssets[] = $assetCollection['output'];
                            }
                        }
                        if (!is_array($GLOBALS['TSFE']->pSetup)) {
                            $GLOBALS['TSFE']->pSetup = [];
                        }

                        $priority = 'z' . str_pad($assetCollection['priority'] ?: '', 4, '0', STR_PAD_LEFT) . '_';
                        switch (substr($assetCollection['output'], strrpos($assetCollection['output'], '.'))) {
                            case '.js':
                                $javascriptTS = 'includeJS.';
                                if ($assetCollection['location'] == 'footer') {
                                    $javascriptTS = 'includeJSFooter.';
                                }
                                if ($GLOBALS['TSFE']) {
                                    if (!is_array($GLOBALS['TSFE']->pSetup[$javascriptTS])) {
                                        $GLOBALS['TSFE']->pSetup[$javascriptTS] = [];
                                    }
                                    $GLOBALS['TSFE']->pSetup[$javascriptTS][$priority . $name] = $assetCollection['output'];
                                    ksort($GLOBALS['TSFE']->pSetup[$javascriptTS]);
                                }
                                break;
                            case '.css':
                            default:
                                if ($assetCollection['location'] != 'none') {
                                    if ($GLOBALS['TSFE']) {
                                        if (!is_array($GLOBALS['TSFE']->pSetup['includeCSS.'])) {
                                            $GLOBALS['TSFE']->pSetup['includeCSS.'] = [];
                                        }
                                        $GLOBALS['TSFE']->pSetup['includeCSS.'][$priority . $name] = $assetCollection['output'];
                                        if ($assetCollection['media']) {
                                            $GLOBALS['TSFE']->pSetup['includeCSS.'][$priority . $name . '.']['media'] = $assetCollection['media'];
                                        }
                                        ksort($GLOBALS['TSFE']->pSetup['includeCSS.']);
                                    }
                                }
                                break;
                        }
                    }
                }
            }
        }

        return $compiledAssets;
    }

    /**
     * @return string
     */
    public function getAppendToBody()
    {
        return $this->appendToBody;
    }

    /**
     * @param array $assetCollection
     * @param boolean $asseticCompile
     * @return array
     */
    protected function addRequireJSAsset($assetCollection, $asseticCompile = false, $cacheTime)
    {
        $compiledAssets = [];
        if ($asseticCompile && !in_array($assetCollection['output'], self::$compiledAssets)) {
            if (!$assetCollection['outputDir']) {
                $assetCollection['outputDir'] = dirname($assetCollection['output'] . '/requirejs');
            }
            $this->ensureDirExists($assetCollection['outputDir']);

            $staticFiles = [];
            foreach (is_array($assetCollection['staticFiles']) ? $assetCollection['staticFiles'] : [] AS $name => $file) {
                $staticFileFiles = [];
                $exclude = [];
                if (is_array($file)) {
                    if ($file['_']) {
                        $exclude = array_map('trim', explode(',', $file['exclude']));
                        $file = $file['_'];
                    } else {
                        continue;
                    }
                }

                if (strpos($file, '*')) {
                    $dir = substr($file, 0, strpos($file, '*'));
                    $ext = substr($file, strpos($file, '*') + 1);

                    $handle = dir(PATH_site . $dir);
                    if ($handle) {
                        while (false !== ($entry = $handle->read())) {
                            if ($entry != '.' && $entry != '..') {
                                if (preg_match('/' . preg_quote($ext) . '$/ism', $entry)) {
                                    if (!in_array(substr($entry, 0, strlen($entry) - strlen($ext)), $exclude)) {
                                        if (file_exists(PATH_site . $dir . $entry)) {
                                            $staticFileFiles[] = $dir . $entry;
                                        }
                                    }
                                }
                            }
                        }
                        $handle->close();
                    }
                } else {
                    if (file_exists(PATH_site . $file)) {
                        $staticFileFiles[] = $file;
                    }
                }

                sort($staticFileFiles);
                $staticFile = $assetCollection['outputDir'] . '/' . $name . '.js';
                $this->createAssetCollection([
                    'files'  => $staticFileFiles,
                    'output' => $staticFile,
                ]);

                $minifier = new \MatthiasMullie\Minify\JS();
                $minifier->add(file_get_contents(PATH_site . $staticFile));
                $minifier->minify(PATH_site . $staticFile);

                $staticFiles[$name] = $staticFile;

                $compiledAssets[] = $staticFile;
            }

            $files = [];
            foreach (is_array($assetCollection['files']) ? $assetCollection['files'] : [] as $key => $file) {
                $exclude = [];
                if (is_array($file)) {
                    if ($file['_']) {
                        $exclude = array_map('trim', explode(',', $file['exclude']));
                        $file = $file['_'];
                    } else {
                        continue;
                    }
                }

                if (strpos($file, '*')) {
                    $dir = substr($file, 0, strpos($file, '*'));
                    $ext = substr($file, strpos($file, '*') + 1);

                    $relDir = '/';
                    if (is_array($assetCollection['baseDirs'])) {
                        foreach ($assetCollection['baseDirs'] as $path) {
                            if (stripos($dir, $path) !== false) {
                                $relDir = str_replace($path, '', $dir);

                                if (substr($relDir, 0, 1) != '/') {
                                    $relDir = '/' . $relDir;
                                }
                                if (substr($relDir, strlen($relDir) - 1) != '/') {
                                    $relDir .= '/';
                                }
                                break;
                            }
                        }
                    }

                    $handle = dir(PATH_site . $dir);
                    if ($handle) {
                        while (false !== ($entry = $handle->read())) {
                            if ($entry != '.' && $entry != '..') {
                                if (preg_match('/' . preg_quote($ext) . '$/ism', $entry)) {
                                    if (!in_array(substr($entry, 0, strlen($entry) - strlen($ext)), $exclude)) {
                                        if (file_exists(PATH_site . $dir . $entry)) {
                                            $files[] = $dir . $entry;

                                            $this->ensureDirExists($assetCollection['outputDir'] . $relDir);
                                            $this->createAssetCollection([
                                                'files'  => [$dir . $entry],
                                                'output' => $assetCollection['outputDir'] . $relDir . $entry,
                                            ]);
                                            $compiledAssets[] = $assetCollection['outputDir'] . $relDir . $entry;
                                        }
                                    }
                                }
                            }
                        }
                        $handle->close();
                    }
                } else {
                    if (file_exists(PATH_site . $file)) {
                        $files[] = PATH_site . $file;

                        $this->createAssetCollection([
                            'files'  => [$file],
                            'output' => $assetCollection['outputDir'] . '/' . $file,
                        ]);
                        $compiledAssets[] = $assetCollection['outputDir'] . '/' . $file;
                    }
                }
            }

            if ($assetCollection['appFile']) {
                $this->createAssetCollection([
                    'files'  => [$assetCollection['appFile']],
                    'output' => $assetCollection['outputDir'] . '/app.js',
                ]);
                $compiledAssets[] = $assetCollection['outputDir'] . '/app.js';

                $appDir = dirname($assetCollection['appFile']) . '/';
                $concatinatedJs = $this->concatRequireJsAssets(
                    PATH_site . $assetCollection['appFile'],
                    PATH_site . $appDir);

                $minifier = new \MatthiasMullie\Minify\JS();
                $minifier->add($concatinatedJs);
                $minifier->minify(PATH_site . $assetCollection['outputDir'] . '/app_concat.js');

                $compiledAssets[] = $assetCollection['outputDir'] . '/app_concat.js';
            }

            $requireJsInfo = 'typo3temp/requirejs.info.js';
            file_put_contents(PATH_site . $requireJsInfo, "\n" . '// build time ' . time());

            $this->createAssetCollection([
                'files'  => [$assetCollection['requireJS'], $requireJsInfo],
                'output' => $assetCollection['output'],
            ]);

            unlink(PATH_site . $requireJsInfo);

            $compiledAssets[] = $assetCollection['output'];
            self::$compiledAssets[] = $assetCollection['output'];
        }

        $paths = '';
        foreach ($assetCollection['libs'] as $libName => $libUrl) {
            if (strpos($libUrl, '*')) {
                $dir = substr($libUrl, 0, strpos($libUrl, '*'));
                $ext = substr($libUrl, strpos($libUrl, '*') + 1);

                if (is_dir($dir)) {
                    $handle = dir(PATH_site . $dir);
                    if ($handle) {
                        while (false !== ($entry = $handle->read())) {
                            if ($entry != '.' && $entry != '..') {
                                if (preg_match('/' . preg_quote($ext) . '$/ism', $entry)) {
                                    if (file_exists(PATH_site . $dir . $entry)) {
                                        $files[] = $dir . $entry;
                                        $paths .= "\n                            " . '"' . rtrim($entry, '.js') . '": "' . rtrim('/' . $dir . $entry, '.js') . '",';
                                    }
                                }
                            }
                        }
                        $handle->close();
                    }
                }
            } else {
                if (!preg_match('/^((http|https):)?(\/)?\//ism', $libUrl)) {
                    $libUrl = '/' . $libUrl;
                }

                $paths .= "\n                            " . '"' . $libName . '": "' . rtrim($libUrl, '.js') . '",';
            }
        }
        if ($paths) {
            $paths = substr($paths, 0, -1);
        }

        $shim = '';
        if (is_array($assetCollection['dependencies'])) {
            foreach ($assetCollection['dependencies'] as $name => $dependencies) {
                $shim .= "\n                            " . '"' . $name . '": { deps: ["' . implode('", "',
                        array_map('trim', explode(',', $dependencies))) . '"] },';
            }
        }
        if ($shim) {
            $shim = substr($shim, 0, -1);
        }

        $this->appendToBody .= '
                <script src="' . $assetCollection['output'] . '?t=' . $cacheTime . '"></script>
                <script>
                    requirejs.config({
                        "urlArgs": "t=' . $cacheTime . '",
                        "baseUrl": "' . ($GLOBALS['TSFE']->tmpl->setup['config.']['cdnURL'] ?: '') . $assetCollection['outputDir'] . '"' . ($paths ? ',
                        "paths": {' . $paths . '
                        }' : '') . ($shim ? ',
                        "shim": {' . $shim . '
                        }' : '') . '
                    });';

        if ($GLOBALS['TSFE']->tmpl->setup['config.']['concatenateJs']) {
            $this->appendToBody .= '
                    requirejs(["app_concat"]);';
        } else {
            $this->appendToBody .= '
                    requirejs(["app"]);';
        }

        $this->appendToBody .= '
                </script>';

        return $compiledAssets;
    }

    /**
     * @param string $filePath
     * @param string $appDir
     * @return string
     */
    protected function concatRequireJsAssets($filePath, $appDir)
    {
        $concatinatedJs = '';
        $appContents = file_get_contents($filePath);
        $paths = [];

        if (preg_match_all('/requirejs[ ]*\([ ]*\[(.+?)\]/ism', $appContents, $matches)) {

            if (preg_match_all('/require\.config\(\{\s*paths:\s*\{(.+?)\}/ism', $appContents, $pathMatches)) {
                foreach ($pathMatches[1] as $pathMatch) {
                    $item = str_replace( "'", '"', $pathMatch);
                    $item = str_replace( [ "\r\n","\n"], '', $item);
                    $item = '{' . $item . '}';
                    $pathJson = json_decode($item);
                    foreach ($pathJson as $name => $path) {
                        $paths[$name] = $path;
                    }
                }
            }

            foreach ($matches[1] as $match) {
                foreach (explode(',', $match) as $item) {
                    $item = str_replace(['"', "'", ' ', "\r\n","\n"], '', $item);
                    if (isset($paths[$item])) {
                        $filePath = $appDir . $paths[$item];
                    } else {
                        $filePath = $appDir . $item;
                    }
                    $filePath .= '.js';
                    if (file_exists($filePath)) {
                        $concatinatedJs .= $this->concatRequireJsAssets($filePath, $appDir);
                    }
                }
            }
        }

        $concatinatedJs .= "\n" . $appContents;

        return $concatinatedJs;
    }

    /**
     * @param array $config
     */
    protected function createAssetCollection($config)
    {
        $settings = [
            'rootPath'  => PATH_site,
            'cachePath' => PATH_site . 'typo3temp',
            'javaPath'  => $this->settings['javaPath'],
            'compile'   => [
                'filters' => array_map('trim', explode(',', $config['filters'])),
                'output'  => $config['output'],
            ],
            'filters'   => $this->getFilterSettings($config),
        ];

        $assetBuilder = new AssetBuilder($settings);
        $asset = $assetBuilder->build($config['files']);

        $this->ensureDirExists(dirname($settings['compile']['output']));
        $aw = new AssetWriter($settings['rootPath']);
        $aw->writeAsset($asset);
    }

    /**
     * @param array $config
     * @return array
     */
    protected function getFilterSettings($config)
    {
        $settings = $this->settings;
        unset($settings['javaPath']);

        if (array_key_exists('compass', $settings) && array_key_exists('plugins', $settings['compass'])) {
            $settings['compass']['plugins'] = array_map('trim', explode(',', $settings['compass']['plugins']));
        }
        if (array_key_exists('sassc', $settings) && array_key_exists('loadPaths', $settings['sassc'])) {
            $settings['sassc']['loadPaths'] = array_values($settings['sassc']['loadPaths']);
        }

        $collectionSettings = (array)$config['settings'];
        ArrayUtility::mergeRecursiveWithOverrule($collectionSettings, $settings);

        return $collectionSettings;
    }

    /**
     * @param string $dir
     */
    protected function ensureDirExists($dir)
    {
        if (!is_dir($dir)) {
            $this->ensureDirExists(dirname($dir));
            @mkdir($dir);
            @chmod($dir, 0775);
        }
    }
}