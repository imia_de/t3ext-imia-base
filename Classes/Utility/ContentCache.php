<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Utility;

use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  Utility
 * @author      David Frerich <d.frerich@imia.de>
 */
class ContentCache
{
    /**
     * @var \TYPO3\CMS\Core\Cache\Frontend\FrontendInterface|boolean
     */
    static protected $contentCache = null;

    /**
     * @return boolean
     */
    static public function enabled()
    {
        $extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['imia_base']);

        return (bool)$extConf['contentCache'];
    }

    /**
     * @param string $identifier
     * @return mixed
     */
    static public function get($identifier)
    {
        return self::getCache()->get($identifier);
    }

    /**
     * @param string $identifier
     * @param mixed $data
     * @param array $tags
     * @param integer $lifetime
     */
    static public function set($identifier, $data, array $tags = [], $lifetime = null)
    {
        self::getCache()->set($identifier, $data, $tags, $lifetime);
    }

    /**
     * @param string $identifier
     * @return boolean
     */
    static public function remove($identifier)
    {
        return self::getCache()->remove($identifier);
    }

    /**
     * @param string $tag
     */
    static public function flushByTag($tag)
    {
        self::getCache()->flushByTag($tag);
    }

    /**
     */
    static public function flush()
    {
        self::getCache()->flush();
    }

    /**
     * @return bool|\TYPO3\CMS\Core\Cache\Frontend\FrontendInterface
     */
    static protected function getCache()
    {
        if (self::$contentCache === null) {
            if ($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['tx_imiabase_content']) {
                /** @var CacheManager $cacheManager */
                $cacheManager = $contentCache = GeneralUtility::makeInstance(CacheManager::class);

                try {
                    self::$contentCache = $cacheManager->getCache('tx_imiabase_content');
                } catch (\Exception $e) {
                }
            }
        }

        return self::$contentCache;
    }
}