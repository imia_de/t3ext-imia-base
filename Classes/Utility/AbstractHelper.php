<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Utility;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Lang\LanguageService;

/**
 * @package     imia_base
 * @subpackage  Utility
 * @author      David Frerich <d.frerich@imia.de>
 * @author      Sascha Lorenz <s.lorenz@imia.de>
 */
abstract class AbstractHelper
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var array
     */
    protected $settings;

    /**
     * @param string $content
     * @param array $arguments
     * @param string $extensionName
     * @param string $pluginName
     * @param string $controllerName
     * @param string $controllerActionName
     * @return string
     */
    protected function renderFluidContent($content, $arguments = [],
                                          $extensionName = 'ImiaBase', $pluginName = 'user', $controllerName = 'User', $controllerActionName = 'user')
    {
        if (trim($content)) {
            $tempPath = GeneralUtility::tempnam('base_' . md5($content) . '_' .
                substr(sha1(md5(uniqid(microtime()))), 0, 10), '.temp');

            if (!is_dir(dirname($tempPath))) {
                mkdir(dirname($tempPath), 0777, true);
            }

            file_put_contents($tempPath, $content);
            $rendered = $this->renderTemplate($tempPath, $arguments, 'html',
                $extensionName, $pluginName, $controllerName, $controllerActionName);
            unlink($tempPath);
        } else {
            $rendered = '';
        }

        return $rendered;
    }

    /**
     * @param string $templatePathAndFilename
     * @param array $arguments
     * @param string $format
     * @param string $extensionName
     * @param string $pluginName
     * @param string $controllerName
     * @param string $controllerActionName
     * @return string
     */
    protected function renderTemplate($templatePathAndFilename, $arguments, $format = 'html',
                                      $extensionName = 'ImiaBase', $pluginName = 'user', $controllerName = 'User', $controllerActionName = 'user')
    {
        $view = $this->getObjectManager()->get('TYPO3\CMS\Fluid\View\StandaloneView');
        $view->setFormat($format);
        $view->getRequest()->setControllerExtensionName($extensionName);
        $view->getRequest()->setPluginName($pluginName);
        $view->getRequest()->setControllerName($controllerName);
        $view->getRequest()->setControllerActionName($controllerActionName);

        $partialRootPath = $this->getSetting('partialRootPath');
        if (method_exists($view, 'setPartialRootPaths')) {
            $view->setPartialRootPaths([$partialRootPath]);
        } else {
            $view->setPartialRootPath($partialRootPath);
        }
        $layoutRootPath = $this->getSetting('layoutRootPath');
        if (method_exists($view, 'setLayoutRootPaths')) {
            $view->setLayoutRootPaths([$layoutRootPath]);
        } else {
            $view->setLayoutRootPath($layoutRootPath);
        }

        $view->setTemplatePathAndFilename($templatePathAndFilename);
        if ($arguments['settings'] && is_array($arguments['settings'])) {
            ArrayUtility::mergeRecursiveWithOverrule($arguments['settings'], $this->getSettings());
        } else {
            $arguments['settings'] = $this->getSettings();
        }

        $view->assignMultiple($arguments);

        return $view->render();
    }

    /**
     * @param array $settings
     */
    protected function initSettings($settings)
    {
        if ($settings && is_array($settings)) {
            $this->settings = $this->getObjectManager()->get('TYPO3\CMS\Extbase\Service\TypoScriptService')
                ->convertTypoScriptArrayToPlainArray($settings);
        }
    }

    /**
     * @param string $key
     * @return mixed
     */
    protected function getSetting($key)
    {
        if (strpos($key, '.') === false) {
            $value = $this->settings[$key];
        } else {
            $keys = explode('.', $key);

            $value = $this->settings[array_shift($keys)];
            foreach ($keys as $subKey) {
                if ($value && is_array($value)) {
                    $value = $value[$subKey];
                } else {
                    $value = null;
                    break;
                }
            }
        }

        if (is_array($value)) {
            return $this->renderTSObj($value);
        } else {
            return $value;
        }
    }

    /**
     * @return array
     */
    protected function getSettings()
    {
        if ($this->settings && is_array($this->settings)) {
            foreach ($this->settings as $key => $setting) {
                $this->settings[$key] = $this->getSetting($key);
            }
        }

        return $this->settings;
    }

    /**
     * @param array $config
     * @return mixed
     */
    protected function renderTSObj(array $config)
    {
        if (array_key_exists('_typoScriptNodeValue', $config) && is_object($GLOBALS['TSFE']->cObj)) {
            $contentType = $config['_typoScriptNodeValue'];
            unset($config['_typoScriptNodeValue']);
            $contentConfig = $this->getObjectManager()->get('TYPO3\CMS\Extbase\Service\TypoScriptService')
                ->convertPlainArrayToTypoScriptArray($config);

            return $GLOBALS['TSFE']->cObj->cObjGetSingle($contentType, $contentConfig);
        } else {
            return $config;
        }
    }

    /**
     * @return ObjectManager
     */
    protected function getObjectManager()
    {
        if (!$this->objectManager) {
            $this->objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
        }

        return $this->objectManager;
    }

    /**
     * @param string $path
     * @return string
     */
    protected static function translate($path)
    {
        if (self::getLanguageService()) {
            return self::getLanguageService()->sL($path);
        } else {
            return LocalizationUtility::translate($path, 'ImiaUsu');
        }
    }

    /**
     * @return LanguageService
     */
    protected static function getLanguageService()
    {
        return $GLOBALS['LANG'];
    }

    /**
     * @return DatabaseConnection
     */
    protected static function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }

    /**
     * @param string $table
     * @return string
     */
    protected static function enableFields($table)
    {
        if ($GLOBALS['TSFE'] && $GLOBALS['TSFE']->sys_page) {
            return $GLOBALS['TSFE']->sys_page->enableFields($table);
        } else {
            return BackendUtility::BEenableFields($table) . BackendUtility::deleteClause($table);
        }
    }
}