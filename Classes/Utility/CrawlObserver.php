<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Utility;

use IMIA\ImiaBase\Command\CrawlCommandController;
use Spatie\Crawler\Url;

/**
 * @package     imia_base
 * @subpackage  Utility
 * @author      David Frerich <d.frerich@imia.de>
 */
class CrawlObserver implements \Spatie\Crawler\CrawlObserver
{
    /**
     * @var CrawlCommandController
     */
    protected $command;

    /**
     * @param CrawlCommandController $command
     */
    public function __construct(CrawlCommandController $command)
    {
        $this->command = $command;
    }

    /**
     * @param Url $url
     */
    public function willCrawl(Url $url)
    {
        $this->command->printFlush($url);
    }

    /**
     * @param Url $url
     * @param \Psr\Http\Message\ResponseInterface|null $response
     */
    public function hasBeenCrawled(Url $url, $response = null)
    {
        $this->command->printFlush('HTTP ' . ($response && $response->getStatusCode() ? $response->getStatusCode() : 'ERR'));
    }

    public function finishedCrawling()
    {
        $this->command->printFlush('Crawling complete');
    }
}