<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Utility;

use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * @package     imia_base
 * @subpackage  Utility
 * @author      David Frerich <d.frerich@imia.de>
 */
class Helper extends AbstractHelper
{
    /**
     * @param boolean $includeOnly
     * @param array $defaultLanguage
     */
    static public function addDynamicConfiguration($includeOnly = false, $defaultLanguage = ['de', 'de_DE', 'bypass'])
    {
        if (is_array($GLOBALS['argv']) && in_array('base_assetic', $GLOBALS['argv'])) {
            $includeOnly = true;
        }

        self::addDomainTypoScriptConstants($includeOnly);
        self::addDynamicLanguageTypoScript($includeOnly, $defaultLanguage);
        self::updateRealUrlConfiguration($includeOnly, $defaultLanguage);
    }

    /**
     * @param boolean $includeOnly
     */
    static public function addDomainTypoScriptConstants($includeOnly = false)
    {
        $cache = new Cache('imia_domains');

        $domainTsConsts = '';
        if ($cache->exists()) {
            $domainTsConsts = $cache->load();
        } elseif (!$includeOnly && self::getDb()) {
            $domainTsConsts = '';
            try {
                $domains = self::getDb()->exec_SELECTgetRows('pid, domainName', 'sys_domain',
                    '1=1' . self::enableFields('sys_domain'), '', 'sorting asc');

                if (array_key_exists(0, $domains)) {
                    $domainTsConsts .= 'config.baseUrl = ' . $domains[0]['domainName'] . "\n\n";
                }
                foreach ($domains as $domain) {
                    $domainTsConsts .= '[globalString = IENV:HTTP_HOST = ' . $domain['domainName'] . ']' . "\n" .
                        '    config.baseUrl = ' . $domain['domainName'] . "\n";
                }

                if ($domainTsConsts) {
                    $domainTsConsts .= '[end]';
                }

                $cache->write($domainTsConsts);
            } catch (\Exception $e) {
            }
        }

        ExtensionManagementUtility::addTypoScript('imia_base', 'constants', $domainTsConsts);
    }

    /**
     * @param boolean $includeOnly
     * @param array $defaultLanguage
     */
    public static function addDynamicLanguageTypoScript($includeOnly = false, $defaultLanguage = ['de', 'de_DE', 'bypass'])
    {
        $cache = new Cache('imia_languages');

        $typoscript = null;
        if ($cache->exists()) {
            $typoscript = $cache->load();
        } elseif (!$includeOnly && self::getDb()) {
            try {
                $typoscript = 'config {
    linkVars = L
    sys_language_uid = 0
    language = ' . $defaultLanguage[0] . '
    locale_all = ' . $defaultLanguage[1] . '.UTF-8
    htmlTag_langKey = ' . $defaultLanguage[0] . '
    defaultGetVars {
        L = 0
    }
}' . "\n\n";

                $languages = self::getDb()->exec_SELECTgetRows('sys_language.uid, sys_language.flag, sys_language.language_isocode',
                    'sys_language', '1' . self::enableFields('sys_language'));

                if ($languages && is_array($languages)) {
                    foreach ($languages as $language) {
                        $typoscript .= '[globalVar = GP:L = ' . $language['uid'] . ']
config {
    sys_language_uid = ' . $language['uid'] . '
    language = ' . $language['language_isocode'] . '
    locale_all = ' . $language['language_isocode'] . '_' . strtoupper($language['flag']) . '.UTF-8
    htmlTag_langKey = ' . $language['language_isocode'] . '
}' . "\n";
                    }
                }

                $typoscript .= '[end]';

                $cache->write($typoscript);
            } catch (\Exception $e) {
            }
        }

        if ($typoscript) {
            ExtensionManagementUtility::addTypoScript('imia_base', 'setup', $typoscript);
        }
    }

    /**
     * @param boolean $includeOnly
     * @param array $defaultLanguage
     */
    static public function updateRealUrlConfiguration($includeOnly = false, $defaultLanguage = ['de', 'de_DE', 'bypass'])
    {
        $cache = new Cache('imia_realurl');

        $realUrlConfig = null;
        if ($cache->exists()) {
            $realUrlConfig = $cache->load();
        } elseif (!$includeOnly && self::getDb()) {
            try {
                $realurlExtConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['realurl']);
                if ($realurlExtConf['configFile'] && file_exists(PATH_site . $realurlExtConf['configFile'])) {
                    global $TYPO3_CONF_VARS;
                    require(PATH_site . $realurlExtConf['configFile']);
                }

                $realUrlConfig = $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'];

                $realUrlLanguagePreVar = [
                    'GETvar'   => 'L',
                    'valueMap' => [],
                ];

                if ($defaultLanguage && $defaultLanguage[2] != 'bypass') {
                    $realUrlLanguagePreVar['valueMap'][strtolower($defaultLanguage[0])] = 0;
                    $realUrlLanguagePreVar['valueDefault'] = strtolower($defaultLanguage[0]);
                } else {
                    $realUrlLanguagePreVar['noMatch'] = 'bypass';
                }

                $languages = self::getDb()->exec_SELECTgetRows('sys_language.uid, sys_language.flag, sys_language.language_isocode',
                    'sys_language', '1' . self::enableFields('sys_language'));

                if ($languages && is_array($languages)) {
                    foreach ($languages as $language) {
                        $key = strtolower($language['language_isocode']);
                        if (array_key_exists($key, $realUrlLanguagePreVar['valueMap'])) {
                            $key .= '-' . strtolower(substr($language['flag'], 0, 2));
                        }

                        $realUrlLanguagePreVar['valueMap'][$key] = (int)$language['uid'];
                    }
                }

                if (is_array($realUrlConfig['_DEFAULT']['preVars'])) {
                    array_unshift($realUrlConfig['_DEFAULT']['preVars'], $realUrlLanguagePreVar);
                } else {
                    $realUrlConfig['_DEFAULT']['preVars'] = [$realUrlLanguagePreVar];
                }

                $domains = self::getDb()->exec_SELECTgetRows('pid, domainName', 'sys_domain',
                    '(sys_domain.redirectTo IS NULL OR sys_domain.redirectTo = "")' . self::enableFields('sys_domain'));

                foreach ($domains as $domain) {
                    $realUrlConfig[$domain['domainName']] = $realUrlConfig['_DEFAULT'];
                    $realUrlConfig[$domain['domainName']]['pagePath']['rootpage_id'] = (int)$domain['pid'];
                }

                $cache->write($realUrlConfig);
            } catch (\Exception $e) {
            }
        }

        if ($realUrlConfig) {
            $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'] = $realUrlConfig;
        }
    }

    /**
     * @param string $color
     * @return array
     */
    static public function convertHex2RGB($color)
    {
        $color = str_replace('#', '', $color);

        if (strlen($color) != 6) {
            return [0, 0, 0];
        } else {
            $rgb = [];
            for ($x = 0; $x < 3; $x++) {
                $rgb[$x] = hexdec(substr($color, (2 * $x), 2));
            }

            return $rgb;
        }
    }

    /**
     * @param array $content
     * @param array $conf
     * @return array
     */
    public function arrayReverse(array $content, $conf)
    {
        return array_reverse($content);
    }

    /**
     * @return DatabaseConnection
     */
    protected static function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}