<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Utility;

use IMIA\ImiaBase\Traits\HookTrait;
use IMIA\ImiaBase\User\Grid;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Configuration\FlexForm\FlexFormTools;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Database\RelationHandler;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\FileRepository;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Service\FlexFormService;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/**
 * @package     imia_base
 * @subpackage  Utility
 * @author      David Frerich <d.frerich@imia.de>
 */
class ProcessingHelper
{
    use HookTrait;

    /**
     * @var \TYPO3\CMS\Core\Cache\Frontend\FrontendInterface|boolean
     */
    static protected $cache = null;

    /**
     * @var array
     */
    static protected $pageData = [];

    /**
     * @var string
     */
    static protected $renderType = null;

    /**
     * @param array $processedData
     * @param array $data
     * @return array
     * @throws \TYPO3\CMS\Core\Configuration\FlexForm\Exception\InvalidIdentifierException
     * @throws \TYPO3\CMS\Core\Configuration\FlexForm\Exception\InvalidParentRowException
     * @throws \TYPO3\CMS\Core\Configuration\FlexForm\Exception\InvalidParentRowLoopException
     * @throws \TYPO3\CMS\Core\Configuration\FlexForm\Exception\InvalidParentRowRootException
     * @throws \TYPO3\CMS\Core\Configuration\FlexForm\Exception\InvalidPointerFieldValueException
     * @throws \TYPO3\CMS\Core\Configuration\FlexForm\Exception\InvalidTcaException
     */
    public static function process($processedData, $data)
    {
        if (isset($data['record']['uid'])) {
            self::$renderType = 'content';
        } elseif (isset($data['page']['uid'])) {
            self::$renderType = 'page';
        } elseif (isset($data['pageSlide']['uid'])) {
            self::$renderType = 'pageSlide';
        }

        $cacheKey = null;
        if (TYPO3_MODE == 'BE') {
            $cacheKey = TYPO3_MODE . '_' . (isset($data['record']['uid']) ? $data['record']['uid'] : '') .
                '_' . (isset($data['page']['uid']) ? $data['page']['uid'] : '');
        }

        if ($cacheKey && self::getCache() && self::getCache()->has($cacheKey)) {
            $processedData = self::getCache()->get($cacheKey);
        } else {
            self::callHookStatic('preProcess', [&$processedData]);

            if (isset($data['record']) && $data['record']['ib_flexform']) {
                /** @var FlexformService $flexformService */
                $flexformService = GeneralUtility::makeInstance(ObjectManager::class)->get(FlexFormService::class);
                $flexformData = $flexformService->convertFlexFormContentToArray($data['record']['ib_flexform']);

                if ($flexformData && is_array($flexformData)) {
                    $processedData['flexform'] = $flexformData;
                }
            }

            self::callHookStatic('afterFlexProcess', [&$processedData]);
            foreach ($data as $type => $subdata) {
                $processedData[$type] = [];
                foreach ($subdata as $key => $value) {
                    if (!in_array($key, ['fe_group', 'pi_flexform', 'l18n_parent', 'l18n_diffsource', 'editlock', 'l10n_source', 'ib_flexform', 'sys_language_uid', 'cruser_id', 'l18n_cfg'])
                        && strpos($key, 't3ver_') !== 0
                        && (array_key_exists($key, $GLOBALS['TCA'][($type == 'page' || $type == 'pageSlide' ? 'pages' : 'tt_content')]['columns']) || in_array($key, ['uid', 'pid']))
                    ) {
                        $processedData[$type][$key] = $value;
                        if (!array_key_exists($key, $processedData)) {
                            switch ($type) {
                                case 'record':
                                    self::resolveTCAColumnValue('tt_content', $key, $processedData[$type]['uid'], $value, $subdata);
                                    $processedData[GeneralUtility::underscoredToLowerCamelCase(GeneralUtility::camelCaseToLowerCaseUnderscored($key))] = $value;
                                    break;
                                case 'page':
                                    if (TYPO3_MODE == 'FE') {
                                        if (!isset(self::$pageData[$processedData[$type]['uid']][$key])) {
                                            self::resolveTCAColumnValue('pages', $key, $processedData[$type]['uid'], $value, $subdata);
                                            self::$pageData[$processedData[$type]['uid']][$key] = $value;
                                        } else {
                                            $value = self::$pageData[$processedData[$type]['uid']][$key];
                                        }

                                        $processedData[$type][$key] = $value;
                                    }
                                    break;
                                case 'pageSlide':
                                    if (TYPO3_MODE == 'FE') {
                                        self::resolveTCAColumnValue('pages', $key, $processedData[$type]['uid'], $value, $subdata);

                                        $processedData[$type][$key] = $value;
                                    }
                                    break;
                            }
                        }
                    }
                }
            }

            if (isset($processedData['flexform'])) {
                foreach (self::convertData($processedData['flexform']) as $key => $flexData) {
                    $processedData[$key] = $flexData;
                }

                if (isset($GLOBALS['TCA']['tt_content']['columns']['ib_flexform']['config']['ds'][$data['record']['CType']])) {
                    /** @var FlexFormTools $flexformTools */
                    $flexformTools = GeneralUtility::makeInstance(FlexFormTools::class);
                    $dataStructureArray = $flexformTools->parseDataStructureByIdentifier(
                        $flexformTools->getDataStructureIdentifier(
                            $GLOBALS['TCA']['tt_content']['columns']['ib_flexform'],
                            'tt_content',
                            'ib_flexform',
                            $data['record']
                        )
                    );

                    if (is_array($dataStructureArray['sheets'])) {
                        foreach ($dataStructureArray['sheets'] as $sheet) {
                            if (is_array($sheet['ROOT']['el'])) {
                                foreach ($sheet['ROOT']['el'] as $name => $element) {
                                    if (isset($element['TCEforms']['config'])) {
                                        $config = $element['TCEforms']['config'];

                                        if (is_array($config) && $config['type']) {
                                            self::resolveColumnValue('tt_content', $config, $processedData['data']['uid'], $processedData[$name], $processedData['data']);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            self::callHookStatic('postProcess', [&$processedData]);

            $cacheTags = [];
            if (isset($data['record']['uid'])) {
                $cacheTags[] = 'content-' . $data['record']['uid'];
            } elseif (isset($data['page']['uid'])) {
                $cacheTags[] = 'page-' . $data['page']['uid'];
            }

            if ($cacheKey && self::getCache()) {
                self::getCache()->set($cacheKey, $processedData, $cacheTags, 0);
            }
        }

        return $processedData;
    }

    /**
     * @param string $table
     * @param array $row
     */
    public static function resolveTCAColumnValues($table, &$row)
    {
        if ($table == 'tx_imiabase_gridcontainers' && isset($row['content'])) {
            unset($row['content']);
        }

        foreach ($row as $key => &$value) {
            if (!in_array($key, ['fe_group', 'l10n_parent', 'l10n_diffsource', 'l10n_source', 'sys_language_uid'])
                && strpos($key, 't3ver_') !== 0
                && array_key_exists($key, $GLOBALS['TCA'][$table]['columns'])
            ) {
                self::resolveTCAColumnValue($table, $key, $row['uid'], $value, $row);
            }
        }
    }

    /**
     * @param string $table
     * @param string $field
     * @param int $parentUid
     * @param mixed $value
     * @param array $row
     */
    public static function resolveTCAColumnValue($table, $field, $parentUid, &$value, $row)
    {
        if (isset($GLOBALS['TCA'][$table]['columns'][$field]['config'])) {
            $config = $GLOBALS['TCA'][$table]['columns'][$field]['config'];
            self::resolveColumnValue($table, $config, $parentUid, $value, $row);
        }
    }

    /**
     * @param string $table
     * @param array $config
     * @param int $parentUid
     * @param mixed $value
     * @param array $row
     */
    protected static function resolveColumnValue($table, $config, $parentUid, &$value, $row)
    {
        switch ($config['type']) {
            case 'user':
                if ($config['userFunc'] == Grid::class . '->gridLayout') {
                    $value = json_decode(str_replace("'", '"', $value), true);
                    if ($value) {
                        $columns = (int)$row['grid_columns'];

                        if (isset($value[$columns])) {
                            $value = $value[$columns];

                            foreach ($value['default'] as $col => $config) {
                                $value['columns'][$col] = [];
                            }
                            foreach (['xs', 'sm', 'md', 'lg', 'xl'] as $type) {
                                if (isset($value[$type])) {
                                    foreach ($value[$type] as $col => $config) {
                                        $value['columns'][$col][$type] = $config;
                                    }
                                }
                            }
                            foreach ($value['default'] as $col => $config) {
                                if (!isset($value['columns'][$col]['xs'])) {
                                    $value['columns'][$col]['xs'] = $config;
                                }
                                if (!isset($value['columns'][$col]['sm'])) {
                                    if (serialize($value['columns'][$col]['xs']) != serialize($config)) {
                                        $value['columns'][$col]['sm'] = $config;
                                    }
                                }
                                if (!isset($value['columns'][$col]['md'])) {
                                    if (isset($value['columns'][$col]['sm']) && serialize($value['columns'][$col]['sm']) != serialize($config)) {
                                        $value['columns'][$col]['md'] = $config;
                                    }
                                }
                                if (!isset($value['columns'][$col]['lg'])) {
                                    if (isset($value['columns'][$col]['md']) && serialize($value['columns'][$col]['md']) != serialize($config)) {
                                        $value['columns'][$col]['lg'] = $config;
                                    }
                                }
                                if (!isset($value['columns'][$col]['xl'])) {
                                    if (isset($value['columns'][$col]['lg']) && serialize($value['columns'][$col]['lg']) != serialize($config)) {
                                        $value['columns'][$col]['xl'] = $config;
                                    }
                                }
                            }
                        } else {
                            $value = ['columns' => []];
                        }
                    }
                }
                break;
            case 'group':
                if ($table != 'pages' || self::$renderType != 'content') {
                    if ($config['internal_type'] == 'db' && $value) {
                        $directlyConnectedIds = GeneralUtility::trimExplode(',', $value);

                        $value = [];
                        foreach ($directlyConnectedIds as $uid) {
                            if (strpos($uid, '_')) {
                                $table = substr($uid, 0, strrpos($uid, '_'));
                                $uid = (int)substr($uid, strrpos($uid, '_') + 1);
                            } else {
                                $table = $config['allowed'];
                            }

                            $row = self::getDb()->exec_SELECTgetSingleRow('*', $table,
                                'uid = ' . $uid . self::getTSFE()->cObj->enableFields($table));
                            if ($row) {
                                if ($table == 'pages') {
                                    $row = self::getTSFE()->sys_page->getPageOverlay($row, self::getTSFE()->sys_language_content);
                                } else {
                                    $row = self::getTSFE()->sys_page->getRecordOverlay($table, $row, self::getTSFE()->sys_language_content);
                                }

                                $row['table'] = $table;
                                $value[] = $row;
                            }
                        }

                        if ($config['maxitems'] == 1) {
                            if (count($value) > 0) {
                                $value = array_first($value);
                            } else {
                                $value = null;
                            }
                        }
                    }
                }
                break;
            case 'select':
                if ($table != 'pages' || self::$renderType != 'content') {
                    if ($config['foreign_table'] && $value) {
                        if ($config['MM']) {
                            // not supported
                        } else {
                            $directlyConnectedIds = GeneralUtility::trimExplode(',', $value);

                            $value = [];
                            foreach ($directlyConnectedIds as $uid) {
                                $row = self::getDb()->exec_SELECTgetSingleRow('*', $config['foreign_table'],
                                    'uid = ' . $uid . self::getTSFE()->cObj->enableFields($config['foreign_table']));
                                if ($row) {
                                    if ($config['foreign_table'] == 'pages') {
                                        $row = self::getTSFE()->sys_page->getPageOverlay($row, self::getTSFE()->sys_language_content);
                                    } else {
                                        $row = self::getTSFE()->sys_page->getRecordOverlay($table, $row, self::getTSFE()->sys_language_content);
                                    }

                                    $value[] = $row;
                                }
                            }

                            if ($config['maxitems'] == 1 || in_array($config['renderType'], ['selectSingle', 'selectSingleBox'])) {
                                if (count($value) > 0) {
                                    $value = array_first($value);
                                } else {
                                    $value = null;
                                }
                            }
                        }
                    }
                }
                break;
            case 'inline':
                if ($table != 'pages' || self::$renderType != 'content') {
                    if (isset($config['l10n']) && $config['l10n'] === 'exclude') {
                        // l10n parent record uid
                    } else {
                        if (isset($row['_LOCALIZED_UID'])) {
                            $parentUid = $row['_LOCALIZED_UID'];
                        }
                    }

                    $directlyConnectedIds = [];
                    if (is_string($value) || is_numeric($value)) {
                        $directlyConnectedIds = GeneralUtility::trimExplode(',', $value);
                    }

                    /** @var RelationHandler $relationHandler */
                    $relationHandler = GeneralUtility::makeInstance(RelationHandler::class);
                    $relationHandler->registerNonTableValues = (bool)$config['allowedIdValues'];
                    $relationHandler->start($value, $config['foreign_table'], $config['MM'], $parentUid, $table, $config);
                    $foreignRecordUids = $relationHandler->getValueArray();
                    $resolvedForeignRecordUids = [];
                    foreach ($foreignRecordUids as $aForeignRecordUid) {
                        if ($config['MM'] || $config['foreign_field']) {
                            $resolvedForeignRecordUids[] = (int)$aForeignRecordUid;
                        } else {
                            foreach ($directlyConnectedIds as $id) {
                                if ((int)$aForeignRecordUid === (int)$id) {
                                    $resolvedForeignRecordUids[] = (int)$aForeignRecordUid;
                                }
                            }
                        }
                    }

                    $value = [];
                    if (count($resolvedForeignRecordUids) > 0) {
                        if ($config['foreign_table'] == 'sys_file_reference') {
                            /** @var FileRepository $fileRepository */
                            $fileRepository = GeneralUtility::makeInstance(FileRepository::class);
                            foreach ($resolvedForeignRecordUids as $resolvedForeignRecordUid) {
                                try {
                                    $fileReference = $fileRepository->findFileReferenceByUid($resolvedForeignRecordUid);
                                    if ($fileReference) {
                                        $file = $fileReference->getOriginalFile();
                                        $fileReferenceProperties = $fileReference->getProperties();
                                        $fileProperties = self::getFileArray($file);
                                        ArrayUtility::mergeRecursiveWithOverrule($fileProperties, $fileReferenceProperties, true, false, false);
                                        $fileProperties['file'] = $file->toArray();
                                        $fileProperties['file']['publicUrl'] = $file->getPublicUrl();

                                        $value[] = $fileProperties;
                                    }
                                } catch (\Exception $e) {
                                }
                            }
                        } else {
                            $relationLanguageField = null;
                            if (isset($GLOBALS['TCA'][$config['foreign_table']]['ctrl']['languageField'])) {
                                $relationLanguageField = $GLOBALS['TCA'][$config['foreign_table']]['ctrl']['languageField'];
                            }

                            $addWhere = '';
                            if ($relationLanguageField) {
                                if (TYPO3_MODE == 'FE' && self::getTSFE()->sys_language_content > 0) {
                                    $addWhere = ' AND ' . $relationLanguageField . ' IN(0,-1,' . (int)self::getTSFE()->sys_language_content . ')';
                                } else {
                                    $addWhere = ' AND ' . $relationLanguageField . ' IN(0,-1,' . (int)$row['sys_language_uid'] . ')';
                                }
                            }

                            $result = self::getDb()->exec_SELECTquery('*', $config['foreign_table'],
                                'uid IN (' . implode(',', $resolvedForeignRecordUids) . ')' . $addWhere .
                                self::getTSFE()->cObj->enableFields($config['foreign_table']), '', $config['foreign_sortby'] ? $config['foreign_sortby'] . ' ASC' : '');

                            while ($row = self::getDb()->sql_fetch_assoc($result)) {
                                if (TYPO3_MODE == 'FE') {
                                    $row = self::getTSFE()->sys_page->getRecordOverlay($config['foreign_table'], $row, self::getTSFE()->sys_language_content);
                                }
                                if ($row) {
                                    $uid = $row['uid'];
                                    self::resolveTCAColumnValues($config['foreign_table'], $row);
                                    $value[$uid] = $row;
                                }
                            }

                            if (!isset($config['foreign_sortby']) || !$config['foreign_sortby']) {
                                $realValue = [];
                                foreach ($resolvedForeignRecordUids as $resolvedForeignRecordUid) {
                                    if (isset($value[$resolvedForeignRecordUid])) {
                                        $realValue[$resolvedForeignRecordUid] = $value[$resolvedForeignRecordUid];
                                    }
                                }
                                $value = $realValue;
                            }
                            $value = array_values($value);
                        }
                    }

                    if ($config['maxitems'] == 1) {
                        if (count($value) > 0) {
                            $value = array_first($value);
                        } else {
                            $value = null;
                        }
                    }
                }
                break;
            case 'flex':
                if ($value) {
                    $array = GeneralUtility::xml2array($value);
                    $value = [];
                    if (isset($array['data']) && is_array($array['data'])) {
                        foreach ($array['data'] as $key => $data) {
                            foreach ($data['lDEF'] as $realKey => $valueArr) {
                                if ($realKey != 'xmlTitle') {
                                    if (isset($valueArr['el']) && is_array($valueArr['el'])) {
                                        $realValue = [];
                                        foreach ($valueArr['el'] as $subVal) {
                                            foreach ($subVal as $subKey => $subValArr)
                                                if (substr($subKey, 0, 1) != '_') {
                                                    if (isset($subValArr['el']) && is_array($subValArr['el'])) {
                                                        $realSubValue = [];
                                                        foreach ($subValArr['el'] as $subSubKey => $subSubVal) {
                                                            $realSubValue[$subSubKey] = $subSubVal['vDEF'];
                                                        }

                                                        $realValue[] = $realSubValue;
                                                    } else {
                                                        $realValue[$subKey] = $subValArr['vDEF'];
                                                    }
                                                }
                                        }

                                        $value[$realKey] = $realValue;
                                    } else {
                                        $value[$realKey] = $valueArr['vDEF'];
                                    }
                                }
                            }
                        }
                    }
                } else {
                    $value = [];
                }
                break;
        }
    }

    /**
     * @param array $data
     * @return array
     */
    protected static function convertData($data)
    {
        if (is_array($data)) {
            $processedData = [];
            foreach ($data as $key => $subdata) {
                $processedData[GeneralUtility::underscoredToLowerCamelCase(GeneralUtility::camelCaseToLowerCaseUnderscored($key))] = self::convertData($subdata);
            }
        } else {
            $processedData = $data;
        }

        return $processedData;
    }

    /**
     * @param File $file
     * @return array
     */
    protected static function getFileArray(File $file)
    {
        $properties = $file->getProperties();
        $stat = $file->getStorage()->getFileInfo($file);
        $array = $file->toArray();

        foreach ($properties as $key => $value) {
            $array[$key] = $value;
        }
        foreach ($stat as $key => $value) {
            $array[$key] = $value;
        }

        return $array;
    }

    /**
     * @return bool|\TYPO3\CMS\Core\Cache\Frontend\FrontendInterface
     */
    protected static function getCache()
    {
        if (self::$cache === null) {
            if ($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['tx_imiabase']) {
                /** @var CacheManager $cacheManager */
                $cacheManager = $contentCache = GeneralUtility::makeInstance(CacheManager::class);

                try {
                    self::$cache = $cacheManager->getCache('tx_imiabase');
                } catch (\Exception $e) {
                }
            }
        }

        return self::$cache;
    }

    /**
     * @return DatabaseConnection
     */
    protected static function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }

    /**
     * @return TypoScriptFrontendController
     */
    protected static function getTSFE()
    {
        return $GLOBALS['TSFE'];
    }
}