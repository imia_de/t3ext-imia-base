<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Utility;

use IMIA\ImiaBase\TypoScript\Parser\TypoScriptParser;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  Utility
 * @author      David Frerich <d.frerich@imia.de>
 */
class TemplateService
{
    /**
     * @param string $idList
     * @param string $templateID
     * @param integer $pid
     * @param array $row
     * @param \TYPO3\CMS\Core\TypoScript\TemplateService $templateService
     */
    public static function includeStaticTypoScriptSources($idList, $templateID, $pid, $row, &$templateService)
    {
        // Static Template Records (static_template): include_static is a list of static templates to include
        // Call function for link rendering:
        if (is_array($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tstemplate.php']['includeStaticTypoScriptSources'])) {
            $_params = [
                'idList'     => &$idList,
                'templateId' => &$templateID,
                'pid'        => &$pid,
                'row'        => &$row,
            ];
            foreach ($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tstemplate.php']['includeStaticTypoScriptSources'] as $_funcRef) {
                GeneralUtility::callUserFunction($_funcRef, $_params, $templateService);
            }
        }
        // If "Include before all static templates if root-flag is set" is set:
        if ($row['static_file_mode'] == 3 && strpos($templateID, 'sys_') === 0 && $row['root']) {
            $templateService->addExtensionStatics($idList, $templateID, $pid, $row);
        }
        // Static Template Files (Text files from extensions): include_static_file is a list of static files to include (from extensions)
        if (trim($row['include_static_file'])) {
            $include_static_fileArr = GeneralUtility::trimExplode(',', $row['include_static_file'], true);
            // Traversing list
            foreach ($include_static_fileArr as $ISF_file) {
                if (strpos($ISF_file, 'EXT:') === 0) {
                    list($ISF_extKey, $ISF_localPath) = explode('/', substr($ISF_file, 4), 2);
                    if ((string)$ISF_extKey !== '' && ExtensionManagementUtility::isLoaded($ISF_extKey) && (string)$ISF_localPath !== '') {
                        $ISF_localPath = rtrim($ISF_localPath, '/') . '/';
                        $ISF_filePath = ExtensionManagementUtility::extPath($ISF_extKey) . $ISF_localPath;
                        if (@is_dir($ISF_filePath)) {
                            $mExtKey = str_replace('_', '', $ISF_extKey . '/' . $ISF_localPath);
                            $subrow = [
                                'constants'           => self::getTypoScriptSourceFileContent($ISF_filePath, 'constants'),
                                'config'              => self::getTypoScriptSourceFileContent($ISF_filePath, 'setup'),
                                'include_static'      => @file_exists(($ISF_filePath . 'include_static.txt')) ? implode(',', array_unique(GeneralUtility::intExplode(',', file_get_contents($ISF_filePath . 'include_static.txt')))) : '',
                                'include_static_file' => @file_exists(($ISF_filePath . 'include_static_file.txt')) ? implode(',', array_unique(explode(',', file_get_contents($ISF_filePath . 'include_static_file.txt')))) : '',
                                'title'               => $ISF_file,
                                'uid'                 => $mExtKey,
                            ];
                            foreach (['constants', 'config'] as $key) {
                                if ($subrow[$key]) {
                                    $subrow[$key] = TypoScriptParser::checkRelativeIncludeLines($subrow[$key], 1, false, 'EXT:' . $ISF_extKey . '/' . $ISF_localPath);
                                }
                            }
                            $subrow = $templateService->prependStaticExtra($subrow);
                            $templateService->processTemplate($subrow, $idList . ',ext_' . $mExtKey, $pid, 'ext_' . $mExtKey, $templateID, $ISF_filePath);
                        }
                    }
                }
            }
        }
        // If "Default (include before if root flag is set)" is set OR
        // "Always include before this template record" AND root-flag are set
        if ($row['static_file_mode'] == 1 || $row['static_file_mode'] == 0 && substr($templateID, 0, 4) === 'sys_' && $row['root']) {
            $templateService->addExtensionStatics($idList, $templateID, $pid, $row);
        }
        // Include Static Template Records after all other TypoScript has been included.
        if (is_array($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tstemplate.php']['includeStaticTypoScriptSourcesAtEnd'])) {
            $_params = [
                'idList'     => &$idList,
                'templateId' => &$templateID,
                'pid'        => &$pid,
                'row'        => &$row,
            ];
            foreach ($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tstemplate.php']['includeStaticTypoScriptSourcesAtEnd'] as $_funcRef) {
                GeneralUtility::callUserFunction($_funcRef, $_params, $templateService);
            }
        }
    }

    /**
     * @param string $filePath
     * @param string $baseName
     * @return string
     */
    protected static function getTypoScriptSourceFileContent($filePath, $baseName)
    {
        $extensions = ['.typoscript', '.ts', '.txt'];
        foreach ($extensions as $extension) {
            $fileName = $filePath . $baseName . $extension;
            if (@file_exists($fileName)) {
                return file_get_contents($fileName);
            }
        }

        return '';
    }
}