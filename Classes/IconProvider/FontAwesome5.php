<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\IconProvider;

use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Imaging\IconProviderInterface;
use TYPO3\CMS\Core\Imaging\IconRegistry;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  IconProvider
 * @author      David Frerich <d.frerich@imia.de>
 */
class FontAwesome5 extends AbstractProvider implements IconProviderInterface
{
    /**
     * @var bool
     */
    protected $addCss = true;

    /**
     * @param string $css
     * @return array
     */
    public function getIconsFromCSS($css)
    {
        $items = [];
        preg_match('/\*[ ]+(Font Awesome.+?)by/ism', $css, $match);
        $items[] = [
            'label' => isset($match[1]) && trim($match[1]) ? trim($match[1]) : 'Font Awesome',
            'value' => '--div--',
        ];

        $brandItems = [];
        preg_match('/url\(["\']?([^"]*?fa\-brands\-400\.svg)/ism', $css, $match);
        if ($match) {
            $fileDir = dirname(GeneralUtility::getFileAbsFileName($this->options['css']));
            $path = realpath($fileDir . '/' . $match[1]);

            if ($path) {
                $brandSvg = file_get_contents($path);
                if (preg_match_all('/glyph\-name="([^"]+)"/ism', $brandSvg, $matches)) {
                    foreach ($matches[1] as $match) {
                        $brandItems[] = $match;
                    }
                }
            }
        }

        $type = 'fa';
        if (isset($this->options['type'])) {
            switch (strtolower($this->options['type'])) {
                case 'solid':
                case 'fas':
                case 's':
                    $type = 'fas';
                    break;
                case 'light':
                case 'fal':
                case 'l':
                    $type = 'fal';
                    break;
                case 'regular':
                case 'far':
                case 'r':
                default:
                    $type = 'far';
                    break;
            }
        }

        $iconRegistry = GeneralUtility::makeInstance(IconRegistry::class);
        if (preg_match_all('/\.fa\-([^:]+):before/ism', $css, $matches)) {
            foreach ($matches[1] as $match) {
                if (in_array($match, $brandItems)) {
                    $itemType = 'fab';
                } else {
                    $itemType = $type;
                }

                $items[] = [
                    'label' => ucwords(str_replace('-', ' ', $match)),
                    'value' => $itemType . ' fa-' . $match,
                    'icon'  => 'fontawesome5-' . $match,
                ];
                $iconRegistry->registerIcon('fontawesome5-' . $match, self::class, [
                    'name' => $match,
                    'type' => $itemType,
                ]);
            }
        }

        return $items;
    }

    /**
     * @param Icon $icon
     * @param array $options
     */
    public function prepareIconMarkup(Icon $icon, array $options = [])
    {
        $icon->setMarkup($this->generateMarkup($icon, $options));
    }

    /**
     * @param Icon $icon
     * @param array $options
     *
     * @throws \InvalidArgumentException
     * @return string
     */
    protected function generateMarkup(Icon $icon, array $options)
    {
        if (empty($options['name'])) {
            throw new \InvalidArgumentException('The option "name" is required and must not be empty', 1440754978);
        }
        if (preg_match('/^[a-zA-Z0-9\\-]+$/', $options['name']) !== 1) {
            throw new \InvalidArgumentException('The option "name" must only contain characters a-z, A-Z, 0-9 or -', 1440754979);
        }

        return '<span class="icon-unify"><i class="' . (isset($options['type']) && $options['type'] ? $options['type'] : 'fa') . ' fa-' . htmlspecialchars($options['name']) . '"></i></span>';
    }
}