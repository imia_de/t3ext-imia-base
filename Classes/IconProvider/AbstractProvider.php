<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\IconProvider;

use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  IconProvider
 * @author      David Frerich <d.frerich@imia.de>
 */
abstract class AbstractProvider
{
    /**
     * @var array
     */
    protected $options;

    /**
     * @var bool
     */
    protected $addCss = false;

    /**
     * @param array $options
     */
    public function __construct($options = [])
    {
        $this->options = $options;

        $this->options['cssFile'] = false;
        if (isset($this->options['css'])) {
            $filePath = GeneralUtility::getFileAbsFileName($this->options['css']);

            if ($filePath) {
                $this->options['cssFile'] = $filePath;

                if (TYPO3_MODE == 'BE' && $this->addCss) {
                    $pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
                    $pageRenderer->addCssFile($this->options['css']);
                }
            }
        }
    }

    /**
     * @return array
     */
    public function getItems()
    {
        $items = [];
        if ($this->options['cssFile']) {
            $iconCss = file_get_contents($this->options['cssFile']);
            $items = $this->getIconsFromCSS($iconCss);
        }

        return $items;
    }

    /**
     * @param $iconCss
     * @return array
     */
    abstract protected function getIconsFromCSS($iconCss);
}