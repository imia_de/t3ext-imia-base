<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\User;

use IMIA\ImiaBase\Utility\AbstractHelper;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * @package     imia_base
 * @subpackage  User
 * @author      Lars Siemon <l.siemon@ima.de>
 */
class Hreflang extends AbstractHelper
{
    /**
     * @var ContentObjectRenderer
     */
    public $cObj;

    /**
     * @param string $content
     * @param array $config
     * @return string
     */
    public function render($content = '', $config)
    {
        $result = $this->getDb()->exec_SELECTquery(
            'uid, language_isocode',
            'sys_language',
            '1 ' . $GLOBALS['TSFE']->sys_page->enableFields('sys_language')
        );

        $languageUids = [];
        $languageHrefIso = [];
        $additionalParams = [];
        while ($row = $this->getDb()->sql_fetch_assoc($result)) {
            $languageUids[] = $row['uid'];
            if ($config['defaultLanguageUid'] == $row['uid']) {
                $languageHrefIso[] = 'x-default';
            } else {
                $languageHrefIso[] = $row['language_isocode'];
            }
            $additionalParams[] = '&L=' . $row['uid'];
        }
        //add default language
        $languageUids[] = 0;
        if ((int)$config['defaultLanguageUid'] == 0) {
            $languageHrefIso[] = 'x-default';
        } else {
            $languageHrefIso[] = $config['standardLanguageIso'];
        }
        $additionalParams[] = '&L=0';

        $config['langConfig.']['special.']['value'] = implode(',', $languageUids);
        $config['langConfig.']['1.']['NO.']['stdWrap.']['cObject.']['value'] = implode(' || ', $languageHrefIso);
        $config['langConfig.']['1.']['NO.']['after.']['cObject.']['stdWrap.']['typolink.']['additionalParams'] = implode(' || ', $additionalParams);

        return $this->cObj->cObjGetSingle('HMENU', $config['langConfig.']);
    }
}