<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\User;

use IMIA\ImiaBase\Utility\AbstractHelper;
use TYPO3\CMS\Backend\Form\Element\UserElement;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Backend\View\BackendLayoutView;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\Page\PageRepository;

/**
 * @package     imia_base
 * @subpackage  User
 * @author      David Frerich <d.frerich@imia.de>
 */
class Grid extends AbstractHelper
{
    /**
     * @var ContentObjectRenderer
     */
    public $cObj;

    /**
     * @var array
     */
    static public $defaultGridLayout = [
        1 => [
            'default' => [
                0 => [
                    'w' => 12,
                ],
            ],
        ],
        2 => [
            'default' => [
                0 => [
                    'w' => 6,
                ],
                1 => [
                    'w' => 6,
                ],
            ],
            'xs' => [
                0 => [
                    'w' => 12,
                ],
                1 => [
                    'w' => 12,
                ],
            ],
        ],
        3 => [
            'default' => [
                0 => [
                    'w' => 4,
                ],
                1 => [
                    'w' => 4,
                ],
                2 => [
                    'w' => 4,
                ],
            ],
            'xs' => [
                0 => [
                    'w' => 12,
                ],
                1 => [
                    'w' => 12,
                ],
                2 => [
                    'w' => 12,
                ],
            ],
        ],
        4 => [
            'default' => [
                0 => [
                    'w' => 3,
                ],
                1 => [
                    'w' => 3,
                ],
                2 => [
                    'w' => 3,
                ],
                3 => [
                    'w' => 3,
                ],
            ],
            'sm' => [
                0 => [
                    'w' => 6,
                ],
                1 => [
                    'w' => 6,
                ],
                2 => [
                    'w' => 6,
                ],
                3 => [
                    'w' => 6,
                ],
            ],
            'xs' => [
                0 => [
                    'w' => 12,
                ],
                1 => [
                    'w' => 12,
                ],
                2 => [
                    'w' => 12,
                ],
                3 => [
                    'w' => 12,
                ],
            ],
        ],
    ];

    /**
     * @param array $params
     * @param UserElement $pObj
     * @return string
     */
    public function gridLayout($params, $pObj)
    {
        $gridLayout = str_replace("'", '"', $params['itemFormElValue']);
        if ($gridLayout) {
            $gridLayout = json_decode($gridLayout, true);
        }

        if (!is_array($gridLayout)) {
            $gridLayout = self::$defaultGridLayout;
        }

        $cssClass = 'grid-layout';
        if (isset($params['fieldConf']['config']['gridLayoutClass'])) {
            $cssClass = $params['fieldConf']['config']['gridLayoutClass'];
        }

        $html = '
            <div class="' . $cssClass . ' base-grid">
                <input type="hidden" name="' . $params['itemFormElName'] . '" id="' . $params['itemFormElID'] . '" 
                    value="' . str_replace('"', "'", json_encode($gridLayout)) . '"
                    data-columns="' . str_replace('grid_layout', 'grid_columns', $params['itemFormElName']) . '">
                <h5 class="enabled">Default</h5>
                <div class="grid-layout-default row"></div>
                <h5 class="enablable">Large Desktop (lg)</h5>
                <div class="grid-layout-lg row" style="display: none"></div>
                <h5 class="enablable">Desktop (md)</h5>
                <div class="grid-layout-md row" style="display: none"></div>
                <h5 class="enablable">Tablet (sm)</h5>
                <div class="grid-layout-sm row" style="display: none"></div>
                <h5 class="enablable">Mobile (xs)</h5>
                <div class="grid-layout-xs row" style="display: none"></div>
            </div>';

        return $html;
    }


    /**
     * @param array $parameters
     */
    public function colPosListItemProcFunc(array $parameters)
    {
        if ($parameters['row']['grid_parent']) {
            $gridParent = BackendUtility::getRecord('tt_content', (int)$parameters['row']['grid_parent']);

            if ($gridParent) {
                $renderGridType = null;
                foreach ($GLOBALS['TCA']['tt_content']['special'] as $specialType => $cTypes) {
                    if (in_array($gridParent['CType'], $cTypes)) {
                        $renderGridType = $specialType;
                        break;
                    }
                }

                if ($renderGridType == 'grid') {
                    $gridLayouts = json_decode(str_replace("'", '"', $gridParent['grid_layout']), true);
                    $gridLayout = $gridLayouts[(int)$gridParent['grid_columns']]['default'];
                } elseif ($renderGridType == 'gridfixed1') {
                    $gridLayout = [
                        0 => [
                            'w'    => 12,
                        ],
                    ];
                } elseif ($renderGridType == 'gridfixed2') {
                    $gridLayout = [
                        0 => [
                            'w'    => 6,
                        ],
                        1 => [
                            'w'    => 6,
                        ],
                    ];
                } elseif ($renderGridType == 'gridfixed3') {
                    $gridLayout = [
                        0 => [
                            'w'    => 4,
                        ],
                        1 => [
                            'w'    => 4,
                        ],
                        2 => [
                            'w'    => 4,
                        ],
                    ];
                } else {
                    /** @var PageRepository $pageRepository */
                    $pageRepository = GeneralUtility::makeInstance(PageRepository::class);
                    $pageRepository->init(true);

                    if ($parameters['row']['sys_language_uid']) {
                        if (is_array($parameters['row']['sys_language_uid'])) {
                            $languageUid = (int)array_shift($parameters['row']['sys_language_uid']);
                        } else {
                            $languageUid = (int)$parameters['row']['sys_language_uid'];
                        }
                    }
                    $gridParent = $pageRepository->getRecordOverlay('tt_content', $gridParent, $languageUid);
                    if (isset($gridParent['_LOCALIZED_UID'])) {
                        $gridParent['uid'] = $gridParent['_LOCALIZED_UID'];
                    }
                    $result = $this->getDb()->exec_SELECTquery('*', 'tx_imiabase_gridcontainers',
                        'content = ' . $gridParent['uid'] . BackendUtility::deleteClause('tx_imiabase_gridcontainers'), '', 'sorting ASC');

                    $gridLayout = [];
                    while ($row = $this->getDb()->sql_fetch_assoc($result)) {
                        if ($row['l10n_parent']) {
                            $row['uid'] = (int)$row['l10n_parent'];
                        }

                        $gridLayout[(int)$row['uid']] = [
                            'w' => 12,
                            'name' => $row['title'],
                        ];
                    }
                }

                $parameters['items'] = [];
                foreach ($gridLayout as $column => $config) {
                    if (isset($config['name'])) {
                        $name = $config['name'];
                    } else {
                        $name = sprintf($this->getLanguageService()->sL('LLL:EXT:imia_base/Resources/Private/Language/Backend.xlf:gridlayout.grid.column'), ($column + 1));
                    }

                    $parameters['items'][] = [$name, $column];
                }
                return;
            }
        }

        /** @var BackendLayoutView $backendLayoutView */
        $backendLayoutView = GeneralUtility::makeInstance(BackendLayoutView::class);
        $backendLayoutView->colPosListItemProcFunc($parameters);
    }
}