<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\User;

use IMIA\ImiaBase\IconProvider\AbstractProvider;
use IMIA\ImiaBase\Registry\FluidStyledContentRegistry;

/**
 * @package     imia_base
 * @subpackage  User
 * @author      David Frerich <d.frerich@imia.de>
 */
class Icons
{
    /**
     * @param array $params
     * @param \TYPO3\CMS\Backend\Form\FormDataProvider\TcaSelectItems $pObj
     */
    public function icons(&$params, $pObj)
    {
        /** @var AbstractProvider $provider */
        foreach (FluidStyledContentRegistry::getIconProviders() as $provider) {
            foreach ($provider->getItems() as $item) {
                $params['items'][] = [$item['label'], $item['value'], isset($item['icon']) ? $item['icon'] : null];
            }
        }
    }
}