<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\User;

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

/**
 * @package     imia_base
 * @subpackage  User
 * @author      David Frerich <d.frerich@imia.de>
 */
class BackendLayout
{
    /**
     * @var array
     */
    static protected $layout = [];

    /**
     * @param string $content
     * @param array $config
     * @return mixed
     */
    static public function get($content = '', $config = [])
    {
        $field = 'backend_layout';
        $fieldNext = 'backend_layout_next_level';

        if ($config['field']) {
            $field = $config['field'];
            $fieldNext = $config['field_next'] ?: $fieldNext;
        } elseif (ExtensionManagementUtility::isLoaded('fluidpages')) {
            $field = 'tx_fed_page_controller_action';
            $fieldNext = 'tx_fed_page_controller_action_sub';
        }

        if (!array_key_exists($field, self::$layout)) {
            self::$layout[$field] = '';
            foreach ($GLOBALS['TSFE']->rootLine as $key => $page) {
                if ($key === 0) {
                    if (array_key_exists($field, $page) && $page[$field]) {
                        self::$layout[$field] = $page[$field];
                        break;
                    }
                } else {
                    if (array_key_exists($fieldNext, $page) && $page[$field]) {
                        self::$layout[$field] = $page[$fieldNext];
                        break;
                    }
                }
            }

            if ($field == 'tx_fed_page_controller_action') {
                self::$layout[$field] = substr(self::$layout[$field], strpos(self::$layout[$field], '->') + 2);
            }
        }

        return self::$layout[$field];
    }

    /**
     * @param $layout
     * @return bool
     */
    static public function is($layout)
    {
        return $layout == self::get() ? true : false;
    }

    /**
     * @param string $content
     * @param array $config
     * @return mixed
     */
    public function getLayout($content = '', $config = [])
    {
        return self::get($content, $config);
    }
}