<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\User;

use IMIA\ImiaBase\Utility\AbstractHelper;
use TYPO3\CMS\Backend\Form\Element\UserElement;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Form\Mvc\Persistence\FormPersistenceManagerInterface;

/**
 * @package     imia_beonesolutions
 * @subpackage  Utility
 * @author      David Frerich <d.frerich@imia.de>
 */
class FormFlexform extends AbstractHelper
{
    /**
     * @param array $params
     * @param UserElement $pObj
     * @return string
     */
    public function textInfo($params, $pObj)
    {
        $identifiers = [];
        try {
            $data = $params['row']['pi_flexform'];
            if (is_array($data) && isset($data['data']['sDEF']['lDEF']['settings.persistenceIdentifier']['vDEF'][0])) {
                /** @var FormPersistenceManagerInterface $formPersistenceManager */
                $formPersistenceManager = GeneralUtility::makeInstance(ObjectManager::class)->get(FormPersistenceManagerInterface::class);
                $formDefinition = $formPersistenceManager->load($data['data']['sDEF']['lDEF']['settings.persistenceIdentifier']['vDEF'][0]);

                $identifiers = $this->getIdentifiers($formDefinition);
            }
        } catch(\Exception $e) {
        }

        $output = '';
        if (is_array($identifiers) && count($identifiers)) {
            $output .= '<strong>Text-Marker</strong><br><small>';
            foreach ($identifiers as $identifier => $label) {
                $output .= $label . ': {' . $identifier . '}<br>';
            }
            $output .= '</small>';
        }

        return $output;
    }

    /**
     * @param array $renderable
     * @return array
     */
    protected function getIdentifiers($renderable)
    {
        if (isset($renderable['renderables']) && is_array($renderable['renderables'])) {
            $identifiers = [];
            foreach ($renderable['renderables'] as $subRenderable) {
                $identifiers = array_merge($identifiers, $this->getIdentifiers($subRenderable));
            }

            return $identifiers;
        } else {
            if (!in_array($renderable['type'], ['Form', 'Page', 'Fieldset', 'StaticText', 'GridRow', 'SummaryPage'])) {
                return [$renderable['identifier'] => $renderable['label']];
            } else {
                return [];
            }
        }
    }
}