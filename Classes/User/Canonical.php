<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\User;

use IMIA\ImiaBase\Utility\AbstractHelper;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * @package     imia_base
 * @subpackage  User
 * @author      David Frerich <d.frerich@imia.de>
 */
class Canonical extends AbstractHelper
{
    /**
     * @var ContentObjectRenderer
     */
    public $cObj;

    /**
     * @param string $content
     * @param array $config
     * @return string
     */
    public function render($content = '', $config = [])
    {
        if (!$config) {
            $config = [];
        }

        if (isset($GLOBALS['TSFE']->page['canonical_overwrite'])) {
            return $GLOBALS['TSFE']->page['canonical_overwrite'];
        } else {
            $config['typolink.']['addQueryString.']['exclude'] .= ',MP';
            $config['typolink.']['addQueryString.']['method'] .= 'GET';
            $linkConfig = array_merge($config['typolink.'], [
                'returnLast'     => 'url',
                'parameter'      => (int)$GLOBALS['TSFE']->page['content_from_pid'] ?: $GLOBALS['TSFE']->id,
                'addQueryString' => '1',
                'absolute'       => '0',
            ]);

            $link = $this->cObj->typolink('', $linkConfig);
            unset($config['typolink.']);

            if ($link == '/') {
                $link = '';
            }

            $link = preg_replace('/^http(s)?:\/\/[^\/]+\//ism', '', $link);
        }

        return $this->cObj->stdWrap($link, $config);
    }
}