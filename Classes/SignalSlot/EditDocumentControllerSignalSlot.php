<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\SignalSlot;

use IMIA\ImiaBase\Registry\FluidStyledContentRegistry;
use TYPO3\CMS\Backend\Controller\EditDocumentController;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class EditDocumentControllerSignalSlot
{
    /**
     * @param EditDocumentController $editDocumentController
     */
    public function initAfter($editDocumentController)
    {
        $webRoot = str_replace(GeneralUtility::getIndpEnv('TYPO3_DOCUMENT_ROOT'), '', PATH_site);

        /** @var PageRenderer $pageRenderer */
        $pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
        $pageRenderer->addJsFile($webRoot . ExtensionManagementUtility::siteRelPath('imia_base') . 'Resources/Public/Javascripts/form.js');
        $pageRenderer->addCssFile($webRoot . ExtensionManagementUtility::siteRelPath('imia_base') . 'Resources/Public/Stylesheets/form.css');

        $includeGridJs = $includeIconCSS = false;
        if (isset($_REQUEST['defVals']['tt_content']['CType'])) {
            foreach ($GLOBALS['TCA']['tt_content']['special'] as $specialType => $cTypes) {
                if (in_array($_REQUEST['defVals']['tt_content']['CType'], $cTypes)) {
                    $includeGridJs = true;
                } elseif ($_REQUEST['defVals']['tt_content']['CType'] = 'ibcontent_icons') {
                    $includeIconCSS = true;
                }
            }
        } else {
            foreach ($this->getRecords($editDocumentController) as $record) {
                foreach ($GLOBALS['TCA']['tt_content']['special'] as $specialType => $cTypes) {
                    if (in_array($record['CType'], $cTypes)) {
                        $includeGridJs = true;
                    } elseif ($record['CType'] = 'ibcontent_icons') {
                        $includeIconCSS = true;
                    }
                }
            }
        }

        if ($includeGridJs) {
            $pageRenderer->loadRequireJsModule('jquery-ui/resizable');
            $pageRenderer->addJsFile($webRoot . ExtensionManagementUtility::siteRelPath('imia_base') . 'Resources/Public/Javascripts/grid.js');
            $pageRenderer->addCssFile($webRoot . ExtensionManagementUtility::siteRelPath('imia_base') . 'Resources/Public/Stylesheets/grid.css');
        }
        if ($includeIconCSS) {
            FluidStyledContentRegistry::getIconProviders();
        }
    }

    /**
     * @param EditDocumentController $editDocumentController
     * @return array
     */
    protected function getRecords($editDocumentController)
    {
        $records = [];
        if (is_array($editDocumentController->editconf)) {
            foreach ($editDocumentController->editconf as $table => $conf) {
                if (is_array($conf) && $GLOBALS['TCA'][$table]) {
                    foreach ($conf as $cKey => $cmd) {
                        if ($cmd === 'edit') {
                            $ids = GeneralUtility::trimExplode(',', $cKey, true);
                            foreach ($ids as $idKey => $theUid) {
                                $records[] = BackendUtility::getRecord($table, $theUid);
                            }
                        }
                    }
                }
            }
        }

        return $records;
    }
}