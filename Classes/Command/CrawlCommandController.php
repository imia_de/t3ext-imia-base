<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Command;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use IMIA\ImiaBase\Utility\CrawlObserver;
use Spatie\Crawler\Crawler;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  Controller
 * @author      David Frerich <d.frerich@imia.de>
 */
class CrawlCommandController extends \TYPO3\CMS\Extbase\Mvc\Controller\CommandController
{
    /**
     * @var string
     */
    protected $debug = '';

    /**
     * @param string $allowedDomains
     * @param string $excludeDomains
     * @return boolean
     */
    public function CrawlCommand($allowedDomains = '*', $excludeDomains = '')
    {
        $this->startFlush();

        if (trim($allowedDomains) == '*' || !trim($allowedDomains)) {
            $allowedDomains = null;
        } else {
            $domains = preg_split('/[,; ]/ism', $allowedDomains);
            $allowedDomains = null;
            if ($domains && is_array($domains)) {
                $allowedDomains = [];
                foreach ($domains as $domain) {
                    $domain = rtrim(trim(preg_replace('/^[^:]*:\/\/(.+)/ism', '$1', trim($domain))), '/');
                    if ($domain) {
                        $allowedDomains[] = $domain;
                    }
                }

                if (!count($allowedDomains)) {
                    $allowedDomains = null;
                }
            }
        }

        if (!trim($excludeDomains)) {
            $excludeDomains = null;
        } else {
            $domains = preg_split('/[,; ]/ism', $excludeDomains);
            $excludeDomains = null;
            if ($domains && is_array($domains)) {
                $excludeDomains = [];
                foreach ($domains as $domain) {
                    $domain = rtrim(trim(preg_replace('/^[^:]*:\/\/(.+)/ism', '$1', trim($domain))), '/');
                    if ($domain) {
                        $excludeDomains[] = $domain;
                    }
                }

                if (!count($excludeDomains)) {
                    $excludeDomains = null;
                }
            }
        }

        if (is_array($allowedDomains) && is_array($excludeDomains)) {
            $allowedDomains = array_diff($allowedDomains, $excludeDomains);
            $excludeDomains = null;
        }

        $where = '';
        if ($allowedDomains) {
            foreach ($allowedDomains as $domain) {
                $where .= ' AND domainName LIKE ' . $this->getDb()->fullQuoteStr(str_replace('*', '%', $domain), 'sys_domain');
            }
        }
        if ($excludeDomains) {
            foreach ($excludeDomains as $domain) {
                $quotedDomains[] = $this->getDb()->fullQuoteStr($domain, 'sys_domain');
                $where .= ' AND domainName NOT LIKE ' . $this->getDb()->fullQuoteStr(str_replace('*', '%', $domain), 'sys_domain');
            }
        }


        $crawlObserver = new CrawlObserver($this);
        $crawlClient = $client = new Client([
            RequestOptions::ALLOW_REDIRECTS => true,
            RequestOptions::COOKIES         => false,
        ]);


        $result = $this->getDb()->exec_SELECTquery('domainName', 'sys_domain', 'hidden = 0 AND redirectTo = ""' . $where);
        while ($row = $this->getDb()->sql_fetch_assoc($result)) {
            $domain = $row['domainName'];
            $this->printFlush('Crawling Domain: ' . $domain);

            $crawler = new Crawler($crawlClient);
            $crawler
                ->setCrawlObserver($crawlObserver)
                ->startCrawling('http://' . $domain);
        }

        $this->endFlush();

        return true;
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }
    /**
     * @param $string
     */
    public function printFlush($string)
    {
        $debug = '[' . date('d.m.Y H:i:s') . '] ' . $string . "\n";
        $this->debug .= $debug;

        if (php_sapi_name() != 'cli') {
            echo $debug;

            flush();
            ob_end_flush();
        }

        file_put_contents(PATH_site . 'typo3temp/logs/crawl.log', $debug, FILE_APPEND);
    }

    protected function startFlush()
    {
        if (php_sapi_name() != 'cli') {
            flush();
            ob_end_flush();
            @ini_set('output_buffering', '0');
            ob_implicit_flush(true);

            echo '<pre class="hidden">';
        }
    }

    protected function endFlush()
    {
        if (php_sapi_name() != 'cli') {
            echo '</pre><style type="text/css">.hidden { display: none; } table.t3-table td { font-size: 12px }</style>';

            if ($this->debug) {
                /** @var FlashMessage $flashMessage */
                $flashMessage = GeneralUtility::makeInstance(FlashMessage::class,
                    '<pre style="background: none transparent; color: #fff; border: none; padding: 0;">' . $this->debug . '</pre>',
                    'Crawl Info',
                    FlashMessage::INFO,
                    true
                );

                /** @var FlashMessageService $flashMessageService */
                $flashMessageService = $this->objectManager->get(FlashMessageService::class);
                $flashMessageService->getMessageQueueByIdentifier()->enqueue($flashMessage);
            }
        }
    }
}