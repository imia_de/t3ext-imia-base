<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Fluidcontent\Hooks;

use TYPO3\CMS\Backend\Utility\BackendUtility;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class WizardItemsHookSubscriber extends \FluidTYPO3\Fluidcontent\Hooks\WizardItemsHookSubscriber
{
    /**
     * @param array $items
     * @param \TYPO3\CMS\Backend\Controller\ContentElement\NewContentElementController
     * @return void
     */
    public function manipulateWizardItems(&$items, &$parentObject) {

        parent::manipulateWizardItems($items, $parentObject);

        $itemGroup = null;
        $groupItems = [];
        foreach ($items as $key => $item) {
            if (!$itemGroup) {
                $itemGroup = array_first(explode('_', $key, 1));
                $groupItems[$itemGroup] = [];
            }

            if (strpos($key, $itemGroup) === 0) {
                $groupItems[$itemGroup][$key] = $item;
            } else {
                $itemGroup = array_first(explode('_', $key, 1));
                $groupItems[$itemGroup] = [];
                $groupItems[$itemGroup][$key] = $item;
            }
        }

        $items = [];
        foreach ($groupItems as $itemGroup) {
            ksort($itemGroup);
            foreach ($itemGroup as $key => $item) {
                $items[$key] = $item;
            }
        }
    }

    /**
     * @inheritdoc
     */
    protected function getWhiteAndBlackListsFromPageAndContentColumn($pageUid, $columnPosition, $relativeUid)
    {
        list($whitelist, $blacklist) = parent::getWhiteAndBlackListsFromPageAndContentColumn($pageUid, $columnPosition, $relativeUid);

        $tsConfig = BackendUtility::getPagesTSconfig($pageUid);
        if (isset($tsConfig['TCEFORM.']['tt_content.']['tx_fed_fcefile.']['removeItems'])) {
            foreach (array_map('trim', explode(',', $tsConfig['TCEFORM.']['tt_content.']['tx_fed_fcefile.']['removeItems'])) as $blacklistedItem) {
                if (in_array($blacklistedItem, $whitelist)) {
                    $whitelist = array_diff($whitelist, [$blacklistedItem]);
                }
                $blacklist[] = $blacklistedItem;
            }
            $blacklist = array_unique($blacklist);
        }

        return [$whitelist, $blacklist];
    }
}
