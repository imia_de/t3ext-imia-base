<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Fluidcontent\Service;

use FluidTYPO3\Flux\Utility\ExtensionNamingUtility;
use FluidTYPO3\Flux\Utility\MiscellaneousUtility;
use TYPO3\CMS\Core\Core\Bootstrap;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class ConfigurationService extends \FluidTYPO3\Fluidcontent\Service\ConfigurationService
{
    /**
     * @inheritdoc
     */
    protected function buildAllWizardTabGroups()
    {
        if (!$GLOBALS['BE_USER']) {
            $bootstrap = Bootstrap::getInstance();
            $bootstrap->initializeBackendUser()->initializeBackendAuthentication(true);
        }

        return parent::buildAllWizardTabGroups();
    }

    /**
     * @return array
     */
    public function getContentTypeSelectorItems()
    {
        $types = $this->getContentElementFormInstances();

        // use from option.group as group name, if exists
        $groups = [];
        foreach ($types as $extension => $forms) {
            foreach ($forms as $form) {
                if ($form->getOption('group')) {
                    $groupName = $this->translateLabel(
                            'fluidcontent.newContentWizard.group.' . $form->getOption('group'),
                            ExtensionNamingUtility::getExtensionKey($extension)
                        ) ?? $form->getOption('group');
                } else {
                    $groupName = $extension;
                }

                $label = $form->getLabel();
                $groups[$groupName][$label] = $form;
            }
        }
        $items = [];
        foreach ($groups as $group => $forms) {
            ksort($forms);
            $enabledElements = [];
            foreach ($forms as $form) {
                $icon = MiscellaneousUtility::getIconForTemplate($form);
                $enabledElements[] = [
                    $form->getLabel(),
                    $form->getOption('contentElementId'),
                    strpos($icon, 'EXT:') === 0 ? $icon : '..' . $icon,
                ];
            }
            if (!empty($enabledElements)) {
                $items[] = [
                    $group,
                    '--div--',
                ];
                $items = array_merge($items, $enabledElements);
            }
        }

        return $items;
    }
}