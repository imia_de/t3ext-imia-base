<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Backend\Tree\View;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class ContentCreationPagePositionMap extends \TYPO3\CMS\Backend\Tree\View\ContentCreationPagePositionMap
{
    /**
     * @inheritdoc
     */
    public function onClickInsertRecord($row, $vv, $moveUid, $pid, $sys_lang = 0)
    {
        $moduleConfig = [
            'edit[tt_content][' . (is_array($row) ? -$row['uid'] : $pid) . ']' => 'new',
            'defVals[tt_content][colPos]'                                      => $vv,
            'defVals[tt_content][sys_language_uid]'                            => $sys_lang,
            'returnUrl'                                                        => $GLOBALS['SOBE']->R_URI,
        ];
        if (GeneralUtility::_GP('grid_parent')) {
            $moduleConfig['defVals[tt_content][grid_parent]'] = (int)GeneralUtility::_GP('grid_parent');
        }

        $location = BackendUtility::getModuleUrl('record_edit', $moduleConfig);

        return 'window.location.href=' . GeneralUtility::quoteJSvalue($location) . '+document.editForm.defValues.value; return false;';
    }
}