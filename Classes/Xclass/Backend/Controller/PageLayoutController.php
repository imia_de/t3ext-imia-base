<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Backend\Controller;

use IMIA\ImiaBase\Traits\HookTrait;
use TYPO3\CMS\Backend\Template\Components\ButtonBar;
use TYPO3\CMS\Backend\Template\Components\Buttons\LinkButton;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class PageLayoutController extends \TYPO3\CMS\Backend\Controller\PageLayoutController
{
    use HookTrait;

    public function clearCache()
    {
        parent::clearCache();

        if (GeneralUtility::_GP('clear_cache')) {
            \IMIA\ImiaBase\Utility\Cache::flushByTag('pagecontent-'.$this->id);
        }

        if (GeneralUtility::_GP('clear_cache_subpages')) {
            /** @var DataHandler $tce */
            $tce = GeneralUtility::makeInstance(DataHandler::class);
            $tce->stripslashes_values = false;
            $tce->start([], []);

            $pageUids = $this->getSubpageUids($this->id);
            foreach ($pageUids as $uid) {
                $tce->clear_cacheCmd($uid);
                \IMIA\ImiaBase\Utility\Cache::flushByTag('pagecontent-'.$uid);
            }
        }
    }

    /**
     * @inheritdoc
     */
    protected function makeButtons()
    {
        parent::makeButtons();

        if (!$this->modTSconfig['properties']['disableAdvanced']) {
            if ($this->getBackendUser()->isAdmin() || !isset($this->modTSconfig['properties']['disableClearCache']) || !$this->modTSconfig['properties']['disableClearCache']) {
                foreach ($this->buttonBar->getButtons() as &$buttonGroup) {
                    /** @var LinkButton $button */
                    foreach ($buttonGroup as &$buttons) {
                        if (is_array($buttons)) {
                            foreach ($buttons as &$button) {
                                if ($button instanceof LinkButton && $button->getTitle() ==
                                    $this->getLanguageService()->sL('LLL:EXT:lang/locallang_core.xlf:labels.clear_cache')
                                ) {
                                    $button->setIcon($this->iconFactory->getIcon('actions-system-cache-clear-impact-low', Icon::SIZE_SMALL));
                                }
                            }
                        }
                    }
                }

                $pageRow = $this->getDb()->exec_SELECTgetSingleRow('COUNT(uid) AS childPages', 'pages',
                    'deleted = 0 AND pid = ' . (int)$this->pageinfo['uid']);

                if ((int)$pageRow['childPages'] > 0) {
                    /** @var LinkButton $clearCacheButton */
                    $clearCacheButton = $this->buttonBar->makeLinkButton()
                        ->setHref(BackendUtility::getModuleUrl($this->moduleName, ['id' => $this->pageinfo['uid'], 'clear_cache_subpages' => '1']))
                        ->setTitle($this->getLanguageService()->sL('LLL:EXT:imia_base/Resources/Private/Language/locallang.xlf:labels.clear_cache.subpages'))
                        ->setIcon($this->iconFactory->getIcon('actions-system-cache-clear-impact-medium', Icon::SIZE_SMALL));

                    $this->buttonBar->addButton($clearCacheButton, ButtonBar::BUTTON_POSITION_RIGHT, 1);
                }
            }

            /** @var LinkButton $reloadButton */
            $reloadButton = $this->buttonBar->makeLinkButton()
                ->setHref(BackendUtility::getModuleUrl($this->moduleName, ['id' => $this->pageinfo['uid']]))
                ->setTitle($this->getLanguageService()->sL('LLL:EXT:lang/locallang_core.xlf:labels.reload'))
                ->setIcon($this->iconFactory->getIcon('actions-refresh', Icon::SIZE_SMALL));

            $this->buttonBar->addButton($reloadButton, ButtonBar::BUTTON_POSITION_RIGHT, 1);
        }

        if (!$this->getBackendUser()->isAdmin() && isset($this->modTSconfig['properties']['disableClearCache']) && $this->modTSconfig['properties']['disableClearCache']) {
            foreach ($this->buttonBar->getButtons() as &$buttonGroup) {
                /** @var LinkButton $button */
                foreach ($buttonGroup as &$buttons) {
                    if (is_array($buttons)) {
                        foreach ($buttons as &$button) {
                            if ($button instanceof LinkButton && $button->getTitle() ==
                                $this->getLanguageService()->sL('LLL:EXT:lang/locallang_core.xlf:labels.clear_cache')
                            ) {
                                $button->setClasses('hidden');
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @param integer $uid
     * @return array
     */
    protected function getSubpageUids($uid)
    {
        $uids = [(int)$uid];

        $result = $this->getDb()->exec_SELECTquery('uid', 'pages', 'deleted = 0 AND pid = ' . (int)$uid);
        while ($row = $this->getDb()->sql_fetch_assoc($result)) {
            $uids = array_merge($uids, $this->getSubpageUids($row['uid']));
        }

        return $uids;
    }

    public function menuConfig()
    {
        parent::menuConfig();

        $this->callHook('menuConfig', [$this]);

        if ($this->getBackendUser()->getTSConfigVal('options.hideDefaultLanguage') && isset($this->MOD_MENU['language'])) {
            unset($this->MOD_MENU['language'][0]);
        }

        if (!array_key_exists((int)$this->MOD_SETTINGS['language'], $this->MOD_MENU['language'])) {
            $this->MOD_SETTINGS['language'] = array_first(array_keys($this->MOD_MENU['language']));
        }
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}