<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Backend\View;

use IMIA\ImiaBase\Traits\HookTrait;
use IMIA\ImiaBase\Utility\BackendTypoScript;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\FormProtection\FormProtectionFactory;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Type\Bitmask\Permission;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Fluid\View\StandaloneView;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class PageLayoutView extends \TYPO3\CMS\Backend\View\PageLayoutView
{
    use HookTrait;

    /**
     * @var ObjectManager
     */
    protected static $objectManager;

    /**
     * @var array
     */
    protected static $languages = [];

    /**
     * @return int
     */
    public function getCurrentLanguage()
    {
        return array_last(array_keys($this->contentElementCache));
    }

    /**
     * @param integer $id
     * @return string
     */
    public function getTable_tt_content($id)
    {
        $webRoot = str_replace(GeneralUtility::getIndpEnv('TYPO3_DOCUMENT_ROOT'), '', PATH_site);

        /** @var $pageRenderer PageRenderer */
        $pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
        $pageRenderer->addCssFile($webRoot . ExtensionManagementUtility::siteRelPath('imia_base') . 'Resources/Public/Stylesheets/layout.css');
        $pageRenderer->addJsFile($webRoot . ExtensionManagementUtility::siteRelPath('imia_base') . 'Resources/Public/Javascripts/js.cookie.js');
        $pageRenderer->addJsFile($webRoot . ExtensionManagementUtility::siteRelPath('imia_base') . 'Resources/Public/Javascripts/layout.js');

        $this->callHook('append', [$pageRenderer, $this]);

        return self::getTable_tt_content_add($this, $id, parent::getTable_tt_content($id));
    }

    /**
     * @param array $row
     * @param integer $space
     * @param boolean $disableMoveAndNewButtons
     * @param boolean $langMode
     * @param boolean $dragDropEnabled
     * @return string
     */
    public function tt_content_drawHeader($row, $space = 0, $disableMoveAndNewButtons = false, $langMode = false, $dragDropEnabled = false)
    {
        if (!$this->languageIconTitles) {
            $this->initializeLanguages();
        }

        $out = parent::tt_content_drawHeader($row, $space, $disableMoveAndNewButtons, !$this->tt_contentConfig['languageMode'], $dragDropEnabled);
        if (strpos($out, '<div class="btn-toolbar">') !== false) {
            $copyCutButtons = '';

            if ($this->tt_contentConfig['showInfo'] && $this->getBackendUser()->recordEditAccessInternals('tt_content', $row)
                && $this->tt_contentConfig['showCommands'] && $this->doEdit && $this->getBackendUser()->check('modules', 'web_list')) {
                $copyIcon = $this->iconFactory->getIcon('actions-edit-copy', Icon::SIZE_SMALL);
                $cutIcon = $this->iconFactory->getIcon('actions-edit-cut', Icon::SIZE_SMALL);

                $isSel = $this->clipboard->isSelected('tt_content', $row['uid']);
                if ($isSel === 'copy') {
                    $copyIcon = $this->iconFactory->getIcon('actions-edit-copy-release', Icon::SIZE_SMALL);
                } elseif ($isSel === 'cut') {
                    $cutIcon = $this->iconFactory->getIcon('actions-edit-cut-release', Icon::SIZE_SMALL);
                }

                $cutCopyParams = [
                    'M'           => 'web_list',
                    'id'          => GeneralUtility::_GP('id'),
                    'moduleToken' => FormProtectionFactory::get('backend')->generateToken('moduleCall', 'web_list'),
                    'returnUrl'   => GeneralUtility::getIndpEnv('REQUEST_URI'),
                    'collapse'    => ['_' => 1],
                ];

                $copyCutButtons .= '<a class="btn btn-default" href="'
                    . $this->clipboard->selUrlDB('tt_content', $row['uid'], 1, ($isSel === 'copy'), $cutCopyParams)
                    . '" title="' . htmlspecialchars($this->getLanguageService()->sL('LLL:EXT:lang/Resources/Private/Language/locallang_core.xlf:cm.copy')) . '">'
                    . $copyIcon->render() . '</a>';

                $copyCutButtons .= '<a class="btn btn-default" href="'
                    . $this->clipboard->selUrlDB('tt_content', $row['uid'], 0, ($isSel === 'cut'), [$cutCopyParams])
                    . '" title="' . htmlspecialchars($this->getLanguageService()->sL('LLL:EXT:lang/Resources/Private/Language/locallang_core.xlf:cm.cut')) . '">'
                    . $cutIcon->render() . '</a>';

                if ($out && $this->getBackendUser()->doesUserHaveAccess($this->pageinfo, Permission::CONTENT_EDIT)) {
                    $copyCutButtons = '<div class="btn-group btn-group-sm" role="group">' . $copyCutButtons . '</div>';
                } else {
                    $copyCutButtons = '';
                }
            }

            $renderGrid = false;
            foreach ($GLOBALS['TCA']['tt_content']['special'] as $specialType => $cTypes) {
                if (in_array($row['CType'], $cTypes)) {
                    $renderGrid = true;
                    break;
                }
            }

            if ($renderGrid) {
                $collapseIcon = $this->iconFactory->getIcon('actions-view-list-collapse', Icon::SIZE_SMALL);
                $expandIcon = $this->iconFactory->getIcon('actions-view-list-expand', Icon::SIZE_SMALL);

                $class = '';
                if ($_COOKIE['content-grid-hidden-' . $row['uid']]) {
                   $class = ' collapsed';
                }

                $toggleButton = '<a class="btn btn-default toggle-grid' . $class . '" href="#" title="' .
                    htmlspecialchars($this->getLanguageService()->sL('LLL:EXT:imia_base/Resources/Private/Language/Backend.xlf:gridlayout.grid.toggle')) . '">'
                    . $collapseIcon->render() . $expandIcon->render() . '</a>';

                $out = str_replace('</div></div></div>', '</div><div class="btn-group btn-group-sm" role="group">' .
                    $toggleButton . '</div></div></div>', $out);
            }

            $out = str_replace('<div class="btn-toolbar">', '<div class="btn-toolbar">' . $copyCutButtons, $out);
        }

        return $out;
    }

    /**
     * @inheritdoc
     */
    public function newLanguageButton($defLanguageCount, $lP, $colPos = 0)
    {
        if (!isset($this->contentElementCache[$lP][$colPos])) {
            $this->contentElementCache[$lP][$colPos] = [];
        }

        return parent::newLanguageButton($defLanguageCount, $lP, $colPos);
    }

    /**
     * @param \TYPO3\CMS\Backend\View\PageLayoutView $obj
     * @param integer $id
     * @param string $out
     * @return string
     */
    public static function getTable_tt_content_add($obj, $id, $out)
    {
        $pageLayoutEnabled = false;
        if (GeneralUtility::_GP('pagelayout')) {
            $pageLayoutEnabled = true;
        } elseif (ExtensionManagementUtility::isLoaded('imia_pageteaser')) {
            $extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['imia_pageteaser']);
            if (is_array($extConf)) {
                if ($extConf['pagelayoutForced']) {
                    $pageLayoutEnabled = true;
                } else {
                    $record = BackendUtility::getRecord('pages', $id);
                    $pageTreeHiddenDoktypes = array_map('intval', explode(',', $extConf['doktypesPageTreeHidden']));
                    if ($record && in_array($record['doktype'], $pageTreeHiddenDoktypes)) {
                        $_GET['pagelayout'] = $_REQUEST['pagelayout'] = 1;
                        $pageLayoutEnabled = true;
                    }
                }
            }
        }

        if (!$obj->tt_contentConfig['single'] && !$obj->tt_contentConfig['languageMode'] && !$pageLayoutEnabled) {
            $languageSelector = null;
            $languageItems = '';

            if ($languageSelector || $languageItems) {
                $out = '<div class="languageSelection"><div class="languageItems">' . $languageItems . '</div><div>' . $languageSelector . '</div></div>' . $out;
            }

            // Special Fluidpages BackendLayout
            $out = self::getSpecialBackendLayout($obj) . $out;
        }

        return $out;
    }

    /**
     * @param \TYPO3\CMS\Backend\View\PageLayoutView $obj
     * @return string
     */
    public static function getSpecialBackendLayout($obj)
    {
        $extensions = \IMIA\ImiaBase\Registry\FluidStyledContentRegistry::getPageExtensionProviders();
        if (count($extensions) > 0) {
            $templateRootPaths = $partialsRootPaths = $layoutsRootPaths = [];
            foreach ($extensions as $extKey) {
                $templateRootPaths[] = 'EXT:' . $extKey . '/Resources/Private/Templates/Page/';
                $layoutsRootPaths[] = 'EXT:' . $extKey . '/Resources/Private/Layouts/Page/';
                $partialsRootPaths[] = 'EXT:' . $extKey . '/Resources/Private/Partials/Page/';
            }

            /** @var StandaloneView $standaloneView */
            $standaloneView = GeneralUtility::makeInstance(StandaloneView::class);
            $standaloneView->setTemplate(str_replace('pagets__', '', $obj->getBackendLayoutView()->getSelectedCombinedIdentifier($obj->pageRecord['uid'])));
            $standaloneView->setTemplateRootPaths($templateRootPaths);
            $standaloneView->setLayoutRootPaths($layoutsRootPaths);
            $standaloneView->setPartialRootPaths($partialsRootPaths);

            return $standaloneView->renderSection('BackendLayout', ['record' => $obj->pageRecord], true);
        } else {
            return '';
        }

        /*
        if (ExtensionManagementUtility::isLoaded('fluidpages')) {
            $pageOutput = '';
            $gridId = '###GRID-' . md5(uniqid(time())) . '###';

            $section = self::getFluxTemplateSection($id, 'BackendLayout', ['grid' => $gridId]);
            if ($section) {
                $pageOutput = $section;
            }

            if (strpos($pageOutput, $gridId) !== false) {
                $pageOutput = str_replace($gridId, $out, $pageOutput);
            } else {
                $pageOutput .= $out;
            }
        } else {
            $backendLayout = str_replace('pagets__', '', $obj->getBackendLayoutView()->getSelectedCombinedIdentifier($id));
            $typoScript = BackendTypoScript::get($id);

            var_dump($typoScript['lib.']);


            /** @var StandaloneView $standaloneView *//*
            $standaloneView = GeneralUtility::makeInstance(StandaloneView::class);
            $standaloneView->setTemplate($backendLayout);
            $standaloneView->setTemplateRootPaths($typoScript['lib.']['stdContent.']['templateRootPaths.']);
            $standaloneView->setLayoutRootPaths($typoScript['lib.']['stdContent.']['layoutRootPaths.']);
            $standaloneView->setPartialRootPaths($typoScript['lib.']['stdContent.']['partialRootPaths.']);

            $pageOutput = $standaloneView->renderSection('BackendLayout', ['record' => $obj->pageRecord], true) . $out;
        }

        return $pageOutput;*/
    }

    /**
     * @param integer $id
     * @param string $sectionName
     * @param array $additionalVariables
     * @return null|string
     */
    public static function getFluxTemplateSection($id, $sectionName, $additionalVariables)
    {
        $record = self::getObjectManager()
            ->get(\FluidTYPO3\Flux\Service\WorkspacesAwareRecordService::class)
            ->getSingle('pages', '*', $id);

        $provider = self::getFluxProvider($record);

        $out = null;
        if ($provider) {
            $templatePathAndFilename = $provider->getTemplatePathAndFilename($record);

            if (file_exists($templatePathAndFilename)) {
                $extensionKey = $provider->getExtensionKey($record);
                $paths = $provider->getTemplatePaths($record);

                $variables = self::getFluxConfigurationService()
                    ->convertFlexFormContentToArray($record[$provider->getFieldName($record)]);
                $variables['record'] = $record;
                $variables = array_merge($variables, $additionalVariables);

                $viewContext = new \FluidTYPO3\Flux\View\ViewContext($templatePathAndFilename, $extensionKey);
                $viewContext->setVariables($variables);
                $viewContext->setTemplatePaths(new \FluidTYPO3\Flux\View\TemplatePaths($paths));

                $exposedView = self::getFluxConfigurationService()
                    ->getPreparedExposedTemplateView($viewContext);

                $exposedView->setTemplatePathAndFilename($templatePathAndFilename);

                $configurationManager = self::getObjectManager()
                    ->get(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::class);

                if (!$GLOBALS['TSFE']->sys_page) {
                    if (!$GLOBALS['TSFE']) {
                        $GLOBALS['TSFE'] = (object)[];
                    }
                    $GLOBALS['TSFE']->sys_page = GeneralUtility::makeInstance(
                        \TYPO3\CMS\Frontend\Page\PageRepository::class);
                }

                $existingContentObject = $configurationManager->getContentObject();
                $contentObject = new ContentObjectRenderer();
                $contentObject->start($record, 'pages');
                $configurationManager->setContentObject($contentObject);

                $previewContent = $exposedView->renderStandaloneSection($sectionName, $variables);

                $configurationManager->setContentObject($existingContentObject);

                if ($previewContent) {
                    $out = $previewContent;
                }
            }
        }

        return $out;
    }

    /**
     * @param array $record
     * @return \FluidTYPO3\Flux\Provider\ProviderInterface
     */
    public static function getFluxProvider($record)
    {
        $provider = null;

        try {
            if ($record) {
                $provider = self::getFluxConfigurationService()
                    ->resolvePrimaryConfigurationProvider('pages', 'tx_fed_page_flexform', $record);

                if ($provider) {
                    $action = $provider->getControllerActionFromRecord($record);
                    if (empty($action)) {
                        $provider = null;
                    }
                }
            }
        } catch (\Exception $e) {
        }

        return $provider;
    }

    /**
     * @return \FluidTYPO3\Fluidpages\Service\ConfigurationService
     */
    public static function getFluxConfigurationService()
    {
        /** @var \FluidTYPO3\Fluidpages\Service\ConfigurationService $configurationService */
        $configurationService = self::getObjectManager()->get(\FluidTYPO3\Fluidpages\Service\ConfigurationService::class);

        return $configurationService;
    }

    /**
     * @return ObjectManager
     */
    public static function getObjectManager()
    {
        if (!self::$objectManager) {
            self::$objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        }

        return self::$objectManager;
    }

    /**
     * @inheritdoc
     */
    public function getContentRecordsPerColumn($table, $id, array $columns, $additionalWhereClause = '', $additionalWhereClauseAdd = '`grid_parent` = 0')
    {
        if ($additionalWhereClauseAdd) {
            if ($additionalWhereClause) {
                $additionalWhereClause .= ' AND ' . $additionalWhereClauseAdd;
            } else {
                $additionalWhereClause .= $additionalWhereClauseAdd;
            }
        }

        return parent::getContentRecordsPerColumn($table, $id, $columns, $additionalWhereClause);
    }

    /**
     * @inheritdoc
     */
    public function checkIfTranslationsExistInLanguage(array $contentElements, $language)
    {
        return parent::checkIfTranslationsExistInLanguage($contentElements, $language);
    }

    /**
     * @inheritdoc
     */
    public function generateTtContentDataArray(array $rowArray)
    {
        return parent::generateTtContentDataArray($rowArray);
    }

    /**
     * @inheritdoc
     */
    public function tt_content_drawFooter(array $row)
    {
        return parent::tt_content_drawFooter($row);
    }

    /**
     * @return string
     */
    public static function getAdditionalStyles()
    {
        $styles = '.languageSelection { border-bottom: 1px solid #cdcdcd; position: relative; top: -15px; text-align: right; min-height: 40px; padding-bottom: 10px }';
        $styles .= '.languageSelection .languageItems { display: inline-block; text-align: left; }';
        $styles .= '.languageSelection .languageItems p { margin: 6px 0; }';
        $styles .= '.languageSelection .languageItems p * { vertical-align: middle }';

        $styles .= '.language-contents { margin: 10px -5px -5px; }';
        $styles .= '.language-contents h4 { padding: 0 5px; }';
        $styles .= '.language-contents .t3-row-header { background: #f1f1f1 !important; }';
        $styles .= '.language-contents .t3-page-ce-body { border-color: #cdcdcd !important; }';
        $styles .= '.language-contents .t3-page-ce-body:last-child { display: none; }';
        $styles .= '.language-contents .create-new-language .ce-icons a { font-size: 11px; color: #444; font-weight: normal; font-style: italic }';
        $styles .= '.language-contents .create-new-language .t3-icon { visibility: visible !important }';
        $styles .= '.language-contents .create-new-language .t3-row-header { opacity: 0.8 }';
        $styles .= '.language-contents .t3-page-lang-copyce { margin: 0 !important; padding: 0 !important; font-size: 10px }';
        $styles .= '.language-contents .t3-page-lang-copyce input { margin: 0 !important }';
        //$styles .= '.language-contents .ce-icons-left > a { display: none !important }';

        // optimizing the basic styles
        $styles .= '.t3-gridTable { margin: 0 !important; }';
        $styles .= '.t3-gridContainer { margin: 0 -12px !important; }';
        $styles .= '#typo3-inner-docbody h1 { margin-bottom: 15px; padding-bottom: 15px; border-bottom: 1px solid #cdcdcd; }';
        $styles .= '#typo3-inner-docbody .t3-page-ce .t3-row-header .ce-icons, #typo3-inner-docbody .t3-page-ce .t3-row-header .ce-icons-left { visibility: visible; }';
        $styles .= '.t3-page-ce .t3-page-ce .t3-page-ce-body { border-color: #cacaca; }';
        $styles .= '.t3-page-ce .t3-page-ce .t3-page-ce-header .t3-row-header { background-color: #cacaca; }';
        $styles .= '#typo3-inner-docbody .t3-page-ce:hover > * > .t3-page-ce-body { border-color: #5b5b5b; }';
        $styles .= '#typo3-inner-docbody .t3-page-ce:hover > * > .t3-page-ce-header .t3-row-header { background-color: #5b5b5b; }';
        $styles .= '#typo3-inner-docbody h1 + .typo3-message { top: 20px !important }';

        return '<style type="text/css">' . $styles . '</style>';
    }
}