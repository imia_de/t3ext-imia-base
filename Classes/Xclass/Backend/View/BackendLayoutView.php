<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Backend\View;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class BackendLayoutView extends \TYPO3\CMS\Backend\View\BackendLayoutView
{
    /**
     * @param array $parameters
     */
    public function addBackendLayoutItems(array $parameters)
    {
        $pageId = $this->determinePageId($parameters['table'], $parameters['row']);
        $pageTsConfig = (array)BackendUtility::getPagesTSconfig($pageId);

        parent::addBackendLayoutItems($parameters);

        $removeItems = [];
        if (isset($pageTsConfig['TCEFORM.']['pages.'][$parameters['field'] . '.']['removeItems'])) {
            $removeItems = GeneralUtility::trimExplode(',', $pageTsConfig['TCEFORM.']['pages.'][$parameters['field'] . '.']['removeItems']);
        }

        $items = $parameters['items'];
        $parameters['items'] = [];
        foreach ($items as $item) {
            if (!in_array($item[1], $removeItems)) {
                $parameters['items'][] = $item;
            }
        }
    }
}