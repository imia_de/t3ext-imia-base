<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Backend\Form\FormDataProvider;

use TYPO3\CMS\Backend\Form\FormDataCompiler;
use TYPO3\CMS\Backend\Form\FormDataGroup\TcaDatabaseRecord;
use TYPO3\CMS\Backend\Form\InlineStackProcessor;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class TcaInline extends \TYPO3\CMS\Backend\Form\FormDataProvider\TcaInline
{
    /**
     * @inheritdoc
     */
    public function addData(array $result)
    {
        $result = parent::addData($result);

        if ($result['tableName'] == 'tt_content' && empty($result['flexParentDatabaseRow']) && $result['command'] == 'new') {
            $defVals = GeneralUtility::_GET('defVals');
            if (is_array($defVals)) {
                $addInline = [];
                foreach ($defVals as $table => $defValues) {
                    foreach ($defValues as $column => $defVal) {
                        if (strpos($column, '.') !== false) {
                            $columnConfig = explode('.', $column);
                            if (isset($GLOBALS['TCA'][$table]['columns'][$columnConfig[0]]['config']['type'])) {
                                $config = $GLOBALS['TCA'][$table]['columns'][$columnConfig[0]]['config'];

                                if ($config['type'] == 'inline') {
                                    $addInline[$config['foreign_table']][$columnConfig[0]][$columnConfig[1]][$columnConfig[2]] = $defVal;
                                }
                            }
                        }
                    }
                }

                foreach ($addInline as $table => $conf) {
                    foreach ($conf as $field => $items) {
                        foreach ($items as $item) {
                            $saveGET = $_GET;
                            $_GET['defVals'] = [$table => $item];
                            $result['processedTca']['columns'][$field]['children'][] = $this->addChild($result, $field);
                            $_GET = $saveGET;
                        }
                    }
                }
            }
        }

        return $result;
    }


    /**
     * Compile a full child record
     *
     * @param array $result Result array of parent
     * @param string $parentFieldName Name of parent field
     * @param int $childUid Uid of child to compile
     * @return array Full result array
     */
    protected function addChild(array $result, $parentFieldName)
    {
        $parentConfig = $result['processedTca']['columns'][$parentFieldName]['config'];
        $childTableName = $parentConfig['foreign_table'];

        /** @var InlineStackProcessor $inlineStackProcessor */
        $inlineStackProcessor = GeneralUtility::makeInstance(InlineStackProcessor::class);
        $inlineStackProcessor->initializeByGivenStructure($result['inlineStructure']);
        $inlineTopMostParent = $inlineStackProcessor->getStructureLevel(0);

        /** @var TcaDatabaseRecord $formDataGroup */
        $formDataGroup = GeneralUtility::makeInstance(TcaDatabaseRecord::class);
        /** @var FormDataCompiler $formDataCompiler */
        $formDataCompiler = GeneralUtility::makeInstance(FormDataCompiler::class, $formDataGroup);

        $vanillaUid = (int)$result['parentPageRow']['uid'];

        $formDataCompilerInput = [
            'command' => 'new',
            'tableName' => $childTableName,
            'vanillaUid' => $vanillaUid,
            'isInlineChild' => true,
            'inlineStructure' => $inlineStackProcessor->getStructure(),
            'inlineFirstPid' => $result['inlineFirstPid'],
            'inlineParentUid' => $result['databaseRow']['uid'],
            'inlineParentTableName' => $result['tableName'],
            'inlineParentFieldName' => $parentFieldName,
            'inlineParentConfig' => $parentConfig,
            'inlineTopMostParentUid' => $inlineTopMostParent['uid'],
            'inlineTopMostParentTableName' => $inlineTopMostParent['table'],
            'inlineTopMostParentFieldName' => $inlineTopMostParent['field'],
        ];

        $mainChild = $formDataCompiler->compile($formDataCompilerInput);
        $mainChild['databaseRow']['pid'] = $result['parentPageRow']['uid'];

        return $mainChild;
    }
}