<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Backend\Form\FormDataProvider;

use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class DatabaseRowInitializeNew extends \TYPO3\CMS\Backend\Form\FormDataProvider\DatabaseRowInitializeNew
{
    /**
     * @inheritdoc
     */
    public function addData(array $result)
    {
        $result = parent::addData($result);

        if ($result['command'] == 'new') {
            $defVals = GeneralUtility::_GET('defVals');
            if (is_array($defVals)) {
                $specialRowData = [];
                foreach ($defVals as $table => $defValues) {
                    foreach ($defValues as $column => $defVal) {
                        if (strpos($column, '.') !== false) {
                            $columnConfig = explode('.', $column);
                            if (isset($GLOBALS['TCA'][$table]['columns'][$columnConfig[0]]['config']['type'])) {
                                $type = $GLOBALS['TCA'][$table]['columns'][$columnConfig[0]]['config']['type'];

                                if ($type == 'flex' && !$result['databaseRow'][$columnConfig[0]]) {
                                    $specialRowData[$columnConfig[0]]['data'][$columnConfig[1]]['lDEF'][$columnConfig[2]]['vDEF'] = $defVal;
                                }
                            }
                        }
                    }
                }
                ArrayUtility::mergeRecursiveWithOverrule($result['databaseRow'], $specialRowData);
            }
        }

        return $result;
    }
}