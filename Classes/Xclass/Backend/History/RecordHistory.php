<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Backend\History;

use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class RecordHistory extends \TYPO3\CMS\Backend\History\RecordHistory
{
    /**
     * @inheritdoc
     */
    protected function getArgument($name)
    {
        if ($name == 'diff') {
            return GeneralUtility::_GP($name);
        } else {
            return parent::getArgument($name);
        }
    }

    /**
     * @inheritdoc
     */
    public function getHistoryData($table, $uid)
    {
        if (empty($GLOBALS['TCA'][$table]) || !$this->hasTableAccess($table) || !$this->hasPageAccess($table, $uid)) {
            // error fallback
            return 0;
        }
        // If table is found in $GLOBALS['TCA']:
        $databaseConnection = $this->getDatabaseConnection();
        $uid = $this->resolveElement($table, $uid);
        // Selecting the $this->maxSteps most recent states:
        $rows = $databaseConnection->exec_SELECTgetRows(
            'sys_history.*, sys_log.userid, sys_log.log_data',
            'sys_history LEFT JOIN sys_log ON sys_history.sys_log_uid = sys_log.uid',
            'sys_history.tablename = ' . $databaseConnection->fullQuoteStr($table, 'sys_history') .
            '	AND sys_history.recuid = ' . (int)$uid,
            '',
            'sys_history.tstamp DESC',
            $this->maxSteps
        );

        $changeLog = [];
        if (!empty($rows)) {
            // Traversing the result, building up changesArray / changeLog:
            foreach ($rows as $row) {
                if ($this->lastSyslogId && (int)$row['tstamp'] < (int)preg_replace('/^([0-9]+).*?$/ism', '$1', $this->lastSyslogId)) {
                    continue;
                }
                $hisDat = unserialize($row['history_data']);
                if ($row['log_data']) {
                    $logData = unserialize($row['log_data']);
                } else {
                    $logData = [];
                }

                if (is_array($hisDat['newRecord']) && is_array($hisDat['oldRecord'])) {
                    // Add information about the history to the changeLog
                    $hisDat['uid'] = $row['uid'];
                    $hisDat['tstamp'] = $row['tstamp'];
                    $hisDat['user'] = $row['userid'];
                    $hisDat['originalUser'] = (empty($logData['originalUser']) ? null : $logData['originalUser']);
                    $hisDat['snapshot'] = $row['snapshot'];
                    $hisDat['fieldlist'] = $row['fieldlist'];
                    $hisDat['tablename'] = $row['tablename'];
                    $hisDat['recuid'] = $row['recuid'];
                    $changeLog[$row['tstamp'] . '_' . $row['uid']] = $hisDat;
                } else {
                    debug('ERROR: [getHistoryData]');

                    return 0;
                }
            }
        }

        if ($this->showInsertDelete) {
            // Select most recent inserts and deletes // WITHOUT snapshots
            $rows = $databaseConnection->exec_SELECTgetRows(
                'uid, userid, action, tstamp, log_data',
                'sys_log',
                'type = 1 AND (action=1 OR action=3) AND tablename = ' .
                $databaseConnection->fullQuoteStr($table, 'sys_log') . ' AND recuid = ' . (int)$uid,
                '',
                'uid DESC',
                $this->maxSteps
            );

            // If none are found, nothing more to do
            if (empty($rows)) {
                return $changeLog;
            }
            foreach ($rows as $row) {
                if ($this->lastSyslogId && (int)$row['tstamp'] < (int)preg_replace('/^([0-9]+).*?$/ism', '$1', $this->lastSyslogId)) {
                    continue;
                }
                $hisDat = [];
                $logData = unserialize($row['log_data']);
                switch ($row['action']) {
                    case 1:
                        // Insert
                        $hisDat['action'] = 'insert';
                        break;
                    case 3:
                        // Delete
                        $hisDat['action'] = 'delete';
                        break;
                }

                $hisDat['tstamp'] = $row['tstamp'];
                $hisDat['user'] = $row['userid'];
                $hisDat['originalUser'] = (empty($logData['originalUser']) ? null : $logData['originalUser']);
                $hisDat['tablename'] = $table;
                $hisDat['recuid'] = $uid;

                $changeLog[$row['tstamp'] . '_log_' . $row['uid']] = $hisDat;
            }
        }

        return $changeLog;
    }
}