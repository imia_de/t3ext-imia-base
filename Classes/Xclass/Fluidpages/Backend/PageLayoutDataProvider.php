<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Fluidpages\Backend;

use FluidTYPO3\Flux\Form;
use FluidTYPO3\Flux\Utility\MiscellaneousUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class PageLayoutDataProvider extends \FluidTYPO3\Fluidpages\Backend\PageLayoutDataProvider
{
    /**
     * @param Form $form
     * @param array $parameters
     * @return string
     */
    protected function renderOption(Form $form, array $parameters)
    {
        $option = [];
        try {
            $extension = $form->getExtensionName();
            $thumbnail = MiscellaneousUtility::getIconForTemplate($form);
            if (NULL !== $thumbnail) {
                if (strpos($thumbnail, '../') === 0) {
                    $thumbnail = substr($thumbnail, 3);
                }
                $thumbnail = ltrim($thumbnail, '/');
                $thumbnail = MiscellaneousUtility::createIcon(GeneralUtility::getFileAbsFileName($thumbnail), 64, 64);
            }
            $template = pathinfo($form->getOption(Form::OPTION_TEMPLATEFILE), PATHINFO_FILENAME);
            $formLabel = $form->getLabel();
            if (strpos($formLabel, 'LLL:') === 0) {
                $label = LocalizationUtility::translate($formLabel, $extension);
            } else {
                $label = $formLabel;
            }
            $optionValue = $extension . '->' . lcfirst($template);
            $option = [$label, $optionValue, $thumbnail];

        } catch (\RuntimeException $error) {
            $this->configurationService->debug($error);
        }

        return $option;
    }
}