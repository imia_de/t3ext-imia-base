<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Fluidpages\Service;

use IMIA\ImiaBase\Xclass\Flux\Form;
use TYPO3\CMS\Backend\Controller\EditDocumentController;
use TYPO3\CMS\Backend\Utility\BackendUtility;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class PageService extends \FluidTYPO3\Fluidpages\Service\PageService
{
    /**
     * @inheritdoc
     */
    public function getAvailablePageTemplateFiles($format = 'html')
    {
        $output = parent::getAvailablePageTemplateFiles($format);

        $pageUid = (int)$this->getSobe()->viewId;
        $tsConfig = BackendUtility::getPagesTSconfig($pageUid);

        $blacklist = [];
        if (isset($tsConfig['TCEFORM.']['pages.']['tx_fed_page_controller_action.']['removeItems'])) {
            foreach (array_map('trim', explode(',', $tsConfig['TCEFORM.']['pages.']['tx_fed_page_controller_action.']['removeItems'])) as $blacklistedItem) {
                $blacklist[] = $blacklistedItem;
            }
            $blacklist = array_unique($blacklist);
        }

        foreach ($output as $extKey => $templates) {
            if (is_array($templates)) {
                /** @var Form $form */
                foreach ($templates as $key => $form) {
                    $extension = $form->getExtensionName();
                    $template = pathinfo($form->getOption(Form::OPTION_TEMPLATEFILE), PATHINFO_FILENAME);
                    $optionValue = $extension . '->' . lcfirst($template);

                    if (in_array($optionValue, $blacklist)) {
                        unset($output[$extKey][$key]);
                    }
                }
            }
        }

        return $output;
    }

    /**
     * @return EditDocumentController
     */
    protected function getSobe()
    {
        return $GLOBALS['SOBE'];
    }
}