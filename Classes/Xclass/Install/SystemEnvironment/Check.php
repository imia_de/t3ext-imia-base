<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Install\SystemEnvironment;

use TYPO3\CMS\Install\Status;

/**
 * @package     imia_base
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class Check extends \TYPO3\CMS\Install\SystemEnvironment\Check
{
    /**
     * @return Status\StatusInterface
     */
    protected function checkCurrentDirectoryIsInIncludePath()
    {
        $includePath = ini_get('include_path');
        $delimiter = $this->isWindowsOs() ? ';' : ':';
        $pathArray = $this->trimExplode($delimiter, $includePath);
        if (!in_array('.', $pathArray) && !in_array('./', $pathArray)) {
            $status = new Status\WarningStatus();
            $status->setTitle('Current directory (./) is not within PHP include path');
            $status->setMessage(
                'include_path = ' . implode(' ', $pathArray) . LF .
                'Normally the current path \'.\' is included in the' .
                ' include_path of PHP. Although TYPO3 does not rely on this,' .
                ' it is an unusual setting that may introduce problems for' .
                ' some extensions.'
            );
        } else {
            $status = new Status\OkStatus();
            $status->setTitle('Current directory (./) is within PHP include path.');
        }

        return $status;
    }
}
