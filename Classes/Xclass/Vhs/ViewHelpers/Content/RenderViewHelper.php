<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Vhs\ViewHelpers\Content;

use FluidTYPO3\Fluidcontent\Provider\ContentProvider;
use IMIA\ImiaBase\Utility\ContentCache;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;
use TYPO3\CMS\Frontend\Page\CacheHashCalculator;

/**
 * @package     imia_base
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class RenderViewHelper extends \FluidTYPO3\Vhs\ViewHelpers\Content\RenderViewHelper
{
    /**
     * @var string
     */
    protected static $cHash;

    /**
     * @return string
     */
    protected static function getCHash()
    {
        if (!self::$cHash) {
            if (self::getTSFE()->no_cache) {
                $cacheHash = GeneralUtility::makeInstance(CacheHashCalculator::class);
                $params = GeneralUtility::_GET();
                if (isset(self::getTSFE()->id)) {
                    $params['id'] = self::getTSFE()->id;
                }
                self::getTSFE()->cHash_array = $cacheHash->getRelevantParameters(GeneralUtility::implodeArrayForUrl('', $params));
            }
            self::$cHash = self::getTSFE()->getHash();
        }

        return self::$cHash;
    }

    /**
     * @inheritdoc
     */
    protected static function renderRecord(array $row)
    {
        $html = '';
        $lastChange = 0;
        $identifier = $row['uid'] . '_' . self::getCHash() . '_' . self::getTSFE()->sys_language_uid;
        $tsfeCache = (!self::getTSFE()->no_cache && !self::getTSFE()->page['no_cache']) || !self::getTSFE()->tmpl->setup['config.']['content_respect_nocache'];

        if (ContentCache::enabled()) {
            if ($tsfeCache) {
                $noCache = false;
                if (getenv('TYPO3_CONTEXT') == 'Development' && $row['CType'] == 'fluidcontent_content') {
                    $lastChangeCached = ContentCache::get($identifier . '_fce');
                    $lastChange = self::getFCELastChange($row);
                    if (!$lastChange) {
                        $lastChange = time();
                    }
                    if ((int)$lastChangeCached < $lastChange) {
                        $noCache = true;
                    }
                } elseif ($row['CType'] == 'list') {
                    $noCache = true;
                }

                if (!$noCache) {
                    $html = ContentCache::get($identifier);
                    if (stripos($html, '<!--INT_SCRIPT') !== false && stripos($html, '<!--DISABLE_CONTENT_CACHE') === false) {
                        $html = '';
                    }
                }
            }
        }

        if (!$html) {
            $html = parent::renderRecord($row);
            if (!$html) {
                $html = '';
            }

            if (ContentCache::enabled()) {
                if ($tsfeCache && stripos($html, '<!--INT_SCRIPT') === false && stripos($html, '<!--DISABLE_CONTENT_CACHE') === false) {
                    $tags = ['page-' . self::getTSFE()->id, 'content-' . $row['uid']];
                    self::addContentChildrenToTags($tags, $row);

                    $lifetime = 0;
                    if (isset($GLOBALS['ContentCache-Lifetime'][$row['uid']])) {
                        $lifetime = (int)$GLOBALS['ContentCache-Lifetime'][$row['uid']];
                    }

                    // HOOK
                    if (is_array($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ContentCache']['preCacheSet'])) {
                        $_params = [
                            'tags' => &$tags,
                            'row' => $row,
                            'identifier' => &$identifier,
                            'lifetime' => $lifetime
                        ];
                        foreach ($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ContentCache']['preCacheSet'] as $_funcRef) {
                            $pObj = new \stdClass();
                            GeneralUtility::callUserFunction($_funcRef, $_params, $pObj);
                        }
                    }

                    ContentCache::set($identifier, $html, $tags, $lifetime);
                    if (getenv('TYPO3_CONTEXT') == 'Development' && $row['CType'] == 'fluidcontent_content' && $lastChange) {
                        ContentCache::set($identifier . '_fce', $lastChange, $tags, $lifetime);
                    }
                }
            }
        }
        $html = str_replace('<!--DISABLE_CONTENT_CACHE-->', '', $html);

        return $html;
    }

    /**
     * @param array $tags
     * @param array $row
     */
    protected static function addContentChildrenToTags(&$tags, $row)
    {
        if ($row['CType'] == 'fluidcontent_content') {
            $result = self::getDb()->exec_SELECTquery('uid, CType', 'tt_content',
                'tx_flux_parent = ' . $row['uid'] . self::enableFields('tt_content'));
            while ($subrow = self::getDb()->sql_fetch_assoc($result)) {
                $tags[] = 'content-' . $subrow['uid'];
                self::addContentChildrenToTags($tags, $subrow);
            }
        }
    }

    /**
     * @param array $row
     * @return integer
     */
    protected static function getFCELastChange($row)
    {
        $lastChange = 0;
        if ($row['tx_fed_fcefile']) {
            try {
                /** @var ObjectManager $objectManager */
                $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
                
                /** @var ContentProvider $contentProvider */
                $contentProvider = $objectManager->get(ContentProvider::class);
                $fcePath = $contentProvider->getTemplatePathAndFilename($row);

                if (file_exists($fcePath)) {
                    $lastChange = (int)filemtime($fcePath);
                    $result = self::getDb()->exec_SELECTquery('uid, tx_fed_fcefile', 'tt_content', 'tx_flux_parent = ' . (int)$row['uid'] . ' AND deleted = 0');
                    while ($subRow = self::getDb()->sql_fetch_assoc($result)) {
                        $lastChangeSub = self::getFCELastChange($subRow);
                        if ($lastChangeSub > $lastChange) {
                            $lastChange = $lastChangeSub;
                        }
                    }
                }
            } catch (\Exception $e) {
            }
        }

        return $lastChange;
    }

    /**
     * @return DatabaseConnection
     */
    protected static function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }

    /**
     * @return TypoScriptFrontendController
     */
    protected static function getTSFE()
    {
        return $GLOBALS['TSFE'];
    }

    /**
     * @param string $table
     * @return string
     */
    protected static function enableFields($table)
    {
        if ($GLOBALS['TSFE'] && $GLOBALS['TSFE']->sys_page) {
            return $GLOBALS['TSFE']->sys_page->enableFields($table);
        } else {
            return BackendUtility::BEenableFields($table) . BackendUtility::deleteClause($table);
        }
    }

    /**
     * Get content records based on column and pid
     *
     * @return array
     */
    protected function getContentRecords()
    {
        $limit = $this->arguments['limit'];

        $pageUid = $this->getPageUid();

        if ((integer) $this->arguments['slide'] === 0) {
            $contentRecords = $this->getSlideRecordsFromPage($pageUid, $limit);
        } else {
            $contentRecords = $this->getSlideRecords($pageUid, $limit);
        }

        if (true === (boolean) $this->arguments['render']) {
            $contentRecords = $this->getRenderedRecords($contentRecords);
        }

        return $contentRecords;
    }

    /**
     * Get records, optionally sliding up the page rootline
     *
     * @param integer $pageUid
     * @param integer $limit
     * @return array
     * @api
     */
    protected function getSlideRecords($pageUid, $limit = null)
    {
        if (null === $limit && false === empty($this->arguments['limit'])) {
            $limit = (integer)$this->arguments['limit'];
        }

        $slide = (integer)$this->arguments['slide'];
        $slideCollectReverse = (boolean)$this->arguments['slideCollectReverse'];
        $slideCollect = (integer)$this->arguments['slideCollect'];

        if (0 < $slideCollect) {
            $slide = $slideCollect;
        }

        // find out which storage page UIDs to read from, respecting slide depth
        $storagePageUids = [];
        if (0 === $slide) {
            $storagePageUids[] = $pageUid;
        } else {
            $reverse = false;
            if (true === $slideCollectReverse && 0 !== $slideCollect) {
                $reverse = true;
            }
            if ($pageUid == self::getTSFE()->id) {
                $rootLine = self::getTSFE()->rootLine;
                if ($reverse) {
                    $rootLine = array_reverse($rootLine);
                }
            } else {
                $rootLine = $this->getPageService()->getRootLine($pageUid, null, $reverse);
            }
            if (-1 !== $slide) {
                if (true === $reverse) {
                    $rootLine = array_slice($rootLine, -$slide);
                } else {
                    $rootLine = array_slice($rootLine, 0, $slide);
                }
            }
            foreach ($rootLine as $page) {
                $storagePageUids[] = (integer)$page['uid'];
            }
        }
        // select records, respecting slide and slideCollect.
        $records = [];
        do {
            $storagePageUid = array_shift($storagePageUids);
            $limitRemaining = null;
            if (null !== $limit) {
                $limitRemaining = $limit - count($records);
                if (0 >= $limitRemaining) {
                    break;
                }
            }
            $recordsFromPageUid = $this->getSlideRecordsFromPage($storagePageUid, $limitRemaining);
            if (0 < count($recordsFromPageUid)) {
                $records = array_merge($records, $recordsFromPageUid);
                if (0 === $slideCollect) {
                    // stop collecting because argument said so and we've gotten at least one record now.
                    break;
                }
            }
        } while (false === empty($storagePageUids));

        return $records;
    }

    /**
     * @param integer $pageUid
     * @param integer $limit
     * @return array[]
     */
    protected function getSlideRecordsFromPage($pageUid, $limit)
    {
        $order = $this->arguments['order'];
        if (false === empty($order)) {
            $sortDirection = strtoupper(trim($this->arguments['sortDirection']));
            if ('ASC' !== $sortDirection && 'DESC' !== $sortDirection) {
                $sortDirection = 'ASC';
            }
            $order = $order . ' ' . $sortDirection;
        }
        $hideUntranslated = (boolean) $this->arguments['hideUntranslated'];

        $currentLanguage = $GLOBALS['TSFE']->sys_language_content;
        if ($GLOBALS['TSFE']->sys_language_contentOL == 'hideUntranslated') {
            $languageUids = [-1, 0, $currentLanguage];
            $hideUntranslated = true;
        } elseif ($GLOBALS['TSFE']->sys_language_contentOL == 1) {
            $languageUids = [-1, 0];
        } else {
            $languageUids = [-1, $currentLanguage];
        }

        $languageCondition = sprintf('(sys_language_uid IN (%s)', implode(", ", $languageUids));
        if (0 < $currentLanguage) {
            if (true === $hideUntranslated) {
                $languageCondition .= ' AND l18n_parent > 0';
            }
            $nestedQuery = self::getDb()->SELECTquery('l18n_parent', 'tt_content', 'sys_language_uid = ' . $currentLanguage . $GLOBALS['TSFE']->cObj->enableFields('tt_content'));
            $languageCondition .= ' AND uid NOT IN (' . $nestedQuery . ')';
        }
        $languageCondition .= ')';

        $contentUids = $this->arguments['contentUids'];
        if (true === is_array($contentUids)) {
            $conditions = 'uid IN (' . implode(',', $contentUids) . ')';
        } else {
            if ($this->arguments['column'] !== null) {
                $conditions = 'colPos = \'' . $this->arguments['column'] . '\' AND pid = ' . (integer) $pageUid;
            } else {
                $conditions = 'pid = ' . (integer) $pageUid;
            }
        }

        $conditions .= $this->contentObject->enableFields('tt_content', false, ['pid' => true, 'hidden' => true]) . ' AND ' . $languageCondition;
        if (true === (boolean) $this->arguments['sectionIndexOnly']) {
            $conditions .= ' AND sectionIndex = 1';
        }
        if (ExtensionManagementUtility::isLoaded('workspaces')) {
            $conditions .= BackendUtility::versioningPlaceholderClause('tt_content');
        }

        $rows = self::getDb()->exec_SELECTgetRows('*', 'tt_content', $conditions, '', $order, $limit);

        if (!ExtensionManagementUtility::isLoaded('workspaces')) {
            return $rows;
        }

        $workspaceId = isset($GLOBALS['BE_USER']) ? $GLOBALS['BE_USER']->workspace : 0;
        if ($workspaceId) {
            foreach ($rows as $index => $row) {
                if (BackendUtility::getMovePlaceholder('tt_content', $row['uid'])) {
                    unset($rows[$index]);
                } else {
                    $rows[$index] = $row;
                }
            }
        }

        return $rows;
    }
}