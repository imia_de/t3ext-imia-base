<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Recordlist\RecordList;

use IMIA\ImiaBase\Traits\HookTrait;
use TYPO3\CMS\Backend\Template\Components\ButtonBar;
use TYPO3\CMS\Backend\Template\Components\Buttons\LinkButton;
use TYPO3\CMS\Backend\Template\ModuleTemplate;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class DatabaseRecordList extends \TYPO3\CMS\Recordlist\RecordList\DatabaseRecordList
{
    use HookTrait;

    /**
     * @var string
     */
    protected $listTable = null;

    /**
     * @return array
     */
    public function getButtons()
    {
        $buttons = parent::getButtons();

        $module = $this->getModule();
        if (!$module->modTSconfig['properties']['disableAdvanced']) {
            foreach ($buttons as &$button) {
                if (stripos($button, '') !== false) {
                    $buttons['cache'] = '<a href="' . htmlspecialchars(($this->listURL() . '&clear_cache=1')) . '" title="'
                        . $this->getLanguageService()->sL('LLL:EXT:lang/locallang_core.xlf:labels.clear_cache', true) . '">'
                        . $this->iconFactory->getIcon('actions-system-cache-clear', Icon::SIZE_SMALL)->render() . '</a>';
                }
            }

            $pageRow = $this->getDb()->exec_SELECTgetSingleRow('COUNT(uid) AS childPages', 'pages',
                'deleted = 0 AND pid = ' . (int)$this->pageinfo['uid']);

            if ((int)$pageRow['childPages'] > 0) {
                $buttons['cache'] .= '<a href="' . htmlspecialchars(($this->listURL() . '&clear_cache_subpages=1')) . '" title="'
                    . $this->getLanguageService()->sL('LLL:EXT:imia_base/Resources/Private/Language/locallang.xlf:labels.clear_cache.subpages', true) . '">'
                    . $this->iconFactory->getIcon('actions-system-cache-clear-impact-medium', Icon::SIZE_SMALL)->render() . '</a>';
            }
        }

        return $buttons;
    }

    /**
     * @param ModuleTemplate $moduleTemplate
     */
    public function getDocHeaderButtons(ModuleTemplate $moduleTemplate)
    {
        parent::getDocHeaderButtons($moduleTemplate);

        if (isset($this->id)) {
            $buttonBar = $moduleTemplate->getDocHeaderComponent()->getButtonBar();

            foreach ($buttonBar->getButtons() as &$buttonGroup) {
                /** @var LinkButton $button */
                foreach ($buttonGroup as &$buttons) {
                    if (is_array($buttons)) {
                        foreach ($buttons as $key => &$button) {
                            if ($button instanceof LinkButton
                                && $button->getTitle() == $this->getLanguageService()->sL('LLL:EXT:lang/locallang_core.xlf:labels.clear_cache')
                            ) {
                                $button->setIcon($this->iconFactory->getIcon('actions-system-cache-clear-impact-low', Icon::SIZE_SMALL));

                                if (!$this->getBackendUser()->isAdmin() && isset($this->modTSconfig['properties']['disableClearCache']) && $this->modTSconfig['properties']['disableClearCache']) {
                                    $button->setClasses('hidden');
                                }
                            } elseif ($button instanceof LinkButton
                                && $button->getTitle() == $this->getLanguageService()->sL('LLL:EXT:lang/locallang_core.xlf:labels.reload')
                            ) {
                                $button->setClasses('hidden');
                            }
                        }
                    }
                }
            }

            if ($this->getBackendUser()->isAdmin() || !isset($this->modTSconfig['properties']['disableClearCache']) || !$this->modTSconfig['properties']['disableClearCache']) {
                $pageRow = $this->getDb()->exec_SELECTgetSingleRow('COUNT(uid) AS childPages', 'pages',
                    'deleted = 0 AND pid = ' . (int)$this->pageinfo['uid']);

                if ((int)$pageRow['childPages'] > 0) {
                    /** @var LinkButton $clearCacheButton */
                    $clearCacheButton = $buttonBar->makeLinkButton()
                        ->setHref($this->listURL() . '&clear_cache_subpages=1')
                        ->setTitle($this->getLanguageService()->sL('LLL:EXT:imia_base/Resources/Private/Language/locallang.xlf:labels.clear_cache.subpages'))
                        ->setIcon($this->iconFactory->getIcon('actions-system-cache-clear-impact-medium', Icon::SIZE_SMALL));

                    $buttonBar->addButton($clearCacheButton, ButtonBar::BUTTON_POSITION_RIGHT);
                }
            }

            /** @var LinkButton $reloadButton */
            $reloadButton = $buttonBar->makeLinkButton()
                ->setHref($this->listURL())
                ->setTitle($this->getLanguageService()->sL('LLL:EXT:lang/locallang_core.xlf:labels.reload'))
                ->setIcon($this->iconFactory->getIcon('actions-refresh', Icon::SIZE_SMALL));

            $buttonBar->addButton($reloadButton, ButtonBar::BUTTON_POSITION_RIGHT);
        }
    }

    /**
     * @inheritdoc
     */
    public function renderListRow($table, $row, $cc, $titleCol, $thumbsCol, $indent = 0)
    {
        $this->listTable = $table;

        return parent::renderListRow($table, $row, $cc, $titleCol, $thumbsCol, $indent);
    }

    /**
     * @inheritdoc
     */
    public function addElement($h, $icon, $data, $rowParams = '', $_ = '', $_2 = '', $colType = 'td')
    {
        $this->callHook('addElement', [&$data, $this->listTable, $this]);

        return parent::addElement($h, $icon, $data, $rowParams, $_, $_2, $colType);
    }

    /**
     * @inheritdoc
     */
    protected function addToCSV(array $row = [])
    {
        $this->callHook('addToCsv', [&$row, $this->listTable, $this]);

        parent::addToCSV($row);
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }

    /**
     * Returns the current BE user.
     *
     * @return \TYPO3\CMS\Core\Authentication\BackendUserAuthentication
     */
    protected function getBackendUser()
    {
        return $GLOBALS['BE_USER'];
    }
}