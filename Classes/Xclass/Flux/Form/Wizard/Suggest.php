<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Flux\Form\Wizard;

use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class Suggest extends \FluidTYPO3\Flux\Form\Wizard\Suggest
{
    /**
     * @return array
     */
    public function buildConfiguration()
    {
        $table = $this->getTable();
        $configuration = parent::buildConfiguration();

        if (count($this->getStoragePageUids()) == 0) {
            unset($configuration[$table]['pidList']);
        }

        return $configuration;
    }

    /**
     * @param array $storagePageUids
     * @return Suggest
     */
    public function setStoragePageUids($storagePageUids)
    {
        if (false === is_array($storagePageUids)) {
            $storagePageUids = GeneralUtility::trimExplode(',', $storagePageUids);
        }

        $this->storagePageUids = [];
        foreach ($storagePageUids as $storagePageUid) {
            if ($storagePageUid != null) {
                $this->storagePageUids[] = (int)$storagePageUid;
            }
        }

        return $this;
    }
}
