<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Flux\Form\Field;

use TYPO3\CMS\Extbase\Persistence\QueryInterface;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class Select extends \FluidTYPO3\Flux\Form\Field\Select
{
    /**
     * @var boolean
     */
    protected $showIconTable;

    /**
     * @var integer
     */
    protected $selIconCols;

    /**
     * @inheritdoc
     */
    public function buildConfiguration()
    {
        $configuration = parent::buildConfiguration();
        if ($this->showIconTable) {
            $configuration['showIconTable'] = $this->getShowIconTable();

            if ($this->selIconCols) {
                $configuration['selicon_cols'] = $this->getSelIconCols();
            }
        }

        return $configuration;
    }

    /**
     * @return boolean
     */
    public function getShowIconTable()
    {
        return $this->showIconTable;
    }

    /**
     * @param boolean $showIconTable
     * @return $this
     */
    public function setShowIconTable($showIconTable)
    {
        $this->showIconTable = $showIconTable;

        return $this;
    }

    /**
     * @return int
     */
    public function getSelIconCols()
    {
        return $this->selIconCols;
    }

    /**
     * @param int $selIconCols
     * @return $this
     */
    public function setSelIconCols($selIconCols)
    {
        $this->selIconCols = $selIconCols;

        return $this;
    }

    /**
     * @return array
     */
    public function getItems()
    {
        $items = parent::getItems();
        if (is_string($this->items)) {
            $funcitems = null;
            try {
                @eval('$funcitems = ' . $this->items . ';');
            } catch (\Exception $e) {}

            if ($funcitems && is_array($funcitems)) {
                $items = [];
                foreach ($funcitems as $key => $item) {
                    $icon = null;
                    if (is_array($item)) {
                        $icon = $item[1];
                        $item = $item[0];
                    }

                    if ($icon) {
                        $items[] = [(string)$item, $key, $icon];
                    } else {
                        $items[] = [(string)$item, $key];
                    }
                }

                $emptyOption = $this->getEmptyOption();
                if (false !== $emptyOption) {
                    array_unshift($items, [$emptyOption, '']);
                }
            } else {
                $items = parent::getItems();
            }
        } else {
            $items = parent::getItems();
        }

        return $items;
    }
}