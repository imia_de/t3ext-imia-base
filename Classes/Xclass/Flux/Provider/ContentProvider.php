<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Flux\Provider;

use FluidTYPO3\Flux\Form;
use FluidTYPO3\Flux\Provider\ProviderInterface;
use FluidTYPO3\Flux\Utility\ExtensionNamingUtility;
use IMIA\ImiaBase\Utility\ContentCache;
use IMIA\ImiaBase\Xclass\Backend\View\PageLayoutView as DefaultPageLayoutView;
use Snowflake\Varnish\Utilities\GeneralUtility;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class ContentProvider extends \FluidTYPO3\Flux\Provider\ContentProvider
{
    /**
     * @var array
     */
    static protected $cache = [];

    /**
     * @inheritdoc
     */
    public function getForm(array $row)
    {
        if (null !== $this->form) {
            return $this->form;
        }

        $formName = 'form';
        $cacheKey = $this->getCacheKeyForStoredVariable($row, $formName);
        if (false === array_key_exists($cacheKey, self::$cache)) {
            $form = null;
            $lastChange = 0;
            $identifier = 'flux-backend-form-' . $row['uid'];
            if (ContentCache::enabled()) {
                $noCache = false;
                if (getenv('TYPO3_CONTEXT') == 'Development') {
                    $lastChangeCached = ContentCache::get($identifier . '_fce');
                    $lastChange = (int)$this->getLastChange($row);

                    if ($lastChange) {
                        if ((int)$lastChangeCached < $lastChange) {
                            $noCache = true;
                        }
                    }
                }

                if (!$noCache) {
                    $form = ContentCache::get($identifier);
                }
            }

            if (!$form) {
                $form = parent::getForm($row);

                if (ContentCache::enabled()) {
                    ContentCache::set($identifier, $form ?: 'null', ['page-' . $row['pid'], 'content-' . $row['uid']], 0);

                    if (getenv('TYPO3_CONTEXT') == 'Development') {
                        ContentCache::set($identifier . '_fce', $lastChange);
                    }
                }
            }

            if (!$form || $form == 'null') {
                $form = null;
            }

            self::$cache[$cacheKey] = $form;
        }

        return self::$cache[$cacheKey];
    }

    /**
     * @param array $row
     * @return integer
     */
    protected function getLastChange($row)
    {
        $lastChange = 0;
        if ($row['tx_fed_fcefile']) {
            try {
                $fceAction = explode(':', $row['tx_fed_fcefile']);
                $fceExtKey = ExtensionNamingUtility::getExtensionKey($fceAction[0]);
                $fcePath = ExtensionManagementUtility::extPath($fceExtKey, 'Resources/Private/Templates/Content/' . $fceAction[1]);
                if (file_exists($fcePath)) {
                    $lastChange = (int)filemtime($fcePath);
                } else {
                    $lastChange = time();
                }
            } catch(\Exception $e) {}
        }

        return $lastChange;
    }
}