<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Flux\View;

use FluidTYPO3\Flux\Form;
use FluidTYPO3\Flux\Provider\ProviderInterface;
use IMIA\ImiaBase\Utility\ContentCache;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class PreviewView extends \FluidTYPO3\Flux\View\PreviewView
{
    /**
     * @inheritdoc
     */
    protected function renderPreviewSection(ProviderInterface $provider, array $row, Form $form = NULL)
    {
        $templatePathAndFilename = $provider->getTemplatePathAndFilename($row);
        if (!$templatePathAndFilename) {
            return null;
        }

        $lastChange = 0;
        $identifier = 'flux-backend-preview-' . $row['uid'];
        $previewContent = null;
        if (ContentCache::enabled()) {
            $noCache = false;
            if (getenv('TYPO3_CONTEXT') == 'Development') {
                $lastChangeCached = ContentCache::get($identifier . '_fce');
                $lastChange = (int)filemtime($templatePathAndFilename);
                if ($lastChange) {
                    if ((int)$lastChangeCached < $lastChange) {
                        $noCache = true;
                    }
                }
            }

            if (!$noCache) {
                $previewContent = ContentCache::get($identifier);
            }
        }

        if (!$previewContent) {
            $previewContent = parent::renderPreviewSection($provider, $row, $form);

            if (ContentCache::enabled()) {
                ContentCache::set($identifier, $previewContent, ['page-' . $row['pid'], 'content-' . $row['uid']], 0);

                if (getenv('TYPO3_CONTEXT') == 'Development') {
                    ContentCache::set($identifier . '_fce', $lastChange);
                }
            }
        }

        return $previewContent;
    }
}