<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Flux\View;

use IMIA\ImiaBase\Xclass\Backend\View\PageLayoutView as DefaultPageLayoutView;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class PageLayoutView extends \FluidTYPO3\Flux\View\PageLayoutView
{
    /**
     * @var array
     */
    protected static $itemsToDraw = [];

    /**
     * @param $id
     * @return string
     */
    public function getTable_tt_content($id)
    {
        $out = parent::getTable_tt_content($id);
        $out = DefaultPageLayoutView::getTable_tt_content_add($this, $id, $out);

        return $out;
    }

    /**
     * @param array $row
     * @return string
     */
    public function tt_content_drawItem($row)
    {
        if (!array_key_exists($row['uid'], self::$itemsToDraw)) {
            self::$itemsToDraw[$row['uid']] = '';

            $out = parent::tt_content_drawItem($row);

            self::$itemsToDraw[$row['uid']] = $out;
        }

        return self::$itemsToDraw[$row['uid']];
    }

    /**
     * @inheritdoc
     */
    public function newLanguageButton($defLanguageCount, $lP, $colPos = 0)
    {
        if (!isset($this->contentElementCache[$lP][$colPos])) {
            $this->contentElementCache[$lP][$colPos] = [];
        }

        return parent::newLanguageButton($defLanguageCount, $lP, $colPos);
    }

    /**
     * @param array $row
     * @param integer $space
     * @param boolean $disableMoveAndNewButtons
     * @param boolean $langMode
     * @param boolean $dragDropEnabled
     * @return string
     */
    public function tt_content_drawHeader($row, $space = 0, $disableMoveAndNewButtons = false, $langMode = false, $dragDropEnabled = false)
    {
        if (!$this->languageIconTitles) {
            $this->initializeLanguages();
        }

        return parent::tt_content_drawHeader($row, $space, $disableMoveAndNewButtons, !$this->tt_contentConfig['languageMode'], $dragDropEnabled);
    }
}