<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Flux\ViewHelpers\Field;

use TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class SelectViewHelper extends \FluidTYPO3\Flux\ViewHelpers\Field\SelectViewHelper
{
    /**
     * @inheritdoc
     */
    public function initializeArguments()
    {
        parent::initializeArguments();

        try {
            $this->registerArgument('showIconTable', 'boolean', '');
        } catch (\Exception $e) {
        }
        $this->registerArgument('selIconCols', 'integer', '');
    }

    /**
     * @inheritdoc
     */
    public static function getComponent(RenderingContextInterface $renderingContext, array $arguments)
    {
        $component = parent::getComponent($renderingContext, $arguments);
        $component->setShowIconTable((boolean)$arguments['showIconTable']);
        $component->setSelIconCols((int)$arguments['selIconCols']);

        return $component;
    }
}