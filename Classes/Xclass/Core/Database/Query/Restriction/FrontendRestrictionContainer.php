<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Core\Database\Query\Restriction;

use TYPO3\CMS\Core\Database\Query\Expression\CompositeExpression;
use TYPO3\CMS\Core\Database\Query\Expression\ExpressionBuilder;
use TYPO3\CMS\Core\Database\Query\Restriction\EndTimeRestriction;
use TYPO3\CMS\Core\Database\Query\Restriction\HiddenRestriction;
use TYPO3\CMS\Core\Database\Query\Restriction\StartTimeRestriction;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class FrontendRestrictionContainer extends \TYPO3\CMS\Core\Database\Query\Restriction\FrontendRestrictionContainer
{
    /**
     * @var bool
     */
    static public $ignoreEnableFields = false;

    public function __construct()
    {
        if (self::$ignoreEnableFields) {
            foreach ($this->defaultRestrictionTypes as $restrictionType) {
                if (!in_array($restrictionType, [
                    HiddenRestriction::class,
                    StartTimeRestriction::class,
                    EndTimeRestriction::class,
                ])) {
                    $this->add($this->createRestriction($restrictionType));
                }
            }
        } else {
            parent::__construct();
        }
    }

    /**
     * @inheritdoc
     */
    public function buildExpression(array $queriedTables, ExpressionBuilder $expressionBuilder): CompositeExpression
    {
        $constraints = [];

        /** @var TypoScriptFrontendController $typoScriptFrontendController */
        $typoScriptFrontendController = $GLOBALS['TSFE'];
        foreach ($this->restrictions as $restriction) {
            foreach ($queriedTables as $tableAlias => $tableName) {
                $disableRestriction = false;
                if ($restriction instanceof HiddenRestriction || $restriction instanceof StartTimeRestriction || $restriction instanceof EndTimeRestriction) {
                    // If display of hidden records is requested, we must disable the hidden restriction.
                    $disableRestriction = $tableName === 'pages' || $tableName === 'pages_language_overlay'
                        ? $typoScriptFrontendController->showHiddenPage
                        : $typoScriptFrontendController->showHiddenRecords;
                }
                if (!$disableRestriction) {
                    $constraints[] = $restriction->buildExpression([$tableAlias => $tableName], $expressionBuilder);
                }
            }
        }

        return $expressionBuilder->andX(...$constraints);
    }
}