<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Core\Resource\Index;

use IMIA\ImiaBase\Utility\ContentCache;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class Indexer extends \TYPO3\CMS\Core\Resource\Index\Indexer
{
    /**
     * @inheritdoc
     */
    public function updateIndexEntry(File $fileObject)
    {
        parent::updateIndexEntry($fileObject);

        $fileReferences = [];
        foreach ($this->getReferences($fileObject) as $reference) {
            if ($reference['tablename'] == 'sys_file_reference') {
                $fileReferences[] = $reference['recuid'];
            }
        }

        if (count($fileReferences) > 0) {
            $result = $this->getDatabaseConnection()->exec_SELECTquery('*',
                'sys_file_reference', 'uid IN (' . implode(',', $fileReferences) . ')');

            /** @var DataHandler $dataHandler */
            $dataHandler = GeneralUtility::makeInstance(DataHandler::class);
            $dataHandler->stripslashes_values = false;
            $dataHandler->start([], []);

            while ($row = $this->getDatabaseConnection()->sql_fetch_assoc($result)) {
                $foreignUid = $row['uid_foreign'];
                if ($row['tablenames'] == 'tt_content') {
                    $content = BackendUtility::getRecord('tt_content', $foreignUid);

                    if (ContentCache::enabled()) {
                        ContentCache::flushByTag('content-' . $foreignUid);
                        ContentCache::flushByTag('contentspecial-' . $foreignUid);

                        if ($content) {
                            ContentCache::flushByTag('pagecontent-' . $content['pid']);
                        }
                    }

                    if ($content) {
                        $dataHandler->clear_cacheCmd($content['pid']);
                    }
                } elseif ($row['tablenames'] == 'pages') {
                    if (ContentCache::enabled()) {
                        ContentCache::flushByTag('pagecontent-' . $foreignUid);
                    }

                    $dataHandler->clear_cacheCmd($foreignUid);
                }
            }
        }
    }

    /**
     * @param File $file
     * @return array
     */
    protected function getReferences(File $file)
    {
        $selectTable = 'sys_file';
        $selectUid = $file->getUid();
        $rows = $this->getDatabaseConnection()->exec_SELECTgetRows(
            '*',
            'sys_refindex',
            'ref_table=' . $this->getDatabaseConnection()->fullQuoteStr($selectTable, 'sys_refindex') . ' AND ref_uid=' . (int)$selectUid . ' AND deleted=0'
        );
        if (!is_array($rows)) {
            $rows = [];
        }

        return $rows;
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDatabaseConnection()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}
