<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\TwLucenesearch\Utility;

use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class Indexer extends \Tollwerk\TwLucenesearch\Utility\Indexer
{
    /**
     * @param \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController $fe
     */
    protected function _indexTSFE(\TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController $fe)
    {
        ini_set('opcache.enable', '0');
        parent::_indexTSFE($fe);

        /* @var $indexerService \Tollwerk\TwLucenesearch\Service\Lucene */
        $indexerService = GeneralUtility::makeInstanceService('index', 'lucene');
        if ($indexerService instanceof \TYPO3\CMS\Core\Service\AbstractService) {
            $queryHits = $indexerService->getByTypeId(self::PAGE, $fe->id, true);

            $uids = [];

            /** @var \Tollwerk\TwLucenesearch\Domain\Model\QueryHit $queryHit */
            foreach ($queryHits as $queryHit) {
                if (in_array($queryHit->getDocument()->getUid(), $uids)) {
                    $indexerService->delete($queryHit->id);
                } else {
                    $uids[] = $queryHit->getDocument()->getUid();

                    $result = $queryHit->getDocument()->getReferenceParameters();
                    if (!isset($result['type'])) {
                        $indexerService->delete($queryHit->id);
                    }
                }
            }
            $indexerService->commit();
        }
    }
}