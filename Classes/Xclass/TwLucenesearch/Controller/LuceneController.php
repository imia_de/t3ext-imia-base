<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\TwLucenesearch\Controller;

use IMIA\ImiaBase\Traits\HookTrait;
use IMIA\ImiaBase\Xclass\Core\Database\DatabaseConnection;
use Tollwerk\TwLucenesearch\Utility\Indexer;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class LuceneController extends \Tollwerk\TwLucenesearch\Controller\LuceneController
{
    use HookTrait;

    /**
     * @param string $searchterm
     * @param int $pointer
     * @param boolean $notfound
     */
    public function resultsAction($searchterm = '', $pointer = 0, $notfound = false)
    {
        ini_set('opcache.enable', '0');

        $fixLockFile = PATH_site . 'typo3temp/lucenefix-' . $GLOBALS['TSFE']->rootLine[0]['uid'] . '.lock';
        if (!file_exists($fixLockFile) || filemtime($fixLockFile) < (time() - 60*60)) {
            @file_put_contents($fixLockFile, time());

            /* @var $indexerService \Tollwerk\TwLucenesearch\Service\Lucene */
            $indexerService = GeneralUtility::makeInstanceService('index', 'lucene');
            if ($indexerService instanceof \TYPO3\CMS\Core\Service\AbstractService) {
                $queryHits = $indexerService->getByTypeId(Indexer::PAGE, null, true);

                $uids = [];
                /** @var \Tollwerk\TwLucenesearch\Domain\Model\QueryHit $queryHit */
                foreach ($queryHits as $queryHit) {
                    if (in_array($queryHit->getDocument()->getUid(), $uids)) {
                        $indexerService->delete($queryHit);
                    } else {
                        $uids[] = $queryHit->getDocument()->getUid();
                        $result = $queryHit->getDocument()->getReferenceParameters();
                        if (!isset($result['type'])) {
                            $indexerService->delete($queryHit);
                        }

                        if (is_array($result) && isset($result['url']) && $result['url']) {
                        } else {
                            if ($_COOKIE['debug']) {
                                $page = BackendUtility::getRecord('pages', $queryHit->getDocument()->getPageUid(), 'uid,pid,fe_group,starttime,endtime,hidden');
                                if ($page['hidden'] || $page['fe_group'] || ($page['starttime'] && (int)$page['starttime'] > time()) || ($page['endtime'] && (int)$page['endtime'] < time())) {
                                    $indexerService->delete($queryHit);
                                }
                            }
                        }
                    }
                }

                $indexerService->commit();
            }
        }

        $this->callHook('resultsAction', [$searchterm, $pointer, $notfound, $this]);

        parent::resultsAction($searchterm, $pointer, $notfound);
    }
}