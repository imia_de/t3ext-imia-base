<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Reports\Report\Status;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Reports\Status as ReportStatus;
use TYPO3\CMS\Lang\LanguageService;

/**
 * @package     imia_base
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class Typo3Status extends \TYPO3\CMS\Reports\Report\Status\Typo3Status
{
    /**
     * @return \TYPO3\CMS\Reports\Status
     */
    protected function getRegisteredXclassStatus()
    {
        $message = '';
        $value = $this->getLang()->getLL('status_none');
        $severity = ReportStatus::OK;

        $xclassFoundArray = [];
        if (array_key_exists('Objects', $GLOBALS['TYPO3_CONF_VARS']['SYS'])) {
            foreach ($GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'] as $originalClass => $override) {
                if (array_key_exists('className', $override)) {
                    $xclassFoundArray[$originalClass] = $override['className'];
                }
            }
        }
        if (!empty($xclassFoundArray)) {
            $value = $this->getLang()->getLL('status_xclassUsageFound');
            $message = '<a href="#" class="btn btn-default btn-sm" onclick="TYPO3.jQuery(\'#report-xclasses\').slideToggle(); return false;">Details</a>';
            $message .= '<div id="report-xclasses" style="display: none; padding-top: 12px">' . $this->getLang()->getLL('status_xclassUsageFound_message') . '<br>';
            $message .= '<ol style="margin-top: 12px">';
            foreach ($xclassFoundArray as $originalClass => $xClassName) {
                $messageDetail = sprintf(
                    $this->getLang()->getLL('status_xclassUsageFound_message_detail'),
                    '<code>' . htmlspecialchars($originalClass) . '</code>',
                    '<code>' . htmlspecialchars($xClassName) . '</code>'
                );
                $message .= '<li>' . $messageDetail . '</li>';
            }
            $message .= '</ol></div>';

            $severity = ReportStatus::NOTICE;
        }

        return GeneralUtility::makeInstance(
            ReportStatus::class,
            $this->getLang()->getLL('status_xclassUsage'),
            $value,
            $message,
            $severity
        );
    }

    /**
     * @return LanguageService
     */
    protected function getLang()
    {
        return $GLOBALS['LANG'];
    }
}
