<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Reports\Report\Status;

use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Resource\ResourceStorage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Lang\LanguageService;
use TYPO3\CMS\Reports\Status;

/**
 * @package     imia_base
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class FalStatus extends \TYPO3\CMS\Reports\Report\Status\FalStatus
{
    /**
     * @return Status
     */
    protected function getMissingFilesStatus()
    {
        $value = $this->getLang()->getLL('status_none');
        $count = 0;
        $maxFilesToShow = 100;
        $message = '';
        $severity = Status::OK;

        /** @var $storageRepository \TYPO3\CMS\Core\Resource\StorageRepository */
        $storageRepository = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Resource\StorageRepository::class);
        $storageObjects = $storageRepository->findAll();
        $storages = [];

        /** @var $storageObject \TYPO3\CMS\Core\Resource\ResourceStorage */
        foreach ($storageObjects as $storageObject) {
            // We only check missing files for storages that are online
            if ($storageObject->isOnline()) {
                $storages[$storageObject->getUid()] = $storageObject;
            }
        }

        if (!empty($storages)) {
            $count = $this->getDb()->exec_SELECTcountRows(
                '*',
                'sys_file',
                'missing=1 AND storage IN (' . implode(',', array_keys($storages)) . ')'
            );
        }

        if ($count) {
            $files = $this->getDb()->exec_SELECTgetRows(
                'sys_file.identifier, sys_file.storage, (SELECT COUNT(*) FROM sys_file_reference WHERE ' .
                'sys_file_reference.uid_local = sys_file.uid AND sys_file_reference.deleted = 0) AS refCount',
                'sys_file',
                'sys_file.missing=1 AND sys_file.storage IN (' . implode(',', array_keys($storages)) . ') HAVING refCount > 0'
            );

            if (count($files)) {
                $count = count($files);

                $value = sprintf($this->getLang()->getLL('status_missingFilesCount'), $count);
                $severity = Status::WARNING;

                $message = '<p>' . $this->getLang()->getLL('status_missingFilesMessage') . '</p>';

                $i = 0;
                foreach ($files as $file) {
                    if ($i < $maxFilesToShow) {
                        /** @var ResourceStorage $storage */
                        $storage = $storages[$file['storage']];
                        $message .= $storage->getName() . ' ' . $file['identifier'] . ' (' . $file['refCount'] . ' References)' . '<br />';
                        $i++;
                    }
                }

                if ($count > $maxFilesToShow) {
                    $message .= '...<br />';
                }
            }
        }

        return GeneralUtility::makeInstance(Status::class, $this->getLang()->getLL('status_missingFiles'), $value, $message, $severity);
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }

    /**
     * @return LanguageService
     */
    protected function getLang()
    {
        return $GLOBALS['LANG'];
    }
}
