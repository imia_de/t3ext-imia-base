<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Frontend\Controller;

use IMIA\ImiaBase\Traits\HookTrait;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\HttpUtility;
use TYPO3\CMS\Core\Utility\MathUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class TypoScriptFrontendController extends \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController
{
    use HookTrait;

    public function sendCacheHeaders()
    {
        $indexedSearch = false;
        if (is_array($this->config['INTincScript'])) {
            foreach ($this->config['INTincScript'] as $key => $conf) {
                if (isset($conf['conf']['extensionName']) && $conf['conf']['extensionName'] == 'IndexedSearch') {
                    $indexedSearch = true;
                }
            }
        }

        if ($indexedSearch) { // dont send cache headers for indexed search (no-store = true and back issue)
            $headers = [
                'Cache-Control: private',
            ];
            $this->isClientCachable = false;

            // Now, if a backend user is logged in, tell him in the Admin Panel log what the caching status would have been:
            if ($this->beUserLogin) {
                if ($this->isStaticCacheble()) {
                    $this->getTimeTracker()->setTSlogMessage('Cache-headers with max-age "' . ($this->cacheExpires - $GLOBALS['EXEC_TIME']) . '" would have been sent');
                } else {
                    $reasonMsg = '';
                    $reasonMsg .= !$this->no_cache ? '' : 'Caching disabled (no_cache). ';
                    $reasonMsg .= !$this->isINTincScript() ? '' : '*_INT object(s) on page. ';
                    $reasonMsg .= !is_array($this->fe_user->user) ? '' : 'Frontend user logged in. ';
                    $this->getTimeTracker()->setTSlogMessage('Cache-headers would disable proxy caching! Reason(s): "' . $reasonMsg . '"', 1);
                }
            }

            // Send headers:
            foreach ($headers as $hL) {
                header($hL);
            }
        } else {
            parent::sendCacheHeaders();
        }
    }

    /**
     * @inhertitdoc
     */
    public function makeCacheHash()
    {
        // No need to test anything if caching was already disabled.
        if ($this->no_cache && !$GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFoundOnCHashError']) {
            return;
        }
        $GET = GeneralUtility::_GET();
        if ($this->cHash && is_array($GET)) {
            // Make sure we use the page uid and not the page alias
            $GET['id'] = $this->id;
            $this->cHash_array = $this->cacheHash->getRelevantParameters(GeneralUtility::implodeArrayForUrl('', $GET));
            $cHash_calc = $this->cacheHash->calculateCacheHash($this->cHash_array);
            if ($cHash_calc && $cHash_calc != $this->cHash) {
                if ($GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFoundOnCHashError']) {
                    $this->pageNotFoundAndExit('Request parameters could not be validated (&cHash comparison failed)');
                } else {
                    $this->disableCache();
                    $this->getTimeTracker()->setTSlogMessage('The incoming cHash "' . $this->cHash . '" and calculated cHash "' . $cHash_calc . '" did not match, so caching was disabled. The fieldlist used was "' . implode(',', array_keys($this->cHash_array)) . '"', 2);
                }
            }
        } elseif (is_array($GET)) {
            // No cHash is set, check if that is correct
            if ($this->cacheHash->doParametersRequireCacheHash(GeneralUtility::implodeArrayForUrl('', $GET))) {
                $this->reqCHash();
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function getPageAndRootline()
    {
        $saveHideWhereDel = $this->sys_page->where_hid_del;
        if ($this->fePreview && strpos($this->sys_page->where_hid_del, 'starttime') !== false) {
            $this->sys_page->where_hid_del = ' AND pages.deleted=0 AND pages.t3ver_state<=0 AND pages.doktype<200';
        }
        parent::getPageAndRootline();
        $this->sys_page->where_hid_del = $saveHideWhereDel;
    }

    public function initUserGroups()
    {
        parent::initUserGroups();

        $this->callHook('initUserGroups', [$this]);
    }

    /**
     * @inheritdoc
     */
    public function isUserOrGroupSet()
    {
        return (is_array($this->fe_user->user) && isset($this->fe_user->user['uid'])) || $this->gr_list !== '0,-1';
    }

    /**
     * Initializes the page renderer object
     */
    protected function initPageRenderer()
    {
        if ($this->pageRenderer !== null) {
            return;
        }

        $this->pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
        $this->pageRenderer->setTemplateFile('EXT:frontend/Resources/Private/Templates/MainPage.html');
    }

    /**
     * Builds a typolink to the current page, appends the type paremeter if required
     * and redirects the user to the generated URL using a Location header.
     */
    protected function redirectToCurrentPage()
    {
        $this->calculateLinkVars();
        // Instantiate \TYPO3\CMS\Frontend\ContentObject to generate the correct target URL

        /** @var $cObj ContentObjectRenderer */
        $cObj = GeneralUtility::makeInstance(ContentObjectRenderer::class);
        $parameter = $this->page['uid'];
        $type = GeneralUtility::_GET('type');

        if ($type && MathUtility::canBeInterpretedAsInteger($type)) {
            $parameter .= ',' . $type;
        }

        $redirectUrl = $cObj->typoLink_URL([
            'parameter'       => $parameter,
            'addQueryString'  => true,
            'addQueryString.' => ['exclude' => 'id'],
        ]);

        // Prevent redirection loop
        if (!empty($redirectUrl) && GeneralUtility::getIndpEnv('REQUEST_URI') !== '/' . $redirectUrl) {
            // redirect and exit
            HttpUtility::redirect($redirectUrl, HttpUtility::HTTP_STATUS_301);
        }
    }
}