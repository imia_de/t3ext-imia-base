<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Frontend\ContentObject;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\LinkHandling\LinkService;
use TYPO3\CMS\Core\TimeTracker\TimeTracker;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectOneSourceCollectionHookInterface;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;
use TYPO3\CMS\Frontend\Page\CacheHashCalculator;
use TYPO3\CMS\Frontend\View\AdminPanelView;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class ContentObjectRenderer extends \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer
{
    /**
     * @var bool
     */
    static protected $includeHidden = false;

    /**
     * @var string
     */
    static protected $cHash;

    /**
     * @var \TYPO3\CMS\Core\Cache\Frontend\FrontendInterface|boolean
     */
    static protected $cache = null;

    /**
     * @param string $name
     * @param array $conf
     * @param string $TSkey
     * @return string
     */
    public function cObjGetSingle($name, $conf, $TSkey = '__')
    {
        $result = null;
        $cacheKey = 'contentelement-' . $this->data['uid'] . '-' . $this->data['pid'] . '-' . $GLOBALS['TSFE']->sys_language_uid . '_' . self::getCHash();
        if ($name == '< lib.contentElement' || $name == '<lib.contentElement') {
            if (self::getCache() && TYPO3_MODE == 'FE') {
                if (self::getCache()->has($cacheKey)) {
                    $result = self::getCache()->get($cacheKey);
                    if (strtoupper($_SERVER['REQUEST_METHOD']) === 'POST' && stripos($result, '<!--PLUGIN_INCLUDED') !== false) {
                        $result = null;
                    }
                }
            } else {
                $cacheKey = null;
            }
        } else {
            $cacheKey = null;
        }

        if (!$result) {
            $result = parent::cObjGetSingle($name, $conf, $TSkey);

            if ($cacheKey && stripos($result, '<!--INT_SCRIPT') === false && stripos($result, '<!--NO_CONTENT_CACHE') === false) {
                if ($conf['templateName'] == 'List') {
                    $result .= '<!--PLUGIN_INCLUDED-->';
                }

                $lifetime = 0;
                if (isset($GLOBALS['ContentCache-Lifetime'][$this->data['uid']])) {
                    $lifetime = (int)$GLOBALS['ContentCache-Lifetime'][$this->data['uid']];
                    $result .= '<!--NO_CONTENT_CACHE-->';
                }

                self::getCache()->set($cacheKey, $result, ['content-' . $this->data['uid'], 'pagecontent-' . $this->data['pid']], $lifetime);
            }
        }
        $result = str_replace('<!--PLUGIN_INCLUDED-->', '', $result);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getRecords($tableName, array $queryConfiguration)
    {
        self::$includeHidden = true;
        $records = parent::getRecords($tableName, $queryConfiguration);
        self::$includeHidden = false;

        $orderBy = $GLOBALS['TCA'][$tableName]['ctrl']['sortby'] ?: 'uid';
        if (isset($queryConfiguration['orderBy'])) {
            $orderByArr = array_map('trim', explode(',', $queryConfiguration['orderBy']));
            if ($orderByArr[0]) {
                $orderBy = $orderByArr[0];
            }
        }

        $hiddenField = $GLOBALS['TCA'][$tableName]['ctrl']['enablecolumns']['disabled'];
        if ($orderBy) {
            $newRecords = [];
            foreach ($records as $key => $record) {
                if (!$hiddenField || !$record[$hiddenField]) {
                    $newRecords[] = $record;
                }
            }

            usort($newRecords, function ($a, $b) use ($orderBy) {
                return $a[$orderBy] <=> $b[$orderBy];
            });
            $records = $newRecords;
        }

        return $records;
    }

    /**
     * @inheritdoc
     */
    public function enableFields($table, $show_hidden = false, array $ignore_array = [])
    {
        if (self::$includeHidden) {
            $show_hidden = true;
            $ignore_array['starttime'] = true;
            $ignore_array['endtime'] = true;
        }

        return parent::enableFields($table, (bool)$show_hidden, $ignore_array);
    }

    /**
     * @inheritdoc
     */
    public function getSlidePids($pidList, $pidConf)
    {
        $whereHidDel = $this->getTypoScriptFrontendController()->sys_page->where_hid_del;

        if ($this->getTypoScriptFrontendController()->sys_page->versioningPreview) {
            /** @var ConnectionPool $connectionPool */
            $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
            $expressionBuilder = $connectionPool->getQueryBuilderForTable('pages')->expr();

            $this->getTypoScriptFrontendController()->sys_page->where_hid_del = ' AND ' . $expressionBuilder->andX(
                    $expressionBuilder->eq('pages.deleted', 0),
                    $expressionBuilder->orX(
                        $expressionBuilder->eq('pages.t3ver_wsid', 0),
                        $expressionBuilder->eq('pages.t3ver_wsid', (int)$this->getTypoScriptFrontendController()->sys_page->versioningWorkspaceId)
                    )
                );
        } else {
            $this->getTypoScriptFrontendController()->sys_page->where_hid_del = $this->getTypoScriptFrontendController()->sys_page
                ->enableFields('pages', $this->getTypoScriptFrontendController()->showHiddenPage, ['fe_group' => true], true);
        }

        $slidePids = parent::getSlidePids($pidList, $pidConf);
        if (GeneralUtility::_GP('MP')) {
            $mp = explode('-', GeneralUtility::_GP('MP'));
            if ($mp[0] == $slidePids) {
                $slidePids = $mp[1];
            }
        }

        $this->getTypoScriptFrontendController()->sys_page->where_hid_del = $whereHidDel;

        return $slidePids;
    }

    /**
     * @inheritdoc
     */
    public function __construct(TypoScriptFrontendController $typoScriptFrontendController = null)
    {
        if (GeneralUtility::_GP('M') !== 'web_FormFormbuilder') {
            if ($typoScriptFrontendController && !($typoScriptFrontendController instanceof TypoScriptFrontendController)) {
                $typoScriptFrontendController = null;
            }
            if ($GLOBALS['TSFE'] && !($GLOBALS['TSFE'] instanceof TypoScriptFrontendController)) {
                $GLOBALS['TSFE'] = null;
            }

            if (!$typoScriptFrontendController && !$GLOBALS['TSFE']) {
                if (!$GLOBALS['TSFE_BACKUP']) {
                    $pageUid = 0;

                    $editconf = GeneralUtility::_GP('edit');
                    if ($editconf && is_array($editconf) && count($editconf) > 0) {
                        foreach ($editconf as $tableName => $tableCmds) {
                            foreach ($tableCmds as $id => $cmd) {
                                if ($cmd == 'new' && $id < 0) {
                                    $id *= -1;
                                } elseif ($cmd == 'new') {
                                    $tableName = 'pages';
                                }

                                $record = BackendUtility::getRecord($tableName, $id);
                                if ($record) {
                                    if ($tableName == 'pages') {
                                        $pageUid = $record['uid'];
                                    } else {
                                        $pageUid = $record['pid'];
                                    }
                                }
                            }
                        }
                    }

                    $record = $this->getPageRecord((int)$_REQUEST['id']);
                    if ($record) {
                        $pageUid = $record['uid'];
                    }

                    $typoScriptFrontendController = $this->initFrontend($pageUid);
                    if (!$GLOBALS['TSFE']) {
                        $GLOBALS['TSFE'] = $typoScriptFrontendController;
                    }

                    $GLOBALS['TSFE_BACKUP'] = $typoScriptFrontendController;
                } else {
                    $typoScriptFrontendController = $GLOBALS['TSFE'] = $GLOBALS['TSFE_BACKUP'];
                }
            }
        }

        parent::__construct($typoScriptFrontendController);
    }

    /**
     * @inheritdoc
     */
    public function cImage($file, $conf)
    {
        $layoutKey = $this->stdWrap($conf['layoutKey'], $conf['layoutKey.']);
        $sourceCollection = $this->getImageSourceCollection($layoutKey, $conf, $file);

        if (!$sourceCollection) {
            $conf['layoutKey'] = 'default';
        }

        return parent::cImage($file, $conf);
    }

    /**
     * @param integer $pageUid
     * @return array
     */
    protected function getPageRecord($pageUid)
    {
        $record = $this->getDb()->exec_SELECTgetSingleRow(
            'uid, pid, fe_group, doktype, starttime, endtime, hidden',
            'pages',
            'uid = ' . $pageUid
        );

        if ($record) {
            if (in_array($record['doktype'], [199, 254, 255])) {
                $record = $this->getPageRecord($record['pid']);
            }
        }

        return $record;
    }

    /**
     * @inheritdoc
     * @throws \Exception
     */
    public function typoLink($linktxt, $conf)
    {
        if ($GLOBALS['TSFE'] && ($GLOBALS['TSFE'] instanceof TypoScriptFrontendController)) {
            if (!$this->typoScriptFrontendController) {
                $this->typoScriptFrontendController = $GLOBALS['TSFE'];
            } else {
                $this->typoScriptFrontendController->sys_language_uid = $GLOBALS['TSFE']->sys_language_uid;
                $this->typoScriptFrontendController->sys_language_content = $GLOBALS['TSFE']->sys_language_content;
                $this->typoScriptFrontendController->linkVars = $GLOBALS['TSFE']->linkVars;
            }
        }

        // HTTPS Schema Fix START
        $linkParameter = trim(isset($conf['parameter.']) ? $this->stdWrap($conf['parameter'], $conf['parameter.']) : $conf['parameter']);
        $resolvedLinkParameters = $this->resolveMixedLinkParameter($linktxt, $linkParameter, $conf);
        if ($resolvedLinkParameters && isset($resolvedLinkParameters['href'])) {
            $linkParameter = $resolvedLinkParameters['href'];
            if ($linkParameter) {
                // Detecting kind of link and resolve all necessary parameters
                $linkService = GeneralUtility::makeInstance(LinkService::class);
                $linkDetails = $linkService->resolve($linkParameter);

                if ($linkDetails['type'] == 'page') {
                    $tsfe = $this->getTypoScriptFrontendController();
                    if (isset($conf['linkAccessRestrictedPages'])) {
                        $disableGroupAccessCheck = (bool)$conf['linkAccessRestrictedPages'];
                    } else {
                        $disableGroupAccessCheck = (bool)$tsfe->config['config']['typolinkLinkAccessRestrictedPages'];
                    }

                    $page = $tsfe->sys_page->getPage($linkParameter, $disableGroupAccessCheck);
                    if (substr((string)$page['doktype'], -1) == '3' && $page['url']) {
                        // do nothing special for external urls
                    } elseif ($tsfe->config['config']['typolinkEnableLinksAcrossDomains'] && $tsfe->config['config']['typolinkCheckRootline']) {
                        $targetDomain = $tsfe->getDomainNameForPid($page['uid']);

                        if ($targetDomain && !$tsfe->domainNameMatchesCurrentRequest($targetDomain)) {
                            // Special mount point optimization START
                            $addedMP = false;
                            if ($GLOBALS['TYPO3_CONF_VARS']['FE']['specialMountRoots'] && stripos($conf['additionalParams'] ?: '', '&MP=') === false) {
                                $specialMountRoots = array_map('intval', explode(',', $GLOBALS['TYPO3_CONF_VARS']['FE']['specialMountRoots']));
                                $targetPageRootline = $tsfe->sys_page->getRootLine($page['uid']);

                                $specialMountTargetPageUids = [];
                                $isInSpecialMountRoots = false;
                                foreach ($targetPageRootline as $rootlinePage) {
                                    if (in_array($rootlinePage['uid'], $specialMountRoots)) {
                                        $isInSpecialMountRoots = true;
                                    }
                                    $specialMountTargetPageUids[] = $rootlinePage['uid'];
                                }

                                if ($isInSpecialMountRoots) {
                                    $result = $this->getDb()->exec_SELECTquery('uid, mount_pid', 'pages',
                                        'mount_pid IN (' . implode(',', $specialMountTargetPageUids) . ')' . $this->enableFields('pages'));

                                    while ($row = $this->getDb()->sql_fetch_assoc($result)) {
                                        $specialTargetDomain = $tsfe->getDomainNameForPid($row['uid']);
                                        if ($tsfe->domainNameMatchesCurrentRequest($specialTargetDomain)) {
                                            if (!$conf['additionalParams']) {
                                                $conf['additionalParams'] = '';
                                            }
                                            $conf['additionalParams'] .= '&MP=' . $row['mount_pid'] . '-' . $row['uid'];
                                            $addedMP = true;

                                            break;
                                        }
                                    }
                                }
                            }
                            // Special mount point optimization END

                            if (!$addedMP) {
                                $conf['forceAbsoluteUrl'] = true;
                            }
                        }
                    }
                }
            }
        }
        // HTTPS Schema Fix END

        $link = parent::typoLink($linktxt, $conf);

        return $link;
    }

    /**
     * @param integer $pageUid
     * @return \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController
     */
    protected function initFrontend($pageUid = 0)
    {
        $tsfeSafe = $GLOBALS['TSFE'];

        $pageNotFoundHandling = $GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling'];
        unset($GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling']);

        $typoScriptFrontendController = null;
        $orgRequest = $_SERVER['REQUEST_URI'] ?: '';
        $orgScriptName = $_SERVER['SCRIPT_NAME'] ?: '';

        ob_start();
        try {
            if (!$pageUid) {
                $root = $this->getDb()->exec_SELECTgetSingleRow('uid', 'pages', 'pid = 0 AND is_siteroot = 1 AND doktype NOT IN (254,255,199)' .
                    BackendUtility::BEenableFields('pages') . BackendUtility::deleteClause('pages'), '', 'pages.sorting');

                if ($root && $root['uid']) {
                    $pageUid = $root['uid'];
                }
            }

            if (!$_SERVER['HTTP_HOST'] && $pageUid) {
                $rootline = BackendUtility::BEgetRootLine($pageUid);
                $host = BackendUtility::firstDomainRecord($rootline);
                $_SERVER['HTTP_HOST'] = $host;
            }

            $_SERVER['REQUEST_URI'] = $_SERVER['SCRIPT_NAME'] = 'typo3';
            if (!is_object($GLOBALS['TT'])) {
                $GLOBALS['TT'] = GeneralUtility::makeInstance(TimeTracker::class);
                $GLOBALS['TT']->start();
            }

            /** @var TypoScriptFrontendController $typoScriptFrontendController */
            $GLOBALS['TSFE'] = GeneralUtility::makeInstance(
                TypoScriptFrontendController::class, $GLOBALS['TYPO3_CONF_VARS'], $pageUid, 0, 1);

            $GLOBALS['TSFE']->cObj = $this;

            $GLOBALS['TSFE']->connectToDB();
            $GLOBALS['TSFE']->initFEuser();

            $GLOBALS['TSFE']->determineId();
            $GLOBALS['TSFE']->initTemplate();
            $GLOBALS['TSFE']->tmpl->getFileName_backPath = PATH_site;
            $GLOBALS['TSFE']->forceTemplateParsing = 1;
            $GLOBALS['TSFE']->getConfigArray();

            $typoScriptFrontendController = $GLOBALS['TSFE'];
        } catch (\Exception $e) {
        }
        ob_end_clean();

        if (php_sapi_name() != 'cli') {
            $_SERVER['REQUEST_URI'] = $orgRequest;
            $_SERVER['SCRIPT_NAME'] = $orgScriptName;

            GeneralUtility::flushInternalRuntimeCaches();
        }

        $GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling'] = $pageNotFoundHandling;
        $GLOBALS['TSFE'] = $tsfeSafe;

        return $typoScriptFrontendController;
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }

    /**
     * @return string
     */
    protected static function getCHash()
    {
        if (!self::$cHash) {
            if ($GLOBALS['TSFE']->no_cache) {
                $cacheHash = GeneralUtility::makeInstance(CacheHashCalculator::class);
                $params = GeneralUtility::_GET();
                if (isset($GLOBALS['TSFE']->id)) {
                    $params['id'] = $GLOBALS['TSFE']->id;
                }
                $GLOBALS['TSFE']->cHash_array = $cacheHash->getRelevantParameters(GeneralUtility::implodeArrayForUrl('', $params));
            }

            if(method_exists($GLOBALS['TSFE'], 'getHash')){
                self::$cHash = $GLOBALS['TSFE']->getHash();
            }
        }

        return self::$cHash;
    }

    /**
     * @return bool|\TYPO3\CMS\Core\Cache\Frontend\FrontendInterface
     */
    protected static function getCache()
    {
        if (self::$cache === null) {
            if (getenv('TYPO3_CONTEXT') != 'Development' && $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['tx_imiabase']) {
                /** @var CacheManager $cacheManager */
                $cacheManager = $contentCache = GeneralUtility::makeInstance(CacheManager::class);

                try {
                    self::$cache = $cacheManager->getCache('tx_imiabase');
                } catch (\Exception $e) {
                }
            }
        }

        return self::$cache;
    }
}