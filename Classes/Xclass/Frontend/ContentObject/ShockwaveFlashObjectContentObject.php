<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Frontend\ContentObject;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class ShockwaveFlashObjectContentObject extends \TYPO3\CMS\Frontend\ContentObject\ShockwaveFlashObjectContentObject
{
    /**
     * @inheritdoc
     */
    public function render($conf = [])
    {
        $filename = isset($conf['file.']) ? $this->cObj->stdWrap($conf['file'], $conf['file.']) : $conf['file'];
        $type = isset($conf['type.']) ? $this->cObj->stdWrap($conf['type'], $conf['type.']) : $conf['type'];

        if ($type == 'video') {
            if ($this->displayAsIframe($filename)) {
                $width = isset($conf['width.']) ? $this->cObj->stdWrap($conf['width'], $conf['width.']) : $conf['width'];
                if (!$width) {
                    $width = $conf[$type . '.']['defaultWidth'];
                }
                $height = isset($conf['height.']) ? $this->cObj->stdWrap($conf['height'], $conf['height.']) : $conf['height'];
                if (!$height) {
                    $height = $conf[$type . '.']['defaultHeight'];
                }

                $content = '<iframe width="' . $width . '" height="' . $height . '" src="' . $this->transformIframeSrc($filename) . '" frameborder="0" allowfullscreen></iframe>';

                if (isset($conf['stdWrap.'])) {
                    $content = $this->cObj->stdWrap($content, $conf['stdWrap.']);
                }

                return $content;
            } else {
                return parent::render($conf);
            }
        } else {
            return parent::render($conf);
        }
    }

    /**
     * @param string $filename
     * @return boolean
     */
    protected function displayAsIframe($filename)
    {
        if (strpos($filename, 'youtube.com') || strpos($filename, 'vimeo.com')) {
            return true;
        }

        return false;
    }

    /**
     * @param string $filename
     * @return string
     */
    protected function transformIframeSrc($filename)
    {
        if (strpos($filename, 'vimeo.com')) {
            $filename = preg_replace('/^.*?clip_id=([0-9]+).*?$/ism', '//player.vimeo.com/video/$1', $filename);
        } elseif (strpos($filename, 'youtube.com')) {
            $filename = preg_replace('/^.*?youtube\.com\/v\/([^\/\?]+).*?$/ism', '//www.youtube.com/embed/$1', $filename);
        }

        return $filename;
    }
}
