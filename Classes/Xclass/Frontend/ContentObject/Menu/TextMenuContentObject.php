<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Frontend\ContentObject\Menu;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class TextMenuContentObject extends \TYPO3\CMS\Frontend\ContentObject\Menu\TextMenuContentObject
{
    /**
     * @var boolean
     */
    protected static $bootstrapVisibilityEnabled;

    /**
     * @inheritdoc
     */
    public function extProc_afterLinking($key)
    {
        if (!$this->I['spacer']) {
            $this->I['theItem'] .= $this->subMenu($this->I['uid'], $this->WMsubmenuObjSuffixes[$key]['sOSuffix']);
        }
        $part = isset($this->I['val']['wrapItemAndSub.']) ? $this->WMcObj->stdWrap($this->I['val']['wrapItemAndSub'], $this->I['val']['wrapItemAndSub.']) : $this->I['val']['wrapItemAndSub'];
        $item = $part ? $this->WMcObj->wrap($this->I['theItem'], $part) : $this->I['theItem'];

        if ($this->isBootstrapVisibilityEnabled()) {
            $row = $this->menuArr[$this->I['key']];
            if ($row['bootstrap_visibility'] && !$this->conf['includeNotInMenu']) {
                $itemArr = false;
                $itemArrExt = false;
                try {
                    if (trim($item, ',') != $item) {
                        $itemArrExt = true;
                    }
                    $itemArr = json_decode(trim($item, ','), true);
                } catch(\Exception $e) {
                }

                $visibilityClass = '';
                foreach (explode(',', $row['bootstrap_visibility']) as $visibility) {
                    $visibilityClass .= 'visible-' . trim($visibility) . ' ';
                }
                $visibilityClass = substr($visibilityClass, 0, -1);

                if ($itemArr) {
                    $itemArr['class'] = $visibilityClass;
                    $item = json_encode($itemArr) . ($itemArrExt ? ',' : '');
                } else {
                    if (preg_match('/^<([^> ]+)([^>]*?class="([^"]*)")?[^>]*?>.*?>$/ism', $item, $match)) {
                        if ($match[2]) {
                            $item = preg_replace('/^(<[^> ]+[^>]*?class="[^"]*)("?[^>]*?>)/ism', '$1 ' . $visibilityClass . '$2', $item);
                        } else {
                            $item = preg_replace('/^(<[^> ]+)/ism', '$1 class="' . $visibilityClass . '"', $item);
                        }
                    } else {
                        $item = '<span class="' . $visibilityClass . '">' . $item . '</span>';
                    }
                }
            }
        }

        $this->WMresult .= $item;
    }

    /**
     * @return bool
     */
    protected function isBootstrapVisibilityEnabled()
    {
        if (!isset(self::$bootstrapVisibilityEnabled)) {
            $extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['imia_base']);
            self::$bootstrapVisibilityEnabled = (bool)$extConf['bootstrapVisibilityPages'];
        }

        return self::$bootstrapVisibilityEnabled;
    }
}
