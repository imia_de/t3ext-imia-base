<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Form\Domain\Finishers;

use TYPO3\CMS\Fluid\View\StandaloneView;
use TYPO3\CMS\Form\Domain\Finishers\Exception\FinisherException;
use TYPO3\CMS\Form\Domain\Runtime\FormRuntime;
use TYPO3\CMS\Form\ViewHelpers\RenderRenderableViewHelper;

/**
 * @package     imia_beonesolutions
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class EmailFinisher extends \TYPO3\CMS\Form\Domain\Finishers\EmailFinisher
{
    /**
     * @param FormRuntime $formRuntime
     * @return StandaloneView
     * @throws FinisherException
     */
    protected function initializeStandaloneView(FormRuntime $formRuntime): StandaloneView
    {
        $format = ucfirst($this->parseOption('format'));

        if ($format == 'Rte') {
            /** @var StandaloneView $standaloneView */
            $standaloneView = $this->objectManager->get(StandaloneView::class);

            $templatePath = PATH_site . 'typo3temp/var/Cache/Code/cache_core/form_temp_' . sha1(uniqid(microtime()) . 'html');
            file_put_contents($templatePath, $this->parseOption('text'));

            $standaloneView->setTemplatePathAndFilename($templatePath);

            $standaloneView->assign('finisherVariableProvider', $this->finisherContext->getFinisherVariableProvider());

            if (isset($this->options['templateRootPaths']) && is_array($this->options['templateRootPaths'])) {
                $standaloneView->setTemplateRootPaths($this->options['templateRootPaths']);
            }

            if (isset($this->options['partialRootPaths']) && is_array($this->options['partialRootPaths'])) {
                $standaloneView->setPartialRootPaths($this->options['partialRootPaths']);
            }

            if (isset($this->options['layoutRootPaths']) && is_array($this->options['layoutRootPaths'])) {
                $standaloneView->setLayoutRootPaths($this->options['layoutRootPaths']);
            }

            if (isset($this->options['variables'])) {
                $standaloneView->assignMultiple($this->options['variables']);
            }

            $standaloneView->assign('form', $formRuntime);
            $standaloneView->getRenderingContext()
                ->getViewHelperVariableContainer()
                ->addOrUpdate(RenderRenderableViewHelper::class, 'formRuntime', $formRuntime);

            return $standaloneView;
        } else {
            return parent::initializeStandaloneView($formRuntime);
        }
    }
}
