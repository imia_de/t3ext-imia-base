<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Form\Hooks;

use IMIA\ImiaBase\User\FormFlexform;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class DataStructureIdentifierHook extends \TYPO3\CMS\Form\Hooks\DataStructureIdentifierHook
{
    /**
     * @inheritdoc
     */
    protected function getAdditionalFinisherSheets(string $persistenceIdentifier, array $formDefinition): array
    {
        $sheets = parent::getAdditionalFinisherSheets($persistenceIdentifier, $formDefinition);

        if (isset($sheets['sheets']) && is_array($sheets['sheets'])) {
            foreach ($sheets['sheets'] as &$sheet) {
                $elements = $sheet['ROOT']['el'];
                $sheet['ROOT']['el'] = [];
                foreach ($elements as $key => $element) {
                    $sheet['ROOT']['el'][$key] = $element;

                    if ($key == 'settings.finishers.EmailToSender.text') {
                        $sheet['ROOT']['el']['settings.finishers.EmailToSender.textInfo'] = [
                            'label'  => '',
                            'config' => [
                                'type'     => 'user',
                                'userFunc' => FormFlexform::class . '->textInfo',
                            ],
                        ];
                    }
                    if ($key == 'settings.finishers.EmailToReceiver.text') {
                        $sheet['ROOT']['el']['settings.finishers.EmailToReceiver.textInfo'] = [
                            'label'  => '',
                            'config' => [
                                'type'     => 'user',
                                'userFunc' => FormFlexform::class . '->textInfo',
                            ],
                        ];
                    }
                }
            }
        }

        return $sheets;
    }
}
