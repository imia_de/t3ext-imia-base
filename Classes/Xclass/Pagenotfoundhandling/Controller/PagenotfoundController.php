<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Pagenotfoundhandling\Controller;

use IMIA\ImiaBase\Traits\HookTrait;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class PagenotfoundController extends \AawTeam\Pagenotfoundhandling\Controller\PagenotfoundController
{
    use HookTrait;

    /**
     * @inheritdoc
     * @throws \InvalidArgumentException
     * @throws \TYPO3\CMS\Core\Package\Exception
     */
    public function main($params, TypoScriptFrontendController $typoScriptFrontendController)
    {
        $this->callHook('main', [$params, $typoScriptFrontendController, $this]);

        return parent::main($params, $typoScriptFrontendController);
    }

    /**
     * @inheritdoc
     */
    protected function _getUrl($url, $includeHeaders = 0, array $headers = [], &$report = null)
    {
        $requestHeaders = $this->_getAllHttpHeaders();
        if (array_key_exists('authorization', $requestHeaders)) {
            $authorizationHeader = $requestHeaders['authorization'];

            // Authorization 'basic' support
            if (strpos($authorizationHeader, 'Basic ') === 0) {
                // check the header value for authentication basic,
                // only base64 characters are allowed
                if (preg_match('~[^a-zA-Z0-9+/=]~', substr($authorizationHeader, 6)) === 0) {
                    $headers[] = 'Authorization: ' . $authorizationHeader;
                }
            }
        }

        try {
            $return = parent::_getUrl($url, $includeHeaders, $headers, $report);
        } catch (\Exception $e) {
            $return = @file_get_contents($url);
            if (!$return) {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $return = curl_exec($ch);
                $returnInfo = curl_getinfo($ch);
                curl_close($ch);

                if ($returnInfo['http_code'] == 400) {
                    header('Location: ' . $url);
                }
            }
        }

        return $return;
    }
}
