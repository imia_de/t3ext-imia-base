<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Realurl\Decoder;

use IMIA\ImiaBase\Traits\HookTrait;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Page\PageRepository;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class UrlDecoder extends \DmitryDulepov\Realurl\Decoder\UrlDecoder
{
    use HookTrait;

    /**
     * @inheritdoc
     */
    protected function searchPages($currentPid, $segment, &$saveToCache)
    {
        $record = BackendUtility::getRecord('pages', $currentPid, 'uid,pid,mount_pid', 'doktype = ' . PageRepository::DOKTYPE_MOUNTPOINT . ' AND mount_pid > 0');
        if ($record && $record['mount_pid']) {
            $currentPid = (int)$record['mount_pid'];
            $this->mountPointVariable = $record['mount_pid'] . '-' . $record['uid'];
            $this->mountPointStartPid = (int)$record['mount_pid'];
        }

        return parent::searchPages($currentPid, $segment, $saveToCache);
    }

    /**
     * @inheritdoc
     */
    protected function createPathCacheEntry($segment, array $pages, array &$shortcutPages)
    {
        $result = parent::createPathCacheEntry($segment, $pages, $shortcutPages);

        if (!$result) {
            foreach ($pages as $page) {
                $originalMountPointPid = 0;
                if ($page['doktype'] == PageRepository::DOKTYPE_SHORTCUT) {
                    // Value is not relevant, key is!
                    $shortcutPages[$page['uid']] = true;
                }
                while ($page['doktype'] == PageRepository::DOKTYPE_MOUNTPOINT && $page['mount_pid_ol'] == 1) {
                    $originalMountPointPid = $page['uid'];
                    $page = $this->pageRepository->getPage($page['mount_pid']);
                    if (!is_array($page)) {
                        $this->tsfe->pageNotFoundAndExit('[realurl] Broken mount point at page with uid=' . $originalMountPointPid);
                    }
                }
                $languageExceptionUids = (string)$this->configuration->get('pagePath/languageExceptionUids');
                if ($this->detectedLanguageId > 0 && !isset($page['_PAGES_OVERLAY']) && (empty($languageExceptionUids) || !GeneralUtility::inList($languageExceptionUids, $this->detectedLanguageId))) {
                    $page = $this->pageRepository->getPageOverlay($page, (int)$this->detectedLanguageId);
                }
                foreach (self::$pageTitleFields as $field) {
                    if (isset($page[$field]) && $page[$field] !== '') {
                        $defaultCompareSegment = $this->utility->convertToSafeString($page[$field], $this->separatorCharacter);
                        $compareSegments = [$defaultCompareSegment . '-' . $page['uid']];

                        $this->callHook('compareSegments', [&$compareSegments, $defaultCompareSegment, $page, $this]);

                        foreach ($compareSegments as $compareSegment) {
                            if ($compareSegment === $segment) {
                                $result = GeneralUtility::makeInstance('DmitryDulepov\\Realurl\\Cache\\PathCacheEntry');
                                /** @var \DmitryDulepov\Realurl\Cache\PathCacheEntry $result */
                                $result->setPageId((int)$page['uid']);
                                if ($this->mountPointVariable !== '') {
                                    $result->setMountPoint($this->mountPointVariable);
                                }
                                if ($originalMountPointPid !== 0) {
                                    // Mount point with mount_pid_ol==1
                                    $this->mountPointVariable = $page['uid'] . '-' . $originalMountPointPid;
                                    // No $this->mountPointStartPid here because this is a substituted page
                                } elseif ((int)$page['doktype'] === PageRepository::DOKTYPE_MOUNTPOINT) {
                                    $this->mountPointVariable = $page['mount_pid'] . '-' . $page['uid'];
                                    $this->mountPointStartPid = (int)$page['mount_pid'];
                                }
                                break 2;
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }
}