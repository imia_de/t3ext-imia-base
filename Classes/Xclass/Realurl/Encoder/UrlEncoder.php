<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Realurl\Encoder;

use DmitryDulepov\Realurl\Exceptions\InvalidLanguageParameterException;
use IMIA\ImiaBase\Traits\HookTrait;
use TYPO3\CMS\Core\Utility\MathUtility;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Page\PageRepository;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class UrlEncoder extends \DmitryDulepov\Realurl\Encoder\UrlEncoder
{
    use HookTrait;

    /**
     * @inheritdoc
     */
    protected function validateLanguageParameter($sysLanguageUid)
    {
        static $sysLanguages = null;

        if (trim($sysLanguageUid) === '') {
            // Allow this case because some people use "L=" for the default language.
            // We convert this to 0 in the setLanguage().
            $isValidLanguageUid = true;
        } elseif (!MathUtility::canBeInterpretedAsInteger($sysLanguageUid)) {
            $isValidLanguageUid = false;
        } elseif ($sysLanguageUid != 0) {
            if ($sysLanguages === null) {
                $sysLanguages = [];
                $rows = $this->databaseConnection->exec_SELECTgetRows('*', 'sys_language', '1=1' . $this->pageRepository->enableFields('sys_language'));
                foreach ($rows as $row) {
                    $sysLanguages[(int)$row['uid']] = (int)$row['uid'];
                }
            }
            $isValidLanguageUid = isset($sysLanguages[(int)$sysLanguageUid]);
        } else {
            // It is zero
            $isValidLanguageUid = true;
        }

        if (!$isValidLanguageUid) {
            $message = sprintf(
                'Bad "L" parameter ("%s") was detected by realurl. ' .
                'Page caching is disabled to prevent spreading of wrong "L" value.',
                addslashes($sysLanguageUid)
            );
            $this->tsfe->set_no_cache($message);
            //$this->logger->error($message, debug_backtrace());
            throw new InvalidLanguageParameterException($sysLanguageUid);
        }
    }

    /**
     * @inheritdoc
     */
    protected function createPathComponentUsingRootline()
    {
        $this->logger->debug('Starting path generation');

        $mountPointParameter = '';
        if (isset($this->urlParameters['MP'])) {
            $mountPointParameter = $this->urlParameters['MP'];
            unset($this->urlParameters['MP']);
        }
        $rootLineUtility = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Utility\\RootlineUtility',
            $this->urlParameters['id'], $mountPointParameter, $this->pageRepository
        );
        /** @var \TYPO3\CMS\Core\Utility\RootlineUtility $rootLineUtility */
        $rootLine = $rootLineUtility->get();

        // Skip from the root of the tree to the first level of pages
        while (count($rootLine) !== 0) {
            $page = array_pop($rootLine);
            if ($page['uid'] == $this->rootPageId) {
                break;
            }
        }

        $languageExceptionUids = (string)$this->configuration->get('pagePath/languageExceptionUids');
        $enableLanguageOverlay = ((int)$this->originalUrlParameters['L'] > 0) && (empty($languageExceptionUids) || !GeneralUtility::inList($languageExceptionUids, $this->sysLanguageUid));

        $components = [];
        $reversedRootLine = array_reverse($rootLine);
        $rootLineMax = count($reversedRootLine) - 1;
        for ($current = 0; $current <= $rootLineMax; $current++) {
            $page = $reversedRootLine[$current];
            // Skip if this page is excluded
            if ($page['tx_realurl_exclude'] && $current !== $rootLineMax) {
                $this->logger->debug(
                    sprintf(
                        'Page %d is excluded from realurl',
                        (int)$page['uid']
                    )
                );
                continue;
            }
            if ($enableLanguageOverlay) {
                $overlay = $this->pageRepository->getPageOverlay($page, (int)$this->originalUrlParameters['L']);
                if (is_array($overlay)) {
                    $page = $overlay;
                    unset($overlay);
                }
            }

            // if path override is set, use path segment also for all subpages to shorten the url and throw away all segments found so far
            if ($page['tx_realurl_pathoverride'] && $page['tx_realurl_pathsegment'] !== '') {
                $this->logger->debug(
                    sprintf(
                        'Path override detected for page %d',
                        (int)$page['uid']
                    )
                );
                $segment = trim($page['tx_realurl_pathsegment'], '/');
                $segments = explode('/', $segment);
                array_walk($segments, function (&$segment) {
                    $segment = rawurlencode($this->utility->convertToSafeString($segment, $this->separatorCharacter));
                });
                // Technically we could do with `$components = $segments` but it fills better to have overriden string here
                $segment = implode('/', $segments);
                unset($segments);
                $components = [$segment];
                continue;
            }

            /* Start: UniqueTitle Extension by dfrerich */
            $result = $this->getDb()->exec_SELECTquery(
                '*',
                'pages',
                'pid = ' . $page['pid'] . $this->pageRepository->deleteClause('pages')
            );

            $segments = [];
            while ($row = $this->getDb()->sql_fetch_assoc($result)) {
                $row = $this->pageRepository->getPageOverlay($row);
                if ($enableLanguageOverlay) {
                    $overlay = $this->pageRepository->getPageOverlay($row, (int)$this->originalUrlParameters['L']);
                    if (is_array($overlay)) {
                        $row = $overlay;
                        unset($overlay);
                    }
                }

                foreach (self::$pageTitleFields as $field) {
                    if (isset($row[$field]) && $row[$field] !== '') {
                        $segment = $this->utility->convertToSafeString($row[$field], $this->separatorCharacter);
                        if ($segment === '') {
                            $segment = $this->emptySegmentValue;
                        }
                        $segment = rawurlencode($segment);
                        $segments[$row['uid']] = $segment;
                        $this->logger->debug(
                            sprintf(
                                'Found path segment "%s" using field "%s"',
                                $segment,
                                $field
                            )
                        );
                        continue 2;
                    }
                }
            }

            $pageSegment = $segments[$page['uid']];
            unset($segments[$page['uid']]);
            $this->callHook('createPathComponentUsingRootline', [&$pageSegment, $page, $this]);

            if (in_array($pageSegment, $segments)) {
                $originalPathSegment = $pageSegment;
                $pageSegment .= '-' . $page['uid'];

                $this->callHook('createPathComponentUsingRootline_duplicate', [&$pageSegment, $originalPathSegment, $page, $this]);
            }

            $components[] = $pageSegment;
            /* End: UniqueTitle Extension by dfrerich */
        }

        if (count($components) > 0) {
            $generatedPath = implode('/', $components);

            foreach ($components as $segment) {
                $this->appendToEncodedUrl($segment);
            }
            if ($reversedRootLine[$rootLineMax]['doktype'] != PageRepository::DOKTYPE_SPACER && $reversedRootLine[$rootLineMax]['doktype'] != PageRepository::DOKTYPE_RECYCLER) {
                $this->addToPathCache($generatedPath);
            }

            $this->logger->debug(
                sprintf(
                    'Generated path: "%s"',
                    $generatedPath
                )
            );
        }
        $this->logger->debug('Finished path generation');
    }

    /**
     * @inheritdoc
     */
    protected function reapplyAbsRefPrefix()
    {
        if ($this->getAbsRefPrefix()) {
            parent::reapplyAbsRefPrefix();
        }
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}