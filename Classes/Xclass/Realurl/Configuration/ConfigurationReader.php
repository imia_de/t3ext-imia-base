<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutinos (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Xclass\Realurl\Configuration;

use IMIA\ImiaBase\Exception\ExceptionWithoutLogging;

/**
 * @package     imia_base
 * @subpackage  Xclass/Realurl
 * @author      David Frerich <d.frerich@imia.de>
 */
class ConfigurationReader extends \DmitryDulepov\Realurl\Configuration\ConfigurationReader
{
    /**
     * @inheritdoc
     */
    protected function setRootPageId()
    {
        try {
            parent::setRootPageId();
        } catch(\Exception $e) {
            throw new ExceptionWithoutLogging($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @inheritdoc
     */
    protected function setRootPageIdFromRootFlag()
    {
        try {
            return parent::setRootPageIdFromRootFlag();
        } catch(\Exception $e) {
            throw new ExceptionWithoutLogging($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @inheritdoc
     */
    protected function setRootPageIdFromTopLevelPages()
    {
        try {
            return parent::setRootPageIdFromTopLevelPages();
        } catch(\Exception $e) {
            throw new ExceptionWithoutLogging($e->getMessage(), $e->getCode());
        }
    }
}
