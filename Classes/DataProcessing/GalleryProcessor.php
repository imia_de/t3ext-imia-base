<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\DataProcessing;

/**
 * @package     imia_base
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class GalleryProcessor extends \TYPO3\CMS\Frontend\DataProcessing\GalleryProcessor
{

    protected function calculateMediaWidthsAndHeights()
    {
        $columnSpacingTotal = ($this->galleryData['count']['columns'] - 1) * $this->columnSpacing;

        $galleryWidthMinusBorderAndSpacing = max($this->galleryData['width'] - $columnSpacingTotal, 1);

        if ($this->borderEnabled) {
            $borderPaddingTotal = ($this->galleryData['count']['columns'] * 2) * $this->borderPadding;
            $borderWidthTotal = ($this->galleryData['count']['columns'] * 2) * $this->borderWidth;
            $galleryWidthMinusBorderAndSpacing = $galleryWidthMinusBorderAndSpacing - $borderPaddingTotal - $borderWidthTotal;
        }

        // User entered a predefined height
        if ($this->equalMediaHeight) {
            $mediaScalingCorrection = 1;
            $maximumRowWidth = 0;

            // Calculate the scaling correction when the total of media elements is wider than the gallery width
            for ($row = 1; $row <= $this->galleryData['count']['rows']; $row++) {
                $totalRowWidth = 0;
                for ($column = 1; $column <= $this->galleryData['count']['columns']; $column++) {
                    $fileKey = (($row - 1) * $this->galleryData['count']['columns']) + $column - 1;
                    if ($fileKey > $this->galleryData['count']['files'] - 1) {
                        break 2;
                    }
                    $currentMediaScaling = $this->equalMediaHeight / max($this->getCroppedDimensionalProperty($this->fileObjects[$fileKey], 'height'), 1);
                    $totalRowWidth += $this->getCroppedDimensionalProperty($this->fileObjects[$fileKey], 'width') * $currentMediaScaling;
                }
                $maximumRowWidth = max($totalRowWidth, $maximumRowWidth);
                $mediaInRowScaling = $totalRowWidth / $galleryWidthMinusBorderAndSpacing;
                $mediaScalingCorrection = max($mediaInRowScaling, $mediaScalingCorrection);
            }

            // Set the corrected dimensions for each media element
            foreach ($this->fileObjects as $key => $fileObject) {
                $mediaHeight = floor($this->equalMediaHeight / $mediaScalingCorrection);
                $mediaWidth = floor(
                    $this->getCroppedDimensionalProperty($fileObject, 'width') * ($mediaHeight / max($this->getCroppedDimensionalProperty($fileObject, 'height'), 1))
                );
                $this->mediaDimensions[$key] = [
                    'width' => $mediaWidth,
                    'height' => $mediaHeight
                ];
            }

            // Recalculate gallery width
            $this->galleryData['width'] = floor($maximumRowWidth / $mediaScalingCorrection);

            // User entered a predefined width
        } elseif ($this->equalMediaWidth) {
            $mediaScalingCorrection = 1;

            // Calculate the scaling correction when the total of media elements is wider than the gallery width
            $totalRowWidth = $this->galleryData['count']['columns'] * $this->equalMediaWidth;
            $mediaInRowScaling = $totalRowWidth / $galleryWidthMinusBorderAndSpacing;
            $mediaScalingCorrection = max($mediaInRowScaling, $mediaScalingCorrection);

            // Set the corrected dimensions for each media element
            foreach ($this->fileObjects as $key => $fileObject) {
                $mediaWidth = floor($this->equalMediaWidth / $mediaScalingCorrection);
                $mediaHeight = floor(
                    $this->getCroppedDimensionalProperty($fileObject, 'height') * ($mediaWidth / max($this->getCroppedDimensionalProperty($fileObject, 'width'), 1))
                );
                $this->mediaDimensions[$key] = [
                    'width' => $mediaWidth,
                    'height' => $mediaHeight
                ];
            }

            // Recalculate gallery width
            $this->galleryData['width'] = floor($totalRowWidth / $mediaScalingCorrection);

            // Automatic setting of width and height
        } else {
            $maxMediaWidth = (int)($galleryWidthMinusBorderAndSpacing / $this->galleryData['count']['columns']);

            if ($this->galleryData['count']['columns'] > 1) {
                if ($GLOBALS['TSFE'] && isset($GLOBALS['TSFE']->tmpl->setup_constants['styles.']['content.']['textmedia.'])) {
                    $textMediaConfig = $GLOBALS['TSFE']->tmpl->setup_constants['styles.']['content.']['textmedia.'];

                    if (is_array($textMediaConfig)) {
                        $minWidth = $smWidth = $mdWidth = $lgWidth = null;
                        if (isset($textMediaConfig['maxWmin'])) {
                            $minWidth = $textMediaConfig['maxWmin'];
                        }
                        if (isset($textMediaConfig['maxWsm'])) {
                            $smWidth = $textMediaConfig['maxWsm'];
                        }
                        if (isset($textMediaConfig['maxWmd'])) {
                            $mdWidth = $textMediaConfig['maxWmd'];
                        }
                        if (isset($textMediaConfig['maxWlg'])) {
                            $lgWidth = $textMediaConfig['maxWlg'];
                        }

                        switch ($this->galleryData['count']['columns']) {
                            case 2:
                                if ($minWidth) {
                                    if ($minWidth > $maxMediaWidth) {
                                        $maxMediaWidth = $minWidth;
                                    }
                                }
                                if ($smWidth) {
                                    if ($smWidth > $maxMediaWidth) {
                                        $maxMediaWidth = $smWidth;
                                    }
                                }
                                break;
                            case 3:
                                if ($minWidth) {
                                    if ($minWidth > $maxMediaWidth) {
                                        $maxMediaWidth = $minWidth;
                                    }
                                }
                                if ($smWidth) {
                                    if ($smWidth > $maxMediaWidth) {
                                        $maxMediaWidth = $smWidth;
                                    }
                                }
                                break;
                            case 4:
                                if ($minWidth) {
                                    if ($minWidth/2 > $maxMediaWidth) {
                                        $maxMediaWidth = $minWidth/2;
                                    }
                                }
                                if ($smWidth) {
                                    if ($smWidth/2 > $maxMediaWidth) {
                                        $maxMediaWidth = $smWidth/2;
                                    }
                                }
                                if ($mdWidth) {
                                    if ($mdWidth/2 > $maxMediaWidth) {
                                        $maxMediaWidth = $mdWidth/2;
                                    }
                                }
                                break;
                            case 5:
                            case 6:
                                if ($minWidth) {
                                    if ($minWidth/2 > $maxMediaWidth) {
                                        $maxMediaWidth = $minWidth/2;
                                    }
                                }
                                if ($smWidth) {
                                    if ($smWidth/3 > $maxMediaWidth) {
                                        $maxMediaWidth = $smWidth/3;
                                    }
                                }
                                if ($mdWidth) {
                                    if ($mdWidth/4 > $maxMediaWidth) {
                                        $maxMediaWidth = $mdWidth/4;
                                    }
                                }
                                break;
                            case 7:
                            case 8:
                            case 9:
                            case 10:
                            case 11:
                            case 12:
                                if ($minWidth) {
                                    if ($minWidth/3 > $maxMediaWidth) {
                                        $maxMediaWidth = $minWidth/3;
                                    }
                                }
                                if ($smWidth) {
                                    if ($smWidth/4 > $maxMediaWidth) {
                                        $maxMediaWidth = $smWidth/4;
                                    }
                                }
                                if ($mdWidth) {
                                    if ($mdWidth/6 > $maxMediaWidth) {
                                        $maxMediaWidth = $mdWidth/6;
                                    }
                                }
                                break;
                        }
                    }
                }
            }

            foreach ($this->fileObjects as $key => $fileObject) {
                $mediaWidth = min($maxMediaWidth, $this->getCroppedDimensionalProperty($fileObject, 'width'));
                $mediaHeight = floor(
                    $this->getCroppedDimensionalProperty($fileObject, 'height') * ($mediaWidth / max($this->getCroppedDimensionalProperty($fileObject, 'width'), 1))
                );
                $this->mediaDimensions[$key] = [
                    'width' => $mediaWidth,
                    'height' => $mediaHeight
                ];
            }
        }
    }
}
