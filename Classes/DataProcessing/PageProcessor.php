<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\DataProcessing;

use IMIA\ImiaBase\Utility\ProcessingHelper;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

/**
 * @package     imia_base
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class PageProcessor implements DataProcessorInterface
{
    /**
     * @param ContentObjectRenderer $cObj
     * @param array $contentObjectConfiguration
     * @param array $processorConfiguration
     * @param array $processedData
     * @return array
     * @throws \TYPO3\CMS\Core\Configuration\FlexForm\Exception\InvalidIdentifierException
     * @throws \TYPO3\CMS\Core\Configuration\FlexForm\Exception\InvalidParentRowException
     * @throws \TYPO3\CMS\Core\Configuration\FlexForm\Exception\InvalidParentRowLoopException
     * @throws \TYPO3\CMS\Core\Configuration\FlexForm\Exception\InvalidParentRowRootException
     * @throws \TYPO3\CMS\Core\Configuration\FlexForm\Exception\InvalidPointerFieldValueException
     * @throws \TYPO3\CMS\Core\Configuration\FlexForm\Exception\InvalidTcaException
     */
    public function process(ContentObjectRenderer $cObj, array $contentObjectConfiguration, array $processorConfiguration, array $processedData)
    {
        if (isset($processorConfiguration['if.']) && !$cObj->checkIf($processorConfiguration['if.'])) {
            return $processedData;
        }

        if ($GLOBALS['TSFE']) {
            $data['page'] = $GLOBALS['TSFE']->page;
            $data['pageSlide'] = $data['page'];

            if (is_array($GLOBALS['TSFE']->rootLine)) {
                foreach ($GLOBALS['TSFE']->rootLine as $pageSlide) {
                    foreach ($pageSlide as $key => $val) {
                        if ($val && !$data['pageSlide'][$key]) {
                            $data['pageSlide'][$key] = $val;
                        }
                    }
                }
            }
        }

        $processedData = ProcessingHelper::process($processedData, $data);

        if (isset($processedData['pageSlide'])) {
            foreach ($GLOBALS['TCA']['pages']['columns'] as $key => $column) {
                if ($column['config']['type'] == 'inline' && $column['config']['foreign_table'] == 'sys_file_reference') {
                    if (array_key_exists($key, $processedData['pageSlide']) && isset($GLOBALS['TSFE']->rootLine[0][$key]) && !$processedData['pageSlide'][$key]) {
                        foreach ($GLOBALS['TSFE']->rootLine as $pageSlide) {
                            if (!$processedData['pageSlide'][$key]) {
                                ProcessingHelper::resolveTCAColumnValue('pages', $key, $pageSlide['uid'], $processedData['pageSlide'][$key], $pageSlide);
                            }
                        }
                    }
                }
            }
        }

        return $processedData;
    }
}
