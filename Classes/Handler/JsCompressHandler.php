<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Handler;

use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Resource\ResourceCompressor;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class JsCompressHandler extends ResourceCompressor
{
    /**
     * @param array $params
     * @param PageRenderer $pObj
     */
    public function handle($params, $pObj)
    {
        if (!empty($params['jsInline'])) {
            foreach ($params['jsInline'] as $name => $properties) {
                if ($properties['compress']) {
                    $params['jsInline'][$name]['code'] = $this->minify($properties['code']);
                }
            }
        }
        $params['jsLibs'] = $this->compressJsFiles($params['jsLibs']);
        $params['jsFiles'] = $this->compressJsFiles($params['jsFiles']);
        $params['jsFooterFiles'] = $this->compressJsFiles($params['jsFooterFiles']);
    }

    /**
     * @param string $filename
     * @param string $contents
     */
    protected function writeFileAndCompressed($filename, $contents)
    {
        parent::writeFileAndCompressed($filename, $this->minify($contents));
    }

    /**
     * @param string $contents
     * @return string
     */
    protected function minify($contents)
    {
        $minifier = new \MatthiasMullie\Minify\JS();
        $minifier->add($contents);

        return $minifier->minify();
    }
}