<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\Toolbar;

use TYPO3\CMS\Backend\Toolbar\ToolbarItemInterface;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * @package     imia_base
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class ToolbarItem implements ToolbarItemInterface
{
    /**
     * @return bool
     */
    public function checkAccess()
    {
        if ($this->getBackendUser() && $this->getBackendUser()->isAdmin()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return string
     */
    public function getItem()
    {
        $asseticUrl = BackendUtility::getModuleUrl(
            'tools_ExtensionmanagerExtensionmanager',
            [
                'tx_extensionmanager_tools_extensionmanagerextensionmanager' => [
                    'extensionKey' => 'imia_base',
                    'controller'   => 'UpdateScript',
                    'action'       => 'show',
                ],
                'modal'                                                      => 1,
            ]
        );

        $iframeHeight = 600;
        $hideIframeTop = 89 + 29 + 22;
        $loaderBg = '/' . ExtensionManagementUtility::siteRelPath('imia_base') . 'Resources/Public/Images/AjaxLoader.gif';

        return '
            <script type="text/javascript">
                function openAsseticTool(link) {
                    TYPO3.Modal.show("' . $this->translate('toolbar.assetic') . '", TYPO3.jQuery(\'<div style="background: transparent url(' . $loaderBg . ') no-repeat center center; overflow:hidden;position:relative;width:570px;height:' . ($iframeHeight - $hideIframeTop) . 'px"><iframe frameborder="0" border="0" src="' . $asseticUrl . '" style="position:absolute;bottom:0;left:0;width:570px;height:' . $iframeHeight . 'px"></iframe></div>\'), TYPO3.Severity.info);
                    //window.open(link.href, "' . $this->translate('toolbar.assetic') . '", "location=0,status=0,scrollbars=1,width=600,height=' . $iframeHeight . '").focus();
                }
            </script>
            <a href="' . $asseticUrl . '" class="toolbar-item-link" onclick="openAsseticTool(this);return false;" target="_blank">
                <span title="' . $this->translate('toolbar.assetic') . '">
                    <span class="icon icon-size-small icon-state-default icon-apps-toolbar-menu-assetic">
                        <span class="icon-markup">
                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             viewBox="0 0 474.557 474.557" style="enable-background:new 0 0 474.557 474.557;" xml:space="preserve">
                        <g>
                            <g>
                                <g>
                                    <path d="M0,2.028V472.53h474.557V2.028H0z M435.471,433.465c-33.996,0-362.303,0-396.363,0c0-33.996,0-358.377,0-392.329
                                        c33.996,0,362.346,0,396.363,0C435.471,75.088,435.471,399.469,435.471,433.465z" class="icon-color"/>
                                    <path d="M124.916,273.625c1.294,4.53,2.006,14.409,2.006,29.574c0,13.007,0.669,22.563,2.071,28.624
                                        c1.402,6.018,4.142,11.174,8.24,15.272c4.077,4.098,9.081,7.075,15.013,8.866c5.954,1.769,14.496,2.653,25.605,2.653h6.579
                                        v-29.66c-8.866,0-14.625-0.561-17.3-1.682c-2.653-1.143-4.638-2.718-5.954-4.897c-1.294-2.157-1.941-5.91-1.941-11.325
                                        c0-3.84-0.345-12.576-0.992-26.295c-0.496-8.973-1.575-16.135-3.236-21.484c-1.639-5.393-4.012-9.944-7.118-13.697
                                        c-3.085-3.775-7.054-7.032-11.907-9.815c5.501-3.581,9.577-6.967,12.274-10.311c2.675-3.257,4.875-7.442,6.601-12.511
                                        c1.704-5.069,2.783-11.691,3.214-19.845c0.733-14.452,1.143-24.289,1.143-29.487c0-5.091,0.626-8.715,1.855-10.807
                                        c1.251-2.071,3.149-3.645,5.738-4.702c2.588-1.014,8.477-1.532,17.623-1.532v-29.509h-6.579c-10.807,0-18.723,0.755-23.728,2.243
                                        c-7.42,2.222-13.072,5.242-16.976,9.06c-3.883,3.84-6.558,8.909-8.046,15.229c-1.467,6.277-2.179,17.429-2.179,33.348
                                        c0,9.858-0.367,16.674-1.035,20.643c-0.82,5.263-2.351,9.642-4.551,13.158c-2.243,3.516-4.832,5.997-7.916,7.485
                                        c-3.02,1.445-7.528,2.373-13.568,2.739v29.681c7.593,0.388,13.201,2.028,16.89,4.897
                                        C120.387,262.344,123.126,267.025,124.916,273.625z" class="icon-color"/>
                                    <path d="M212.084,331.392l6.73,14.366c7.42-2.653,13.568-6.32,18.4-10.915c4.832-4.595,8.089-9.815,9.815-15.617
                                        c1.747-5.846,2.61-13.805,2.61-23.814v-24.785h-34.751v34.729h16.761c-0.129,6.838-1.726,12.295-4.659,16.48
                                        C224.034,325.87,219.051,329.105,212.084,331.392z" class="icon-color"/>
                                    <rect x="214.888" y="174.012" width="34.751" height="34.772" class="icon-color"/>
                                    <path d="M311.18,356.349c7.464-2.243,13.201-5.22,17.106-9.081c3.883-3.818,6.536-8.93,7.938-15.272
                                        c1.402-6.363,2.179-17.515,2.179-33.499c0-9.75,0.367-16.609,1.014-20.557c0.884-5.263,2.438-9.642,4.659-13.137
                                        c2.179-3.494,4.767-6.018,7.809-7.485c3.042-1.467,7.55-2.373,13.568-2.696h-0.043v-29.66c-7.615-0.453-13.18-2.071-16.933-4.918
                                        c-3.667-2.826-6.407-7.571-8.262-14.107c-1.251-4.616-1.855-14.452-1.855-29.509c0-13.029-0.712-22.541-2.157-28.56
                                        c-1.424-6.105-4.163-11.195-8.197-15.358c-4.055-4.142-9.038-7.054-14.97-8.801c-5.975-1.726-14.517-2.61-25.626-2.61h-6.557
                                        v29.466c9.34,0,15.337,0.561,17.99,1.683c2.696,1.1,4.551,2.675,5.565,4.724c1.057,2.049,1.553,6.169,1.639,12.36
                                        c0.453,19.608,1.186,32.291,2.071,38.072c1.381,8.369,4.055,15.294,7.873,20.686c2.826,3.84,7.248,7.658,13.352,11.627
                                        c-4.702,2.696-8.52,5.846-11.54,9.448c-3.041,3.559-5.436,7.96-7.205,13.072c-1.812,5.177-2.955,12.08-3.322,20.6
                                        c-0.669,14.172-1.1,23.814-1.1,28.819c-0.086,5.026-0.669,8.585-1.984,10.699c-1.316,2.136-3.279,3.71-5.975,4.853
                                        c-2.718,1.122-8.52,1.639-17.364,1.639v29.66h6.622C298.216,358.571,306.154,357.795,311.18,356.349z" class="icon-color"/>
                                    </g>
                                </g>
                            </g>
                            </svg>
                        </span>
                    </span>
                </span>
            </a>';
    }

    /**
     * @return bool
     */
    public function hasDropDown()
    {
        return false;
    }

    /**
     * @return string
     */
    public function getDropDown()
    {
        return '';
    }

    /**
     * @return array
     */
    public function getAdditionalAttributes()
    {
        return [];
    }

    /**
     * @return int
     */
    public function getIndex()
    {
        return 50;
    }

    /**
     * @param string $key
     * @param array $arguments
     * @return string
     */
    protected function translate($key, $arguments = [])
    {
        return (string)LocalizationUtility::translate($key, 'imia_base', $arguments);
    }

    /**
     * @return BackendUserAuthentication
     */
    protected function getBackendUser()
    {
        return $GLOBALS['BE_USER'];
    }
}
