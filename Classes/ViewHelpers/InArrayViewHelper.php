<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\ViewHelpers;

/**
 * @package     imia_base
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class InArrayViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
    /**
     * @param mixed $value
     * @param array $array
     * @return boolean
     */
    public function render($value, $array = null)
    {
        if (!is_array($array)) {
            $array = (array)$this->renderChildren();
        }

        if (is_object($value) && method_exists($value, 'getUid')) {
            $value = get_class($value) . '_' . $value->getUid();
        }
        if (is_object($array)) {
            $newArr = [];
            foreach ($array as $arrValue) {
                if (is_object($arrValue) && method_exists($arrValue, 'getUid')) {
                    $arrValue = get_class($arrValue) . '_' . $arrValue->getUid();
                }
                $newArr[] = $arrValue;
            }

            $array = $newArr;
        }

        return in_array($value, $array);
    }
}