<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\ViewHelpers\Iterator;

use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Fluid\Core\ViewHelper\Exception;
use TYPO3\CMS\Fluid\Core\ViewHelper\Facets\CompilableInterface;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithContentArgumentAndRenderStatic;

/**
 * @package     imia_base
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class MergeViewHelper extends AbstractViewHelper implements CompilableInterface
{
    use CompileWithContentArgumentAndRenderStatic;

    /**
     * @var boolean
     */
    protected $escapeChildren = false;

    /**
     * @var boolean
     */
    protected $escapeOutput = false;

    /**
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument(
            'a',
            'mixed',
            'First array/Traversable - if not set, the ViewHelper can be in a chain (inline-notation)'
        );
        $this->registerArgument('b', 'mixed', 'Second array or Traversable');
        $this->registerArgument(
            'useKeys',
            'boolean',
            'If TRUE comparison is done while also observing and merging the keys used in each array',
            false,
            false
        );
    }

    /**
     * @param array $arguments
     * @param \Closure $renderChildrenClosure
     * @param RenderingContextInterface $renderingContext
     * @return array
     */
    public static function renderStatic(
        array $arguments,
        \Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    )
    {
        $a = static::arrayFromArrayOrTraversableOrCSVStatic($renderChildrenClosure());
        $b = static::arrayFromArrayOrTraversableOrCSVStatic($arguments['b'], $arguments['useKeys']);

        return static::mergeArraysStatic($a, $b);
    }

    /**
     * @param mixed $candidate
     * @param boolean $useKeys
     *
     * @return array
     */
    protected static function arrayFromArrayOrTraversableOrCSVStatic($candidate, $useKeys = true)
    {
        if (true === $candidate instanceof \Traversable) {
            return iterator_to_array($candidate, $useKeys);
        } elseif (true === $candidate instanceof QueryResultInterface) {
            /** @var QueryResultInterface $candidate */
            return $candidate->toArray();
        }
        if (true === is_string($candidate)) {
            return GeneralUtility::trimExplode(',', $candidate, true);
        } elseif (true === is_array($candidate)) {
            return $candidate;
        }

        return [];
    }

    /**
     * @param array $array1
     * @param array $array2
     * @return array
     */
    protected static function mergeArraysStatic($array1, $array2)
    {
        ArrayUtility::mergeRecursiveWithOverrule($array1, $array2);

        return $array1;
    }
}
