<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\ViewHelpers\BackendLayout;

use IMIA\ImiaBase\Utility\BackendLayoutUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * @package     imia_base
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class ContainerViewHelper extends AbstractViewHelper
{
    /**
     * @var bool
     */
    protected $escapeOutput = false;

    /**
     * @param integer $pageUid
     * @param string $table
     * @param string $label
     * @param integer $returnPageUid
     * @param string $columns
     * @return string
     */
    public function render($pageUid, $table = 'pages', $label = null, $returnPageUid = null, $columns = null)
    {
        $output = '';
        $contents = $this->renderChildren();

        if (!$label) {
            $label = LocalizationUtility::translate('backendlayout', 'ImiaBase');
        }


        $webRoot = str_replace(GeneralUtility::getIndpEnv('TYPO3_DOCUMENT_ROOT'), '', PATH_site);
        $extended = false;
        if (strpos($contents, '<!--EXTENDED-PROPERTY-->') !== false) {
            $extended = true;

            $output .= '
                <script src="' . $webRoot . ExtensionManagementUtility::siteRelPath('imia_base') . 'Resources/Public/Javascripts/js.cookie.js" type="text/javascript"></script>
                <script type="text/javascript">
                    TYPO3.jQuery(document).ready(function(){
                        TYPO3.jQuery("#extended-page-settings").click(function(){
                            var checked = TYPO3.jQuery(this).is(":checked");
                            Cookies.set("extended-page-settings", checked ? "1" : "");
                            if (checked) {
                                TYPO3.jQuery("tr.extended-property").hide().removeClass("hidden").fadeIn();
                            } else {
                                TYPO3.jQuery("tr.extended-property").fadeOut();
                            }
                        });
                    });
                </script>';
        }

        $editUrl = BackendLayoutUtility::getRecordEditLink($returnPageUid, $pageUid, $table, $columns);
        $editIcon = BackendLayoutUtility::getEditIconHtml();

        $output .= '
            <style type="text/css">
                .t3-pagesettings th {
                    text-align: left !important;
                }
                .t3-pagesettings .t3-page-ce-wrapper {
                    clear: both;
                    padding: 1px 0;
                }
                .t3-pagesettings .t3-page-ce-wrapper th,
                .t3-pagesettings .t3-page-ce-wrapper td {
                    padding: 3px 0 6px;
                    vertical-align: top;
                }
                .t3-pagesettings .t3-page-ce-wrapper tr:last-child th,
                .t3-pagesettings .t3-page-ce-wrapper tr:last-child td {
                    padding-bottom: 0;
                }
                .t3-pagesettings .t3-page-ce-wrapper th {
                    line-height: 24px;
                    padding-right: 12px;
                    padding-left: 0;
                    padding-top: 0;
                    text-align: right;
                    position: relative;
                }
                .t3-pagesettings .t3-page-ce-wrapper th a {
                    visibility: hidden;
                    top: -2px;
                    right: 12px;
                    position: absolute;
                }
                .t3-pagesettings .t3-page-ce-wrapper th:hover a {
                    visibility: visible;
                }
                .t3-pagesettings table {
                    table-layout: fixed;
                    width: 100%;
                }
                .t3-pagesettings ul {
                    padding-left: 15px;
                    margin: 0;
                }
                .t3-pagesettings .property-contents {
                    width: 100%;
                    display: block;
                }
                .t3-pagesettings .property-contents figure,
                .t3-pagesettings .property-contents figure > img {
                    max-width: 100% !important;
                    height: auto !important;
                }
                .t3-pagesettings .t3-page-column-header-left {
                    width: 25%;
                    float: left;
                }
                .t3-pagesettings .t3-page-column-header-left .t3-page-column-header-icons {
                    right: 7px;
                }
                .t3-pagesettings .t3-page-column-header-right {
                    width: 75%;
                    float: left;
                    text-align: right;
                }
                .t3-pagesettings .t3-page-column-header div.checkbox {
                    margin: 0;
                }
            </style>
            <div class="t3-grid-container t3-pagesettings">
                <table border="0" cellspacing="0" cellpadding="0" width="100%" class="t3-page-columns t3-grid-table t3js-page-columns">
                    <tbody>
                        <tr>
                            <td valign="top" colspan="1" class="t3-grid-cell t3-page-column">
                                <div class="t3-page-column-header t3-page-column-header-left">
                                    <div class="t3-page-column-header-icons">
                                        <a onclick="window.location.href=\'' . $editUrl . '\'; return false;">
                                            ' . $editIcon . '
                                        </a>
                                    </div>
                                    <div class="t3-page-column-header-label">' . $label . '</div>
                                </div>
                                <div class="t3-page-column-header t3-page-column-header-right">
                                    ' . ($extended ? '
                                    <div class="checkbox">
                                        <label for="extended-page-settings">
                                            <input type="checkbox" class="checkbox" name="extended-page-settings" ' . ($_COOKIE['extended-page-settings'] ? ' checked="checked"' : '') . ' id="extended-page-settings" value="1">
                                            ' . LocalizationUtility::translate('backendlayout.extended', 'ImiaBase') . '
                                        </label>
                                    </div>' : '&nbsp') . '
                                </div>
                                <div class="t3-page-ce-wrapper">
                                    <div class="t3-page-ce">
                                    ' . $contents . '
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>';

        return $output;
    }
}