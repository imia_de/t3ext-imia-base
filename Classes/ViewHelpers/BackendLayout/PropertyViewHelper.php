<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\ViewHelpers\BackendLayout;

use IMIA\ImiaBase\Utility\BackendLayoutUtility;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * @package     imia_base
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class PropertyViewHelper extends AbstractViewHelper
{
    /**
     * @var bool
     */
    protected $escapeOutput = false;

    /**
     * @param integer $pageUid
     * @param string $name
     * @param string $label
     * @param string $table
     * @param integer $returnPageUid
     * @param boolean $standalone
     * @param boolean $exclude
     * @return string
     */
    public function render($pageUid, $name, $label = null, $table = 'pages', $returnPageUid = null, $standalone = false, $exclude = false)
    {
        $contents = $this->renderChildren();
        if (!$label) {
            $label = $name;
        }

        $editUrl = BackendLayoutUtility::getRecordEditLink($returnPageUid, $pageUid, $table, $name);
        $editIcon = BackendLayoutUtility::getEditIconHtml();

        if ($standalone) {
            $output = '
                <a onclick="window.location.href=\'' . $editUrl . '\'; return false;" href="#" style="text-decoration: none !important">
                    ' . $contents . '
                </a>';
        } else {
            $output = '
                ' . ($exclude ? '<!--EXTENDED-PROPERTY-->
                ' : '') .
                '<tr class="property' . ($exclude ? ' extended-property' . ($_COOKIE['extended-page-settings'] ? '' : ' hidden') : '') . '">
                    <th>
                        ' . $label . '

                        <a onclick="window.location.href=\'' . $editUrl . '\'; return false;" href="#">
                            ' . $editIcon . '
                        </a>
                    </th>
                    <td>
                        <a onclick="window.location.href=\'' . $editUrl . '\'; return false;" href="#" class="property-contents">
                            ' . $contents . '
                        </a>
                    </td>
                </tr>';
        }

        return $output;
    }
}