<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\ViewHelpers;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;
use TYPO3\CMS\Frontend\DataProcessing\MenuProcessor;

/**
 * @package     imia_base
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class MenuViewHelper extends AbstractViewHelper
{
    /**
     * @param int $entryLevel
     * @param string $special
     * @param string $specialValue
     * @param string $specialData
     * @param string $specialUserFunc
     * @param string $specialRange
     * @param int $levels
     * @param boolean $expandAll
     * @param boolean $includeNotInMenu
     * @param boolean $includeSpacer
     * @param string $titleField
     * @param string $alternativeSortingField
     * @param string $excludeDoktypes
     * @return array
     */
    public function render($entryLevel = null, $special = null, $specialValue = null, $specialData = null, $specialUserFunc = null,
                           $specialRange = null, $levels = 1, $expandAll = true, $includeNotInMenu = false,
                           $includeSpacer = false, $titleField = 'nav_title // title', $alternativeSortingField = null,
                           $excludeDoktypes = '100,103,104,106,110,111,112,113,114,115,116,120,123,124,126,130,133,134,136,140,143,144,146')
    {
        $processedData = [];
        $processorConfiguration = [
            'levels'                  => (int)$levels,
            'expandAll'               => $expandAll ? 1 : 0,
            'includeSpacer'           => $includeSpacer ? 1 : 0,
            'as'                      => 'menu',
            'titleField'              => $titleField,
            'alternativeSortingField' => $alternativeSortingField,
            'includeNotInMenu'        => $includeNotInMenu ? 1 : 0,
            'excludeDoktypes'         => $excludeDoktypes,
        ];

        if ($entryLevel !== null) {
            $processorConfiguration['entryLevel'] = $entryLevel;
        }
        if ($special) {
            $processorConfiguration['special'] = $special;
            if ($specialValue) {
                $processorConfiguration['special.'] = [
                    'value' => $specialValue,
                ];
            } elseif ($specialData) {
                $specialValue = $this->getTSFE()->cObj->cObjGetSingle('TEXT', [
                    'data' => $specialData,
                ]);
                if (!$specialValue) {
                    $specialValue = '-1';
                }
                $processorConfiguration['special.'] = [
                    'value' => $specialValue,
                ];
            } elseif ($specialUserFunc) {
                $specialValue = $this->getTSFE()->cObj->cObjGetSingle('USER', [
                    'userFunc' => $specialUserFunc,
                ]);
                if (!$specialValue) {
                    $specialValue = '-1';
                }
                $processorConfiguration['special.'] = [
                    'value' => $specialValue,
                ];
            } elseif ($specialRange) {
                $processorConfiguration['special.'] = [
                    'range' => $specialRange,
                ];
            }
        }

        /** @var MenuProcessor $menuProcessor */
        $menuProcessor = GeneralUtility::makeInstance(MenuProcessor::class);
        $processedData = $menuProcessor->process($this->getTSFE()->cObj, [], $processorConfiguration, $processedData);

        if ($processedData && isset($processedData['menu'])) {
            return $processedData['menu'];
        } else {
            return [];
        }
    }

    /**
     * @return TypoScriptFrontendController
     */
    protected function getTSFE()
    {
        return $GLOBALS['TSFE'];
    }
}