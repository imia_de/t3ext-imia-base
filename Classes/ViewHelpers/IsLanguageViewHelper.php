<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\ViewHelpers;

use TYPO3\CMS\Core\Database\DatabaseConnection;

/**
 * @package     imia_base
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class IsLanguageViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
    /**
     * @param integer|string $language
     * @param string $defaultTitle
     * @return bool
     */
    public function render($language, $defaultTitle = null)
    {
        $currentLanguageUid = $GLOBALS['TSFE']->sys_language_uid;
        if (true === is_numeric($language)) {
            $languageUid = intval($language);
        } else {
            $row = $this->getDb()->exec_SELECTgetSingleRow('uid', 'sys_language',
                'title LIKE ' . $this->getDb()->fullQuoteStr('sys_language', $language));
            if (false !== $row) {
                $languageUid = intval($row['uid']);
            } else {
                if ((string)$language === $defaultTitle) {
                    $languageUid = 0;
                } else {
                    $languageUid = -1;
                }
            }
        }

        return $languageUid === $currentLanguageUid;
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}
