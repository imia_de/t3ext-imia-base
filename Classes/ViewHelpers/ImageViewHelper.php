<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\ViewHelpers;

use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Imaging\ImageManipulation\CropVariantCollection;
use TYPO3\CMS\Core\Resource\DuplicationBehavior;
use TYPO3\CMS\Core\Resource\Exception\ResourceDoesNotExistException;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Model\AbstractFileFolder;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * @package     imia_base
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class ImageViewHelper extends \TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper
{
    /**
     * Initialize arguments.
     */
    public function initializeArguments()
    {
        $this->registerArgument('additionalAttributes', 'array', 'Additional tag attributes. They will be added directly to the resulting HTML tag.', false);
        $this->registerArgument('data', 'array', 'Additional data-* attributes. They will each be added with a "data-" prefix.', false);
        $this->registerUniversalTagAttributes();
        $this->registerTagAttribute('alt', 'string', 'Specifies an alternate text for an image', false);
        $this->registerTagAttribute('ismap', 'string', 'Specifies an image as a server-side image-map. Rarely used. Look at usemap instead', false);
        $this->registerTagAttribute('longdesc', 'string', 'Specifies the URL to a document that contains a long description of an image', false);
        $this->registerTagAttribute('usemap', 'string', 'Specifies an image as a client-side image-map', false);
        $this->registerArgument('cropVariant', 'string', 'select a cropping variant, in case multiple croppings have been specified or stored in FileReference', false, 'default');
    }

    /**
     * @param string $src
     * @param string $width
     * @param string $height
     * @param int $minWidth
     * @param int $minHeight
     * @param int $maxWidth
     * @param int $maxHeight
     * @param bool $treatIdAsReference
     * @param \object $image
     * @param string|bool $crop
     * @param bool $absolute
     * @param bool $useSourceCollection
     * @param array $sourceCollectionConfiguration
     *
     * @throws \TYPO3\CMS\Fluid\Core\ViewHelper\Exception
     * @return string
     */
    public function render($src = null, $width = null, $height = null, $minWidth = null, $minHeight = null,
                           $maxWidth = null, $maxHeight = null, $treatIdAsReference = false, $image = null, $crop = null,
                           $absolute = false, $useSourceCollection = false, $sourceCollectionConfiguration = null)
    {
        if (is_null($src) && is_null($image) || !is_null($src) && !is_null($image)) {
            throw new \TYPO3\CMS\Fluid\Core\ViewHelper\Exception('You must either specify a string src or a File object.', 1382284106);
        }

        if ($absolute && (!isset($_SERVER['HTTP_HOST']) || !$_SERVER['HTTP_HOST'])) {
            $row = $this->getDb()->exec_SELECTgetSingleRow('domainName', 'sys_domain', 'hidden = 0 AND redirectTo = ""');
            if ($row) {
                $_SERVER['HTTP_HOST'] = $row['domainName'];
                GeneralUtility::flushInternalRuntimeCaches();
            }
        }

        try {
            $image = $this->imageService->getImage($src, $image, $treatIdAsReference);
            $cropString = $crop;
            if ($cropString === null && $image->hasProperty('crop') && $image->getProperty('crop')) {
                $cropString = $image->getProperty('crop');
            }
            $cropVariantCollection = CropVariantCollection::create((string)$cropString);
            $cropVariant = $this->arguments['cropVariant'] ?: 'default';
            $cropArea = $cropVariantCollection->getCropArea($cropVariant);

            $imageConf = [];
            $maxWidthTS = (int)$GLOBALS['TSFE']->tmpl->setup['tt_content.']['image.']['20.']['pageMaxW']
                ?: (int)$GLOBALS['TSFE']->tmpl->setup['tt_content.']['image.']['20.']['maxW'];

            if ($useSourceCollection) {
                $imageConf = $GLOBALS['TSFE']->tmpl->setup['tt_content.']['image.']['20.']['1.'];
                unset($imageConf['file']);

                if ($sourceCollectionConfiguration) {
                    if ($sourceCollectionConfiguration['container'] && isset($GLOBALS['TSFE']->tmpl->setup['tt_content.']['image.']['20.']['containerMaxW'])) {
                        $maxWidthTS = $GLOBALS['TSFE']->tmpl->setup['tt_content.']['image.']['20.']['containerMaxW'];
                    }
                    $buffer = 1;
                    if (isset($sourceCollectionConfiguration['buffer'])) {
                        $buffer = 1.3;
                    }
                    $altMaxWidth = $maxWidthTS;

                    if (is_array($imageConf['sourceCollection.'])) {
                        foreach ($imageConf['sourceCollection.'] as $key => &$conf) {
                            if (isset($conf['maxW'])) {
                                if (isset($conf['if.']['value']) && $conf['if.']['value'] == $conf['maxW']) {
                                    $conf['if.']['value'] = &$conf['maxW'];
                                }

                                $collectionConfiguration = null;
                                $realKey = substr($key, 0, -1);
                                if (array_key_exists($realKey, $sourceCollectionConfiguration)) {
                                    $collectionConfiguration = $sourceCollectionConfiguration[$realKey];
                                } elseif (array_key_exists('default', $sourceCollectionConfiguration)) {
                                    $collectionConfiguration = $sourceCollectionConfiguration['default'];
                                }

                                if ($collectionConfiguration) {
                                    if (isset($collectionConfiguration['width'])) {
                                        $conf['maxW'] = round((float)$collectionConfiguration['width']);
                                    } else {
                                        if (isset($collectionConfiguration['padding'])) {
                                            $conf['maxW'] = round($conf['maxW'] - (float)$collectionConfiguration['padding']);
                                        }
                                        if (isset($collectionConfiguration['percent'])) {
                                            $conf['maxW'] = round((float)$conf['maxW'] * (float)$collectionConfiguration['percent'] / 100);
                                        }

                                        $conf['maxW'] = (string)round((float)$conf['maxW'] * $buffer);
                                    }
                                }
                            }
                        }
                    }

                    if (isset($sourceCollectionConfiguration['default'])) {
                        if (isset($sourceCollectionConfiguration['default']['width'])) {
                            if ($altMaxWidth) {
                                $altMaxWidth = round((float)$sourceCollectionConfiguration['default']['width']);
                            }
                        } else {
                            if (isset($sourceCollectionConfiguration['default']['padding'])) {
                                if ($altMaxWidth) {
                                    $altMaxWidth = round($altMaxWidth - (float)$sourceCollectionConfiguration['default']['padding']);
                                }
                            }
                            if (isset($sourceCollectionConfiguration['default']['percent'])) {
                                if ($altMaxWidth) {
                                    $altMaxWidth = round($altMaxWidth * (float)$sourceCollectionConfiguration['default']['percent'] / 100);
                                }
                            }

                            $altMaxWidth = round($altMaxWidth * $buffer);
                        }

                        $imageConf['sourceCollection.']['lg.'] = [
                            'maxW'            => (string)$altMaxWidth,
                            'srcsetCandidate' => '1920w',
                            'dataKey'         => 'lg',
                        ];
                    }
                }
            }

            $processingInstructions = [
                'width'     => $width,
                'height'    => $height,
                'minWidth'  => $minWidth,
                'minHeight' => $minHeight,
                'maxWidth'  => $maxWidth,
                'maxHeight' => $maxHeight,
                'crop'      => $cropArea->isEmpty() ? null : $cropArea->makeAbsoluteBasedOnFile($image),
            ];
            $processedImage = $this->imageService->applyProcessingInstructions($image, $processingInstructions);
            $imageUri = $this->imageService->getImageUri($processedImage, $absolute);

            if (!$this->tag->hasAttribute('data-focus-area')) {
                $focusArea = $cropVariantCollection->getFocusArea($cropVariant);
                if (!$focusArea->isEmpty()) {
                    $this->tag->addAttribute('data-focus-area', $focusArea->makeAbsoluteBasedOnFile($image));
                }
            }
            $this->tag->addAttribute('src', $imageUri);
            $this->tag->addAttribute('width', $processedImage->getProperty('width'));
            $this->tag->addAttribute('height', $processedImage->getProperty('height'));

            $alt = $image->getProperty('alternative');
            $title = $image->getProperty('title');

            // The alt-attribute is mandatory to have valid html-code, therefore add it even if it is empty
            if (empty($this->arguments['alt'])) {
                $this->tag->addAttribute('alt', $alt);
            }
            if (empty($this->arguments['title']) && $title) {
                $this->tag->addAttribute('title', $title);
            }

            if ($useSourceCollection && TYPO3_MODE == 'FE') {
                $identifier = sha1(serialize($processingInstructions)) . '_' . $image->getName();

                $processedAltFolder = null;
                $processedImageAlt = null;

                $storage = $image->getStorage();
                if (!$storage || !$storage->getUid()) {
                    $storage = ResourceFactory::getInstance()->getDefaultStorage();
                }

                try {
                    if (!$storage->hasFolder('_processedsrcset_')) {
                        $processedAltFolder = $storage->createFolder('_processedsrcset_');
                    }
                    $processedAltFolder = $storage->getFolder('_processedsrcset_');
                    $processedImageAlt = $storage->getFileInFolder($identifier, $processedAltFolder);
                } catch (\Exception $e) {
                }

                if (!$processedImageAlt && $processedAltFolder) {
                    $filePath = PATH_site . 'typo3temp/' . $identifier;
                    try {
                        @copy($processedImage->getForLocalProcessing(false), $filePath);
                        $processedImageAlt = $storage->addFile(
                            $filePath,
                            $processedAltFolder,
                            $identifier,
                            DuplicationBehavior::REPLACE,
                            false
                        );
                    } catch (\Exception $e) {
                    }
                    @unlink($filePath);
                }

                if ($processedImageAlt) {
                    $info = [
                        0 => $processedImageAlt->getProperty('width'),
                        1 => $processedImageAlt->getProperty('height'),
                        2 => $processedImageAlt->getProperty('extension'),
                        3 => $processedImageAlt->getForLocalProcessing(false),
                    ];
                    $GLOBALS['TSFE']->lastImageInfo = $info;

                    /** @var ContentObjectRenderer $cObj */
                    $cObj = $GLOBALS['TSFE']->cObj;
                    $sourceCollection = $cObj->getImageSourceCollection(
                        $imageConf['layoutKey'],
                        $imageConf,
                        $processedImageAlt
                    );

                    $this->tag->addAttribute('srcset', $processedImageAlt->getPublicUrl() .
                        ' ' . $processedImageAlt->getProperty('width') . 'w,' . $sourceCollection);

                    if (isset($sourceCollectionConfiguration['sizes'])) {
                        $this->tag->addAttribute('sizes', '(min-width: ' . $processedImageAlt->getProperty('width') . 'px) ' .
                            $processedImageAlt->getProperty('width') . 'px,' . $sourceCollectionConfiguration['sizes']);
                    } else {
                        $this->tag->addAttribute('sizes', '(min-width: ' . $processedImageAlt->getProperty('width') . 'px) ' .
                            $processedImageAlt->getProperty('width') . 'px,' . '100vw');
                    }
                }
            }
        } catch (ResourceDoesNotExistException $e) {
            // thrown if file does not exist
        } catch (\UnexpectedValueException $e) {
            // thrown if a file has been replaced with a folder
        } catch (\RuntimeException $e) {
            // RuntimeException thrown if a file is outside of a storage
        } catch (\InvalidArgumentException $e) {
            // thrown if file storage does not exist
        }

        return $this->tag->render();
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}
