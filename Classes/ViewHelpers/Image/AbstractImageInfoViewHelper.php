<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\ViewHelpers\Image;

use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * @package     imia_base
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
abstract class AbstractImageInfoViewHelper extends AbstractViewHelper
{
    /**
     * @var ResourceFactory
     */
    protected $resourceFactory;

    /**
     * Construct resource factory
     */
    public function __construct()
    {
        $this->resourceFactory = GeneralUtility::makeInstance(ResourceFactory::class);
    }

    /**
     * Initialize arguments.
     *
     * @return void
     * @api
     */
    public function initializeArguments()
    {
        $this->registerArgument(
            'src',
            'mixed',
            'Path to or id of the image file to determine info for. In case a FileReference is supplied, ' .
            'treatIdAsUid and treatIdAsReference will automatically be activated.',
            true
        );
        $this->registerArgument(
            'treatIdAsUid',
            'boolean',
            'If TRUE, the path argument is treated as a resource uid.'
        );
        $this->registerArgument(
            'treatIdAsReference',
            'boolean',
            'If TRUE, the path argument is treated as a reference uid and will be resolved to a resource via ' .
            'sys_file_reference.',
            false,
            false
        );
    }

    /**
     * @throws \Exception
     * @return array
     */
    public function getInfo()
    {
        $src = $this->arguments['src'];
        $treatIdAsUid = (boolean)$this->arguments['treatIdAsUid'];
        $treatIdAsReference = (boolean)$this->arguments['treatIdAsReference'];

        if (null === $src) {
            $src = $this->renderChildren();
            if (null === $src) {
                return [];
            }
        }

        if (is_object($src) && $src instanceof FileReference) {
            $src = $src->getUid();
            $treatIdAsUid = true;
            $treatIdAsReference = true;
        }

        if (true === $treatIdAsUid || true === $treatIdAsReference) {
            $id = (integer)$src;
            $info = [];
            if (true === $treatIdAsUid) {
                $info = $this->getInfoByUid($id);
            } elseif (true === $treatIdAsReference) {
                $info = $this->getInfoByReference($id);
            }
        } else {
            $file = GeneralUtility::getFileAbsFileName($src);
            if (false === file_exists($file) || true === is_dir($file)) {
                throw new \Exception(
                    'Cannot determine info for "' . $file . '". File does not exist or is a directory.',
                    1357066532
                );
            }
            $imageSize = getimagesize($file);
            $info = [
                'width'  => $imageSize[0],
                'height' => $imageSize[1],
                'type'   => $imageSize['mime'],
            ];
        }

        return $info;
    }

    /**
     * @param integer $id
     * @return array
     * @throws \TYPO3\CMS\Core\Resource\Exception\ResourceDoesNotExistException
     */
    public function getInfoByReference($id)
    {
        $fileReference = $this->resourceFactory->getFileReferenceObject($id);
        $file = $fileReference->getOriginalFile();

        return $this->getFileArray($file);
    }

    /**
     * @param integer $uid
     * @return array
     * @throws \TYPO3\CMS\Core\Resource\Exception\FileDoesNotExistException
     */
    public function getInfoByUid($uid)
    {
        $file = $this->resourceFactory->getFileObject($uid);

        return $this->getFileArray($file);
    }

    /**
     * @param File $file
     * @return array
     */
    protected function getFileArray(File $file)
    {
        $properties = $file->getProperties();
        $stat = $file->getStorage()->getFileInfo($file);
        $array = $file->toArray();

        foreach ($properties as $key => $value) {
            $array[$key] = $value;
        }
        foreach ($stat as $key => $value) {
            $array[$key] = $value;
        }

        return $array;
    }
}
