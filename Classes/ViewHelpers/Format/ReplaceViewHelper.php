<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\ViewHelpers\Format;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * @package     imia_base
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class ReplaceViewHelper extends AbstractViewHelper
{
    /**
     * @param string $substring
     * @param string $content
     * @param string $replacement
     * @param integer $count
     * @param boolean $caseSensitive
     * @return string
     */
    public function render($substring, $content = null, $replacement = '', $count = null, $caseSensitive = true)
    {
        if (null === $content) {
            $content = $this->renderChildren();
        }
        $function = (true === $caseSensitive ? 'str_replace' : 'str_ireplace');

        return $function($substring, $replacement, $content, $count);
    }
}
