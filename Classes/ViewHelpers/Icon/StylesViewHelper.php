<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\ViewHelpers\Icon;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/**
 * @package     imia_base
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class StylesViewHelper extends AbstractViewHelper
{
    /**
     * @param array $icon
     * @return string
     */
    public function render($icon = null)
    {
        if (!$icon) {
            $icon = $this->renderChildren();
        }

        if (is_array($icon)) {
            $this->getTSFE()->setCss('iconstyles-' . $icon['uid'], '
                #icon-' . $icon['uid'] . ' {
                    display: inline-block;
                    line-height: 0;
                    ' . ($icon['color'] ? 'color: ' . $icon['color'] . ';' : '') . '
                    ' . ($icon['background_color'] ? 'padding: 15px; border-radius: 50%; background-color: ' . $icon['background_color'] . ';' : '') . '
                }
                #icon-' . $icon['uid'] . ' i {
                    display: inline-block;
                    position: relative;
                    overflow: hidden;
                    text-align: center;
                    font-size: ' . floor((int)$icon['size'] * 0.8) . 'px;
                    line-height: ' . (int)$icon['size'] . 'px;
                    width: ' . (int)$icon['size'] . 'px;
                    height: ' . (int)$icon['size'] . 'px;
                }
                ' . ($icon['color_hover'] || $icon['background_color_hover'] ? '
                    #icon-' . $icon['uid'] . ':hover {
                        ' . ($icon['color_hover'] ? 'color: ' . $icon['color_hover'] . ';' : '') . '
                        ' . ($icon['background_color_hover'] ? 'background-color: ' . $icon['background_color_hover'] . ';' : '') . '
                    }
                ' : '') . '
            ');
        }

        return '';
    }

    /**
     * @return TypoScriptFrontendController
     */
    protected function getTSFE()
    {
        return $GLOBALS['TSFE'];
    }
}