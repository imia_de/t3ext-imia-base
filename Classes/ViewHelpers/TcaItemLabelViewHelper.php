<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\ViewHelpers;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * @package     imia_base
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class TcaItemLabelViewHelper extends AbstractViewHelper
{
    /**
     * @param string $table
     * @param string $column
     * @param string $item
     * @param string $row
     * @return string
     */
    public function render($table, $column, $item, $row = [])
    {
        $label = '[' . $item . ']';
        if (isset($GLOBALS['TCA'][$table]['columns'][$column])
            && isset($GLOBALS['TCA'][$table]['columns'][$column]['config']['items'])
            && is_array($GLOBALS['TCA'][$table]['columns'][$column]['config']['items'])
        ) {
            $items = $GLOBALS['TCA'][$table]['columns'][$column]['config']['items'];
            if (isset($GLOBALS['TCA'][$table]['columns'][$column]['config']['itemsProcFunc'])) {
                if (!isset($row['pid'])) {
                    $row['pid'] = (int)GeneralUtility::_GP('id');
                }

                $pObj = GeneralUtility::makeInstance(\TYPO3\CMS\Backend\Form\FormDataProvider\TcaSelectItems::class);
                $params = [
                    'items'    => &$items,
                    'config'   => $GLOBALS['TCA'][$table]['columns'][$column]['config'],
                    'TSconfig' => null,
                    'table'    => 'tt_content',
                    'field'    => $column,
                    'row'      => $row,
                ];
                GeneralUtility::callUserFunction($GLOBALS['TCA'][$table]['columns'][$column]['config']['itemsProcFunc'], $params, $pObj);
            }
            foreach ($items as $tcaItem) {
                if ($tcaItem[1] == $item) {
                    $label = LocalizationUtility::translate($tcaItem[0], 'ImiaBase') ?: $tcaItem[0];
                    break;
                }
            }
        }

        return $label;
    }
}