<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2017 IMIA net based solutions (info@imia.de)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

namespace IMIA\ImiaBase\ViewHelpers\Link;

use IMIA\ImiaBase\Service\FrontendService;

/**
 * @package     imia_base
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class PageViewHelper extends \TYPO3\CMS\Fluid\ViewHelpers\Link\PageViewHelper
{
    /**
     * @param int|NULL $pageUid
     * @param array $additionalParams
     * @param int $pageType
     * @param bool $noCache
     * @param bool $noCacheHash
     * @param string $section
     * @param bool $linkAccessRestrictedPages
     * @param bool $absolute
     * @param bool $addQueryString
     * @param array $argumentsToBeExcludedFromQueryString
     * @param string $addQueryStringMethod
     * @param integer $language
     * @return string
     */
    public function render($pageUid = null, array $additionalParams = [], $pageType = 0, $noCache = false,
                           $noCacheHash = false, $section = '', $linkAccessRestrictedPages = false, $absolute = false,
                           $addQueryString = false, array $argumentsToBeExcludedFromQueryString = [],
                           $addQueryStringMethod = null, $language = 0)
    {
        $oldUriBuilder = $this->controllerContext->getUriBuilder();
        if (TYPO3_MODE != 'FE') {
            $absolute = true;
            FrontendService::replace((int)$pageUid, $language);
            $this->controllerContext->setUriBuilder(FrontendService::getUriBuilder());
        }

        $output = parent::render($pageUid, $additionalParams, $pageType, $noCache,
            $noCacheHash, $section, $linkAccessRestrictedPages, $absolute,
            $addQueryString, $argumentsToBeExcludedFromQueryString, $addQueryStringMethod);

        if (TYPO3_MODE != 'FE') {
            FrontendService::reset();
            $this->controllerContext->setUriBuilder($oldUriBuilder);
        }

        return $output;
    }
}