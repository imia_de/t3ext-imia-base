<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\ViewHelpers;

/**
 * @package     imia_base
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class InArrayKeyViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
    /**
     * @param string $value
     * @param string $subvalue
     * @param array $array
     * @param mixed $checkValue
     * @return boolean
     */
    public function render($value, $subvalue = null, $array = null, $checkValue = null)
    {
        if (!is_array($array)) {
            $array = (array)$this->renderChildren();
        }

        $exists = array_key_exists((string)$value, $array);
        if ($checkValue !== null && $exists && !$subvalue) {
            $exists = $array[$value] == $checkValue ? true : false;
        }

        if ($exists && $subvalue) {
            if (is_array($array[$value]) && $subvalue) {
                $exists = array_key_exists((string)$subvalue, $array[$value]);
                if ($checkValue !== null && $exists) {
                    $exists = $array[$value][$subvalue] == $checkValue ? true : false;
                }
            }
        }

        return $exists;
    }
}