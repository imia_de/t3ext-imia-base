<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\ViewHelpers\Variable;

/**
 * @package     imia_base
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class SetViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
    /**
     * @param string $name
     * @param mixed $value
     */
    public function render($name, $value = null)
    {
        if (null === $value) {
            $value = $this->renderChildren();
        }

        if (strpos($name, '.') !== false) {
            $names = explode('.', $name);
            $realValue = null;

            $varName = array_shift($names);
            if ($this->templateVariableContainer->exists($varName)) {
                $realValue = $this->templateVariableContainer->get($varName);
                $this->templateVariableContainer->remove($varName);
            }
            if (!is_array($realValue)) {
                $realValue = [];
            }
            $currentValue = &$realValue;

            foreach ($names as $name) {
                $currentValue = &$currentValue[$name];
            }
            $currentValue = $value;

            $this->templateVariableContainer->add($varName, $realValue);
        } else {
            if ($this->templateVariableContainer->exists($name)) {
                $this->templateVariableContainer->remove($name);
            }
            $this->templateVariableContainer->add($name, $value);
        }
    }
}