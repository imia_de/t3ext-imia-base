<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\ViewHelpers\Content;

use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

/**
 * @package     imia_base
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class GetViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    /**
     * @var boolean
     */
    protected $escapeOutput = false;

    /**
     * @return void
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('colPos', 'integer', '', false, 0);
        $this->registerArgument('contentUids', 'array', '', false, null);
        $this->registerArgument('pageUid', 'integer', '', false, null);
        $this->registerArgument('slide', 'integer', '', false, 0);
        $this->registerArgument('slideCollect', 'integer', '', false, 0);
        $this->registerArgument('slideCollectReverse', 'integer', '', false, 0);
        $this->registerArgument('slideCollectFuzzy', 'integer', '', false, 0);
        $this->registerArgument('gridParent', 'integer', '', false, 0);
        $this->registerArgument('includeRecordsWithoutDefaultTranslation', 'boolean', '', false, true);
        $this->registerArgument('as', 'string', 'The name of the iteration variable', false, null);
    }

    /**
     * @param array $arguments
     * @param \Closure $renderChildrenClosure
     * @param RenderingContextInterface $renderingContext
     * @return string|array
     */
    public static function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        $conf = self::getTSFE()->tmpl->setup['styles.']['content.']['get.'];

        $conf['select.']['includeRecordsWithoutDefaultTranslation'] = $arguments['includeRecordsWithoutDefaultTranslation'];
        if (is_array($arguments['contentUids']) && count($arguments['contentUids'])) {
            $conf['select.']['uidInList'] = implode(',', $arguments['contentUids']);
            $conf['select.']['pidInList'] = self::getTSFE()->id;

            $result = self::getDb()->exec_SELECTquery('pid', 'tt_content',
                'uid IN (' . implode(',', array_map('intval', $arguments['contentUids'])) . ')', 'pid');
            while ($row = self::getDb()->sql_fetch_assoc($result)) {
                $conf['select.']['pidInList'] .= ',' . $row['pid'];
            }

            if (isset($conf['select.']['where'])) {
                unset($conf['select.']['where']);
            }
        } else {
            $conf['select.']['where'] = '{#colPos}=' . (int)$arguments['colPos'] . ' AND {#grid_parent}=' . (int)$arguments['gridParent'];
            if ($arguments['pageUid']) {
                $conf['select.']['pidInList'] = $arguments['pageUid'];
            }

            if ($arguments['gridParent']) {
                $gridParentRecord = self::getDb()->exec_SELECTgetSingleRow('pid, l18n_parent', 'tt_content',
                    'uid = ' . (int)$arguments['gridParent']);
                if ($gridParentRecord) {
                    if ($gridParentRecord['l18n_parent']) {
                        $arguments['gridParent'] = $gridParentRecord['l18n_parent'];
                        $conf['select.']['where'] = '{#colPos}=' . (int)$arguments['colPos'] . ' AND {#grid_parent}=' . (int)$arguments['gridParent'];
                    }

                    $tsfeIdIncluded = false;
                    if (!$conf['select.']['pidInList']) {
                        $conf['select.']['pidInList'] = self::getTSFE()->id;
                        $tsfeIdIncluded = true;
                    }

                    if (!$tsfeIdIncluded || self::getTSFE()->id != $gridParentRecord['pid']) {
                        $conf['select.']['pidInList'] .= ',' . $gridParentRecord['pid'];
                    }

                    // workaround for problem with grid parent and localized record moved to different grid
                    if (self::getTSFE()->sys_language_content > 0) {
                        $contentUids = [];

                        $result = self::getDb()->exec_SELECTquery('uid, l18n_parent', 'tt_content',
                            'colPos = ' . (int)$arguments['colPos'] . ' AND grid_parent = ' . (int)$arguments['gridParent'] .
                            ' AND sys_language_uid IN (-1,' . (int)self::getTSFE()->sys_language_content . ')' . self::getTSFE()->cObj->enableFields('tt_content'));

                        while ($row = self::getDb()->sql_fetch_assoc($result)) {
                            $contentUids[] = $row['l18n_parent'] ?: $row['uid'];
                        }

                        if (count($contentUids) > 0) {
                            $conf['select.']['uidInList'] = implode(',', $contentUids);
                            unset($conf['select.']['where']);
                        }
                    }
                }
            } elseif ($arguments['slide']) {
                $conf['slide'] = $arguments['slide'];
                $conf['slide.'] = [];

                if ($arguments['slideCollect']) {
                    $conf['slide.']['collect'] = $arguments['slideCollect'];
                }
                if ($arguments['slideCollectReverse']) {
                    $conf['slide.']['collectReverse'] = $arguments['slideCollectReverse'];
                }
                if ($arguments['slideCollectFuzzy']) {
                    $conf['slide.']['collectFuzzy'] = $arguments['slideCollectFuzzy'];
                }
            } else {
                // workaround for problem with grid parent and localized record moved out of grid
                if (self::getTSFE()->sys_language_content > 0) {
                    $contentUids = [];

                    $result = self::getDb()->exec_SELECTquery('uid, l18n_parent, sys_language_uid, grid_parent',
                        'tt_content', 'colPos = ' . (int)$arguments['colPos'] . ' AND grid_parent = ' . (int)$arguments['gridParent'] .
                        ' AND sys_language_uid IN (-1,' . (int)self::getTSFE()->sys_language_content . ')' . self::getTSFE()->cObj->enableFields('tt_content'));

                    while ($row = self::getDb()->sql_fetch_assoc($result)) {
                        $contentUids[] = $row['l18n_parent'] ?: $row['uid'];
                    }

                    if (count($contentUids) > 0) {
                        $conf['select.']['uidInList'] = implode(',', $contentUids);
                        unset($conf['select.']['where']);
                    }
                }
            }
        }

        $contentElements = self::getRecords($conf);
        if (isset($arguments['as']) && $arguments['as']) {
            $templateVariableContainer = $renderingContext->getVariableProvider();
            $templateVariableContainer->add($arguments['as'], $contentElements);

            return '';
        } else {
            return $contentElements;
        }
    }

    /**
     * @param array $conf
     * @return array
     */
    protected static function getRecords($conf)
    {
        $conf['table'] = isset($conf['table.']) ? trim(self::getTSFE()->cObj->stdWrap($conf['table'], $conf['table.'])) : trim($conf['table']);
        $conf['select.'] = !empty($conf['select.']) ? $conf['select.'] : [];
        $slide = isset($conf['slide.']) ? (int)self::getTSFE()->cObj->stdWrap($conf['slide'], $conf['slide.']) : (int)$conf['slide'];
        if (!$slide) {
            $slide = 0;
        }
        $slideCollect = isset($conf['slide.']['collect.']) ?
            (int)self::getTSFE()->cObj->stdWrap($conf['slide.']['collect'], $conf['slide.']['collect.']) :
            (int)$conf['slide.']['collect'];
        if (!$slideCollect) {
            $slideCollect = 0;
        }
        $slideCollectReverse = isset($conf['slide.']['collectReverse.']) ?
            (int)self::getTSFE()->cObj->stdWrap($conf['slide.']['collectReverse'], $conf['slide.']['collectReverse.']) :
            (int)$conf['slide.']['collectReverse'];
        $slideCollectReverse = (bool)$slideCollectReverse;
        $slideCollectFuzzy = isset($conf['slide.']['collectFuzzy.'])
            ? (bool)self::getTSFE()->cObj->stdWrap($conf['slide.']['collectFuzzy'], $conf['slide.']['collectFuzzy.'])
            : (bool)$conf['slide.']['collectFuzzy'];
        if (!$slideCollect) {
            $slideCollectFuzzy = true;
        }
        $again = false;

        $allRecords = [];
        do {
            $currentRecords = [];
            $records = self::getTSFE()->cObj->getRecords($conf['table'], $conf['select.']);
            if (!empty($records)) {
                self::getTSFE()->cObj->currentRecordTotal = count($records);

                /** @var $cObj ContentObjectRenderer */
                $cObj = GeneralUtility::makeInstance(ContentObjectRenderer::class);
                $cObj->setParent(self::getTSFE()->cObj->data, self::getTSFE()->cObj->currentRecord);
                self::getTSFE()->cObj->currentRecordNumber = 0;

                foreach ($records as $row) {
                    // Call hook for possible manipulation of database row for cObj->data
                    if (is_array($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_content_content.php']['modifyDBRow'])) {
                        foreach ($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_content_content.php']['modifyDBRow'] as $_classRef) {
                            $_procObj = GeneralUtility::getUserObj($_classRef);
                            $_procObj->modifyDBRow($row, $conf['table']);
                        }
                    }

                    $currentRecords[] = $row;
                }
            }
            if ($slideCollectReverse) {
                $allRecords = array_merge($currentRecords, $allRecords);
            } else {
                $allRecords = array_merge($allRecords, $currentRecords);
            }
            if ($slideCollect > 0) {
                $slideCollect--;
            }
            if ($slide) {
                if ($slide > 0) {
                    $slide--;
                }
                $conf['select.']['pidInList'] = self::getTSFE()->cObj->getSlidePids($conf['select.']['pidInList'], $conf['select.']['pidInList.']);
                if (isset($conf['select.']['pidInList.'])) {
                    unset($conf['select.']['pidInList.']);
                }
                $again = (string)$conf['select.']['pidInList'] !== '';
            }
        } while ($again && $slide && (count($allRecords) === 0 && $slideCollectFuzzy || $slideCollect));

        return $allRecords;
    }

    /**
     * @return DatabaseConnection
     */
    protected static function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }

    /**
     * @return TypoScriptFrontendController
     */
    protected static function getTSFE()
    {
        return $GLOBALS['TSFE'];
    }
}