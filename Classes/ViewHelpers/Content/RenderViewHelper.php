<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBase\ViewHelpers\Content;

use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/**
 * @package     imia_base
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class RenderViewHelper extends AbstractViewHelper
{
    /**
     * @var boolean
     */
    protected $escapeOutput = false;

    /**
     * @param int $colPos
     * @param array $contentUids
     * @param int $pageUid
     * @param int $slide
     * @param int $slideCollect
     * @param int $slideCollectReverse
     * @param int $slideCollectFuzzy
     * @param int $gridParent
     * @param bool $includeRecordsWithoutDefaultTranslation
     * @return string
     */
    public function render($colPos = 0, $contentUids = null, $pageUid = null, $slide = 0, $slideCollect = 0, $slideCollectReverse = 0,
                           $slideCollectFuzzy = 0, $gridParent = 0, $includeRecordsWithoutDefaultTranslation = true)
    {
        $confName = $this->getTSFE()->tmpl->setup['styles.']['content.']['get'];
        $conf = $this->getTSFE()->tmpl->setup['styles.']['content.']['get.'];

        $conf['select.']['includeRecordsWithoutDefaultTranslation'] = $includeRecordsWithoutDefaultTranslation;
        if (is_array($contentUids) && count($contentUids)) {
            $conf['select.']['uidInList'] = implode(',', $contentUids);
            $conf['select.']['pidInList'] = $this->getTSFE()->id;

            $result = $this->getDb()->exec_SELECTquery('pid', 'tt_content',
                'uid IN (' . implode(',', array_map('intval', $contentUids)) . ')', 'pid');
            while ($row = $this->getDb()->sql_fetch_assoc($result)) {
                $conf['select.']['pidInList'] .= ',' . $row['pid'];
            }

            if (isset($conf['select.']['where'])) {
                unset($conf['select.']['where']);
            }
        } else {
            $conf['select.']['where'] = '{#colPos}=' . (int)$colPos . ' AND {#grid_parent}=' . (int)$gridParent;

            $sqlPid = '';
            if ($pageUid) {
                $conf['select.']['pidInList'] = $pageUid;
                $sqlPid = 'pid = '.intval($pageUid).' AND ';
            }

            if ($gridParent) {
                $gridParentRecord = $this->getDb()->exec_SELECTgetSingleRow('pid, l18n_parent', 'tt_content',
                    'uid = ' . (int)$gridParent);
                if ($gridParentRecord) {
                    if ($gridParentRecord['l18n_parent']) {
                        $gridParent = $gridParentRecord['l18n_parent'];
                        $conf['select.']['where'] = '{#colPos}=' . (int)$colPos . ' AND {#grid_parent}=' . (int)$gridParent;
                    }

                    $tsfeIdIncluded = false;
                    if (!$conf['select.']['pidInList']) {
                        $conf['select.']['pidInList'] = $this->getTSFE()->id;
                        $tsfeIdIncluded = true;
                    }

                    if (!$tsfeIdIncluded || $this->getTSFE()->id != $gridParentRecord['pid']) {
                        $conf['select.']['pidInList'] .= ',' . $gridParentRecord['pid'];
                    }

                    // workaround for problem with grid parent and localized record moved to different grid
                    if ($this->getTSFE()->sys_language_content > 0) {
                        $contentUids = [];

                        $result = $this->getDb()->exec_SELECTquery('uid, l18n_parent', 'tt_content',
                            'colPos = ' . (int)$colPos . ' AND grid_parent = ' . (int)$gridParent .
                            ' AND sys_language_uid IN (-1,' . (int)$this->getTSFE()->sys_language_content . ')' . $this->getTSFE()->cObj->enableFields('tt_content'));

                        while ($row = $this->getDb()->sql_fetch_assoc($result)) {
                            $contentUids[] = $row['l18n_parent'] ?: $row['uid'];
                        }

                        if (count($contentUids) > 0) {
                            $conf['select.']['uidInList'] = implode(',', $contentUids);
                            unset($conf['select.']['where']);
                        }
                    }
                }
            }
            elseif ($slide) {
                $conf['slide'] = $slide;
                $conf['slide.'] = [];

                if ($slideCollect) {
                    $conf['slide.']['collect'] = $slideCollect;
                }
                if ($slideCollectReverse) {
                    $conf['slide.']['collectReverse'] = $slideCollectReverse;
                }
                if ($slideCollectFuzzy) {
                    $conf['slide.']['collectFuzzy'] = $slideCollectFuzzy;
                }
            }
            else {
                // workaround for problem with grid parent and localized record moved out of grid
                if ($this->getTSFE()->sys_language_content > 0) {
                    $contentUids = [];

                    $result = $this->getDb()->exec_SELECTquery('uid, l18n_parent, sys_language_uid, grid_parent',
                        'tt_content', ' '.$sqlPid.' colPos = ' . (int)$colPos . ' AND grid_parent = ' . (int)$gridParent .
                        ' AND sys_language_uid IN (-1,' . (int)$this->getTSFE()->sys_language_content . ')' . $this->getTSFE()->cObj->enableFields('tt_content'));

                    while ($row = $this->getDb()->sql_fetch_assoc($result)) {
                        $contentUids[] = $row['l18n_parent'] ?: $row['uid'];
                    }

                    if (count($contentUids) > 0) {
                        $conf['select.']['uidInList'] = implode(',', $contentUids);
                        unset($conf['select.']['where']);
                    }
                }
            }
        }

        return $this->getTSFE()->cObj->cObjGetSingle($confName, $conf);
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }

    /**
     * @return TypoScriptFrontendController
     */
    protected function getTSFE()
    {
        return $GLOBALS['TSFE'];
    }
}