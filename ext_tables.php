<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

// Page/User TS
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/TSConfig/page.typoscript">
    TCAdefaults.pages.lastUpdated = ' . time());

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig(
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/TSConfig/user.typoscript">');

\IMIA\ImiaBase\Utility\ExtensionUtility::registerPageIcon($_EXTKEY, 'menu', 'Resources/Public/Images/Icons/Svg/Content/menu-1.svg', 'Navigation');

$GLOBALS['TCA']['sys_file_reference']['ctrl']['iconfile'] =
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath($_EXTKEY) . 'Resources/Public/Images/FileReference.png';

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['typo3/backend.php']['renderPreProcess']['tx_imiabase'] =
    \IMIA\ImiaBase\Hook\Backend\BackendController::class . '->renderPreProcess';

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['typo3/backend.php']['renderPostProcess']['tx_imiabase'] =
    \IMIA\ImiaBase\Hook\Backend\BackendController::class . '->renderPostProcess';

$GLOBALS['TBE_STYLES']['inDocStyles_TBEstyle'] .= "\n" . \TYPO3\CMS\Core\Utility\GeneralUtility::getUrl(
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Resources/Public/Stylesheets/doc.css');

$luceneConfiguration = &$GLOBALS['TBE_MODULES']['_configuration']['web_TwLucenesearchLucene'];
if ($luceneConfiguration) {
    $luceneConfiguration['labels'] = 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/ModLucene.xlf';
    $luceneConfiguration['icon'] = 'EXT:' . $_EXTKEY . '/Resources/Public/Images/Search.svg';
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_imiabase_gridcontainers');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_imiabase_slides');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_imiabase_icons');