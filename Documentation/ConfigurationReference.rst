﻿=========================
Configuration Reference
=========================

TypoScript Reference
=====================

.. ..................................
.. container:: table-row

	Property
		config.assetic / page.config.assetic

	Data type
		none

	Description
		Assetic Configuration



config.assetic / page.config.assetic
-----------

.. ..................................
.. container:: table-row

	Property
		enabled

	Data type
		boolean

	Description
		Enables the assetic parsing

	Default
        0

.. ..................................
.. container:: table-row

	Property
		settings

	Data type
		array

	Description
        Configuration for assetic