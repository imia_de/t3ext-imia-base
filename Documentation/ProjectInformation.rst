﻿=====================
Project Information
=====================

The imia_base Extension started with the intention to create a standardized configuration for alle TYPO3 Projects for the Company `IMIA net based solutions`_.
But it soon grew in size as more and more features were added.

Now it's a great extension to start your new project with, especially if you want to use `Flux`_ as your TYPO3 Template engine and to use `Compass`_ for css preprocessing.

.. _IMIA net based solutions: http://www.imia.de
.. _Compass: http://www.compass-style.org

Features
============================
* TypoScript Improvements
    * Static TypoScript with .ts Ending (additionally to .txt)
    * Relative Includes for Static TypoScript
    * Assetic for including Javascripts & Stylesheets with Filters
        * Compass (SASS/SCSS)
        * CssEmbed
        * YUI Compressor

* Base Configuration for TYPO3
    * HTML5-Tag with IE-Conditions (Boilerplate-Style)
    * PageTS
        * defaultLanguage: german (de)
        * newContentElement renderMode: tabs
        * defaultAccess permissions for pages
        * default RTE config
    * TypoScript
        * default assetic configuration
        * imia.base.pagetitle (rootline-based page title)

* User-Functions, Helpers
    * Canonical Url userFunc
    * Cache-Utility
    * ExtensionUtility
        * registerPageIcon
        * addDisplayCond
    * Helper
        * addDynamicConfiguration (baseUrl, realUrl-Config, language-TypoScript) based on created domain records and languages
        * convertHex2RGB

* Core- and Extensions Bugfixes
    * Rsaauth patch for multiple logins on same page (http://forge.typo3.org/issues/24877)

Releases
============================

You can download the latest version of this extension in the `TER (imia_base)`_.

The source code is maintained by `David Frerich`_ on `Bitbucket`_.

Feedback is gladly seen, so submit your `Tickets`_ or `Pull-Requests`_.

.. _TER (imia_base): http://typo3.org/extensions/repository/view/imia_base
.. _David Frerich: https://bitbucket.org/cdfre
.. _Bitbucket: https://bitbucket.org/imia_de/t3ext-imia-base
.. _Tickets: https://bitbucket.org/imia_de/t3ext-imia-base/issues
.. _Pull-Requests: https://bitbucket.org/imia_de/t3ext-imia-base/pull-requests


Bugs and Known Issues
============================

Bugs can be reported on the Bitbucket `Bug Tracker`_.

No bug or issue is known to date.

.. _Bug Tracker: https://bitbucket.org/imia_de/t3ext-imia-base/issues?status=new&status=open


Change Log
============================

`Bitbucket Commits`_

.. _Bitbucket Commits: https://bitbucket.org/imia_de/t3ext-imia-base/commits/all