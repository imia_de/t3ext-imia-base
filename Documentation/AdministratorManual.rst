﻿============================
Administrator Manual
============================

Installation
============================

If you want to use assetic, you have to install the software dependencies on your server.


Dependencies for Assetic Filters
----------------------------

Java (yui_css and yui_js)
^^^^^^^^^^

.. code-block:: code

        add non-free to debian/ubuntu debs in apt/sources.list
        apt-get install sun-java6-jre

debian / ubuntu example


Compass (compass)
^^^^^^^^^^

.. code-block:: code

    apt-get install ruby rubygems

    gem update
    gem install compass --pre

debian / ubuntu example


Compass Plugins (if you like to use one)
""""""""""

.. code-block:: code

    gem install compass-h5bp

    gem install bootstrap-sass

debian / ubuntu example
