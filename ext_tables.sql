CREATE TABLE `tt_content` (
  `spaceAfter`           SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL,
  `spaceBefore`          SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL,
  `bootstrap_visibility` VARCHAR(255)                     NOT NULL DEFAULT '',
  `grid_columns`         SMALLINT(5) DEFAULT '2'          NOT NULL,
  `grid_layout`          TEXT,
  `grid_parent`          INT(11) DEFAULT '0'              NOT NULL,
  `grid_children`        INT(11) DEFAULT '0'              NOT NULL,
  `grid_containers`      INT(11) DEFAULT '0'              NOT NULL,
  `ib_flexform`          TEXT,
  `layout`               TINYTEXT,
);

CREATE TABLE `tx_imiabase_gridcontainers` (
  `uid`              INT(11) UNSIGNED                NOT NULL AUTO_INCREMENT,
  `pid`              INT(11) UNSIGNED DEFAULT '0'    NOT NULL,
  `tstamp`           INT(11) UNSIGNED DEFAULT '0'    NOT NULL,
  `crdate`           INT(11) UNSIGNED DEFAULT '0'    NOT NULL,
  `cruser_id`        INT(11) UNSIGNED DEFAULT '0'    NOT NULL,
  `deleted`          TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
  `hidden`           TINYINT(4) UNSIGNED DEFAULT '0' NOT NULL,
  `sorting`          INT(11) UNSIGNED DEFAULT '0'    NOT NULL,
  `sys_language_uid` INT(11) UNSIGNED DEFAULT '0'    NOT NULL,
  `l10n_parent`      INT(11) UNSIGNED DEFAULT '0'    NOT NULL,
  `l10n_state`       TEXT,
  `l10n_diffsource`  MEDIUMBLOB,
  `t3_origuid`       INT(11) DEFAULT '0'             NOT NULL,
  `content`          INT(10) DEFAULT '0'             NOT NULL,
  `title`            VARCHAR(255) DEFAULT ''         NOT NULL,

  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`),
  KEY `language` (`sys_language_uid`, `l10n_parent`),
  KEY `t3_origuid` (`t3_origuid`)
);

CREATE TABLE `tx_imiabase_slides` (
  `uid`              INT(11) UNSIGNED                  NOT NULL AUTO_INCREMENT,
  `pid`              INT(11) UNSIGNED DEFAULT '0'      NOT NULL,
  `tstamp`           INT(11) UNSIGNED DEFAULT '0'      NOT NULL,
  `crdate`           INT(11) UNSIGNED DEFAULT '0'      NOT NULL,
  `cruser_id`        INT(11) UNSIGNED DEFAULT '0'      NOT NULL,
  `deleted`          TINYINT(3) UNSIGNED DEFAULT '0'   NOT NULL,
  `hidden`           TINYINT(4) UNSIGNED DEFAULT '0'   NOT NULL,
  `sorting`          INT(11) UNSIGNED DEFAULT '0'      NOT NULL,
  `sys_language_uid` INT(11) UNSIGNED DEFAULT '0'      NOT NULL,
  `l10n_parent`      INT(11) UNSIGNED DEFAULT '0'      NOT NULL,
  `l10n_state`       TEXT,
  `l10n_diffsource`  MEDIUMBLOB,
  `t3_origuid`       INT(11) DEFAULT '0'               NOT NULL,
  `content`          INT(10) DEFAULT '0'               NOT NULL,
  `title`            VARCHAR(255) DEFAULT ''           NOT NULL,
  `background_color` VARCHAR(10) DEFAULT ''            NOT NULL,
  `image`            INT(11) DEFAULT '0'               NOT NULL,
  `video`            INT(11) DEFAULT '0'               NOT NULL,
  `text`             TEXT,
  `link`             VARCHAR(255) DEFAULT ''           NOT NULL,
  `link_text`        VARCHAR(255) DEFAULT ''           NOT NULL,

  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`),
  KEY `language` (`sys_language_uid`, `l10n_parent`),
  KEY `t3_origuid` (`t3_origuid`)
);

CREATE TABLE `tx_imiabase_icons` (
  `uid`                    INT(11) UNSIGNED                  NOT NULL AUTO_INCREMENT,
  `pid`                    INT(11) UNSIGNED DEFAULT '0'      NOT NULL,
  `tstamp`                 INT(11) UNSIGNED DEFAULT '0'      NOT NULL,
  `crdate`                 INT(11) UNSIGNED DEFAULT '0'      NOT NULL,
  `cruser_id`              INT(11) UNSIGNED DEFAULT '0'      NOT NULL,
  `deleted`                TINYINT(3) UNSIGNED DEFAULT '0'   NOT NULL,
  `hidden`                 TINYINT(4) UNSIGNED DEFAULT '0'   NOT NULL,
  `sorting`                INT(11) UNSIGNED DEFAULT '0'      NOT NULL,
  `content`                INT(10) DEFAULT '0'               NOT NULL,
  `icon`                   VARCHAR(255) DEFAULT ''           NOT NULL,
  `size`                   INT(10) DEFAULT '24'              NOT NULL,
  `color`                  VARCHAR(10) DEFAULT ''            NOT NULL,
  `color_hover`            VARCHAR(10) DEFAULT ''            NOT NULL,
  `background_color`       VARCHAR(10) DEFAULT ''            NOT NULL,
  `background_color_hover` VARCHAR(10) DEFAULT ''            NOT NULL,
  `link`                   VARCHAR(255) DEFAULT ''           NOT NULL,

  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
);

CREATE TABLE `pages` (
  `nav_icon`             VARCHAR(255) NOT NULL DEFAULT '',
  `bootstrap_visibility` VARCHAR(255) NOT NULL DEFAULT ''
);

CREATE TABLE `be_groups` (
  `pagetypes_select` TEXT
);

CREATE TABLE `sys_domain` (
  `singlesignon_disabled` TINYINT(2) NOT NULL DEFAULT '0'
);