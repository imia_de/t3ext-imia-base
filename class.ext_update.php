<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use IMIA\ImiaBase\Utility\AssetUtility;

class ext_update
{
    /**
     * @return bool
     */
    public function access()
    {
        return true;
    }

    /**
     * @return string
     */
    public function main()
    {
        $content = '';
        if (GeneralUtility::_GP('modal')) {
            $content = '<style type="text/css">.module-body {padding-left: 0; padding-right: 0; padding-bottom: 0}</style>';
        }

        $params = GeneralUtility::_GP('base');
        if (!is_array($params)) {
            $params = [
                'action' => 'index',
            ];
        }
        $requestUri = preg_replace('/&base[^&]+/ism', '', GeneralUtility::getIndpEnv('REQUEST_URI'));

        $extensions = AssetUtility::getExtensionAssets();
        switch ($params['action']) {
            case 'update':
                if ($params['extKey']) {
                    $extension = $extensions[$params['extKey']];
                    $parsedAssets = [];
                    try {
                        $parsedAssets = AssetUtility::parseAssets($extension, $params['force'] ? true : false);
                    } catch (\Exception $e) {
                        $flashMessageService = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessageService');
                        $flashMessage = GeneralUtility::makeInstance('TYPO3\CMS\Core\Messaging\FlashMessage',
                            $e->getMessage(),
                            '',
                            FlashMessage::ERROR,
                            true
                        );
                        $flashMessageService->getMessageQueueByIdentifier()->enqueue($flashMessage);

                        $content .= $flashMessageService->getMessageQueueByIdentifier()->renderFlashMessages() . '<br><br>';
                    }

                    $content .=
                        '<h2>' . $extension['em_conf']['title'] . '</h2>' .
                        '<blockquote style="margin: 24px 0 12px; font-size: 12px; word-wrap: break-word;">' .
                        (count($parsedAssets) > 0 ? implode("<br>", $parsedAssets) : $this->translate('update.noassets')) . '</blockquote><br>' .
                        '<a href="' . $requestUri . '" class="t3-button btn btn-default">' .
                        $this->translate('update.back') .
                        '</a>';

                    break;
                }
            case 'index':
            default:
                foreach ($extensions as $extension) {
                    $content .=
                        '<div>' .
                        '   <p style="margin: 0 0 12px; padding: 0;">' .
                        '       <strong>' . ($extension['em_conf']['title'] ?: $extension['ext_key']) . '</strong>' .
                        '   </p>' .
                        '   <p>' .
                        implode('<br>', $extension['assets']) .
                        '   </p>' .
                        '   <p style="margin-top: 6px; padding: 0;">' .
                        '       <a href="' . $requestUri . '&base[action]=update&base[extKey]=' . $extension['ext_key'] . '" class="t3-button btn btn-default">' .
                        '           <span class="t3js-icon icon icon-size-small icon-state-default icon-actions-refresh"><span class="icon-markup"><img src="/typo3/sysext/core/Resources/Public/Icons/T3Icons/actions/actions-refresh.svg" width="16" height="16"></span></span> ' . $this->translate('update.update') .
                        '       </a>' .
                        '       <a href="' . $requestUri . '&base[action]=update&base[extKey]=' . $extension['ext_key'] . '&base[force]=1" class="t3-button btn btn-default">' .
                        '           <span class="t3js-icon icon icon-size-small icon-state-default icon-actions-refresh"><span class="icon-markup"><img src="/typo3/sysext/core/Resources/Public/Icons/T3Icons/actions/actions-refresh.svg" width="16" height="16"></span></span> ' . $this->translate('update.updateforce') .
                        '       </a>' .
                        '   </p>' .
                        '</div>';
                }

                break;
        }

        return $content;
    }

    /**
     * Translate by key
     *
     * @param string $key
     * @param array $arguments
     * @return string
     */
    protected function translate($key, $arguments = [])
    {
        return (string)LocalizationUtility::translate($key, 'imia_base', $arguments);
    }
}
