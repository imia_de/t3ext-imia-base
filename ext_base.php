<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/* ##################
 * ## PAGE LAYOUTS ##
 * ################## */

\IMIA\ImiaBase\Registry\FluidStyledContentRegistry::registerPageLayout('Standard', 'EXT:imia_base/Resources/Public/Images/Icons/Svg/default.svg', [
    [
        [
            'name'   => 'content',
            'colPos' => 0,
        ],
    ],
]);

/* ######################
 * ## CONTENT ELEMENTS ##
 * ###################### */

\IMIA\ImiaBase\Registry\FluidStyledContentRegistry::registerContentElement('Grid', 'special',
    'EXT:imia_base/Resources/Public/Images/Icons/Svg/Grid/4cols.svg', 'grid', null, null, null, null, true);

\IMIA\ImiaBase\Registry\FluidStyledContentRegistry::registerContentElement('Accordion', 'special',
    'EXT:imia_base/Resources/Public/Images/Icons/Svg/Content/accordion-1.svg', 'accordion', null, null, null, null, true);

\IMIA\ImiaBase\Registry\FluidStyledContentRegistry::registerContentElement('Tabs', 'special',
    'EXT:imia_base/Resources/Public/Images/Icons/Svg/Content/tabs-1.svg', 'tabs', null, null, null, null, true);

\IMIA\ImiaBase\Registry\FluidStyledContentRegistry::registerContentElement('Carousel', 'special',
    'EXT:imia_base/Resources/Public/Images/Icons/Svg/Content/carousel-3.svg', 'noheader', null, null, null, null, true);

\IMIA\ImiaBase\Registry\FluidStyledContentRegistry::registerContentElement('Icons', 'special',
    'EXT:imia_base/Resources/Public/Images/Icons/Svg/Media/image-2.svg', 'default', null, null, null, null, true);

\IMIA\ImiaBase\Registry\FluidStyledContentRegistry::registerContentElement('Slider', 'special',
    'EXT:imia_base/Resources/Public/Images/Icons/Svg/Content/carousel-1.svg', 'gridfixed1', null, null, null, null, true);