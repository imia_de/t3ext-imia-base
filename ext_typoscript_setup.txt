module.tx_form {
    view {
        templateRootPaths.100 = EXT:imia_base/Resources/Private/Overrides/Form/Backend/Templates/
        partialRootPaths.100 = EXT:imia_base/Resources/Private/Overrides/Form/Backend/Partials/
        layoutRootPaths.100 = EXT:imia_base/Resources/Private/Overrides/Form/Backend/Layouts/
    }

    settings {
        yamlConfigurations {
            90 = EXT:imia_base/Configuration/Form/Setup.yaml
        }
    }
}

plugin.tx_form {
    settings {
        yamlConfigurations {
            90 = EXT:imia_base/Configuration/Form/Setup.yaml
        }
    }
}
