<?php
$EM_CONF[$_EXTKEY] = [
    'title'            => 'IMIA Base',
    'description'      => 'Base for IMIA TYPO3 Installations',
    'category'         => 'misc',
    'author'           => 'David Frerich',
    'author_email'     => 'd.frerich@imia.de',
    'author_company'   => 'IMIA net based solutions',
    'state'            => 'stable',
    'uploadfolder'     => 1,
    'clearCacheOnLoad' => 0,
    'version'          => '8.3.1',
    'constraints'      => [
        'depends'   => [
            'php'           => '7.0.0-0.0.0',
            'typo3'         => '8.7.0-8.7.999',
            'imia_composer' => '8.0.0-0.0.0',
        ],
        'conflicts' => [
        ],
        'suggests'  => [
        ],
    ],
];