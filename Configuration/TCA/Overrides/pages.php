<?php
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', [
    'nav_icon'        => [
        'exclude' => true,
        'label'  => 'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:page_navicon',
        'config' => [
            'type'     => 'select',
            'items'    => [
                ['', ''],
            ],
            'itemsProcFunc' => \IMIA\ImiaBase\User\Icons::class . '->icons',
        ],
    ],
]);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette('pages', 'title', 'nav_icon', 'after: nav_title');