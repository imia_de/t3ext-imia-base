<?php
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', [
    'spaceAfter'      => [
        'exclude' => true,
        'label'   => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:spaceAfter_formlabel',
        'config'  => [
            'type'    => 'input',
            'size'    => '5',
            'max'     => '5',
            'eval'    => 'int',
            'range'   => [
                'lower' => '0',
            ],
            'default' => 0,
        ],
    ],
    'spaceBefore'     => [
        'exclude' => true,
        'label'   => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:spaceBefore_formlabel',
        'config'  => [
            'type'    => 'input',
            'size'    => '5',
            'max'     => '5',
            'eval'    => 'int',
            'range'   => [
                'lower' => '0',
            ],
            'default' => 0,
        ],
    ],
    'ib_flexform'     => [
        'label'       => '',
        'displayCond' => 'FIELD:CType:IN:ibcontent',
        'config'      => [
            'type'            => 'flex',
            'ds_pointerField' => 'CType',
            'ds'              => [
                'default' => 'FILE:EXT:imia_base/Configuration/FlexForms/Content/Default.xml',
            ],
        ],
    ],
    'grid_columns'    => [
        'label'  => 'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:content_gridcolumns',
        'config' => [
            'type'        => 'select',
            'renderType'  => 'selectSingle',
            'size'        => 1,
            'maxitems'    => 1,
            'items'       => [
                ['1 Spalte', '1', 'EXT:imia_base/Resources/Public/Images/Icons/Svg/Grid/1cols.svg'],
                ['2 Spalten', '2', 'EXT:imia_base/Resources/Public/Images/Icons/Svg/Grid/2cols.svg'],
                ['3 Spalten', '3', 'EXT:imia_base/Resources/Public/Images/Icons/Svg/Grid/3cols.svg'],
                ['4 Spalten', '4', 'EXT:imia_base/Resources/Public/Images/Icons/Svg/Grid/4cols.svg'],
            ],
            'default'     => '2',
            'fieldWizard' => [
                'selectIcons' => [
                    'disabled' => false,
                ],
            ],
        ],
    ],
    'grid_layout'     => [
        'label'  => 'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:content_gridlayout',
        'config' => [
            'type'     => 'user',
            'userFunc' => \IMIA\ImiaBase\User\Grid::class . '->gridLayout',
        ],
    ],
    'grid_parent'     => [
        'label'    => 'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:content_gridparent',
        'l10n_mode' => 'exclude',
        'onChange' => 'reload',
        'config'   => [
            'type'                => 'select',
            'renderType'          => 'selectSingle',
            'foreign_table'       => 'tt_content',
            'foreign_table_where' => ' AND tt_content.pid = ###CURRENT_PID### AND (tt_content.l18n_parent IS NULL OR tt_content.l18n_parent = 0) AND tt_content.uid != ###THIS_UID###',
            'items'               => [
                ['', '0'],
            ],
            'size'                => 1,
            'maxitems'            => 1,
            'default'             => '0',
        ],
    ],
    'grid_children'   => [
        'label'     => '[children]',
        'l10n_mode' => '', // don't set to exclude: Problematic copying of inline relations
        'config'    => [
            'type'           => 'inline',
            'foreign_table'  => 'tt_content',
            'foreign_field'  => 'grid_parent',
            'foreign_sortby' => 'sorting',
            'maxitems'       => 999999,
        ],
    ],
    'grid_containers' => [
        'label'  => 'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:content_gridcontainers',
        'config' => [
            'type'           => 'inline',
            'foreign_table'  => 'tx_imiabase_gridcontainers',
            'foreign_field'  => 'content',
            'foreign_sortby' => 'sorting',
            'maxitems'       => 999999,
            'appearance'     => [
                'useSortable'                     => true,
                'showPossibleLocalizationRecords' => true,
                'showRemovedLocalizationRecords'  => true,
                'enabledControls'                 => [
                    'info'     => true,
                    'new'      => true,
                    'dragdrop' => true,
                    'sort'     => true,
                    'hide'     => true,
                    'delete'   => true,
                    'localize' => true,
                ],
                'behaviour'                       => [
                    'localizationMode'                     => 'select',
                    'localizeChildrenAtParentLocalization' => true,
                ],
            ],
        ],
    ],
]);

$GLOBALS['TCA']['tt_content']['special'] = [
    'grid'      => [],
    'accordion' => [],
    'tabs'      => [],
];

$GLOBALS['TCA']['tt_content']['types']['ibcontent_default']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.headers;headers,
                    ib_flexform,
                --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
                    --palette--;;language,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                    --palette--;;hidden,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';

$GLOBALS['TCA']['tt_content']['types']['ibcontent_noheader']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
                    ib_flexform,
                --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
                    --palette--;;language,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                    --palette--;;hidden,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';

$GLOBALS['TCA']['tt_content']['types']['ibcontent_simple']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
                    ib_flexform,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
                    --palette--;;language,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                    --palette--;;hidden,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';

$GLOBALS['TCA']['tt_content']['palettes']['grid']['showitem'] = 'grid_columns, --linebreak--, grid_layout';
$GLOBALS['TCA']['tt_content']['types']['ibcontent_grid']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.headers;headers,
                    --palette--;LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:content_palette_grid;grid,
                    ib_flexform,
                --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
                    --palette--;;language,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                    --palette--;;hidden,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';

$GLOBALS['TCA']['tt_content']['types']['ibcontent_gridfixed1']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.headers;headers,
                    ib_flexform,
                --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
                    --palette--;;language,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                    --palette--;;hidden,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
$GLOBALS['TCA']['tt_content']['types']['ibcontent_gridfixed2'] = $GLOBALS['TCA']['tt_content']['types']['ibcontent_gridfixed1'];
$GLOBALS['TCA']['tt_content']['types']['ibcontent_gridfixed3'] = $GLOBALS['TCA']['tt_content']['types']['ibcontent_gridfixed1'];

$GLOBALS['TCA']['tt_content']['types']['ibcontent_accordion']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.headers;headers,
                    grid_containers;LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:content_gridcontainers_accordion,
                    ib_flexform,
                --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
                    --palette--;;language,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                    --palette--;;hidden,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';

$GLOBALS['TCA']['tt_content']['types']['ibcontent_tabs']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.headers;headers,
                    grid_containers;LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:content_gridcontainers_tabs,
                    ib_flexform,
                --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
                    --palette--;;language,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                    --palette--;;hidden,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';