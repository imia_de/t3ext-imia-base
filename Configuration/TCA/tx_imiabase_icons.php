<?php
return [
    'ctrl'      => [
        'label'         => 'icon',
        'sortby'        => 'sorting',
        'tstamp'        => 'tstamp',
        'crdate'        => 'crdate',
        'cruser_id'     => 'cruser_id',
        'title'         => 'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:icons',
        'delete'        => 'deleted',
        'iconfile'      => 'EXT:imia_base/Resources/Public/Images/Icons/Svg/Media/image-2.svg',
        'enablecolumns' => [
            'disabled' => 'hidden',
        ],
        'hideTable'     => true,
    ],
    'interface' => [
        'always_description'  => 0,
        'showRecordFieldList' => 'title',
    ],
    'columns'   => [
        'hidden'                 => [
            'exclude' => true,
            'label'   => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
            'config'  => [
                'type'  => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:hidden.I.0',
                    ],
                ],
            ],
        ],
        'icon'                   => [
            'label'  => 'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:icons_icon',
            'config' => [
                'type'          => 'select',
                'items'         => [],
                'itemsProcFunc' => \IMIA\ImiaBase\User\Icons::class . '->icons',
                'minitems'      => 1,
            ],
        ],
        'size'                   => [
            'label'  => 'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:icons_size',
            'config' => [
                'type'    => 'input',
                'eval'    => 'num,required',
                'default' => '24',
            ],
        ],
        'color'                  => [
            'label'  => 'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:icons_color',
            'config' => [
                'type'       => 'input',
                'renderType' => 'colorpicker',
                'default'    => '#000000',
            ],
        ],
        'color_hover'            => [
            'label'  => 'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:icons_colorhover',
            'config' => [
                'type'       => 'input',
                'renderType' => 'colorpicker',
            ],
        ],
        'background_color'       => [
            'label'  => 'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:icons_backgroundcolor',
            'config' => [
                'type'       => 'input',
                'renderType' => 'colorpicker',
            ],
        ],
        'background_color_hover' => [
            'label'  => 'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:icons_backgroundcolorhover',
            'config' => [
                'type'       => 'input',
                'renderType' => 'colorpicker',
            ],
        ],
        'link'                   => [
            'label'  => 'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:icons_link',
            'config' => [
                'type'       => 'input',
                'renderType' => 'inputLink',
            ],
        ],
    ],
    'types'     => [
        [
            'showitem' => 'icon, size, color, color_hover, background_color, background_color_hover, link',
        ],
    ],
];