<?php
return [
    'ctrl'      => [
        'label'                    => 'title',
        'sortby'                   => 'sorting',
        'tstamp'                   => 'tstamp',
        'crdate'                   => 'crdate',
        'cruser_id'                => 'cruser_id',
        'title'                    => 'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:slides',
        'delete'                   => 'deleted',
        'origUid'                  => 't3_origuid',
        'transOrigPointerField'    => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'languageField'            => 'sys_language_uid',
        'iconfile'                 => 'EXT:imia_base/Resources/Public/Images/Icons/Svg/Media/image-1.svg',
        'enablecolumns'            => [
            'disabled' => 'hidden',
        ],
        'hideTable'                => true,
    ],
    'interface' => [
        'always_description'  => 0,
        'showRecordFieldList' => 'title',
    ],
    'columns'   => [
        'hidden'           => [
            'exclude' => true,
            'label'   => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
            'config'  => [
                'type'  => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:hidden.I.0',
                    ],
                ],
            ],
        ],
        'sys_language_uid' => [
            'exclude' => true,
            'label'   => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config'  => [
                'type'       => 'select',
                'renderType' => 'selectSingle',
                'special'    => 'languages',
                'items'      => [
                    [
                        'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple',
                    ],
                ],
                'default'    => 0,
            ],
        ],
        'l10n_parent'      => [
            'exclude'     => true,
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'label'       => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.l10n_parent',
            'config'      => [
                'type'                => 'select',
                'renderType'          => 'selectSingle',
                'items'               => [
                    [
                        '',
                        0,
                    ],
                ],
                'foreign_table'       => 'tt_content',
                'foreign_table_where' => 'AND tt_content.pid=###CURRENT_PID### AND tt_content.sys_language_uid IN (-1,0)',
                'default'             => 0,
            ],
        ],
        'background_color' => [
            'label'  => 'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:slides_backgroundcolor',
            'config' => [
                'type'       => 'input',
                'renderType' => 'colorpicker',
            ],
        ],
        'image'            => [
            'label'  => 'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:slides_image',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('image', [
                'appearance'       => [
                    'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference',
                    'headerThumbnail'            => [
                        'width'  => '64m',
                        'height' => '64m',
                    ],
                ],
                'maxitems'         => '1',
                'overrideChildTca' => [
                    'types' => [
                        0 => [
                            'showitem' => 'crop, --palette--;;filePalette',
                        ],
                        1 => [
                            'showitem' => 'crop, --palette--;;filePalette',
                        ],
                        2 => [
                            'showitem' => 'crop, --palette--;;filePalette',
                        ],
                        3 => [
                            'showitem' => 'crop, --palette--;;filePalette',
                        ],
                        4 => [
                            'showitem' => 'crop, --palette--;;filePalette',
                        ],
                        5 => [
                            'showitem' => 'crop, --palette--;;filePalette',
                        ],
                    ],
                ],
            ], 'gif,jpg,jpeg,tif,tiff,bmp,pcx,tga,png,pdf,ai,svg,webp'),
        ],
        'video'            => [
            'label'  => 'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:slides_video',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('video', [
                'maxitems'         => '1',
                'overrideChildTca' => [
                    'types' => [
                        0 => [
                            'showitem' => '--palette--;;filePalette',
                        ],
                        1 => [
                            'showitem' => '--palette--;;filePalette',
                        ],
                        2 => [
                            'showitem' => '--palette--;;filePalette',
                        ],
                        3 => [
                            'showitem' => '--palette--;;filePalette',
                        ],
                        4 => [
                            'showitem' => '--palette--;;filePalette',
                        ],
                        5 => [
                            'showitem' => '--palette--;;filePalette',
                        ],
                    ],
                ],
            ], 'mp4'),
        ],
        'title'            => [
            'label'  => 'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:slides_title',
            'config' => [
                'type' => 'input',
                'size' => 50,
                'max'  => 255,
            ],
        ],
        'text'             => [
            'label'  => 'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:slides_text',
            'config' => [
                'type'           => 'text',
                'enableRichtext' => true,
            ],
        ],
        'link'             => [
            'label'  => 'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:slides_link',
            'config' => [
                'type'       => 'input',
                'renderType' => 'inputLink',
            ],
        ],
        'link_text'        => [
            'label'  => 'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:slides_linktext',
            'config' => [
                'type' => 'input',
            ],
        ],
    ],
    'types'     => [
        [
            'showitem' => 'background_color, image, video, title, text, link, link_text',
        ],
    ],
];