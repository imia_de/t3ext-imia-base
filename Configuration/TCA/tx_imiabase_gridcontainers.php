<?php
return [
    'ctrl'      => [
        'label'                    => 'title',
        'sortby'                   => 'sorting',
        'tstamp'                   => 'tstamp',
        'crdate'                   => 'crdate',
        'cruser_id'                => 'cruser_id',
        'title'                    => 'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:gridcontainers',
        'delete'                   => 'deleted',
        'origUid'                  => 't3_origuid',
        'transOrigPointerField'    => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'languageField'            => 'sys_language_uid',
        'iconfile'                 => 'EXT:imia_base/Resources/Public/Images/Icons/Svg/Grid/1cols.svg',
        'enablecolumns'            => [
            'disabled' => 'hidden',
        ],
        'hideTable'                => true,
    ],
    'interface' => [
        'always_description'  => 0,
        'showRecordFieldList' => 'title',
    ],
    'columns'   => [
        'hidden'           => [
            'exclude' => true,
            'label'   => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
            'config'  => [
                'type'  => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:hidden.I.0',
                    ],
                ],
            ],
        ],
        'sys_language_uid' => [
            'exclude' => true,
            'label'   => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config'  => [
                'type'       => 'select',
                'renderType' => 'selectSingle',
                'special'    => 'languages',
                'items'      => [
                    [
                        'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple',
                    ],
                ],
                'default'    => 0,
            ],
        ],
        'l10n_parent'      => [
            'exclude'     => true,
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'label'       => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.l10n_parent',
            'config'      => [
                'type'                => 'select',
                'renderType'          => 'selectSingle',
                'items'               => [
                    [
                        '',
                        0,
                    ],
                ],
                'foreign_table'       => 'tt_content',
                'foreign_table_where' => 'AND tt_content.pid=###CURRENT_PID### AND tt_content.sys_language_uid IN (-1,0)',
                'default'             => 0,
            ],
        ],
        'title'            => [
            'label'  => 'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:gridcontainers_title',
            'config' => [
                'type' => 'input',
                'size' => 50,
                'max'  => 255,
            ],
        ],
        'content'          => [
            'label'  => 'LLL:EXT:imia_base/Resources/Private/Language/DB.xlf:gridcontainers_content',
            'config' => [
                'type'          => 'group',
                'internal_type' => 'db',
                'allowed'       => 'tt_content',
                'size'          => 1,
            ],
        ],
    ],
    'types'     => [
        [
            'showitem' => 'title',
        ],
    ],
];